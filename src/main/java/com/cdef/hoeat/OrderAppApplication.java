package com.cdef.hoeat;

import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;

import com.bumptech.glide.Glide;
import com.cdef.commonmodule.Networking.NetworkModule;
import com.cdef.commonmodule.utils.typekit.Typekit;
import com.cdef.hoeat.Networking.DaggerNetworkComponent;
import com.cdef.hoeat.Networking.NetworkComponent;


/**
 * Created by kimjinmo on 2016. 11. 12..
 * 애플리케이션 전반적인 관리를 하는 클래스. 생명주기가 따로 존재함 참고바람
 */

public class OrderAppApplication extends Application {


    //현재 방문한 손님 정보
    public static Context mContext;
    public static boolean isOpen = false;

//    public static RefWatcher getRefWatcher(Context context) {
//        OrderAppApplication application = (OrderAppApplication) context.getApplicationContext();
//        return application.refWatcher;
//    }
//
//    public RefWatcher refWatcher;

    @Override
    public void onCreate() {
        super.onCreate();

        Typekit.getInstance()
                .addNormal(Typekit.createFromAsset(this, "nanumgothic.otf"))
                .addBold(Typekit.createFromAsset(this, "nanumgothic_bold.otf"));

        //실 배포할때 빼야할 코드
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            // This process is dedicated to LeakCanary for heap analysis.
//            // You should not init your app in this process.
//            return;
//        }
//
//        this.refWatcher = LeakCanary.install(this);



    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }

    public NetworkComponent networkComponent = DaggerNetworkComponent.builder()
            .networkModule(new NetworkModule())
            .build();

    public static NetworkComponent getNetworkComponent(Context context) {
        return ((OrderAppApplication) context.getApplicationContext()).networkComponent;
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Glide.get(this).clearMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Glide.get(this).trimMemory(level);
    }


}
