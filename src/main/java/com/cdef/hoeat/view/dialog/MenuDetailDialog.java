package com.cdef.hoeat.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.TextViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.cdef.commonmodule.dataModel.MenuItemData;
import com.cdef.commonmodule.dataModel.MenuSideGroupItemData;
import com.cdef.commonmodule.dataModel.MenuSideOptionItemData;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.hoeat.R;
import com.cdef.hoeat.view.adapter.CartListAdapter3;
import com.cdef.hoeat.view.adapter.HorizontalMenuAdapter;
import com.cdef.hoeat.view.customView.menuOption.MenuDetailSetGroupListItem;
import com.cdef.hoeat.databinding.DialogMenuDetailBinding;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorListenerAdapter;
import com.nineoldandroids.animation.ArgbEvaluator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;

import java.util.ArrayList;

/**
 * Created by kimjinmo on 2017. 1. 2..
 */

public class MenuDetailDialog extends Dialog {


    HorizontalMenuAdapter horizontalMenuAdapter;
    CartListAdapter3 mCartListAdapter;

    ArrayList<MenuItemData> mMenuList = new ArrayList<>();
    ArrayList<MenuItemData> mCartList = new ArrayList<>();

    ////멤버변수
    private Context mContext;
    private static MenuDetailDialog mInstance;
    ////

    private DialogMenuDetailBinding binding;

    public static MenuDetailDialog getmInstanceWithoutInit() {
        return MenuDetailDialog.mInstance;
    }

    public void setLoadingState() {
        super.dismiss();
    }

    View decorView;
    int uiOption;

    /**
     * 소프트 버튼을 없앤다던지 UI와 관련된 작업을 처리한다.
     **/
    public void setUI() {

        if (decorView == null) {
            decorView = getWindow().getDecorView();
            uiOption = getWindow().getDecorView().getSystemUiVisibility();


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                uiOption |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                uiOption |= View.SYSTEM_UI_FLAG_FULLSCREEN;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                uiOption |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }


        LogUtil.d("소프트 버튼 상태 : " + decorView);
        decorView.setSystemUiVisibility(uiOption);
    }


    public static MenuDetailDialog getInstance(Context context) {
        if (MenuDetailDialog.mInstance != null) {
            MenuDetailDialog.mInstance.dismiss();
        }

        if (MenuDetailDialog.mInstance == null) {
            MenuDetailDialog.mInstance = new MenuDetailDialog(context);
        }

        MenuDetailDialog.mInstance.init(context);
        return MenuDetailDialog.mInstance;
    }

    public static void clearInstance() {
        MenuDetailDialog.mInstance = null;
    }


    RequestManager glide;
    RequestOptions requestOptions;

    private MenuDetailDialog(Context context) {
        super(context);
//        this.mContext = context;
        this.mContext = context;
        glide = Glide.with(context);
        registerHandler();
    }

    private MenuDetailDialog(Context context, int themeResId) {
        super(context, themeResId);
//        this.mContext = context;
        glide = Glide.with(context);
        registerHandler();
    }

    private MenuDetailDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
//        this.mContext = context;
        registerHandler();
    }

    private int selectedPosition = 0;

    public MenuDetailDialog setData(String categoryName, ArrayList<MenuItemData> list, int position) {
        HorizontalMenuAdapter.SelectedPosition = position;
        this.selectedPosition = position;
        this.binding.setCategoryName(categoryName);
        this.mMenuList.clear();
        this.mMenuList.addAll(list);
        if (list.size() > 0) {
            this.binding.setSelectedMenu(list.get(position));
            isSetOption();
        }

        if (binding.imageSelectedMenu.getMeasuredWidth() > 0 && binding.imageSelectedMenu.getMeasuredHeight() > 0) {
            this.glide.load(this.binding.getSelectedMenu().foodImage).apply(this.requestOptions).into(new SimpleTarget<Drawable>() {
                @Override
                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                    binding.imageSelectedMenu.setBackground(resource);
                }
            });
        }

        this.horizontalMenuAdapter.setmListData(this.mMenuList);
        return MenuDetailDialog.mInstance;
    }


    public MenuDetailDialog setCart(ArrayList<MenuItemData> cartList) {
        this.mCartList = cartList;  //일부러 얕은 복사를 함.
        this.mCartListAdapter.setmListData(this.mCartList);
        this.binding.setCartItemCount(this.mCartList.size());
        this.mCartListAdapter.notifyDataSetChanged();
        return this;
    }

    /**
     * 초기 뷰 셋팅을 위한 부분
     **/
    private void registerHandler() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.requestOptions = new RequestOptions();
        this.requestOptions.override(Utils.dpToPx(mContext, 300), Utils.dpToPx(mContext, 200));
        this.requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        this.requestOptions.placeholder(R.drawable.img_sample_menu);
        this.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                LogUtil.d("다이얼로그 키 누르는거 : " + keyEvent.getAction());
                if (keyEvent.getAction() == KeyEvent.KEYCODE_BACK) {
                    dismiss();
                }
                return false;
            }

        });

        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_menu_detail, null, false);
        this.binding.setDialog(this);
        this.binding.setIsCartListMode(false);
        this.binding.setScrollLeftButtonVisible(false);
        this.binding.setScrollRightButtonVisible(true);
        this.binding.textMenuIntro.setMovementMethod(ScrollingMovementMethod.getInstance());
        this.binding.setTotalPricePerOne(0);


        this.mCartListAdapter = new CartListAdapter3();
        this.mCartListAdapter.setListener(new CartListAdapter3.OnItemClickListener() {
            @Override
            public void onClickCountButton(boolean isMinus, int position) {
                clickCartListButtonsListener.onClickCountButton(isMinus, position);

            }

            @Override
            public void onClickDeleteButton(int position) {
                clickCartListButtonsListener.onClickDeleteButton(position);

            }
        });

        ///cartlist를 셋팅하자
        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        this.binding.recyclerViewCart.setLayoutManager(lm);
        this.binding.recyclerViewCart.setAdapter(mCartListAdapter);
        this.binding.recyclerViewCart.addOnScrollListener(new RecyclerView.OnScrollListener() {

            int findFirstVisibleItemPosition = 0;
            int findFirstCompletelyVisibleItemPosition = 0;
            int findLastVisibleItemPosition = 0;
            int findLastCompletelyVisibleItemPosition = 0;
            int totalItem = 0;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


                ///위쪽이 보일때
                this.findFirstVisibleItemPosition = ((LinearLayoutManager) binding.recyclerViewCart.getLayoutManager()).findFirstVisibleItemPosition();
                this.findFirstCompletelyVisibleItemPosition = ((LinearLayoutManager) binding.recyclerViewCart.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                this.findLastVisibleItemPosition = ((LinearLayoutManager) binding.recyclerViewCart.getLayoutManager()).findLastVisibleItemPosition();
                this.findLastCompletelyVisibleItemPosition = ((LinearLayoutManager) binding.recyclerViewCart.getLayoutManager()).findLastCompletelyVisibleItemPosition();
                if (mCartListAdapter != null)
                    this.totalItem = mCartListAdapter.getItemCount();

//                ///reverse가 적용되었음.
//                ///top
//                if ((totalItem != 0 && this.findLastVisibleItemPosition != 0 && findLastCompletelyVisibleItemPosition != 0 && findLastVisibleItemPosition != findLastCompletelyVisibleItemPosition)) {
//                    binding.viewCartShadowTop.setVisibility(View.VISIBLE);
//
//                } else {
//                    binding.viewCartShadowTop.setVisibility(View.GONE);
//                }
//
//
//                if ((totalItem != 0 && this.findLastVisibleItemPosition != 0 && findLastCompletelyVisibleItemPosition != 0)
//                        && this.findFirstCompletelyVisibleItemPosition != 0
//                        ) {
//                    binding.viewCartShadowBottom.setVisibility(View.VISIBLE);
//                } else {
//                    binding.viewCartShadowBottom.setVisibility(View.GONE);
//                }


                ///reverse 없앰.
                ///top
                if ((totalItem != 0 && this.findLastVisibleItemPosition != 0 && findLastCompletelyVisibleItemPosition != 0 && findLastVisibleItemPosition != findLastCompletelyVisibleItemPosition)) {
                    binding.viewCartShadowBottom.setVisibility(View.VISIBLE);

                } else {
                    binding.viewCartShadowBottom.setVisibility(View.GONE);
                }


                if ((totalItem != 0 && this.findLastVisibleItemPosition != 0 && findLastCompletelyVisibleItemPosition != 0)
                        && this.findFirstCompletelyVisibleItemPosition != 0
                        ) {
                    binding.viewCartShadowTop.setVisibility(View.VISIBLE);
                } else {
                    binding.viewCartShadowTop.setVisibility(View.GONE);
                }


                LogUtil.d("recyclerViewCart findFirstVisibleItemPosition : " + ((LinearLayoutManager) binding.recyclerViewCart.getLayoutManager()).findFirstVisibleItemPosition());
                LogUtil.d("recyclerViewCart findFirstCompletelyVisibleItemPosition : " + ((LinearLayoutManager) binding.recyclerViewCart.getLayoutManager()).findFirstCompletelyVisibleItemPosition());
                LogUtil.d("recyclerViewCart findLastVisibleItemPosition : " + ((LinearLayoutManager) binding.recyclerViewCart.getLayoutManager()).findLastVisibleItemPosition());
                LogUtil.d("recyclerViewCart findLastCompletelyVisibleItemPosition : " + ((LinearLayoutManager) binding.recyclerViewCart.getLayoutManager()).findLastCompletelyVisibleItemPosition());
            }
        });


        TextViewCompat.setAutoSizeTextTypeWithDefaults(binding.textQuantity, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);

        setContentView(binding.getRoot());

        ///2. 하위 뷰 셋팅
        Window window = this.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setUI();

        this.binding.imageSelectedMenu.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (binding.imageSelectedMenu.getMeasuredWidth() > 0) {
                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) binding.imageSelectedMenu.getLayoutParams();
                    lp.height = (int) (binding.imageSelectedMenu.getMeasuredWidth() * (2 / 3.0));
                    binding.imageSelectedMenu.setLayoutParams(lp);
                    binding.imageSelectedMenu.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    glide.load(binding.getSelectedMenu().foodImage).apply(requestOptions).into(binding.imageSelectedMenu);
                }
            }
        });

        horizontalMenuAdapter = new HorizontalMenuAdapter(getContext(), Glide.with(getContext()), this.selectedPosition);
        horizontalMenuAdapter.setListener((v, itemData, currentPosition, prevPosition) -> {
            selectQuantity(1);
            binding.setSelectedMenu(itemData);
            isSetOption();
            glide.load(binding.getSelectedMenu().foodImage).apply(requestOptions).into(binding.imageSelectedMenu);
            this.selectedPosition = currentPosition;
            horizontalMenuAdapter.notifyItemChanged(currentPosition);
            horizontalMenuAdapter.notifyItemChanged(prevPosition);

        });
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.recyclerView.setAdapter(horizontalMenuAdapter);
        binding.recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                binding.setScrollLeftButtonVisible(
                        (((LinearLayoutManager) binding.recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition()
                                != 0

                        ));
                binding.setScrollRightButtonVisible(
                        !((horizontalMenuAdapter.getItemCount() > 0
                                && (horizontalMenuAdapter.getItemCount() - 1) == ((LinearLayoutManager) binding.recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition()))
                );


            }
        });
        try {
            binding.recyclerView.getLayoutManager().scrollToPosition(selectedPosition);
        } catch (Exception e) {
        }
        ;


        ///버튼 초기화
        selectQuantity(1);

    }

    @Override
    public void show() {
        super.show();
        ///선택된 아이템의 위치로 스크롤 이동

        try {
            this.binding.recyclerView.scrollToPosition(this.selectedPosition);
        } catch (Exception e) {

        }
    }

    @Override
    public boolean dispatchKeyEvent(@NonNull KeyEvent event) {


        return super.dispatchKeyEvent(event);
    }


    public MenuDetailDialog setOnDismissListener_(@Nullable OnDismissListener listener) {
        super.setOnDismissListener(listener);
        return this;
    }

    ///담기 버튼 누른경우 리스너
    public interface ClickAddCartListener {
        void addCart(MenuItemData selectedMenu);
    }

    ///바로 주문 누른경우 리스너
    public interface ClickOrderDirectListener {
        void orderDirect();
    }

    ///장바구니 목록 버튼 이벤트 처리
    CartListAdapter3.OnItemClickListener clickCartListButtonsListener;


    public MenuDetailDialog setClickCartListButtonsListener(CartListAdapter3.OnItemClickListener listButtonsListener) {
        this.clickCartListButtonsListener = listButtonsListener;
        if (this.mCartListAdapter != null) {
            this.mCartListAdapter.setListener(this.clickCartListButtonsListener);
        }
        return this;
    }

    public MenuDetailDialog setClickAddCartListener(ClickAddCartListener clickAddCartListener) {
        this.clickAddCartListener = clickAddCartListener;
        return this;
    }

    public MenuDetailDialog setClickOrderDirectListener(ClickOrderDirectListener clickOrderDirectListener) {
        this.clickOrderDirectListener = clickOrderDirectListener;
        return this;

    }

    private ClickAddCartListener clickAddCartListener;
    private ClickOrderDirectListener clickOrderDirectListener;

    @Override
    public boolean onKeyDown(int keyCode, @NonNull KeyEvent event) {
        LogUtil.d("다이얼로그 onkeydown : " + event.getAction());
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        LogUtil.d("다이얼로그 onBackPressed : ");

        super.onBackPressed();
    }

    public MenuDetailDialog init(Context context) {
        ///클린에 대한 메소드가 들어가야함

        if (this.isShowing())
            this.dismiss();
        this.mContext = context;
        return this;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        MenuDetailDialog.mInstance = null;
        this.clickAddCartListener = null;
        this.clickOrderDirectListener = null;
    }

    ///현재 생성된 인스턴스가 있고 이것을 접근하려 할때 사용함
    public static MenuDetailDialog getmInstance() {
        return MenuDetailDialog.mInstance;
    }


    public void complete() {
        LogUtil.d("MessageDialogBuilder 에서 complete : " + !((Activity) this.mContext).isFinishing());
        if (!((Activity) this.mContext).isFinishing())
            this.show();
    }

    public void changeQuantity(Boolean isMinus) {
        //루이팡 요청에 의해 안보이게 처리됨
        //181127
//
//        int iCount = binding.getSelectedMenuQuantity();
//
//        if (isMinus) {
//            if (iCount > 1) {
//                iCount--;
//            }
//        } else {
//            iCount++;
//        }
//
//        this.binding.setSelectedMenuQuantity(iCount);
    }

    public void selectQuantity(int index) {
        LogUtil.d("수량조절 : " + index);
        this.binding.setSelectedMenuQuantity(index);

    }

    public void clickAddCart(View view) {

        if (this.binding.getSelectedMenu() != null && this.binding.getSelectedMenu().isSet == 1 && checkNecessaryAndSelect() != null) {
            Toast.makeText(getContext(), "필수 옵션을 선택해주세요.", Toast.LENGTH_SHORT).show();
            this.binding.scrollViewOptions.setScrollY((int) checkNecessaryAndSelect().getY());
            ObjectAnimator.ofObject(checkNecessaryAndSelect(), "backgroundColor", new ArgbEvaluator(), Color.parseColor("#30ff0000"), Color.TRANSPARENT)
                    .setDuration(700)
                    .start();
            return;
        }


        if (this.clickAddCartListener != null) {

            MenuItemData selectedMenuItemTemp = null;
            try {
                selectedMenuItemTemp = (MenuItemData) this.binding.getSelectedMenu().getClone();
                selectedMenuItemTemp.selectedSideOptions = new ArrayList<>();
                for (MenuSideOptionItemData item : selectedOptions) {
                    selectedMenuItemTemp.selectedSideOptions.add((MenuSideOptionItemData) item.getClone());
                }
                selectedMenuItemTemp.quantity = this.binding.getSelectedMenuQuantity().intValue();
                LogUtil.d("옵션 목록의 정체를 밝혀라 3 : " + selectedMenuItemTemp.selectedSideOptions);

                this.clickAddCartListener.addCart(selectedMenuItemTemp);
                this.binding.setCartItemCount(this.mCartList.size());
                ValueAnimator animator = ObjectAnimator.ofFloat(this.binding.textCartItemCount, "textSize", Utils.getPixels(TypedValue.COMPLEX_UNIT_SP, 21), Utils.getPixels(TypedValue.COMPLEX_UNIT_SP, 25));
                animator.setDuration(400);
                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        animator.removeListener(this);
                        binding.textCartItemCount.setTextSize(TypedValue.COMPLEX_UNIT_SP, 21);
                    }
                });

                animator.start();

                this.mCartListAdapter.setmListData(this.mCartList);
                this.mCartListAdapter.notifyDataSetChanged();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                LogUtil.d("담기 버튼 눌렀다가 에러남 : " + e.getLocalizedMessage());
            }
        }
    }


    public void clickOrderDirect(View view) {
        if (this.clickOrderDirectListener != null) {
            this.clickOrderDirectListener.orderDirect();
        }
    }

    public void reset() {
        this.isSetOption();
    }


    /**
     * 메뉴가 선택될때 후속 뷰 처리를 위해 추가한 코드
     */
    public void isSetOption() {
        selectedOptions = null;
//        if (selectedOptions == null)
        selectedOptions = new ArrayList<>();
//        selectedOptions.clear();
        this.binding.setSelectedMenuQuantity(1);

        if (this.binding.getSelectedMenu() != null) {
            if (this.binding.getSelectedMenu().isSet == 1) {
                this.binding.layoutSetOption.removeAllViews();  //이전건 다 지워버림
                for (MenuSideGroupItemData groupItem : this.binding.getSelectedMenu().sideGroup) {
                    this.binding.layoutSetOption.addView(
                            MenuDetailSetGroupListItem.getInstance(getContext())
                                    .setData(groupItem)
                                    .setClickListener((selectedOptions, menuDetailSetGroupListItem) -> {
                                        MenuOptionDialog.getInstance(getContext())
                                                .setInitSelectedItem(selectedOptions)
                                                .setGroupItem(groupItem)
                                                .setSubmitListener((selectedOptionList) -> {
                                                    menuDetailSetGroupListItem.setSelectedOptionList(selectedOptionList);
                                                    setTotalPriceBySelectedOptions();
                                                })
                                                .complete();
                                    }));
                }
                setTotalPriceBySelectedOptions();
            } else {
                this.binding.setTotalPricePerOne(this.binding.getSelectedMenu().price);
            }
        }


    }


    /**
     * 옵션 그룹 목록중 필수인데 선택 안한 항목이 있는지 검사
     **/
    public MenuDetailSetGroupListItem checkNecessaryAndSelect() {
        for (int index = 0; index < this.binding.layoutSetOption.getChildCount(); index++) {
            ///true 일때 문제 없음
            if (!((MenuDetailSetGroupListItem) this.binding.layoutSetOption.getChildAt(index)).isNecessaryAndSelect()) {
                LogUtil.d("옵션 필수 선택안한거 : " + ((MenuDetailSetGroupListItem) this.binding.layoutSetOption.getChildAt(index)).getData().groupName);
                return (MenuDetailSetGroupListItem) this.binding.layoutSetOption.getChildAt(index);
//                return ((MenuDetailSetGroupListItem) this.binding.layoutSetOption.getChildAt(index)).getData();

            }
        }

        return null;
    }


    /**
     * 옵션 전체에서 선택된 항목에 대한 정보를 가져와야함. 해당 옵션을 array로 가져온다
     */
    public ArrayList<MenuSideOptionItemData> getSelectedOptions() {

        ArrayList<MenuSideOptionItemData> tmp = new ArrayList<>();
        for (int index = 0; index < this.binding.layoutSetOption.getChildCount(); index++) {
            ///true 일때 문제 없음
            if (((MenuDetailSetGroupListItem) this.binding.layoutSetOption.getChildAt(index)).getSelectedOptions() != null)
                tmp.addAll(((MenuDetailSetGroupListItem) this.binding.layoutSetOption.getChildAt(index)).getSelectedOptions());
        }

        return tmp;

    }

    public ArrayList<MenuSideOptionItemData> selectedOptions = new ArrayList<>();

    public void setTotalPriceBySelectedOptions() {
        selectedOptions.clear();
        this.selectedOptions = getSelectedOptions();
        int totalPrice = 0;
        for (MenuSideOptionItemData item : this.selectedOptions) {
            totalPrice += item.quantity * item.price;

        }
        LogUtil.d("선택된 메뉴의 옵션들의 가격 : " + totalPrice);
        LogUtil.d("선택된 메뉴의 가격 : " + this.binding.getSelectedMenu());

        this.binding.setTotalPricePerOne((totalPrice + this.binding.getSelectedMenu().price) * this.binding.getSelectedMenuQuantity());
        this.binding.setStrOptionList(com.cdef.hoeat.utils.Utils.getSelectedItemListStr(this.selectedOptions));

    }

    public void goCartList(View view) {

        this.binding.setIsCartListMode(!this.binding.getIsCartListMode());

    }

    public void closeCartListMode() {
        this.binding.setIsCartListMode(false);
    }


}
