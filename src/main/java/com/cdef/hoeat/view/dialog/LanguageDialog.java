package com.cdef.hoeat.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.DialogBillBinding;
import com.cdef.hoeat.databinding.DialogLanguageBinding;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.cdef.hoeat.view.adapter.LanguageAdapter;
import com.cdef.hoeat.viewModel.BillViewModel;

/**
 * Created by kimjinmo on 2019.07.20
 * 언어 선택
 */

public class LanguageDialog extends Dialog {

    LanguageAdapter languageAdapter;
    ////멤버변수
    private Context mContext;
    private static LanguageDialog mInstance;

    private DialogLanguageBinding binding;

    public static LanguageDialog getmInstanceWithoutInit() {
        return LanguageDialog.mInstance;
    }

    public void setLoadingState() {
        super.dismiss();
    }


    public static LanguageDialog getInstance(Context context) {
        if (LanguageDialog.mInstance != null) {
            LanguageDialog.mInstance.dismiss();
        }

        if (LanguageDialog.mInstance == null) {
            LanguageDialog.mInstance = new LanguageDialog(context);
        }

        LanguageDialog.mInstance.init(context);
        return LanguageDialog.mInstance;
    }

    public static void clearInstance() {
        LanguageDialog.mInstance = null;
    }


    RequestManager glide;
    RequestOptions requestOptions;

    private LanguageDialog(Context context) {
        super(context);
//        this.mContext = context;
        this.mContext = context;
        glide = Glide.with(context);
        registerHandler();
    }

    private LanguageDialog(Context context, int themeResId) {
        super(context, themeResId);
//        this.mContext = context;
        glide = Glide.with(context);
        registerHandler();
    }

    private LanguageDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
//        this.mContext = context;
        registerHandler();
    }

    public static boolean checkShowing() {
        return (mInstance != null && mInstance.isShowing());

    }


    /**
     * 초기 뷰 셋팅을 위한 부분
     **/
    private void registerHandler() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_language, null, false);
        this.binding.setDialog(this);
        setContentView(binding.getRoot());

        ///2. 하위 뷰 셋팅
//        Window window = this.getWindow();
//        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
//        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

//        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) this.binding.layoutMain.getLayoutParams();
//        lp.width = (int) (Utils.getWindosWidth(mContext) * (637 / 1280.0));
//        lp.height = (int) (lp.width * (595 / 637.0));
//        this.binding.layoutMain.setLayoutParams(lp);

        this.languageAdapter = new LanguageAdapter(getContext(), Glide.with(getContext()));

        this.binding.recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 4));
        this.binding.recyclerView.setAdapter(this.languageAdapter);
        if(this.languageAdapter != null)
            this.languageAdapter.setSelectItemListener(this.selectItemListener);

        if (TabletSettingUtils.getInstacne().getBranchInfo() != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption.lang != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption.lang.size() > 0
        ) {

            this.languageAdapter.setmListData(TabletSettingUtils.getInstacne().getBranchInfo().storeOption.lang);
            this.languageAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean dispatchKeyEvent(@NonNull KeyEvent event) {


        return super.dispatchKeyEvent(event);
    }

    public LanguageDialog init(Context context) {
        ///클린에 대한 메소드가 들어가야함

        if (this.isShowing())
            this.dismiss();
        this.mContext = context;
        return this;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (this.closeListener != null)
            this.closeListener.close();
        LanguageDialog.mInstance = null;
    }

    ///현재 생성된 인스턴스가 있고 이것을 접근하려 할때 사용함
    public static LanguageDialog getmInstance() {
        return LanguageDialog.mInstance;
    }


    public void complete() {
        if (!((Activity) this.mContext).isFinishing())
            this.show();
    }

    public void clickMain() {

    }

    public CloseListener closeListener;

    public LanguageDialog setCloseListener(CloseListener closeListener) {
        this.closeListener = closeListener;
        return this;
    }

    public interface CloseListener {
        void close();
    }

    public LanguageDialog setSelectItemListener(LanguageAdapter.SelectItemListener selectItemListener) {
        this.selectItemListener = selectItemListener;
        if(this.languageAdapter != null)
            this.languageAdapter.setSelectItemListener(this.selectItemListener);
        return this;
    }

    LanguageAdapter.SelectItemListener selectItemListener;

}
