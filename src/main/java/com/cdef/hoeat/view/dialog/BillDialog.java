package com.cdef.hoeat.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import androidx.annotation.NonNull;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.hoeat.R;
import com.cdef.hoeat.viewModel.BillViewModel;
import com.cdef.hoeat.databinding.DialogBillBinding;

/**
 * Created by kimjinmo on 2017. 1. 2..
 */

public class BillDialog extends Dialog {


    ////멤버변수
    private Context mContext;
    private static BillDialog mInstance;
    ////
    BillViewModel billViewModel;


    private DialogBillBinding binding;

    public static BillDialog getmInstanceWithoutInit() {
        return BillDialog.mInstance;
    }

    public void setLoadingState() {
        super.dismiss();
    }

    View decorView;
    int uiOption;

    /**
     * 소프트 버튼을 없앤다던지 UI와 관련된 작업을 처리한다.
     **/
    public void setUI() {

        if (decorView == null) {
            decorView = getWindow().getDecorView();
            uiOption = getWindow().getDecorView().getSystemUiVisibility();


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                uiOption |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                uiOption |= View.SYSTEM_UI_FLAG_FULLSCREEN;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                uiOption |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }


        LogUtil.d("소프트 버튼 상태 : " + decorView);
        decorView.setSystemUiVisibility(uiOption);
    }


    public static BillDialog getInstance(Context context) {
        if (BillDialog.mInstance != null) {
            BillDialog.mInstance.dismiss();
        }

        if (BillDialog.mInstance == null) {
            BillDialog.mInstance = new BillDialog(context);
        }

        BillDialog.mInstance.init(context);
        return BillDialog.mInstance;
    }

    public static void clearInstance() {
        BillDialog.mInstance = null;
    }


    RequestManager glide;
    RequestOptions requestOptions;

    private BillDialog(Context context) {
        super(context);
//        this.mContext = context;
        this.mContext = context;
        glide = Glide.with(context);
        registerHandler();
    }

    private BillDialog(Context context, int themeResId) {
        super(context, themeResId);
//        this.mContext = context;
        glide = Glide.with(context);
        registerHandler();
    }

    private BillDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
//        this.mContext = context;
        registerHandler();
    }

//    private int selectedPosition = 0;
//
//    public BillDialog setData(String categoryName, ArrayList<MenuItemData> list, int position) {
//        HorizontalMenuAdapter.SelectedPosition = position;
//        this.selectedPosition = position;
//        this.binding.setCategoryName(categoryName);
//        this.mMenuList.clear();
//        this.mMenuList.addAll(list);
//        if (list.size() > 0) {
//            this.binding.setSelectedMenu(list.get(position));
//            isSetOption();
//        }
//
//        if (binding.imageSelectedMenu.getMeasuredWidth() > 0 && binding.imageSelectedMenu.getMeasuredHeight() > 0) {
//            this.glide.load(this.binding.getSelectedMenu().foodImage).apply(this.requestOptions).into(new SimpleTarget<Drawable>() {
//                @Override
//                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
//                    binding.imageSelectedMenu.setBackground(resource);
//                }
//            });
//        }
//
//        this.horizontalMenuAdapter.setmListData(this.mMenuList);
//        return BillDialog.mInstance;
//    }
//
//
//    public BillDialog setCart(ArrayList<MenuItemData> cartList) {
//        this.mCartList = cartList;  //일부러 얕은 복사를 함.
//        this.mCartListAdapter.setmListData(this.mCartList);
//        this.binding.setCartItemCount(this.mCartList.size());
//        this.mCartListAdapter.notifyDataSetChanged();
//        return this;
//    }


    public BillDialog setBillViewModel(BillViewModel billViewModel) {
        this.binding.setBillViewModel(billViewModel);
        this.binding.setOrderList(billViewModel.mPagingOrderList.getValue());
        this.binding.setTotalPrice(billViewModel.mOrderedTotalPrice.getValue());
        this.binding.setCurrentPage(billViewModel.mOrderListPageIndex.getValue());
        this.binding.setTotalPage(billViewModel.mTotalPage.getValue());
        this.binding.setPersonCount(billViewModel.mPersonCount.getValue());
        return this;
    }

    public static boolean checkShowing() {
        return (mInstance != null && mInstance.isShowing());

    }


    /**
     * 초기 뷰 셋팅을 위한 부분
     **/
    private void registerHandler() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_bill, null, false);
        this.binding.setDialog(this);

        billViewModel = new BillViewModel();
        billViewModel.initRepository(getContext());

        setContentView(binding.getRoot());

        ///2. 하위 뷰 셋팅
        Window window = this.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setUI();

        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) this.binding.layoutMain.getLayoutParams();
        lp.width = (int) (Utils.getWindosWidth(mContext) * (637 / 1280.0));
        lp.height = (int) (lp.width * (595 / 637.0));
        this.binding.layoutMain.setLayoutParams(lp);
    }

    @Override
    public boolean dispatchKeyEvent(@NonNull KeyEvent event) {


        return super.dispatchKeyEvent(event);
    }

    public BillDialog init(Context context) {
        ///클린에 대한 메소드가 들어가야함

        if (this.isShowing())
            this.dismiss();
        this.mContext = context;
        return this;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (this.closeListener != null)
            this.closeListener.close();
        BillDialog.mInstance = null;
    }

    ///현재 생성된 인스턴스가 있고 이것을 접근하려 할때 사용함
    public static BillDialog getmInstance() {
        return BillDialog.mInstance;
    }


    public void complete() {
        if (!((Activity) this.mContext).isFinishing())
            this.show();
    }

    public void clickMain() {

    }

    public CloseListener closeListener;

    public BillDialog setCloseListener(CloseListener closeListener) {
        this.closeListener = closeListener;
        return this;
    }

    public interface CloseListener {
        void close();
    }

}
