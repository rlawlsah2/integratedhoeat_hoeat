package com.cdef.hoeat.view.dialog.loadingDialog;


import android.app.ProgressDialog;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableField;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.WindowManager;

import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.hoeat.R;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.hoeat.databinding.CustomProgressBinding;

/**
 * Created by Mehmet Deniz on 20/06/2017.
 *
 *
 * converted by jinmo on 31/01/2017.
 */


public class LoadingDialog extends ProgressDialog implements LifecycleOwner {

    ObservableField<String> mTitle = new ObservableField<>();
    CustomProgressBinding binding;
    public LoadingDialog(Context context, String title) {
        super(context, R.style.CustomDialog);
        mTitle.set(title);
    }

    public LoadingDialog(Context context, int theme, String title) {
        super(context, theme);
        mTitle.set(title);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(getContext());
    }

    private void init(Context context) {
        setCancelable(false);
        setCanceledOnTouchOutside(false);

        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.custom_progress, null, false);
        setContentView(binding.getRoot());
        binding.setTitle(this.mTitle.get());

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = Utils.dpToPx(context, 300);
        params.height = Utils.dpToPx(context, 180);//WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes(params);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
    }

    public LoadingDialog setTitle(String title)
    {
        this.mTitle.set(title);
        binding.setTitle(this.mTitle.get());
        return this;
    }
    public void setTitleNew(String title)
    {
        this.mTitle.set(title);
    }

    @Override
    public void show() {
        LogUtil.d("loading show");
        if(binding != null)
            binding.setTitle(this.mTitle.get());
        super.show();
    }

    @Override
    public void dismiss() {
        LogUtil.d("loading dismiss");

        super.dismiss();
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return null;
    }
}