package com.cdef.hoeat.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cdef.commonmodule.dataModel.BannerDefaultData;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.hoeat.R;
import com.cdef.hoeat.view.adapter.EventBannerListAdapter;
import com.cdef.hoeat.view.dialog.loadingDialog.LoadingDialog;
import com.cdef.hoeat.view.viewPager.EventViewPagerAdapter;
import com.cdef.hoeat.viewModel.BillViewModel;
import com.cdef.hoeat.databinding.DialogEventBinding;

import net.yslibrary.android.keyboardvisibilityevent.Unregistrar;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by kimjinmo on 2017. 1. 2..
 */

public class ADDialog extends Dialog {


    ////멤버변수
    private Context mContext;
    private static ADDialog mInstance;


    RequestOptions options = null;
    Unregistrar unregistrar;
    EventBannerListAdapter eventBannerListAdapter;
    //    boolean isRandomBannerMode = false;
    boolean isKeyBoardShow = false;

    LoadingDialog loadingDialog;

    Random random = new Random();
    ///오토 스크롤링을 위해 필요함.
    CompositeSubscription scrollComSub = new CompositeSubscription();
    boolean autoScrollAvailable = true; ////false가 되는경우는 1. 스크롤링중일때, 2. 참여방법같은거 열람중일때,
    EventViewPagerAdapter eventViewPagerAdapter;
    Date lastTouchTime = null;
    Date nowTime = null;


    private DialogEventBinding binding;

    public static ADDialog getmInstanceWithoutInit() {
        return ADDialog.mInstance;
    }

    public void setLoadingState() {
        super.dismiss();
    }

    View decorView;
    int uiOption;

    /**
     * 소프트 버튼을 없앤다던지 UI와 관련된 작업을 처리한다.
     **/
    public void setUI() {

        if (decorView == null) {
            decorView = getWindow().getDecorView();
            uiOption = getWindow().getDecorView().getSystemUiVisibility();


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                uiOption |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                uiOption |= View.SYSTEM_UI_FLAG_FULLSCREEN;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                uiOption |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }


        LogUtil.d("소프트 버튼 상태 : " + decorView);
        decorView.setSystemUiVisibility(uiOption);
    }


    public static ADDialog getInstance(Context context) {
        if (ADDialog.mInstance != null) {
            ADDialog.mInstance.dismiss();
        }

        if (ADDialog.mInstance == null) {
            ADDialog.mInstance = new ADDialog(context);
        }

        ADDialog.mInstance.init(context);
        return ADDialog.mInstance;
    }

    public static void clearInstance() {
        ADDialog.mInstance = null;
    }


    RequestManager glide;
    RequestOptions requestOptions;

    private ADDialog(Context context) {
        super(context);
//        this.mContext = context;
        this.mContext = context;
        glide = Glide.with(context);
        registerHandler();
    }

    private ADDialog(Context context, int themeResId) {
        super(context, themeResId);
//        this.mContext = context;
        glide = Glide.with(context);
        registerHandler();
    }

    private ADDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
//        this.mContext = context;
        registerHandler();
    }

//    private int selectedPosition = 0;
//
//    public BillDialog setData(String categoryName, ArrayList<MenuItemData> list, int position) {
//        HorizontalMenuAdapter.SelectedPosition = position;
//        this.selectedPosition = position;
//        this.binding.setCategoryName(categoryName);
//        this.mMenuList.clear();
//        this.mMenuList.addAll(list);
//        if (list.size() > 0) {
//            this.binding.setSelectedMenu(list.get(position));
//            isSetOption();
//        }
//
//        if (binding.imageSelectedMenu.getMeasuredWidth() > 0 && binding.imageSelectedMenu.getMeasuredHeight() > 0) {
//            this.glide.load(this.binding.getSelectedMenu().foodImage).apply(this.requestOptions).into(new SimpleTarget<Drawable>() {
//                @Override
//                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
//                    binding.imageSelectedMenu.setBackground(resource);
//                }
//            });
//        }
//
//        this.horizontalMenuAdapter.setmListData(this.mMenuList);
//        return BillDialog.mInstance;
//    }
//
//
//    public BillDialog setCart(ArrayList<MenuItemData> cartList) {
//        this.mCartList = cartList;  //일부러 얕은 복사를 함.
//        this.mCartListAdapter.setmListData(this.mCartList);
//        this.binding.setCartItemCount(this.mCartList.size());
//        this.mCartListAdapter.notifyDataSetChanged();
//        return this;
//    }


    public ADDialog setBillViewModel(BillViewModel billViewModel) {
//        this.binding.setBillViewModel(billViewModel);
        return this;
    }

    public ADDialog setBannerList(boolean isRandomBannerMode, ArrayList<BannerDefaultData> bannerList) {
        if (bannerList == null || bannerList.size() == 0) {
            //배너 없음
            this.binding.setEventCount(0);
        } else {
            this.binding.setIsRandomBannerMode(isRandomBannerMode);
            if (isRandomBannerMode) {
                ///자동 광고로 인해 띄워진 경우
                this.eventViewPagerAdapter = new EventViewPagerAdapter(mContext, bannerList, Glide.with(mContext));
                this.binding.viewPager.setAdapter(this.eventViewPagerAdapter);
                this.binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        //화면 변하면 셋팅
//                        adViewModel.mSelectedBanner.setValue(bannerList.get(position));
                        binding.setSelectedBannerItem(bannerList.get(position));
                        setInputHint(bannerList.get(position));
                        binding.setEventDetailADType(bannerList.get(position).adType);
                        binding.setCurrentEventPosition(position + 1);

                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
                this.binding.setEventCount(bannerList.size());
                this.binding.setCurrentEventPosition(1);

                ///첫페이지는 이벤트로 처리 안되니까 여기서 처리.
//                this.adViewModel.mSelectedBanner.setValue(bannerList.get(0));
                this.binding.setSelectedBannerItem(bannerList.get(0));
                this.binding.setEventDetailADType(bannerList.get(0).adType);
                this.binding.setEventDetailFlag(true);
//                    this.binding.layoutTop.bringToFront();

                this.binding.textRandomEventClose.bringToFront();

                this.scrollComSub.add(Observable.interval(10, 10, TimeUnit.SECONDS)
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(aLong -> {
                            // here is the task that should repeat
                            //1. viewPager가 보일때
                            //2. 스크롤중이 아닐때
                            //3. 참여방법이나 참여를 보고있는 경우가 아닐때

                            if (this.nowTime == null)
                                this.nowTime = new Date();
                            long mills = 2500;
                            if (this.lastTouchTime != null)
                                mills = lastTouchTime.getTime() - nowTime.getTime();

                            if (this.binding.viewPager.getVisibility() == View.VISIBLE
                                    && this.binding.layoutEventSubscription.getVisibility() == View.GONE
                                    && this.binding.layoutAuth.getVisibility() == View.GONE
                                    && this.autoScrollAvailable
                                    && mills > 2000
                                    ) {

                                if (this.binding.viewPager.getAdapter().getCount() > 1 && this.binding.viewPager.getCurrentItem() + 1 == this.binding.viewPager.getAdapter().getCount())  ///마지막 페이지
                                {
                                    this.binding.viewPager.setCurrentItem(0, false);
                                } else {
                                    this.binding.viewPager.setCurrentItem(this.binding.viewPager.getCurrentItem() + 1, false);
                                }
                            }
                        })
                );


            } else {
                ///직접 들어와서 열람한 케이스
                eventBannerListAdapter.setmListData(bannerList);
                this.binding.recyclerView.setAdapter(eventBannerListAdapter);
                this.eventBannerListAdapter.notifyDataSetChanged();
                this.binding.setEventCount(bannerList.size());
            }
        }
        return this;
    }

    public static boolean checkShowing() {
        return (mInstance != null && mInstance.isShowing());

    }


    /**
     * 초기 뷰 셋팅을 위한 부분
     **/
    private void registerHandler() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_event, null, false);
        this.binding.setAdDialog(this);
        setContentView(binding.getRoot());

        ///1. 윈도우 셋팅
        Window window = this.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawableResource(R.color.black);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
        ///2. 하위 뷰 셋팅

        this.binding.setEventCount(0);
        this.binding.setCurrentEventPosition(0);


        options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(false)
                .override(Utils.getWindosWidth(mContext), Utils.getWindosHeight(mContext));


//        this.loadingDialog = new LoadingDialog(this, "처리중..");

        ///recyclerview와 adapter 초기화
        this.eventBannerListAdapter = new EventBannerListAdapter(mContext, Glide.with(mContext));
        this.eventBannerListAdapter.setListener((v, itemData) -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                this.binding.imageEvent.setBackgroundColor(mContext.getColor(R.color.black));
            } else {
                this.binding.imageEvent.setBackgroundColor(mContext.getResources().getColor(R.color.black));
            }
            Glide.with(mContext).clear(this.binding.imageEvent);

            try {
                this.binding.setSelectedBannerItem(itemData);
                setInputHint(itemData);


                if (itemData.files.size() > 0) {
                    Glide.with(mContext).load(itemData.files.get(
                            random.nextInt(itemData.files.size())
                    ).fileUrl)
                            .apply(options)
                            .into(this.binding.imageEvent);
                }


                this.binding.setEventDetailADType(itemData.adType);

            } catch (Exception e) {

            }

            this.binding.setEventDetailFlag(true);
            this.binding.layoutTop.bringToFront();
//            this.adViewModel.getBannerDetail(itemData.uuid, TabletSettingUtils.getInstacne().getTableNo(), 0, "na");
            this.bannerDetailListener.getBannerDetail(itemData);

        });
        this.binding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        this.binding.inputAuth.setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                    case KeyEvent.KEYCODE_ENTER:
                        binding.buttonGoAuth.performClick();
                        return true;
                    default:
                        break;
                }
            }
            return false;
        });

        setUI();

        this.binding.layoutTop.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                binding.layoutTop.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) binding.layoutTop.getLayoutParams();
                lp.height = (int) (Utils.getWindosHeight(mContext) * (1 / 8.0));

                binding.layoutTop.setLayoutParams(lp);

            }
        });

    }

    private void setInputHint(BannerDefaultData bannerDefaultData) {
        if (bannerDefaultData != null) {
            if (bannerDefaultData.actionParamType != null) {
                this.binding.setActionParamType(bannerDefaultData.actionParamType);
                if (bannerDefaultData.actionParamType.equals("email"))
                    this.binding.inputAuth.setHint("이메일 주소를 입력해주세요.");
                else if (bannerDefaultData.actionParamType.equals("phone"))
                    this.binding.inputAuth.setHint("휴대폰 번호를 입력해주세요.");

            }
        }
    }

    @Override
    public boolean dispatchKeyEvent(@NonNull KeyEvent event) {


        return super.dispatchKeyEvent(event);
    }

    public ADDialog init(Context context) {
        ///클린에 대한 메소드가 들어가야함

        if (this.isShowing())
            this.dismiss();
        this.mContext = context;
        return this;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if(this.scrollComSub != null)
        {
            this.scrollComSub.clear();
        }
        if (this.closeListener != null)
            this.closeListener.close();
        ADDialog.mInstance = null;
        this.mContext = null;
    }

    ///현재 생성된 인스턴스가 있고 이것을 접근하려 할때 사용함
    public static ADDialog getmInstance() {
        return ADDialog.mInstance;
    }


    public void complete() {
        if (!((Activity) this.mContext).isFinishing())
            this.show();
    }

    public void clickMain() {

    }

    public CloseListener closeListener;

    public ADDialog setCloseListener(CloseListener closeListener) {
        this.closeListener = closeListener;
        return this;
    }

    public interface CloseListener {
        void close();
    }

    public BannerDetailListener bannerDetailListener;

    public ADDialog setBannerDetailListener(BannerDetailListener closeListener) {
        this.bannerDetailListener = closeListener;
        return this;
    }

    public interface BannerDetailListener {
        void getBannerDetail(BannerDefaultData selectedItem);
    }

    public AuthListener authListener;

    public ADDialog setAuthListener(AuthListener authListener) {
        this.authListener = authListener;
        return this;
    }

    public interface AuthListener {
        void goAuth(BannerDefaultData bannerDefaultData, String input);
    }


    ////이벤트 참여방법 보기
    public void viewEventSubscription(View view) {
        this.binding.layoutEventSubscription.bringToFront();
        this.binding.layoutEventSubscription.setVisibility(View.VISIBLE);
    }


    public void clickOutOfLayoutAuth(View view) {

//        if (this.isKeyBoardShow) {
//            UIUtil.hideKeyboard(getOwnerActivity());
//            return;
//        }

        this.binding.layoutEventSubscription.setVisibility(View.GONE);
        this.binding.layoutAuth.setVisibility(View.GONE);
//        UIUtil.hideKeyboard(getOwnerActivity());
    }

    public void goAuth(View view) {
        if (this.authListener != null) {
            this.authListener.goAuth(this.binding.getSelectedBannerItem(), this.binding.inputAuth.getText().toString());
        }

    }


    public void viewInputAuth(View view) {

        this.binding.layoutAuth.bringToFront();
        this.binding.layoutAuth.setVisibility(View.VISIBLE);
    }


    public void closeADSelectionPage(View view) {

        try {

            if (this.binding.getIsRandomBannerMode()) {
                dismiss();
                return;
            }
            if (this.binding.getEventDetailFlag()) {
                this.binding.setEventDetailFlag(false);
                Glide.with(mContext).clear(this.binding.imageEvent);
            } else
                dismiss();
        } catch (Exception e) {
            dismiss();
        }
    }


    public void closeADOnlyScreenMode(View view) {
        if (this.binding != null && (this.binding.getEventDetailADType() == null || this.binding.getEventDetailADType().equals("display"))) {
            dismiss();
        }
    }

    public void clearInput() {
        this.binding.inputAuth.setText("");
    }

}
