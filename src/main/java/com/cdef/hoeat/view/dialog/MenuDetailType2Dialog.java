package com.cdef.hoeat.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.TextViewCompat;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.LoginUtil;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.commonmodule.dataModel.MenuItemData;
import com.cdef.commonmodule.dataModel.MenuSideGroupItemData;
import com.cdef.commonmodule.dataModel.MenuSideOptionItemData;
import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.DialogMenuDetailType2Binding;
import com.cdef.hoeat.view.adapter.CartListAdapter3;
import com.cdef.hoeat.view.adapter.type2.SetMenuSelectedOptionListAdapter;
import com.cdef.hoeat.view.customView.menuOption.type2.MenuDetailSetGroupListItem;

import java.util.ArrayList;

/**
 * Created by kimjinmo on 2019.06.15
 * 타입 2에서 사용중인 UI를 참조해 변경한 방식. Type2 OrderFragment 참
 **/

public class MenuDetailType2Dialog extends Dialog {

    ArrayList<MenuItemData> mMenuList = new ArrayList<>();
    ArrayList<MenuItemData> mCartList = new ArrayList<>();
    public ArrayList<MenuSideOptionItemData> selectedOptions = new ArrayList<>();
    SetMenuSelectedOptionListAdapter selectedOptionListAdapter;

    ////멤버변수
    private Context mContext;
    private static MenuDetailType2Dialog mInstance;
    ////

    private DialogMenuDetailType2Binding binding;

    public static MenuDetailType2Dialog getmInstanceWithoutInit() {
        return MenuDetailType2Dialog.mInstance;
    }


    View decorView;
    int uiOption;

    /**
     * 소프트 버튼을 없앤다던지 UI와 관련된 작업을 처리한다.
     **/
    public void setUI() {

        if (decorView == null) {
            decorView = getWindow().getDecorView();
            uiOption = getWindow().getDecorView().getSystemUiVisibility();


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                uiOption |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                uiOption |= View.SYSTEM_UI_FLAG_FULLSCREEN;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                uiOption |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }


        LogUtil.d("소프트 버튼 상태 : " + decorView);
        decorView.setSystemUiVisibility(uiOption);
    }


    public static MenuDetailType2Dialog getInstance(Context context) {
        if (MenuDetailType2Dialog.mInstance != null) {
            MenuDetailType2Dialog.mInstance.dismiss();
        }

        if (MenuDetailType2Dialog.mInstance == null) {
            MenuDetailType2Dialog.mInstance = new MenuDetailType2Dialog(context);
        }

        MenuDetailType2Dialog.mInstance.init(context);
        return MenuDetailType2Dialog.mInstance;
    }

    public static void clearInstance() {
        MenuDetailType2Dialog.mInstance = null;
    }


    RequestManager glide;
    RequestOptions requestOptions;

    private MenuDetailType2Dialog(Context context) {
        super(context);
//        this.mContext = context;
        this.mContext = context;
        glide = Glide.with(context);
        registerHandler();
    }

    private MenuDetailType2Dialog(Context context, int themeResId) {
        super(context, themeResId);
//        this.mContext = context;
        glide = Glide.with(context);
        registerHandler();
    }

    private MenuDetailType2Dialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
//        this.mContext = context;
        registerHandler();
    }

    private int selectedPosition = 0;


    public MenuDetailType2Dialog setData(MenuItemData selectedMenu) {
        this.binding.setSelectedSetMenu(selectedMenu);
        this.setSelectedMenuViews(selectedMenu);
        this.getSelectedOptions();

        return MenuDetailType2Dialog.mInstance;
    }

    /**
     * 초기 뷰 셋팅을 위한 부분
     **/
    private void registerHandler() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.requestOptions = new RequestOptions();
        this.requestOptions.override(Utils.dpToPx(mContext, 300), Utils.dpToPx(mContext, 200));
        this.requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true);
        this.requestOptions.placeholder(R.drawable.img_sample_menu);
        this.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                LogUtil.d("다이얼로그 키 누르는거 : " + keyEvent.getAction());
                if (keyEvent.getAction() == KeyEvent.KEYCODE_BACK) {
                    dismiss();
                }
                return false;
            }

        });

        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_menu_detail_type2, null, false);
        this.binding.setDialog(this);
        this.binding.imageSelectedMenu.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int width = binding.imageSelectedMenu.getMeasuredWidth();
                LogUtil.d("선택된 이미지 영역 사이즈 : " + width);
                if (width > 3) {
                    binding.imageSelectedMenu.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    RelativeLayout.LayoutParams ilp = (RelativeLayout.LayoutParams) binding.imageSelectedMenu.getLayoutParams();
                    ilp.height = (int) (width * 0.66);
                    binding.imageSelectedMenu.setLayoutParams(ilp);
                    binding.imageSelectedMenu.setVisibility(View.VISIBLE);
                }
            }
        });

        setContentView(binding.getRoot());

        ///2. 하위 뷰 셋팅
        Window window = this.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setUI();


    }

    @Override
    public void show() {
        super.show();
    }

    @Override
    public boolean dispatchKeyEvent(@NonNull KeyEvent event) {
        return super.dispatchKeyEvent(event);
    }


    public MenuDetailType2Dialog setOnDismissListener_(@Nullable OnDismissListener listener) {
        super.setOnDismissListener(listener);
        return this;
    }

    ///담기 버튼 누른경우 리스너
    public interface ClickAddCartListener {
        void addCart(MenuItemData selectedMenu);
    }

    ///바로 주문 누른경우 리스너
    public interface ClickOrderDirectListener {
        void orderDirect();
    }
    private ClickAddCartListener clickAddCartListener;

    public MenuDetailType2Dialog setClickAddCartListener(ClickAddCartListener clickAddCartListener) {
        this.clickAddCartListener = clickAddCartListener;
        return this;
    }
    public void addCart()
    {


        ///필수 항목 점검
        int optionCount = this.binding.getSelectedSetMenu().sideGroup.size();
        int index = 0;

        LogUtil.d("addCart optionCount : " + optionCount);


        for (index = 0; index < optionCount; index++) {
            LogUtil.d("addCart index : " + index);
            if (this.binding.getSelectedSetMenu().sideGroup.get(index).isNecessary == 1 &&
                    ((MenuDetailSetGroupListItem) this.binding.layoutMenuSetOptions.getChildAt(index)).getSelectedOptions().size() == 0
            )  //필수인데 선택 안한경우
            {


                this.binding.scrollViewOptions.scrollTo(0, (int) this.binding.layoutMenuSetOptions.getChildAt(index).getY());

                Toast.makeText(getContext(), (LoginUtil.getInstance().getSelectedLanguage() == null || LoginUtil.getInstance().getSelectedLanguage().equals("ko") ? "[" + this.binding.getSelectedSetMenu().sideGroup.get(index).groupName + "] 를 선택해주세요." : "please select " + this.binding.getSelectedSetMenu().sideGroup.get(index).groupName + " option"), Toast.LENGTH_SHORT).show();
                ((MenuDetailSetGroupListItem) this.binding.layoutMenuSetOptions.getChildAt(index)).showHint();
                return;
            }
//            index++;
        }

        ///셋트메뉴 담는 로직 추가
        MenuItemData selectedMenuItemTemp = null;
        try {
            selectedMenuItemTemp = (MenuItemData) this.binding.getSelectedSetMenu().getClone();
            selectedMenuItemTemp.selectedSideOptions = new ArrayList<>();
            for (MenuSideOptionItemData item : selectedOptions) {
                selectedMenuItemTemp.selectedSideOptions.add((MenuSideOptionItemData) item.getClone());
            }

            this.clickAddCartListener.addCart(selectedMenuItemTemp);
            return;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            LogUtil.d("담기 버튼 눌렀다가 에러남 : " + e.getLocalizedMessage());
        }


    }

    ///장바구니 목록 버튼 이벤트 처리
    CartListAdapter3.OnItemClickListener clickCartListButtonsListener;

    public MenuDetailType2Dialog init(Context context) {
        ///클린에 대한 메소드가 들어가야함

        if (this.isShowing())
            this.dismiss();
        this.mContext = context;
        return this;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        MenuDetailType2Dialog.mInstance = null;
    }

    ///현재 생성된 인스턴스가 있고 이것을 접근하려 할때 사용함
    public static MenuDetailType2Dialog getmInstance() {
        return MenuDetailType2Dialog.mInstance;
    }


    public void complete() {
        LogUtil.d("MessageDialogBuilder 에서 complete : " + !((Activity) this.mContext).isFinishing());
        if (!((Activity) this.mContext).isFinishing())
            this.show();
    }

    private void setSelectedMenuViews(MenuItemData itemData) {
        this.binding.setSelectedSetMenu(itemData);


        if (itemData.isSet == 1) {
            this.glide.load(itemData.foodImage).apply(this.requestOptions)
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            glide.load(resource).apply(requestOptions).into(binding.imageSelectedMenu);
                        }
                    });
            for (MenuSideGroupItemData groupItem : itemData.sideGroup) {
                this.binding.layoutMenuSetOptions.addView(
                        MenuDetailSetGroupListItem.getInstance(getContext())
                                .setData(groupItem)
                                .setClickListener((selectedOptions, menuDetailSetGroupListItem) -> {

                                    this.getSelectedOptions();
//                                    this.binding.textSelectedOptions.setText(com.cdef.hoeat.utils.Utils.getSelectedItemListStr(selectedOptions));
//                                    MenuOptionDialog.getInstance(getContext())
//                                            .setInitSelectedItem(selectedOptions)
//                                            .setGroupItem(groupItem)
//                                            .setSubmitListener((selectedOptionList) -> {
//                                            })
//                                            .complete();
                                }));
            }

            this.getSelectedOptions();

        } else if (itemData.showDetail == 1) {
            this.glide.load(itemData.foodImage).apply(this.requestOptions).into(new SimpleTarget<Drawable>() {
                @Override
                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                    glide.load(resource).apply(requestOptions).into(binding.imageSelectedMenu);
                }
            });
        }

    }

    private void getSelectedOptions() {

//        this.binding.layoutResultSelectedOptions.removeAllViews();
        int setOptionCount = this.binding.getSelectedSetMenu().sideGroup.size();
        this.selectedOptions.clear();

        for (int index = 0; index < setOptionCount; index++) {
            selectedOptions.addAll(((MenuDetailSetGroupListItem) this.binding.layoutMenuSetOptions.getChildAt(index)).getSelectedOptions());
        }
////        this.binding.layoutResultSelectedOptions.removeAllViews();
//        for (int i = 0; i < tmp.size(); i++) {
//            this.selectedOptions.put(tmp.get(i).CMDTCD, tmp.get(i));
//            this.binding.layoutResultSelectedOptions.addView(
//                    MenuDetailResultOptionListItem.getInstance(getContext()).
//                            setOption(tmp.get(i))
//            );
//        }

        if (this.selectedOptionListAdapter == null) {
            this.selectedOptionListAdapter = new SetMenuSelectedOptionListAdapter();
            this.selectedOptionListAdapter.setmListData(this.selectedOptions);
            this.selectedOptionListAdapter.setHasStableIds(true);
            this.binding.recyclerViewSelectedOptions.setAdapter(this.selectedOptionListAdapter);
        } else {
            this.selectedOptionListAdapter.setmListData(this.selectedOptions);
        }
        this.selectedOptionListAdapter.notifyDataSetChanged();
        this.binding.textMenuResultTotalPrice.setText(
                Utils.setComma((com.cdef.hoeat.utils.Utils.getSelectedOptionsTotalPrice(this.selectedOptions) + this.binding.getSelectedSetMenu().price) * this.binding.getSelectedSetMenu().quantity) + "원"
        );

    }

    public void touchBlock() {

    }

}
