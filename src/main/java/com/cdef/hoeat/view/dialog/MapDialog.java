package com.cdef.hoeat.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Build;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cdef.commonmodule.dataModel.firebaseDataSet.TableInfo;
import com.cdef.commonmodule.login.TableMapItem;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.hoeat.R;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.cdef.hoeat.databinding.DialogMapBinding;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by kimjinmo on 2017. 1. 2..
 * 테이블 맵 표기
 */

public class MapDialog extends Dialog {

    DialogMapBinding binding;

    final int base = 50;     ///api 로 가져온 결과 단위가 몇으로 끊어지는지. 현재는 50단위
    final int changedBase = 80;  ///변경할 단위. 50을 80으로 바꾸다는 의미임
    int rowCount;   //가로로 최대 몇개까지 있는지
    int columnCount; //세로로 최대 몇개까지 있는지
    int maxWidth;
    int maxHeight;
    int layoutWidth;
    int layoutHeight;

    private MODE mode = MODE.VIEW;

    public MapDialog setMODE(MODE mode) {
        this.mode = mode;
        this.binding.setMode(this.mode);
        return this;
    }

    public enum MODE {
        GAME, CHAT, VIEW
    }



    public void goClose(View v) {
        dismiss();
    }

    ////멤버변수
    private Context mContext;
    private static MapDialog mInstance;
    ////

    public static MapDialog getmInstance() {
        return MapDialog.mInstance;
    }

    public static MapDialog getInstance(Context context) {
        if (MapDialog.mInstance != null) {
            MapDialog.mInstance.dismiss();
        }

        if (MapDialog.mInstance == null) {
            MapDialog.mInstance = new MapDialog(context);
        }

        MapDialog.mInstance.init(context);
        return MapDialog.mInstance;
    }

    public static void clearInstance() {
        MapDialog.mInstance = null;
    }


    private MapDialog(Context context) {
        super(context);
        this.mContext = context;
        registerHandler();
    }

    private MapDialog(Context context, int themeResId) {
        super(context, themeResId);
//        this.mContext = context;
        registerHandler();
    }

    private MapDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
//        this.mContext = context;
        registerHandler();
    }

    private static final int MiniMapWidth = 230;

    float touchStartPointX = 0;
    float touchStartPointY = 0;

    int mapWidth = 0;
    int mapHeight = 0;
    int mapOutSideWidth = 0;
    int mapOutSideHeight = 0;
    int differenceWidth = 0;
    int differenceHegight = 0;
    int moveX = 0;  ///누른 지점으로부터 이동한 거리
    int moveY = 0;
    float prevX = 0;  ///이벤트 전 X
    float currentX = 0;  ///현재 X
    float prevY = 0;  ///이벤트 전 X
    float currentY = 0;  ///현재 X

    private static final int ConvertedBoxSize = 70;

    public int width = 0;
    public int margin = 0;

    /**
     * 홀 버튼을 초기화한다.
     */
    private void initHallButtons() {
        this.binding.layoutHallButtons.removeAllViews();
        for (String key : TabletSettingUtils.getInstacne().getConvertedTableMaps().keySet()) {
            TextView button = new TextView(getContext());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                button.setTextColor(getContext().getColorStateList(R.color.selector_table_map_hall_item_title));
            }
            else
            {
                button.setTextColor(getContext().getResources().getColorStateList(R.color.selector_table_map_hall_item_title));
            }
            button.setText(key);
            button.setPadding(Utils.dpToPx(getContext(), 10), Utils.dpToPx(getContext(), 10), Utils.dpToPx(getContext(), 10), Utils.dpToPx(getContext(), 10));
            button.setGravity(Gravity.CENTER);
            button.setTag(key);
            button.setBackgroundResource(R.color.transparent);
            button.setSelected(false);
            button.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28);
            button.setOnClickListener(view -> {
                LogUtil.d("홀 선택 버튼 누름 key : " + view.getTag());
                if (!view.isSelected()) {
                    setSelectionHallButtons(view);
                    initMapItem((String) view.getTag());
                }
            });

            this.binding.layoutHallButtons.addView(button, 0);
        }
    }

    private void setSelectionHallButtons(View view) {
        for (int index = 0; index < this.binding.layoutHallButtons.getChildCount(); index++) {
            this.binding.layoutHallButtons.getChildAt(index).setSelected((this.binding.layoutHallButtons.getChildAt(index).equals(view)));
            LogUtil.d("홀 구분 선택 상황 index : " + index + " / " + this.binding.layoutHallButtons.getChildAt(index).isSelected());
        }
    }

    private TextView getSelectedHallButton() {
        for (int index = 0; index < this.binding.layoutHallButtons.getChildCount(); index++) {
            if (this.binding.layoutHallButtons.getChildAt(index).isSelected())
                return (TextView) this.binding.layoutHallButtons.getChildAt(index);
        }
        return null;
    }

    private TextView getSelectedHallButton(String key) {
        for (int index = 0; index < this.binding.layoutHallButtons.getChildCount(); index++) {
            if (this.binding.layoutHallButtons.getChildAt(index).getTag().equals(key))
                return (TextView) this.binding.layoutHallButtons.getChildAt(index);
        }
        return null;
    }

    private TextView getDefaultHallButton() {
        if (this.binding.layoutHallButtons.getChildCount() > 0)
            return (TextView) this.binding.layoutHallButtons.getChildAt(0);
        else
            return null;
    }

    /**
     * 초기 뷰 셋팅을 위한 부분
     **/
    private void registerHandler() {

        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_map, null, false);
        this.binding.setDialog(this);
        this.binding.setMode(this.mode);

        this.width = Utils.dpToPx(getContext(), MapDialog.ConvertedBoxSize - 10);
        this.margin = Utils.dpToPx(getContext(), 5);


        setContentView(binding.getRoot());


        this.binding.layoutButtonClose.setVisibility(View.VISIBLE);

        ///1. 윈도우 셋팅
        Window window = this.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawableResource(R.color.lowBlack);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
        ///2. 하위 뷰 셋팅

//        initMapItem();
        this.binding.layoutMiniMapOutSide.bringToFront();

    }


    private ArrayList<TableMapItem> selectedHallTableList = null;

    private void clearAllTableItems() {

        this.binding.layoutMap.removeAllViews();


    }

    private void initVariables() {
        this.mapWidth = 0;
        this.mapHeight = 0;
        this.mapOutSideHeight = 0;
        this.mapOutSideWidth = 0;
        this.differenceWidth = 0;
        this.differenceHegight = 0;
    }

    View.OnTouchListener dragListener = new View.OnTouchListener() {
        private static final int MAX_CLICK_DURATION = 500;
        private long startClickTime;

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (mapWidth < 2) {
                mapWidth = binding.layoutMap.getMeasuredWidth();
            }
            if (mapHeight < 2) {
                mapHeight = binding.layoutMap.getMeasuredHeight();
            }

            if (mapOutSideWidth < 2) {
                mapOutSideWidth = binding.layoutMapOutSide.getMeasuredWidth();
            }
            if (mapOutSideHeight < 2) {
                mapOutSideHeight = binding.layoutMapOutSide.getMeasuredHeight();
            }

            if (mapWidth >= 2 && mapOutSideWidth >= 2) {
                differenceWidth = (mapWidth - mapOutSideWidth);
            }


            if (mapHeight >= 2 && mapOutSideHeight >= 2) {
                differenceHegight = (mapHeight - mapOutSideHeight);
            }

            LogUtil.d("테이블 맵 이동 이벤트 맵 뷰 사이즈 mapWidth : " + mapWidth + " / mapOutSideWidth : " + mapOutSideWidth + " / mapHeight : " + mapHeight + " / mapOutSideHeight : " + mapOutSideHeight);


            ///생성된 map의 크기가 감싸고있는 레이아웃보다 작으면 동작할 필요가 없음
//                if (mapWidth < mapOutSideWidth
//                        && mapHeight < mapOutSideHeight) {
//                    return false;
//                }


            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                startClickTime = Calendar.getInstance().getTimeInMillis();
                touchStartPointX = motionEvent.getRawX();
                touchStartPointY = motionEvent.getRawY();
                prevX = touchStartPointX;
                currentX = touchStartPointX;
                prevY = touchStartPointY;
                currentY = touchStartPointY;
                binding.layoutMiniMapOutSide.setAlpha(0.4f);
            }
            if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                if (clickDuration < MAX_CLICK_DURATION) {
                    //click event has occurred
                    view.performClick();
                    return true;
                }
                binding.layoutMiniMapOutSide.setAlpha(0.1f);

            }
            if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                ///1. layoutMap.getX < (layoutMap.width < layoutMapOutSide.width)


                prevX = currentX;
                currentX = motionEvent.getRawX();

                prevY = currentY;
                currentY = motionEvent.getRawY();

                moveX = (int) (motionEvent.getRawX() - touchStartPointX);
                moveY = (int) (motionEvent.getRawY() - touchStartPointY);

//                    if(Math.abs(moveX) > Math.abs(differenceWidth))
//                    {
//                        touchStartPointX = motionEvent.getRawX();
//                        return false;
//                    }


                ///좌우 움직임
                ///오른쪽 끝에 닿았는데 계속 왼쪽으로 스크롤할때
                if (currentX < prevX && Math.abs(binding.layoutMap.getX() + (currentX - prevX)) >= Math.abs(differenceWidth)) {
                    binding.layoutMap.setX(-differenceWidth);
                } else {
                    ///왼쪽으로 움직일 경우
                    if ((mapWidth > mapOutSideWidth)
                            && currentX < prevX) {
                        binding.layoutMap.setX((int) (Math.abs(binding.layoutMap.getX() + (currentX - prevX)) > Math.abs(differenceWidth) ? -differenceWidth : binding.layoutMap.getX() + (currentX - prevX)));
                    }
                }
                ///왼쪽 끝에 닿았는데 계속 오른쪽으로 스크롤할때
                if (currentX > prevX && binding.layoutMap.getX() >= 0) {
                    binding.layoutMap.setX(0);
                } else {
                    ///오른쪽으로 움직일 경우
                    if ((mapWidth > mapOutSideWidth)
                            && currentX > prevX) {
                        binding.layoutMap.setX((int) (binding.layoutMap.getX() > 0 ? 0 : binding.layoutMap.getX() + (currentX - prevX)));
                    }
                }


                ///상하 움직임
                ///아래 끝에 닿았는데 계속 위쪽으로 스크롤할때
                if (currentY < prevY && Math.abs(binding.layoutMap.getY() + (currentY - prevY)) >= Math.abs(differenceHegight)) {
                    binding.layoutMap.setY(-differenceHegight);
                } else {
                    ///왼쪽으로 움직일 경우
                    if ((mapHeight > mapOutSideHeight)
                            && currentY < prevY) {
                        binding.layoutMap.setY((int) (Math.abs(binding.layoutMap.getY() + (currentY - prevY)) > Math.abs(differenceHegight) ? -differenceHegight : binding.layoutMap.getY() + (currentY - prevY)));
                    }
                }
                ///위 끝에 닿았는데 계속 아래쪽으로 스크롤할때
                if (currentY > prevY && binding.layoutMap.getY() >= 0) {
                    binding.layoutMap.setY(0);
                } else {

                    ///오른쪽으로 움직일 경우
                    if ((mapHeight > mapOutSideHeight)
                            && currentY > prevY) {
                        binding.layoutMap.setY((int) (binding.layoutMap.getY() > 0 ? 0 : binding.layoutMap.getY() + (currentY - prevY)));
                    }
                }


                if (binding.layoutMiniMapOutSide.getVisibility() == View.VISIBLE) {
                    LogUtil.d("테이블 맵 이동 이벤트 이동 절대값 layoutMap.getX() : " + Math.abs(binding.layoutMap.getX()) + " / differenceWidth : " + differenceWidth + " / 이동 절대값 layoutMap.getY() : " + Math.abs(binding.layoutMap.getY()) + " / differenceWidth : " + differenceHegight);

                    double ratioMoveX = Math.abs(binding.layoutMap.getX()) / (differenceWidth * 1.0);
                    int miniMapX = (int) Math.abs((binding.viewMiniMap.getMeasuredWidth() - (binding.layoutMiniMapOutSide.getMeasuredWidth())) * ratioMoveX);
                    binding.viewMiniMap.setX(miniMapX);


                    double ratioMoveY = Math.abs(binding.layoutMap.getY()) / (differenceHegight * 1.0);
                    int miniMapY = (int) Math.abs((binding.viewMiniMap.getMeasuredHeight() - (binding.layoutMiniMapOutSide.getMeasuredHeight())) * ratioMoveY);
                    binding.viewMiniMap.setY(miniMapY);
                }


            }
//            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
//                return false;
//            }


            return true;
        }
    };


    private void initMapItem(String hallKey) {

        initVariables();

        this.binding.layoutMap.setOnTouchListener(dragListener);
        this.binding.layoutMapOutSide.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                LogUtil.d("맵 사이즈 : " + binding.layoutMapOutSide.getMeasuredWidth());
                if (binding.layoutMapOutSide.getMeasuredWidth() > 2 && binding.layoutMap.getMeasuredWidth() > 2) {
                    binding.layoutMapOutSide.getViewTreeObserver().removeOnGlobalLayoutListener(this);


                    ///미니맵 바깥 사이즈 조절
                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) binding.layoutMiniMapOutSide.getLayoutParams();
                    lp.width = Utils.dpToPx(getContext(), MapDialog.MiniMapWidth);
                    lp.height = (int) (lp.width * (binding.layoutMapOutSide.getMeasuredHeight() / (binding.layoutMapOutSide.getMeasuredWidth() * 1.0)));
                    binding.layoutMiniMapOutSide.setLayoutParams(lp);


                }


            }
        });
        this.binding.layoutMap.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                LogUtil.d("맵 사이즈 : " + binding.layoutMap.getMeasuredWidth());
                if (binding.layoutMap.getMeasuredWidth() > 2 && binding.layoutMapOutSide.getMeasuredWidth() > 2) {
                    binding.layoutMap.getViewTreeObserver().removeOnGlobalLayoutListener(this);


                    ///미니맵 뷰 여부. 1%씩만 보정값을 더 줘보자
                    if (binding.layoutMap.getMeasuredWidth() <= binding.layoutMapOutSide.getMeasuredWidth() * 1.01
                            && binding.layoutMap.getMeasuredHeight() <= binding.layoutMapOutSide.getMeasuredHeight() * 1.01) {
                        binding.layoutMiniMapOutSide.setVisibility(View.GONE);
                    } else {
                        binding.layoutMiniMapOutSide.setVisibility(View.VISIBLE);

                        ///미니맵 사이즈 조절
                        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) binding.viewMiniMap.getLayoutParams();
                        int layoutWidthMapOutSide = binding.layoutMapOutSide.getMeasuredWidth();
                        int layoutWidthMap = binding.layoutMap.getMeasuredWidth();

                        int layoutHeightMapOutSide = binding.layoutMapOutSide.getMeasuredHeight();
                        int layoutHeightMap = binding.layoutMap.getMeasuredHeight();
                        lp.width = Utils.dpToPx(getContext(), MapDialog.MiniMapWidth);
                        lp.height = (int) (lp.width * (binding.layoutMapOutSide.getMeasuredHeight() / (binding.layoutMapOutSide.getMeasuredWidth() * 1.0)));


                        ///박스 리사이징
                        lp.width = (int) (lp.width * ((layoutWidthMap > layoutWidthMapOutSide ? (layoutWidthMapOutSide / (layoutWidthMap * 1.0)) : 1.0)));
                        lp.height = (int) (lp.height * ((layoutHeightMap > layoutHeightMapOutSide ? (layoutHeightMapOutSide / (layoutHeightMap * 1.0)) : 1.0)));
                        binding.viewMiniMap.setLayoutParams(lp);

                        goSelectedTablePosition(lp.width, Utils.dpToPx(getContext(), MapDialog.MiniMapWidth), lp.height, ((int) (lp.width * (binding.layoutMapOutSide.getMeasuredHeight() / (binding.layoutMapOutSide.getMeasuredWidth() * 1.0)))));
                    }


                }


            }
        });


        if (TabletSettingUtils.getInstacne().getTableMaps() != null && TabletSettingUtils.getInstacne().getTableMaps().size() > 0) {

            LogUtil.d("테이블 초기화 값 확인중 hallName: " + this.binding.getSelectedTableHallName());
            LogUtil.d("테이블 초기화 값 확인중 tableNo: " + this.binding.getSelectedItem());
            if (hallKey != null) {
                ////1. 선택된 항목이 있는경우
                selectedHallTableList = TabletSettingUtils.getInstacne().getConvertedTableMaps().get(hallKey);

            } else {
                ////2. 완전 처음 접근
                selectedHallTableList = TabletSettingUtils.getInstacne().getTableMaps().get(0).talbeMapJson;
                this.binding.setSelectedTableHallName(TabletSettingUtils.getInstacne().getTableMaps().get(0).hallName);
            }


            ///0. 맵 초기화
            clearAllTableItems();

            ///1. 홀 구분 버튼 추가

            ///2. 하위 테이블 표현
            int mapHeight = 0;
            int mapWidth = 0;
            int maxX = 0;
            int maxY = 0;


            for (TableMapItem item : selectedHallTableList) {
                maxX = (item.x > maxX) ? item.x : maxX;
                maxY = (item.y > maxY) ? item.y : maxY;
            }

            FrameLayout.LayoutParams mapLP = (FrameLayout.LayoutParams) this.binding.layoutMap.getLayoutParams();
            mapLP.width = Utils.dpToPx(getContext(), (int) (maxX + (maxX / 50 * (MapDialog.ConvertedBoxSize - 50))) + width + (2 * margin));
            mapLP.height = Utils.dpToPx(getContext(), (int) (maxY + (maxY / 50 * (MapDialog.ConvertedBoxSize - 50))) + width + (2 * margin));
            LogUtil.d("테이블 맵 전체 가로 : " + mapLP.width);
            LogUtil.d("테이블 맵 전체 세로 : " + mapLP.height);
            this.binding.layoutMap.setLayoutParams(mapLP);
            this.binding.layoutMap.setX(0);
            this.binding.layoutMap.setY(0);


            for (TableMapItem item : selectedHallTableList) {

                com.cdef.hoeat.view.customView.TableMapItem button = new com.cdef.hoeat.view.customView.TableMapItem(getContext());
                button.setOnTouchListener(dragListener);
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(width, width);
                lp.addRule(RelativeLayout.ALIGN_PARENT_TOP);
                lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);

                lp.setMargins(Utils.dpToPx(getContext(), ((int) ((item.x + (item.x / 50 * (MapDialog.ConvertedBoxSize - 50)))))) + margin, Utils.dpToPx(getContext(), (int) (item.y + (item.y / 50 * (MapDialog.ConvertedBoxSize - 50)))) + margin, margin, margin);
                button.setLayoutParams(lp);
                button.setData(item);
                button.setMODE(this.binding.getMode());
                button.setOnClickListener(view -> {
                    if (!item.tNo.equals(TabletSettingUtils.getInstacne().getTableNo())) {
                        selectionChange(view);
                        binding.setSelectedTableHallName((String) getSelectedHallButton().getTag());
                        binding.setSelectedItem(((com.cdef.hoeat.view.customView.TableMapItem) view).getData());
                    }
                });

                if ((this.binding.getSelectedItem() != null
                        && this.binding.getSelectedItem().tNo.equals(item.tNo))
                        && this.binding.getSelectedTableHallName() != null
                        && getSelectedHallButton() != null
                        && getSelectedHallButton().getTag().equals(this.binding.getSelectedTableHallName())) {
                    button.setSelected(true);
                }

                this.binding.layoutMap.addView(button);
            }

        } else {
            Toast.makeText(getContext(), "테이블 맵 정보가 없습니다.", Toast.LENGTH_LONG).show();
            dismiss();
        }
        setTableStatusByUsingQueue();
    }

    /**
     * 선택된 맵이 있을경우 위치 바로잡기
     */
    public void goSelectedTablePosition(int miniMapWidth, int miniMapOutSideWidth, int miniMapHeight, int miniMapOutSideHeight) {
        ///선택된 테이블이 있으면서, 지금보는 홀이 선택한 테이블의 홀과 일치할 경우만
//        if (this.binding.getSelectedItem() != null && this.binding.getSelectedTableHallName() != null && getSelectedHallButton() != null && getSelectedHallButton().getTag().equals(this.binding.getSelectedTableHallName())) {
//            LogUtil.d("테이블 맵 goSelectedTablePosition");
//            LogUtil.d("테이블 맵 goSelectedTablePosition 의 miniMapWidth : " + miniMapWidth);
//            LogUtil.d("테이블 맵 goSelectedTablePosition 의 miniMapOutSideWidth : " + miniMapOutSideWidth);
//            LogUtil.d("테이블 맵 goSelectedTablePosition 의 miniMapHeight : " + miniMapHeight);
//            LogUtil.d("테이블 맵 goSelectedTablePosition 의 miniMapOutSideHeight : " + miniMapOutSideHeight);
//
//            ///미니맵 뷰 여부. 1%씩만 보정값을 더 줘보자
//            if (binding.layoutMap.getMeasuredWidth() <= binding.layoutMapOutSide.getMeasuredWidth() * 1.01
//                    && binding.layoutMap.getMeasuredHeight() <= binding.layoutMapOutSide.getMeasuredHeight() * 1.01) {
//            } else {
//                LogUtil.d("테이블 맵 goSelectedTablePosition 위치조정중");
//
//                ///스크롤이 존재하는 경우임. 이때만 포지션을 옮겨야 함
//
//                int convertedX = Utils.dpToPx(getContext(), (int) (this.binding.getSelectedItem().x + (this.binding.getSelectedItem().x / 50 * (MapDialog.ConvertedBoxSize - 50))));
//                int convertedY = Utils.dpToPx(getContext(), (int) (this.binding.getSelectedItem().y + (this.binding.getSelectedItem().y / 50 * (MapDialog.ConvertedBoxSize - 50))));
//
//
//                int selectedBoxCenterX = (int) (convertedX + margin + width / 2.0);
//                int selectedBoxCenterY = (int) (convertedY + margin + width / 2.0);
//
//                ///화면의 중점에 둘수 있는지 체크.
//                ///X부터. x에 outSideWidth/2.0 을 더해서 map의 끝을 넘지 않는지 체크한다.
//                if (((int) (selectedBoxCenterX + this.binding.layoutMapOutSide.getMeasuredWidth() / 2.0)) < this.binding.layoutMap.getMeasuredWidth()) {
//
//                    double widthRatio = selectedBoxCenterX / (this.binding.layoutMap.getMeasuredWidth() * 1.0);
//                    this.binding.layoutMap.setX(-Math.abs((int) ((this.binding.layoutMap.getMeasuredWidth() - this.binding.layoutMapOutSide.getMeasuredWidth()) * widthRatio)));
//                    this.binding.viewMiniMap.setX(Math.abs((int) ((miniMapWidth - miniMapOutSideWidth) *
//                            Math.abs((this.binding.layoutMap.getX() / (this.binding.layoutMap.getMeasuredWidth() - this.binding.layoutMapOutSide.getMeasuredWidth()) * 1.0))
//                    )));
//                    ///근데 혹시 이동한 X 위치가 박스 반층보다 작으면 그냥 이동시키지 말것.
//                    if (Math.abs(this.binding.layoutMap.getX()) < (width + margin)) {
//                        this.binding.layoutMap.setX(0);
//                        this.binding.viewMiniMap.setX(0);
//
//                    }
//
//
//                } else {
//                    this.binding.layoutMap.setX(-Math.abs((int) (this.binding.layoutMap.getMeasuredWidth() - this.binding.layoutMapOutSide.getMeasuredWidth())));
//                    this.binding.viewMiniMap.setX(Math.abs((int) (miniMapWidth - miniMapOutSideWidth)));
//
//                }
//
//
//                ///Y부터. y에 outSideHeight/2.0 을 더해서 map의 끝을 넘지 않는지 체크한다.
//                if (((int) (selectedBoxCenterY + this.binding.layoutMapOutSide.getMeasuredHeight() / 2.0)) < this.binding.layoutMap.getMeasuredHeight()) {
//                    LogUtil.d("테이블 맵 goSelectedTablePosition 위치조정중 Y축 1");
//
//                    double heightRatio = selectedBoxCenterY / (this.binding.layoutMap.getMeasuredHeight() * 1.0);
//                    this.binding.layoutMap.setY(-Math.abs((int) ((this.binding.layoutMap.getMeasuredHeight() - this.binding.layoutMapOutSide.getMeasuredHeight()) * heightRatio)));
//                    this.binding.viewMiniMap.setY(Math.abs((int) ((miniMapHeight - miniMapOutSideHeight)
//                            * Math.abs((this.binding.layoutMap.getY() / (this.binding.layoutMap.getMeasuredHeight() - this.binding.layoutMapOutSide.getMeasuredHeight()) * 1.0)))));
//
//                    LogUtil.d("테이블 맵 goSelectedTablePosition 위치조정중 Y축 : " + this.binding.viewMiniMap.getY());
//                    LogUtil.d("테이블 맵 goSelectedTablePosition 위치조정중 miniMapHeight : " + miniMapHeight);
//                    LogUtil.d("테이블 맵 goSelectedTablePosition 위치조정중 miniMapOutSideHeight : " + miniMapOutSideHeight);
//
//
//                    ///근데 혹시 이동한 Y 위치가 박스 반층보다 작으면 그냥 이동시키지 말것.
//                    if (Math.abs(this.binding.layoutMap.getY()) < (width + margin)) {
//                        this.binding.layoutMap.setY(0);
//                        this.binding.viewMiniMap.setY(0);
//                    }
//
//
//                } else {
//                    LogUtil.d("테이블 맵 goSelectedTablePosition 위치조정중 Y축 2");
//                    this.binding.layoutMap.setY(-Math.abs((int) (this.binding.layoutMap.getMeasuredHeight() - this.binding.layoutMapOutSide.getMeasuredHeight())));
//                    this.binding.viewMiniMap.setY(Math.abs((int) (miniMapHeight - miniMapOutSideHeight)));
//                    LogUtil.d("테이블 맵 goSelectedTablePosition 위치조정중 Y축 : " + this.binding.viewMiniMap.getY());
//
//                }
//
//            }
//
//        } else {
        this.binding.layoutMap.setY(0);
        this.binding.viewMiniMap.setY(0);
//        }
    }


    public MapDialog init(Context context) {
        ///클린에 대한 메소드가 들어가야함

        if (this.isShowing())
            this.dismiss();
        this.mContext = context;

        return this;
    }

    public MapDialog setListener(onSelectListener listener) {
        this.submitListener = listener;
        return this;
    }

    public onSelectListener submitListener;

    public interface onSelectListener {
        void submit(String tableHallName, TableMapItem selectedTable);

    }

    public MapDialog onDismissListener(OnDismissListener listener) {
        this.setOnDismissListener(listener);
        return this;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        MapDialog.mInstance = null;
    }

    public void complete() {
        if (!((Activity) this.mContext).isFinishing())
            this.show();
    }

    public MapDialog setSelectedTableNo(String hallName, String tableNo) {


        if (hallName == null || tableNo == null) {
            this.binding.setSelectedTableHallName(null);
            this.binding.setSelectedItem(null);

        } else {
            this.binding.setSelectedTableHallName(hallName);
            ArrayList<TableMapItem> tmp = TabletSettingUtils.getInstacne().getConvertedTableMaps().get(hallName);
            TableMapItem selectdTable = null;
            for (TableMapItem item : tmp) {
                if (item.tNo.equals(tableNo)) {
                    selectdTable = item;
                    break;
                }
            }
            this.binding.setSelectedItem(selectdTable);
        }

        initHallButtons();  /// 홀 버튼 먼저 생성


        ///처음 hall name 지정방법
        if (getSelectedHallButton(this.binding.getSelectedTableHallName()) != null) {
            getSelectedHallButton(this.binding.getSelectedTableHallName()).performClick();
        } else {
            if (getDefaultHallButton() != null)
                getDefaultHallButton().performClick();
            else
                Toast.makeText(getContext(), "테이블 정보가 없습니다. 다시 로그인후 시도해주세요.", Toast.LENGTH_SHORT).show();
        }


        return this;
    }


    public void selectionChange(View view) {
        if (this.binding != null && this.binding.layoutMap != null) {
            for (int index = 0; index < this.binding.layoutMap.getChildCount(); index++) {
                this.binding.layoutMap.getChildAt(index).setSelected(view.equals(this.binding.layoutMap.getChildAt(index)));
            }
        }
    }

    public void submit() {
        if (this.binding.getSelectedTableHallName() != null && this.binding.getSelectedItem() != null)
            submitListener.submit(this.binding.getSelectedTableHallName(), this.binding.getSelectedItem());
        else
            Toast.makeText(getContext(), "테이블을 선택해주세요.", Toast.LENGTH_SHORT).show();
    }

    public void close() {
        if (TabletSettingUtils.getInstacne().getSelectedTableNo() != null)
            dismiss();
        else
            Toast.makeText(getContext(), "테이블을 선택해주세요.", Toast.LENGTH_SHORT).show();

    }


    private synchronized void setFBTableStatus(String tableNo, TableInfo tableInfo) {
        for (TableMapItem item : this.selectedHallTableList) {
            if (item.tNo != null && item.tNo.equals(tableNo)) {
                item.tableInfo = tableInfo;
                setFBTableStatusView(tableNo, tableInfo);
                return;
            }
        }
    }

    /**
     * 큐로부터 가져온 정보를 기반으로 테이블의 인원을 셋팅합니다.
     **/
    private synchronized void setTableStatusByUsingQueue() {
        for (TableMapItem item : this.selectedHallTableList) {
            LogUtil.d("setTableStatusByUsingQueue item : " + (TabletSettingUtils.getInstacne().getTableInfoList().get(item.tNo)));

            if (item.tNo != null && (TabletSettingUtils.getInstacne().getTableInfoList().get(item.tNo)) != null) {
                setTableStatusByUsingQueueView(item.tNo, (TabletSettingUtils.getInstacne().getTableInfoList().get(item.tNo)));
            }
        }
    }

    private synchronized void setFBTableStatusView(String tableNo, TableInfo tableInfo) {
        for (int index = 0; index < this.binding.layoutMap.getChildCount(); index++) {
            if (((com.cdef.hoeat.view.customView.TableMapItem) this.binding.layoutMap.getChildAt(index)).getData().tNo.equals(tableNo)) {
//                ((com.cdef.hoeat.View.CustomView.TableMapItem) this.binding.layoutMap.getChildAt(index)).updateGameState(tableInfo.gameId);
                return;
            }
        }
    }

    /**
     * 큐로부터 가져온 정보를 기반으로 테이블의 인원을 셋팅합니다.
     **/
    private synchronized void setTableStatusByUsingQueueView(String tableNo, TableMapItem tableInfo) {
        for (int index = 0; index < this.binding.layoutMap.getChildCount(); index++) {
            if (((com.cdef.hoeat.view.customView.TableMapItem) this.binding.layoutMap.getChildAt(index)).getData().tNo.equals(tableNo)) {
                if (tableInfo != null && tableInfo.tableInfo != null && (tableInfo.tableInfo.members.women != 0 || tableInfo.tableInfo.members.men != 0)) {
                    LogUtil.d(tableNo + "큐 기반 테이블의 인원 정보 : " + (tableInfo.tableInfo.members.women + tableInfo.tableInfo.members.men));
                }
                ((com.cdef.hoeat.view.customView.TableMapItem) this.binding.layoutMap.getChildAt(index)).setData(tableInfo);
                ((com.cdef.hoeat.view.customView.TableMapItem) this.binding.layoutMap.getChildAt(index)).refresh();
            }
        }
    }


}
