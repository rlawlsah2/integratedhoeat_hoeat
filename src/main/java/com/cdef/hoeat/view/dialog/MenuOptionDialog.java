package com.cdef.hoeat.view.dialog;

import android.app.Dialog;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.cdef.commonmodule.dataModel.MenuSideGroupItemData;
import com.cdef.commonmodule.dataModel.MenuSideOptionItemData;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.hoeat.R;
import com.cdef.hoeat.utils.Utils;
import com.cdef.hoeat.view.customView.menuOption.MenuDetailSetOptionListItem;
import com.cdef.hoeat.databinding.DialogMenuOptionBinding;

import java.util.ArrayList;

/**
 * Created by kimjinmo on 2017. 1. 2..
 */

public class MenuOptionDialog extends Dialog {

    ////멤버변수
    private Context mContext;
    private static MenuOptionDialog mInstance;
    ArrayList<MenuSideOptionItemData> selectedItemList;

    ////

    DialogMenuOptionBinding binding;

    public static MenuOptionDialog getmInstanceWithoutInit() {
        return MenuOptionDialog.mInstance;
    }

    public void setLoadingState() {
        super.dismiss();
    }

    public static MenuOptionDialog getInstance(Context context) {
        if (MenuOptionDialog.mInstance != null) {
            MenuOptionDialog.mInstance.dismiss();
        }

        if (MenuOptionDialog.mInstance == null) {
            MenuOptionDialog.mInstance = new MenuOptionDialog(context);
        }

        MenuOptionDialog.mInstance.init(context);
        return MenuOptionDialog.mInstance;
    }

    public static void clearInstance() {
        MenuOptionDialog.mInstance = null;
    }


    private MenuOptionDialog(Context context) {
        super(context);
//        this.mContext = context;
        this.mContext = context;
        registerHandler();
    }

    private MenuOptionDialog(Context context, int themeResId) {
        super(context, themeResId);
//        this.mContext = context;
        registerHandler();
    }

    private MenuOptionDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
//        this.mContext = context;
        registerHandler();
    }


    @Override
    public void onBackPressed() {
        LogUtil.d("다이얼로그 onBackPressed : ");
        super.onBackPressed();
    }

    /**
     * 초기 뷰 셋팅을 위한 부분
     **/
    private void registerHandler() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_menu_option, null, false);
        this.binding.setDialog(this);
        setContentView(binding.getRoot());


        ///2. 하위 뷰 셋팅
        Window window = this.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCanceledOnTouchOutside(false);

    }

    public MenuOptionDialog setOnDismissListener_(@Nullable OnDismissListener listener) {
        super.setOnDismissListener(listener);
        return this;
    }

    public MenuOptionDialog init(Context context) {
        ///클린에 대한 메소드가 들어가야함

        if (this.isShowing())
            this.dismiss();
        this.mContext = context;
        return this;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        clearAll();
        System.gc();
    }

    private void clearAll() {
        this.selectedItemList = null;
        this.submitListener = null;
        this.binding.layoutOptions.removeAllViews();
        this.mContext = null;
        MenuOptionDialog.mInstance = null;

    }

    @Override
    public void show() {
        super.show();
        LogUtil.d("옵션 선택창의 인스턴스는 뭐지 : " + this);
        this.binding.setSelectedOptionsStr(getSelectedItemListStr());
    }

    public MenuOptionDialog setInitSelectedItem(ArrayList<MenuSideOptionItemData> selectedItemList) {
        this.selectedItemList = selectedItemList;
        return this;
    }


    public MenuOptionDialog setGroupItem(MenuSideGroupItemData menuSideGroupItemData) {
        this.binding.setGroupItem(menuSideGroupItemData);
        this.binding.layoutOptions.removeAllViews();
        for (MenuSideOptionItemData optionItemData : menuSideGroupItemData.sideOptions) {
            this.binding.layoutOptions.addView(
                    MenuDetailSetOptionListItem.getInstance(getContext())
                            .setData((getSelectedItem(optionItemData)) == null ? optionItemData : getSelectedItem(optionItemData))
                            ////처음 접근할때 defaultItem==1 이면 체크, 또는 선택된 항목이면 체크
                            .setInitSelection(((this.selectedItemList.size() == 0 && optionItemData.defaultItem == 1) || isSelectedItem(optionItemData)))
                            .setItemSelectionListener(view -> {

                                ///1개 항목만 선택할 수 있는지 확인. maxGroupItem의 갯수가 0이거나 선택된 갯수보다 크면 계속. 아니면 중지
                                if (this.binding.getGroupItem().maxGroupItem == 0 || (this.binding.getGroupItem().maxGroupItem > selectedItemCount() && this.binding.getGroupItem().maxGroupItem != 1)) {
                                    view.setSelection(view);
                                } else if (this.binding.getGroupItem().maxGroupItem == 1) {
                                    //다 지우고 진행
                                    if (!view.isSelected()) {
                                        deSelectAll();
                                        view.setSelection(view);
                                    } else {
                                        deSelectAll();
                                    }
                                } else {
                                    Toast.makeText(getContext(), "최대 " + this.binding.getGroupItem().maxGroupItem + "종류까지만 선택가능합니다.", Toast.LENGTH_SHORT).show();
                                }

                                this.binding.setSelectedOptionsStr(getSelectedItemListStr());


                            })
                            .setItemChangeQuantityListener(((view, isMinus) -> {

                                if (isMinus) {
                                    view.changeQuantity(view, isMinus); //마이너스면 딱히 상관없음
                                } else {
                                    ///1개 항목만 선택할 수 있는지 확인. maxGroupItem의 갯수가 0이거나 선택된 갯수보다 크면 계속. 아니면 중지
                                    if (this.binding.getGroupItem().maxGroupItem == 0 || (this.binding.getGroupItem().maxGroupItem > selectedItemCount() && this.binding.getGroupItem().maxGroupItem != 1)) {
                                        view.changeQuantity(view, isMinus);
                                    } else if (this.binding.getGroupItem().maxGroupItem == 1) {
                                        //다 지우고 진행
                                        if (!view.isSelected()) {
                                            deSelectAll();
                                            view.changeQuantity(view, isMinus);
                                        } else {
                                            view.changeQuantity(view, isMinus);
                                        }
                                    } else {
                                        Toast.makeText(getContext(), "최대 " + this.binding.getGroupItem().maxGroupItem + "종류까지만 선택가능합니다.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                                this.binding.setSelectedOptionsStr(getSelectedItemListStr());

                            }))
            );
        }

        return this;
    }

    /**
     * 선택된 아이템 갯수 가져오는 함수
     */
    public int selectedItemCount() {
        int count = 0;
        for (int i = 0; i < this.binding.layoutOptions.getChildCount(); i++) {
            if (((MenuDetailSetOptionListItem) this.binding.layoutOptions.getChildAt(i)).isSelected())
                count++;
        }
        return count;
    }

    /**
     * 모든 아이템 선택 해제
     */
    public void deSelectAll() {
        for (int i = 0; i < this.binding.layoutOptions.getChildCount(); i++) {
            ((MenuDetailSetOptionListItem) this.binding.layoutOptions.getChildAt(i)).setSelected(false);
        }
    }

    /**
     * 선택된 아이템 목록 가져오기
     */
    public ArrayList<MenuSideOptionItemData> getSelectedItemList() {

        if (this.selectedItemList == null)
            this.selectedItemList = new ArrayList<>();
        this.selectedItemList.clear();


        for (int i = 0; i < this.binding.layoutOptions.getChildCount(); i++) {
            if ((this.binding.layoutOptions.getChildAt(i)).isSelected()) {
                try {
                    this.selectedItemList.add((MenuSideOptionItemData) ((MenuDetailSetOptionListItem) this.binding.layoutOptions.getChildAt(i)).getItem().getClone());
                } catch (CloneNotSupportedException e) {

                }
            }
        }

        return this.selectedItemList;
    }

    /**
     * 선택된 아이템을 문자열로 표시
     **/
    public String getSelectedItemListStr() {
        getSelectedItemList();
        return Utils.getSelectedItemListStr(selectedItemList);
    }

    /**
     * 해당 아이템이 선택항목에 포함된 항목인지 체크
     **/
    public boolean isSelectedItem(MenuSideOptionItemData input) {
        boolean tmp = false;
        if (this.selectedItemList == null)
            this.selectedItemList = new ArrayList<>();
        for (MenuSideOptionItemData item : this.selectedItemList) {
            if (item.CMDTCD.equals(input.CMDTCD)) {
                tmp = true;
                break;
            }
        }
        return tmp;
    }

    /**
     * 해당 아이템이 선택항목에 포함되었을 경우 해당 선택된 항목으로 넘겨줌. 그래야 수량같은걸 다 맞춤
     **/
    public MenuSideOptionItemData getSelectedItem(MenuSideOptionItemData input) {
        if (this.selectedItemList == null)
            this.selectedItemList = new ArrayList<>();
        for (MenuSideOptionItemData item : this.selectedItemList) {
            if (item.CMDTCD.equals(input.CMDTCD)) {
                return item;
            }
        }
        return null;
    }


    public void goSubmit() {
        if (this.submitListener != null) {
            this.submitListener.submit(this.selectedItemList);
            dismiss();
        }
    }

    public void complete() {
        this.show();
    }

    SubmitListener submitListener;

    public MenuOptionDialog setSubmitListener(SubmitListener submitListener) {
        this.submitListener = submitListener;
        return this;
    }

    public interface SubmitListener {
        void submit(ArrayList<MenuSideOptionItemData> list);
    }


}
