package com.cdef.hoeat.view.viewHolder;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.cdef.hoeat.databinding.AdapterPrivateChatListBinding;

/**
 * Created by kimjinmo on 2018. 4. 30..
 * 1:1  보이는 화면 처리
 */


public class PrivateChatListAdapterViewHolder extends RecyclerView.ViewHolder {
    public AdapterPrivateChatListBinding binding;
    public PrivateChatListAdapterViewHolder(View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
    }
}
