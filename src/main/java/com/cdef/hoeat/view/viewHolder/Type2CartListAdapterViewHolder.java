package com.cdef.hoeat.view.viewHolder;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.cdef.hoeat.databinding.Type2AdapterCartListBinding;

/**
 * Created by kimjinmo on 2018. 4. 30..
 */


public class Type2CartListAdapterViewHolder extends RecyclerView.ViewHolder {


    public Type2AdapterCartListBinding binding;

    public Type2CartListAdapterViewHolder(View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
    }
}
