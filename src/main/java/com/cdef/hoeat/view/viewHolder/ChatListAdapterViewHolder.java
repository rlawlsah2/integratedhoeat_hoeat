package com.cdef.hoeat.view.viewHolder;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.cdef.hoeat.databinding.AdapterChatBinding;

/**
 * Created by kimjinmo on 2018. 4. 30..
 * 채팅 보이는 화면 처리
 */


public class ChatListAdapterViewHolder extends RecyclerView.ViewHolder {


    public AdapterChatBinding binding;

    public ChatListAdapterViewHolder(View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
    }
}
