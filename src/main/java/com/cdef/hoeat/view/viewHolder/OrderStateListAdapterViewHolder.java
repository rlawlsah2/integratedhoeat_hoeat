package com.cdef.hoeat.view.viewHolder;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.cdef.hoeat.databinding.AdapterOrderStateListBinding;

/**
 * Created by kimjinmo on 2018. 4. 30..
 */

public class OrderStateListAdapterViewHolder extends RecyclerView.ViewHolder {


    public AdapterOrderStateListBinding binding;

    public OrderStateListAdapterViewHolder(View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
    }
}