package com.cdef.hoeat.view.viewHolder;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.cdef.hoeat.databinding.AdapterCartListBinding;

/**
 * Created by kimjinmo on 2018. 4. 30..
 */


public class CartListAdapterViewHolder extends RecyclerView.ViewHolder {


    public AdapterCartListBinding binding;

    public CartListAdapterViewHolder(View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
    }
}
