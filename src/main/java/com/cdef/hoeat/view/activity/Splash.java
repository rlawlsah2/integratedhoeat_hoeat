package com.cdef.hoeat.view.activity;

import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import androidx.annotation.Nullable;

import android.widget.Toast;

import com.cdef.commonmodule.utils.ConstantsUtils;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.PreferenceUtils;
import com.cdef.commonmodule.utils.typekit.TypekitContextWrapper;
import com.cdef.hoeat.OrderAppApplication;
import com.cdef.hoeat.R;
import com.cdef.hoeat.utils.DefaultExceptionHandler;
import com.cdef.hoeat.utils.NavigationUtils;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.cdef.hoeat.databinding.ActivitySplashBinding;
import com.cdef.hoeat.utils.Utils;
import com.cdef.hoeat.view.fragment.LoginFragment;
import com.cdef.hoeat.view.type2.activity.MainActivity2;
import com.cdef.hoeat.viewModel.BillViewModel;
import com.cdef.hoeat.viewModel.LoginViewModel;
import com.cdef.hoeat.viewModel.OrderViewModel;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class Splash extends BaseActivity {


    ///키보드 view 상태를 체크하기위한 변수, 어디서든 접근하기 위해  static으로 사용

    Date lastTouchTime = null;
    ActivitySplashBinding binding = null;
    LoginViewModel loginViewModel;

    OrderViewModel orderViewModel;
    BillViewModel billViewModel;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LogUtil.d("주문앱 intent 체크 onNewIntent 시작");

        if (intent != null) {

            String apiKey = intent.getStringExtra("apiKey");
            String tableNo = intent.getStringExtra("tableNo");
            String branchName = intent.getStringExtra("branchName");

            LogUtil.d("주문앱 intent 체크 onNewIntent : " + apiKey);
            LogUtil.d("주문앱 intent 체크 onNewIntent : " + tableNo);
            LogUtil.d("주문앱 intent 체크 onNewIntent : " + branchName);

            if (apiKey != null || tableNo != null) {
                if (!tableNo.equals(TabletSettingUtils.getInstacne().getTableNo())) {
                    Toast.makeText(getApplicationContext(), "테이블 번호가 변경되어 다시 로그인합니다.",
                            Toast.LENGTH_SHORT).show();


                    finish();
                    Intent newIntent = new Intent(getApplicationContext(), Splash.class);
                    newIntent.putExtra("apiKey", apiKey);
                    newIntent.putExtra("tableNo", tableNo);
                    newIntent.putExtra("branchName", branchName);

                    newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(newIntent);
                    return;

//                    throw new NullPointerException("tableNo Changed");
//                    Bundle bundle = new Bundle();
//                    bundle.putString("apiKey", apiKey);
//                    bundle.putString("tableNo", tableNo);
//                    NavigationUtils.detailOpenFragment(getSupportFragmentManager(), R.id
//                    .layoutFragment, LoginFragment.class.getSimpleName(), bundle);
//                    return;
                }

            }
        }

        if (TabletSettingUtils.getInstacne().getBranchInfo() != null) {
            //이미 로그인을 한경우

            if (TabletSettingUtils.getInstacne().getBranchInfo().storeOption.appType == null
                    || TabletSettingUtils.getInstacne().getBranchInfo().storeOption.appType.equals(
                    "type1")) {
                Intent intent2 = new Intent(this, MainActivity.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent2);
            } else {

                Intent intent2 = new Intent(this, MainActivity2.class);
                intent2.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent2);
            }

        } else {
            //오류처리로 인해 돌아온경우엔 로그아웃부터
///터치시간 체크
            Intent getIntent = intent;
            if (getIntent != null) {
                String token = getIntent.getStringExtra("apiKey");
                String tableNo = getIntent.getStringExtra("tableNo");
                String branchName = getIntent.getStringExtra("branchName");

                if (token != null || tableNo != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString("apiKey", token);
                    bundle.putString("tableNo", tableNo);
                    bundle.putString("branchName", branchName);
                    NavigationUtils.detailOpenFragment(getSupportFragmentManager(),
                            R.id.layoutFragment, LoginFragment.class.getSimpleName(), bundle);
                    return;
                }
            }
            finish();
        }

    }

    @Override
    public void onBackPressed() {
        setUI();
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        this.binding.setActivity(this);
        OrderAppApplication.mContext = this;

        Utils.setVersion(this);


        ///앱에서 떨어진 Exception에 대해 자동 재시작 기능을 위해 추가한 코드
        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(this));

        this.loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        this.loginViewModel.initRepository(this);

        this.orderViewModel = ViewModelProviders.of(this).get(OrderViewModel.class);
        this.orderViewModel.initRepository(this);

        this.billViewModel = ViewModelProviders.of(this).get(BillViewModel.class);
        this.billViewModel.initRepository(this);

        this.setUI();
        this.updateVersionCode();

        this.lastTouchTime = Calendar.getInstance().getTime(); //마지막 터치 시간값을 초기화

        ///터치시간 체크
        Intent getIntent = getIntent();
        if (getIntent != null) {
            String token = getIntent.getStringExtra("apiKey");
            String tableNo = getIntent.getStringExtra("tableNo");
            String branchName = getIntent.getStringExtra("branchName");

            if (token != null || tableNo != null) {
                Bundle bundle = new Bundle();
                bundle.putString("apiKey", token);
                bundle.putString("tableNo", tableNo);
                bundle.putString("branchName", branchName);
                NavigationUtils.detailOpenFragment(getSupportFragmentManager(), R.id.layoutFragment,
                        LoginFragment.class.getSimpleName(), bundle);
                return;
            }
        }

        Toast.makeText(getApplicationContext(), "매니저를 통해 실행해주세요", Toast.LENGTH_SHORT).show();
        finish();


    }


    public void goMain() {


        if (TabletSettingUtils.getInstacne().getBranchInfo().storeOption.appType == null
                || TabletSettingUtils.getInstacne().getBranchInfo().storeOption.appType.equals(
                "type1")) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        } else {

//        finish();
            PreferenceUtils.addBooleanValuePref(this, ConstantsUtils.PREF_FILENAME_TUTORIAL,
                    ConstantsUtils.PREF_KEY_TUTORIAL, false);
            if (TabletSettingUtils.getInstacne().getOrderId() != null) {
                PreferenceUtils.addBooleanValuePref(this, ConstantsUtils.PREF_FILENAME_TUTORIAL,
                        ConstantsUtils.PREF_KEY_TUTORIAL, true);
            }

            Intent intent = new Intent(this, MainActivity2.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }


    }


    @Override
    protected void onDestroy() {
        if (this.compositeSubscription != null) {
            this.compositeSubscription.clear();
        }
        super.onDestroy();
    }

    /**
     * 버전코드
     **/
    public void updateVersionCode() {
        PackageInfo pi = null;
        //버전코드 등록
        try {
            pi = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (pi != null) {
            String versionCode = String.valueOf(pi.versionCode);
            String versionName = pi.versionName;
            this.binding.textVersion.setText(versionName + "(" + versionCode + ")");
        }
    }


}
