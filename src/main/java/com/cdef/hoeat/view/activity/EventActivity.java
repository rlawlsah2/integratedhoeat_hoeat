package com.cdef.hoeat.view.activity;


import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cdef.commonmodule.dataModel.MenuItemData;
import com.cdef.hoeat.R;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.cdef.hoeat.view.adapter.EventBannerListAdapter;
import com.cdef.hoeat.view.dialog.loadingDialog.LoadingDialog;
import com.cdef.hoeat.view.dialog.MessageDialogBuilder;
import com.cdef.hoeat.view.viewPager.EventViewPagerAdapter;
import com.cdef.hoeat.viewModel.ADViewModel;
import com.cdef.hoeat.viewModel.BillViewModel;
import com.cdef.hoeat.viewModel.OrderViewModel;
import com.cdef.hoeat.databinding.ActivityEventBinding;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.Unregistrar;
import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import io.reactivex.annotations.Nullable;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by kimjinmo on 2018. 3. 26..
 * <p>
 * 변경전 : 랜덤으로 1개 뽑아와서 보여줌
 * 변경후 : 전체 목록을 가져와서 슬라이딩으로 보여줘야 함
 */

public class EventActivity extends BaseActivity {

    ActivityEventBinding binding = null;
    RequestOptions options = null;
    Unregistrar unregistrar;
    ADViewModel adViewModel;
    EventBannerListAdapter eventBannerListAdapter;
    boolean isRandomBannerMode = false;
    boolean isKeyBoardShow = false;

    OrderViewModel orderViewModel;
    BillViewModel billViewModel;
    LoadingDialog loadingDialog;

    Random random = new Random();
    ///오토 스크롤링을 위해 필요함.
    CompositeSubscription scrollComSub = new CompositeSubscription();
    boolean autoScrollAvailable = true; ////false가 되는경우는 1. 스크롤링중일때, 2. 참여방법같은거 열람중일때,
    EventViewPagerAdapter eventViewPagerAdapter;
    Date lastTouchTime = null;
    Date nowTime = null;

    Context mContext;


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        ///마지막 터치 시간 갱신
//        this.lastTouchTime = Calendar.getInstance().getTime();

        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            this.autoScrollAvailable = false;
        } else if (ev.getAction() == MotionEvent.ACTION_UP) {
            this.autoScrollAvailable = true;
            this.lastTouchTime = Calendar.getInstance().getTime(); //마지막 터치 시간값을 초기화

        }

        if (ev.getPointerCount() > 1) {
            LogUtil.d("Multitouch detected!");
            ev.setAction(MotionEvent.ACTION_CANCEL);
            return super.dispatchTouchEvent(ev);
        }


        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Bundle bundle = getIntent().getExtras();
        ///광고 유형을 정함. 하나만 보내던가 목록으로 보여주던가
        if (bundle != null && (bundle.getInt("isRandomEvent") > 0)) {
            ////랜덤 광고로 변경할것.
            this.adViewModel.getAdList((TabletSettingUtils.getInstacne().getTableInfoData().men +
                            TabletSettingUtils.getInstacne().getTableInfoData().women)
                    , (TabletSettingUtils.getInstacne().getTableInfoData().men != 0 && TabletSettingUtils.getInstacne().getTableInfoData().women != 0 ? "both" :
                            (TabletSettingUtils.getInstacne().getTableInfoData().men != 0 && TabletSettingUtils.getInstacne().getTableInfoData().women == 0 ? "male" :
                                    (TabletSettingUtils.getInstacne().getTableInfoData().men == 0 && TabletSettingUtils.getInstacne().getTableInfoData().women != 0 ? "female" :
                                            "na")))
                    , "screen"
            );
//            this.adViewModel.getRandomBanner(TabletSettingUtils.getInstacne().getTableNo(), 0, "na", "screen");
            this.isRandomBannerMode = true;
            getIntent().removeExtra("isRandomEvent");
        } else {
            this.adViewModel.getAdList((TabletSettingUtils.getInstacne().getTableInfoData().men +
                            TabletSettingUtils.getInstacne().getTableInfoData().women)
                    , (TabletSettingUtils.getInstacne().getTableInfoData().men != 0 && TabletSettingUtils.getInstacne().getTableInfoData().women != 0 ? "both" :
                            (TabletSettingUtils.getInstacne().getTableInfoData().men != 0 && TabletSettingUtils.getInstacne().getTableInfoData().women == 0 ? "male" :
                                    (TabletSettingUtils.getInstacne().getTableInfoData().men == 0 && TabletSettingUtils.getInstacne().getTableInfoData().women != 0 ? "female" :
                                            "na")))
                    , "event"
            );
            this.isRandomBannerMode = false;
        }
        this.binding.setIsRandomBannerMode(this.isRandomBannerMode);

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContext = this;

        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_event);
        this.binding.setActivity(this);

        this.binding.setEventCount(0);
        this.binding.setCurrentEventPosition(0);


        options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(false)
                .override(Utils.getWindosWidth(this), Utils.getWindosHeight(this));


        this.loadingDialog = new LoadingDialog(this, "처리중..");

        ///recyclerview와 adapter 초기화
        this.eventBannerListAdapter = new EventBannerListAdapter(this, Glide.with(this));
        this.eventBannerListAdapter.setListener((v, itemData) -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                this.binding.imageEvent.setBackgroundColor(getColor(R.color.black));
            } else {
                this.binding.imageEvent.setBackgroundColor(getResources().getColor(R.color.black));
            }
            Glide.with(this).clear(this.binding.imageEvent);

            try {
                this.adViewModel.mSelectedBanner.setValue(itemData);


                if (itemData.files.size() > 0) {
                    Glide.with(this).load(itemData.files.get(
                            random.nextInt(itemData.files.size())
                    ).fileUrl)
                            .apply(options)
                            .into(this.binding.imageEvent);
                }


                this.binding.setEventDetailADType(itemData.adType);

            } catch (Exception e) {

            }

            this.binding.setEventDetailFlag(true);
            this.binding.layoutTop.bringToFront();
            this.adViewModel.getBannerDetail(itemData.uuid, TabletSettingUtils.getInstacne().getTableNo(), 0, "na");
        });
        this.binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.adViewModel = ViewModelProviders.of(this).get(ADViewModel.class);
        this.orderViewModel = ViewModelProviders.of(this).get(OrderViewModel.class);
        this.orderViewModel.initRepository(this);
        this.billViewModel = ViewModelProviders.of(this).get(BillViewModel.class);
        this.billViewModel.initRepository(this);
        this.billViewModel.getOrderList(TabletSettingUtils.getInstacne().getTableNo());
        this.adViewModel.initRepository(this);

        ///랜덤배너 보기 보드일경우 바로 화면 전환해줘야 한다.
        this.adViewModel.randomBanner.observe(this, bannerDefaultData -> {

            try {

                if (bannerDefaultData == null) {
                    finish();
                    return;
                }

                this.adViewModel.mSelectedBanner.setValue(bannerDefaultData);


                if (bannerDefaultData.files.size() > 0) {
                    Glide.with(this).load(bannerDefaultData.files.get(
                            random.nextInt(bannerDefaultData.files.size())
                    ).fileUrl)
                            .apply(options)
                            .into(this.binding.imageEvent);
                }
                this.binding.setEventDetailADType(bannerDefaultData.adType);

            } catch (Exception e) {

            }

            this.binding.setEventDetailFlag(true);
            this.binding.layoutTop.bringToFront();
            this.adViewModel.getBannerDetail(bannerDefaultData.uuid, TabletSettingUtils.getInstacne().getTableNo(), 0, "na");
        });
        this.adViewModel.bannerList.observe(this, bannerList -> {

            LogUtil.d("배너 목록 리스너 상태 : " + bannerList);
            LogUtil.d("배너 목록 리스너 상태 : " + bannerList.size());

            if (bannerList == null || bannerList.size() == 0) {
                //배너 없음
                this.binding.setEventCount(0);
            } else {

                if (isRandomBannerMode) {
                    ///자동 광고로 인해 띄워진 경우
                    this.eventViewPagerAdapter = new EventViewPagerAdapter(this, bannerList, Glide.with(this));
                    this.binding.viewPager.setAdapter(this.eventViewPagerAdapter);
                    this.binding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                        }

                        @Override
                        public void onPageSelected(int position) {
                            //화면 변하면 셋팅
                            adViewModel.mSelectedBanner.setValue(bannerList.get(position));
                            binding.setEventDetailADType(bannerList.get(position).adType);
                            binding.setCurrentEventPosition(position + 1);

                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });
                    this.binding.setEventCount(bannerList.size());
                    this.binding.setCurrentEventPosition(1);

                    ///첫페이지는 이벤트로 처리 안되니까 여기서 처리.
                    this.adViewModel.mSelectedBanner.setValue(bannerList.get(0));
                    this.binding.setEventDetailADType(bannerList.get(0).adType);
                    this.binding.setEventDetailFlag(true);
//                    this.binding.layoutTop.bringToFront();

                    this.binding.textRandomEventClose.bringToFront();

                    this.scrollComSub.add(Observable.interval(10, 10, TimeUnit.SECONDS)
                            .subscribeOn(Schedulers.computation())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(aLong -> {
                                // here is the task that should repeat
                                //1. viewPager가 보일때
                                //2. 스크롤중이 아닐때
                                //3. 참여방법이나 참여를 보고있는 경우가 아닐때

                                if (this.nowTime == null)
                                    this.nowTime = new Date();
                                long mills = 2500;
                                if (this.lastTouchTime != null)
                                    mills = lastTouchTime.getTime() - nowTime.getTime();

                                if (this.binding.viewPager.getVisibility() == View.VISIBLE
                                        && this.binding.layoutEventSubscription.getVisibility() == View.GONE
                                        && this.binding.layoutAuth.getVisibility() == View.GONE
                                        && this.autoScrollAvailable
                                        && mills > 2000
                                        ) {

                                    if (this.binding.viewPager.getAdapter().getCount() > 1 && this.binding.viewPager.getCurrentItem() + 1 == this.binding.viewPager.getAdapter().getCount())  ///마지막 페이지
                                    {
                                        this.binding.viewPager.setCurrentItem(0, false);
                                    } else {
                                        this.binding.viewPager.setCurrentItem(this.binding.viewPager.getCurrentItem() + 1, false);
                                    }
                                }
                            })
                    );


                } else {
                    ///직접 들어와서 열람한 케이스
                    eventBannerListAdapter.setmListData(bannerList);
                    this.binding.recyclerView.setAdapter(eventBannerListAdapter);
                    this.eventBannerListAdapter.notifyDataSetChanged();
                    this.binding.setEventCount(bannerList.size());
                }
            }

        });


        ////테이블 번호에서 완료 누르면 바로 진행되게끔
        this.binding.inputAuth.setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                    case KeyEvent.KEYCODE_ENTER:
                        binding.buttonGoAuth.performClick();
                        return true;
                    default:
                        break;
                }
            }
            return false;
        });

        // get Unregistrar
        unregistrar = KeyboardVisibilityEvent.registerEventListener(
                this,
                isOpen -> {
                    this.isKeyBoardShow = isOpen;
                    setUI();
                });
        setUI();

        this.binding.layoutTop.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                binding.layoutTop.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) binding.layoutTop.getLayoutParams();
                lp.height = (int) (Utils.getWindosHeight(mContext) * (1 / 8.0));

                binding.layoutTop.setLayoutParams(lp);

            }
        });

        this.adViewModel.getADActionResult.observe(this, adActionData -> {
            if (adActionData != null) {
                //테스트 확인
                LogUtil.d("adAction 결과");
                LogUtil.d("adAction 결과 actionResult: " + adActionData.actionResult);

                if (adActionData.actionResult != null) {
                    if (adActionData.actionResult.equals("OK")) {
                        if (adActionData.reward != null) {
                            MenuItemData menuItemData = new MenuItemData();
                            menuItemData.CMDTCD = adActionData.reward.rewardCode;
                            menuItemData.quantity = 1;
                            menuItemData.foodName = "[광고 할인]";
                            menuItemData.price = adActionData.reward.value;

                            ArrayList<MenuItemData> list = new ArrayList<>();
                            list.add(menuItemData);
                            orderViewModel.orderToQueue(TabletSettingUtils.getInstacne().getTableNo(), list);
                        }
                    } else if (adActionData.actionResult.equals("DUPLICATED")) {
                        Toast.makeText(this, "이미 참여한 회원입니다.", Toast.LENGTH_SHORT)
                                .show();
                    } else if (adActionData.actionResult.equals("DENY")) {

                        Toast.makeText(this, "참여 대상이 아닙니다.", Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        Toast.makeText(this, "가입한 이력이 없거나 일시적인 오류로 참여할 수 없습니다. (" + adActionData.actionResult + ")", Toast.LENGTH_SHORT)
                                .show();
                    }

                }


            }
        });
        this.adViewModel.mSelectedBanner.observe(this, bannerDefaultData -> {
            if (bannerDefaultData != null) {
                if (bannerDefaultData.actionParamType != null) {
                    this.binding.setActionParamType(bannerDefaultData.actionParamType);
                    if (bannerDefaultData.actionParamType.equals("email"))
                        this.binding.inputAuth.setHint("이메일 주소를 입력해주세요.");
                    else if (bannerDefaultData.actionParamType.equals("phone"))
                        this.binding.inputAuth.setHint("휴대폰 번호를 입력해주세요.");

                }
            }
        });
        ///주문상황표시
        this.orderViewModel.orderState.observe(this, orderState -> {
            switch (orderState) {
                case NONE:
                    this.loadingDialog.dismiss();
                    break;
                case LOADING:
                    this.loadingDialog.setTitleNew(getResources().getString(R.string.loadingDialog_title_cart));
                    this.loadingDialog.show();
                    break;
                case ERROR:
                    this.loadingDialog.dismiss();
                    Toast.makeText(this, "포스기 주문 프로그램을 실행해주세요.", Toast.LENGTH_SHORT).show();
                    this.orderViewModel.orderState.setValue(OrderViewModel.ORDERSTATE.NONE);
                    break;
                case COMPLETE:
                    this.loadingDialog.dismiss();

                    MessageDialogBuilder.getInstance(this)
                            .setContent("이벤트 참여 완료.\n[계산서]에서 확인하실 수 있습니다.")
                            .setDefaultButton()
                            .complete();
                    this.binding.inputAuth.setText("");
                    this.adViewModel.getADActionResult.setValue(null);
                    this.orderViewModel.orderState.setValue(OrderViewModel.ORDERSTATE.NONE);
                    break;
                case COMPLETEWITHERROR1:
                    this.loadingDialog.dismiss();

                    MessageDialogBuilder.getInstance(this)
                            .setContent("이벤트 참여 완료.(E1)")
                            .setDefaultButton()
                            .complete();
                    this.orderViewModel.orderState.setValue(OrderViewModel.ORDERSTATE.NONE);
                    break;
                case COMPLETEWITHERROR2:
                    this.loadingDialog.dismiss();
                    MessageDialogBuilder.getInstance(this)
                            .setContent("이벤트 참여 완료.(E2)")
                            .setDefaultButton()
                            .complete();
                    this.orderViewModel.orderState.setValue(OrderViewModel.ORDERSTATE.NONE);
                    break;
            }
        });
    }

    public void closeADOnlyScreenMode(View view) {
        if (this.binding != null && (this.binding.getEventDetailADType() == null || this.binding.getEventDetailADType().equals("display"))) {
            finish();
        }
    }


    public void closeADSelectionPage(View view) {

        try {

            if (this.isRandomBannerMode) {
                finish();
                return;
            }
            if (this.binding.getEventDetailFlag()) {
                this.binding.setEventDetailFlag(false);
                Glide.with(this).clear(this.binding.imageEvent);
            } else
                finish();
        } catch (Exception e) {
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (this.scrollComSub != null) {
            this.scrollComSub.clear();
            this.scrollComSub.unsubscribe();
        }
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregistrar.unregister();


    }


    public void viewInputAuth(View view) {

        this.binding.layoutAuth.bringToFront();
        this.binding.layoutAuth.setVisibility(View.VISIBLE);
    }

    /**
     * 인증요청 하러 가기
     */
    public void goAuth(View view) {
//        test();
//        return;
        ///현재 테이블 주문금액이 1000원 이상인경우에만 할인을 할 수 있다.
        LogUtil.d("현재 주문금액 : " + this.billViewModel.mOrderedTotalPrice.getValue());
        if (this.billViewModel.mOrderedTotalPrice.getValue() >= 1000) {

            ///입력값 타입에 따라 다르게 동작해야 한다
            String input = this.binding.inputAuth.getText().toString();
            if (input != null) {


                if (this.adViewModel.mSelectedBanner.getValue().actionParamType.equals("email")) {
                    //0. 이메일주소 입력의 경우
                    if (Utils.checkEMail(input)) {
                        ///이메일주소 맞음.
                        this.adViewModel.getADAction(this.adViewModel.mSelectedBanner.getValue().uuid, this.adViewModel.mSelectedBanner.getValue().actionParam, input, TabletSettingUtils.getInstacne().getTableNo(), 0, "na");
                    } else {
                        ///이메일주소 아님.
                        Toast.makeText(this, "이메일주소를 바르게 입력해주세요.", Toast.LENGTH_SHORT).show();
                    }
                } else if (this.adViewModel.mSelectedBanner.getValue().actionParamType.equals("phone")) {
                    //1. 휴대폰 번호 입력의 경우
                    if (Utils.checkMobilePhoneNumber(input)) {
                        ///전화번호 맞음.
                        this.adViewModel.getADAction(this.adViewModel.mSelectedBanner.getValue().uuid, this.adViewModel.mSelectedBanner.getValue().actionParam, input, TabletSettingUtils.getInstacne().getTableNo(), 0, "na");
                    } else {
                        ///전화번호 아님.
                        Toast.makeText(this, "휴대폰 번호를 바르게 입력해주세요.", Toast.LENGTH_SHORT).show();
                    }
                }

            } else {

            }
        } else {
            Toast.makeText(this, "주문금액 총액이 1000원 미만인 경우 참여할 수 없습니다.", Toast.LENGTH_SHORT).show();
        }


    }

    ////이벤트 참여방법 보기
    public void viewEventSubscription(View view) {
        this.binding.layoutEventSubscription.bringToFront();
        this.binding.layoutEventSubscription.setVisibility(View.VISIBLE);
    }


    public void clickOutOfLayoutAuth(View view) {

        if (this.isKeyBoardShow) {
            UIUtil.hideKeyboard(this);
            return;
        }

        this.binding.layoutEventSubscription.setVisibility(View.GONE);
        this.binding.layoutAuth.setVisibility(View.GONE);
        UIUtil.hideKeyboard(this);
    }


    /**
     * 주문 관련 처리가 필요함.
     * 1. cart를 비우고 새로 채워넣던지 임시 cart를 써야함.
     * 2. 그 '임시 cart'에 리워드 상품이 들어가야함
     * 3. '임시 cart'를 기반으로 주문넣기가 실행되어야 함.
     */


    private void test() {

        MenuItemData menuItemData = new MenuItemData();
        menuItemData.CMDTCD = "CF0002";
        menuItemData.quantity = 1;
        menuItemData.foodName = "[광고 할인]";
        menuItemData.price = -1000;

        ArrayList<MenuItemData> list = new ArrayList<>();
        list.add(menuItemData);

//        orderViewModel.orderForEvent(list);
    }

}
