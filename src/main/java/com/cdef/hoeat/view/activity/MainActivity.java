package com.cdef.hoeat.view.activity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.cdef.commonmodule.dataModel.TableInfoData;
import com.cdef.commonmodule.utils.LoginUtil;
import com.cdef.commonmodule.utils.typekit.TypekitContextWrapper;
import com.cdef.commonmodule.dataModel.firebaseDataSet.Message;
import com.cdef.commonmodule.dataModel.OrderCategoryData;
import com.cdef.hoeat.Exceptions.RestartExceptions;
import com.cdef.hoeat.OrderAppApplication;
import com.cdef.hoeat.R;
import com.cdef.commonmodule.utils.CustomAnimationUtils;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.ResourceUtil;
import com.cdef.commonmodule.utils.TabletSetting;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.hoeat.utils.DefaultExceptionHandler;
import com.cdef.hoeat.utils.NavigationUtils;
import com.cdef.hoeat.utils.QueueUtils;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.cdef.hoeat.view.customView.SideMenuListItem;
import com.cdef.hoeat.view.dialog.ADDialog;
import com.cdef.hoeat.view.dialog.LanguageDialog;
import com.cdef.hoeat.view.dialog.MessageDialogBuilder;
import com.cdef.hoeat.view.fragment.BaseFragment;
import com.cdef.hoeat.view.fragment.BillFragment;
import com.cdef.hoeat.view.fragment.OrderStateFragment;
import com.cdef.hoeat.view.fragment.PresentFragment;
import com.cdef.hoeat.view.fragment.TableSettingFragment;
import com.cdef.hoeat.viewModel.ADViewModel;
import com.cdef.hoeat.viewModel.BillViewModel;
import com.cdef.hoeat.viewModel.MainViewModel;
import com.cdef.hoeat.viewModel.OrderViewModel;
import com.cdef.hoeat.databinding.ActivityMainBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class MainActivity extends BaseActivity {


    ///키보드 view 상태를 체크하기위한 변수, 어디서든 접근하기 위해  static으로 사용

    Date lastTouchTime = null;
    ActivityMainBinding binding = null;
    MainViewModel mainViewModel;
    CompositeSubscription adManager = new CompositeSubscription();
    CompositeSubscription compositeSubscription = new CompositeSubscription();

    ADViewModel adViewModel;
    OrderViewModel orderViewModel;
    BillViewModel billViewModel;

    RequestManager glide;

    public void goPresent(View view) {
        this.binding.setPresentButtonSelected(true);
        clearSelectionSideMenuListSub();
        NavigationUtils.detailOpenFragment(getSupportFragmentManager(), R.id.layoutFragment, PresentFragment.class.getSimpleName(), null);
    }

    public void goTableSetting(View view) {
        this.binding.setPresentButtonSelected(false);
        clearSelectionSideMenuListSub();
        NavigationUtils.detailOpenFragment(getSupportFragmentManager(), R.id.layoutFragment, TableSettingFragment.class.getSimpleName(), null);
    }

    @Override
    public void onBackPressed() {
        setUI();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        ///로딩중일땐 터치 못하게
        if (getSupportFragmentManager().findFragmentById(R.id.layoutFragment) instanceof BaseFragment) {
            if (((BaseFragment) getSupportFragmentManager().findFragmentById(R.id.layoutFragment)).isLoading()) {
                return false;
            }
        }

        ///마지막 터치 시간 갱신
        this.lastTouchTime = Calendar.getInstance().getTime();
        setUI();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            if (ev.getPointerCount() > 1) {
                LogUtil.d("Multitouch detected!");
                ev.setAction(MotionEvent.ACTION_CANCEL);
                return super.dispatchTouchEvent(ev);
            }
        }


        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    Handler handler = new Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel notificationChannel = new NotificationChannel("hoeat_fcm", "호잇FCM", NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription("channel description");
//            notificationChannel.enableLights(true);
//            notificationChannel.setLightColor(Color.GREEN);
//            notificationChannel.enableVibration(true);
//            notificationChannel.setVibrationPattern(new long[]{100, 200, 100, 200});
//            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        this.binding.setActivity(this);
        if(this.glide == null)
        {
            this.glide = Glide.with(this);
        }
        LogUtil.d("앱 시작");
        OrderAppApplication.mContext = this;



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            this.binding.layoutNewMessage.setY(-Utils.dpToPx(this, (63 + 2)));
        }

        ///로고 길게 누르면 재시작 뜸.
        this.binding.imageLogo.setOnLongClickListener(view -> {
            restarter();
            return true;
        });

//        this.binding.layoutChat.setMapListener(() -> {
//
////            if (TabletSettingUtils.getInstacne().get() == null) {
////                Toast.makeText(this, "메뉴 주문후 이용해주세요.", Toast.LENGTH_SHORT).show();
////                return;
////            }
//
//            this.compositeSubscription.add(this.loginViewModel.getAllMapInfo(TabletSettingUtils.getInstacne().getBranchInfo().storeBranchUid)
//                    .subscribeOn(Schedulers.computation())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(mapList -> {
//                        TabletSettingUtils.getInstacne().setTableMaps(mapList);
//                        this.mainViewModel.getTableInfoList(QueueUtils.getTableInfoList(TabletSettingUtils.getInstacne().getBranchInfo().posIp))
//                                .subscribeOn(Schedulers.computation())
//                                .observeOn(AndroidSchedulers.mainThread())
//                                .subscribe(result -> {
//                                    TabletSettingUtils.getInstacne().setTableInfoList(result);
//                                }, error -> {
//                                    LogUtil.d("왜 매장 테이블 정보가 없냐 : " + error);
//                                    Toast.makeText(this, "일시적으로 매장 테이블 상태를 가져올 수 없습니다.", Toast.LENGTH_SHORT).show();
//
//                                }, () -> {
//                                    this.binding.layoutChat.openNewPrivateChat();
//                                });
//
//
//                    }, throwable -> {
//                        LogUtil.d("테이블 선택 작업에 문제 : " + throwable.getMessage());
//
//                    }));
//
//
////            if (TabletSettingUtils.getInstacne().getConvertedTableMaps() == null) {
////
////
////
////            } else {
////                this.binding.layoutChat.openNewPrivateChat();
////            }
//
//        });


        ///앱에서 떨어진 Exception에 대해 자동 재시작 기능을 위해 추가한 코드
        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(this));

        ///기본배경
        this.glide.asDrawable().load(R.drawable.img_background)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        resource.setColorFilter(Color.parseColor("#50000000"), PorterDuff.Mode.DARKEN);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            binding.layoutMain.setBackground(resource);
                        }
                        else
                        {
                            binding.layoutMain.setBackgroundDrawable(resource);
                        }

                    }
                });

        this.mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        this.mainViewModel.initRepository(this);

        this.orderViewModel = ViewModelProviders.of(this).get(OrderViewModel.class);
        this.orderViewModel.initRepository(this);
        ///큐 상태 체크
        this.orderViewModel.queueState.observe(this, queueState -> {
            switch (queueState) {
                case NONE:
                    break;
                case LOADING:
                    break;
                case ERROR:
                    Toast.makeText(this, "광고할인 취소. 포스를 확인해주세요", Toast.LENGTH_SHORT).show();
                    this.orderViewModel.queueState.setValue(OrderViewModel.QUEUESTATE.NONE);
                    break;
                case COMPLETE:
                    if (ADDialog.getmInstanceWithoutInit() != null && ADDialog.getmInstanceWithoutInit().isShowing()) {
                        ADDialog.getmInstanceWithoutInit().clearInput();
                    }
                    MessageDialogBuilder.getInstance(this).setTitle("광고할인")
                            .setContent("광고할인 되었습니다. 주문내역에서 확인하실 수 있습니다..")
                            .setDefaultButton()
                            .complete();

                    break;
            }
        });


        this.adViewModel = ViewModelProviders.of(this).get(ADViewModel.class);
        this.adViewModel.initRepository(this);
        this.adViewModel.bannerList.observe(this, bannerDefaultData -> {
            ADDialog.getInstance(this)
                    .setBannerList(this.adViewModel.isRandomMode.getValue(), bannerDefaultData)
                    .setBannerDetailListener(bannerSelectedItem -> {
                        int member = 0;
                        String sex = null;
                        try {
                            member = (TabletSettingUtils.getInstacne().getTableInfoData().men + TabletSettingUtils.getInstacne().getTableInfoData().women);
                            sex = (TabletSettingUtils.getInstacne().getTableInfoData().men != 0 && TabletSettingUtils.getInstacne().getTableInfoData().women != 0 ? "both" :
                                    (TabletSettingUtils.getInstacne().getTableInfoData().men != 0 && TabletSettingUtils.getInstacne().getTableInfoData().women == 0 ? "male" :
                                            (TabletSettingUtils.getInstacne().getTableInfoData().men == 0 && TabletSettingUtils.getInstacne().getTableInfoData().women != 0 ? "female" :
                                                    "na")));
                        } catch (Exception e) {
                            member = 0;
                            sex = "na";

                        }
                        this.adViewModel.getBannerDetail(bannerSelectedItem.uuid, TabletSettingUtils.getInstacne().getTableNo(), member, sex);

                    })
                    .setAuthListener((selectedItem, input) -> {
                        if (input == null || input.length() == 0) {
                            Toast.makeText(this, "인증용 휴대폰번호 입력해주세요.", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        if(TabletSettingUtils.getInstacne().getOrderId() != null)
                        {
                            ///입력값 타입에 따라 다르게 동작해야 한다
                            if (input != null) {

                                int member = 0;
                                String sex = null;
                                try {
                                    member = (TabletSettingUtils.getInstacne().getTableInfoData().men + TabletSettingUtils.getInstacne().getTableInfoData().women);
                                    sex = (TabletSettingUtils.getInstacne().getTableInfoData().men != 0 && TabletSettingUtils.getInstacne().getTableInfoData().women != 0 ? "both" :
                                            (TabletSettingUtils.getInstacne().getTableInfoData().men != 0 && TabletSettingUtils.getInstacne().getTableInfoData().women == 0 ? "male" :
                                                    (TabletSettingUtils.getInstacne().getTableInfoData().men == 0 && TabletSettingUtils.getInstacne().getTableInfoData().women != 0 ? "female" :
                                                            "na")));
                                } catch (Exception e) {
                                    member = 0;
                                    sex = "na";

                                }

                                if (selectedItem.actionParamType.equals("email")) {
                                    //0. 이메일주소 입력의 경우
                                    if (Utils.checkEMail(input)) {
                                        ///이메일주소 맞음.
                                        this.adViewModel.getADAction(selectedItem.uuid, selectedItem.actionParam, input, TabletSettingUtils.getInstacne().getTableNo(), member, sex);
                                    } else {
                                        ///이메일주소 아님.
                                        Toast.makeText(this, "이메일주소를 바르게 입력해주세요.", Toast.LENGTH_SHORT).show();
                                    }
                                } else if (selectedItem.actionParamType.equals("phone")) {
                                    //1. 휴대폰 번호 입력의 경우
                                    if (Utils.checkMobilePhoneNumber(input)) {
                                        ///전화번호 맞음.
                                        this.adViewModel.getADAction(selectedItem.uuid, selectedItem.actionParam, input, TabletSettingUtils.getInstacne().getTableNo(), member, sex);
                                    } else {
                                        ///전화번호 아님.
                                        Toast.makeText(this, "휴대폰 번호를 바르게 입력해주세요.", Toast.LENGTH_SHORT).show();
                                    }
                                }

                            } else {

                            }
                        }
                        else
                        {
                            if(TabletSettingUtils.getInstacne().getOrderId() != null)
                            {
                                ///입력값 타입에 따라 다르게 동작해야 한다
                                if (input != null) {

                                    int member = 0;
                                    String sex = null;
                                    try {
                                        member = (TabletSettingUtils.getInstacne().getTableInfoData().men + TabletSettingUtils.getInstacne().getTableInfoData().women);
                                        sex = (TabletSettingUtils.getInstacne().getTableInfoData().men != 0 && TabletSettingUtils.getInstacne().getTableInfoData().women != 0 ? "both" :
                                                (TabletSettingUtils.getInstacne().getTableInfoData().men != 0 && TabletSettingUtils.getInstacne().getTableInfoData().women == 0 ? "male" :
                                                        (TabletSettingUtils.getInstacne().getTableInfoData().men == 0 && TabletSettingUtils.getInstacne().getTableInfoData().women != 0 ? "female" :
                                                                "na")));
                                    } catch (Exception e) {
                                        member = 0;
                                        sex = "na";

                                    }

                                    if (selectedItem.actionParamType.equals("email")) {
                                        //0. 이메일주소 입력의 경우
                                        if (Utils.checkEMail(input)) {
                                            ///이메일주소 맞음.
                                            this.adViewModel.getADAction(selectedItem.uuid, selectedItem.actionParam, input, TabletSettingUtils.getInstacne().getTableNo(), member, sex);
                                        } else {
                                            ///이메일주소 아님.
                                            Toast.makeText(this, "이메일주소를 바르게 입력해주세요.", Toast.LENGTH_SHORT).show();
                                        }
                                    } else if (selectedItem.actionParamType.equals("phone")) {
                                        //1. 휴대폰 번호 입력의 경우
                                        if (Utils.checkMobilePhoneNumber(input)) {
                                            ///전화번호 맞음.
                                            this.adViewModel.getADAction(selectedItem.uuid, selectedItem.actionParam, input, TabletSettingUtils.getInstacne().getTableNo(), member, sex);
                                        } else {
                                            ///전화번호 아님.
                                            Toast.makeText(this, "휴대폰 번호를 바르게 입력해주세요.", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                } else {

                                }
                            }
                            else
                            {
                                this.compositeSubscription.add(this.billViewModel.getOrderedList(TabletSettingUtils.getInstacne().getTableNo())
                                        .subscribe(response -> {
                                            if (response != null) {

                                                ///입력값 타입에 따라 다르게 동작해야 한다
                                                if (input != null) {

                                                    int member = 0;
                                                    String sex = null;
                                                    try {
                                                        member = (TabletSettingUtils.getInstacne().getTableInfoData().men + TabletSettingUtils.getInstacne().getTableInfoData().women);
                                                        sex = (TabletSettingUtils.getInstacne().getTableInfoData().men != 0 && TabletSettingUtils.getInstacne().getTableInfoData().women != 0 ? "both" :
                                                                (TabletSettingUtils.getInstacne().getTableInfoData().men != 0 && TabletSettingUtils.getInstacne().getTableInfoData().women == 0 ? "male" :
                                                                        (TabletSettingUtils.getInstacne().getTableInfoData().men == 0 && TabletSettingUtils.getInstacne().getTableInfoData().women != 0 ? "female" :
                                                                                "na")));
                                                    } catch (Exception e) {
                                                        member = 0;
                                                        sex = "na";

                                                    }

                                                    if (selectedItem.actionParamType.equals("email")) {
                                                        //0. 이메일주소 입력의 경우
                                                        if (Utils.checkEMail(input)) {
                                                            ///이메일주소 맞음.
                                                            this.adViewModel.getADAction(selectedItem.uuid, selectedItem.actionParam, input, TabletSettingUtils.getInstacne().getTableNo(), member, sex);
                                                        } else {
                                                            ///이메일주소 아님.
                                                            Toast.makeText(this, "이메일주소를 바르게 입력해주세요.", Toast.LENGTH_SHORT).show();
                                                        }
                                                    } else if (selectedItem.actionParamType.equals("phone")) {
                                                        //1. 휴대폰 번호 입력의 경우
                                                        if (Utils.checkMobilePhoneNumber(input)) {
                                                            ///전화번호 맞음.
                                                            this.adViewModel.getADAction(selectedItem.uuid, selectedItem.actionParam, input, TabletSettingUtils.getInstacne().getTableNo(), member, sex);
                                                        } else {
                                                            ///전화번호 아님.
                                                            Toast.makeText(this, "휴대폰 번호를 바르게 입력해주세요.", Toast.LENGTH_SHORT).show();
                                                        }
                                                    }

                                                } else {

                                                }
                                            } else {
                                                Toast.makeText(this, "주문금액 총액이 1000원 미만인 경우 참여할 수 없습니다.", Toast.LENGTH_SHORT).show();
                                            }

                                        }, error -> {
                                            Toast.makeText(this, "지금은 이용할 수 없습니다.(e.na.Q)", Toast.LENGTH_SHORT).show();

                                        }, () -> {

                                        }));
                            }


                        }



                    })
                    .setCloseListener(() -> {
                        this.lastTouchTime = Calendar.getInstance().getTime(); //마지막 터치 시간값을 초기화
                    })
                    .complete();
//            if (bannerDefaultData != null && bannerDefaultData.size() > 0) {
//
//            }
        });
        this.adViewModel.getADActionResult.observe(this, adActionData -> {
            if (adActionData != null) {
                //테스트 확인
                LogUtil.d("adAction 결과");
                LogUtil.d("adAction 결과 actionResult: " + adActionData.actionResult);

                if (adActionData.actionResult != null) {


                    if (adActionData.actionResult.equals("OK")) {
                        if (adActionData.reward != null) {
                            JSONObject jsonObject = new JSONObject();
                            JSONObject reward = new JSONObject();
                            try {
                                jsonObject.put("type", "ad");
                                jsonObject.put("tableNo", TabletSettingUtils.getInstacne().getTableNo() + "");

                                reward.put("value", adActionData.reward.value);
                                reward.put("rewardCode", adActionData.reward.rewardCode);
                                reward.put("rewardType", adActionData.reward.rewardType);

                                jsonObject.put("reward", reward);

                                this.orderViewModel.requestQueueEvent(QueueUtils.requestQueueEvent(TabletSettingUtils.getInstacne().getBranchInfo().posIp)
                                        , jsonObject.toString()
                                );


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    } else if (adActionData.actionResult.equals("DUPLICATED")) {
                        Toast.makeText(this, "이미 참여한 회원입니다.", Toast.LENGTH_SHORT)
                                .show();
                    } else if (adActionData.actionResult.equals("DENY")) {

                        Toast.makeText(this, "참여 대상이 아닙니다.", Toast.LENGTH_SHORT)
                                .show();
                    } else {
                        Toast.makeText(this, "가입한 이력이 없거나 일시적인 오류로 참여할 수 없습니다. (" + adActionData.actionResult + ")", Toast.LENGTH_SHORT)
                                .show();
                    }

                }


            }
        });


        this.billViewModel = ViewModelProviders.of(this).get(BillViewModel.class);
        this.billViewModel.initRepository(this);

//        this.internetConnectionDialog = new LoadingDialog(this, getString(R.string.loadingDialog_title_connection));

        this.setUI();
        this.updateVersionCode();

        this.lastTouchTime = Calendar.getInstance().getTime(); //마지막 터치 시간값을 초기화

        this.goMain();



    }

    Observable observableNotice = Observable.interval(1, 10, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread());

    @Override
    protected void onResume() {
        super.onResume();

        LogUtil.d("테스트해보자 : " + getIntent());
        this.lastTouchTime = Calendar.getInstance().getTime();

        ///공지사항을 체크하고 띄워주는 부분
        if (TabletSettingUtils.getInstacne().getBranchInfo() != null) {
            ///notice 관련 셋팅
            if (LoginUtil.getInstance(this).getmNotice() != null && LoginUtil.getInstance(this).getmNotice().notice.size() > 0) {
                adManager.add(
                        this.observableNotice
                                .subscribe(aLong -> {
                                    // here is the task that should repeat
                                    this.binding.textNotice.setText(LoginUtil.getInstance(this).getmNotice().getRandomNotice());
                                })
                );


            }
        }

        ///로그인 정보를 보고서 주문현황 버튼을 표시할지 결정한다
        if (TabletSettingUtils.getInstacne().getBranchInfo() != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption.advertisement) {


            adManager.add(Observable.interval(1, 5, TimeUnit.SECONDS)
                            .subscribeOn(Schedulers.computation())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(aLong -> {
                                // here is the task that should repeat
                                ///광고를 띄우기위한 조건이 무엇일까?
                                //0. 광고를 띄우고 있을땐 굳이 확인할 필요가 없지?
                                //1. 마지막 터치로부터 checkInterval만큼의 시간이 지나야 가능
                                //2. 적어도 사용하기를 누른 이후. (즉, layoutIntro 가 Visible.GONE 상태인 경우)
                                //3. 마지막 광고 띄운 이후 (=? checkinterval)


                                if (binding.layoutIntro.getVisibility() == View.GONE
                                        && TabletSettingUtils.getInstacne().getOrderId() != null
                                        && lastTouchTime != null
                                        && Utils.timeToShowAD(lastTouchTime, TabletSettingUtils.getInstacne().getBranchInfo().storeOption.adShowInterval))
//                        binding.layoutAD.setVisibility(View.VISIBLE);
                                    if (ADDialog.getmInstanceWithoutInit() != null && ADDialog.getmInstanceWithoutInit().isShowing()) {

                                    } else {
//                                        NavigationUtils.openNewActivity(this, EventActivity.class, 2);
                                        this.adViewModel.isRandomMode.setValue(true);
                                        this.adViewModel.getAdList((TabletSettingUtils.getInstacne().getTableInfoData().men +
                                                        TabletSettingUtils.getInstacne().getTableInfoData().women)
                                                , (TabletSettingUtils.getInstacne().getTableInfoData().men != 0 && TabletSettingUtils.getInstacne().getTableInfoData().women != 0 ? "both" :
                                                        (TabletSettingUtils.getInstacne().getTableInfoData().men != 0 && TabletSettingUtils.getInstacne().getTableInfoData().women == 0 ? "male" :
                                                                (TabletSettingUtils.getInstacne().getTableInfoData().men == 0 && TabletSettingUtils.getInstacne().getTableInfoData().women != 0 ? "female" :
                                                                        "na")))
                                                , "screen"
                                        );
                                    }


                                LogUtil.d("광고 매니저 테스트 : " + aLong);

                            })
            );
        }

        ///채팅 관려된 처리를 추가한다.

        if ((TabletSettingUtils.getInstacne().getBranchInfo() != null) && this.binding.layoutChat != null) {
            ///채팅방 다시 초기화
            try {
                if (!TabletSettingUtils.getInstacne().getBranchInfo().storeOption.storeChat
                        && !TabletSettingUtils.getInstacne().getBranchInfo().storeOption.tableChat) {
                    this.binding.layoutChatButton.setVisibility(View.GONE);
                } else {
                    this.binding.layoutChatButton.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {

            }

            setPrivateChatListener();
            this.binding.layoutChat.setup();
            this.binding.layoutChat.setCloseListener(() -> {
                closeChat(null);
            });
        }

        LogUtil.d("광고 매니저 테스트 onResume " + adManager.isUnsubscribed());
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (TabletSettingUtils.getInstacne().getBranchInfo() != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption.advertisement) {
            adManager.clear();
        }
        LogUtil.d("광고 매니저 테스트 onPause " + adManager.isUnsubscribed());

    }

    @Override
    protected void onDestroy() {
        if (this.compositeSubscription != null)
            this.compositeSubscription.clear();
        if (this.adManager != null)
            this.adManager.clear();
        super.onDestroy();
    }

    /**
     * 로그인 & 기본 데이터 셋팅이 끝난후 주문화면으로 넘어가기위한 부분이다.
     * 1. 왼쪽 버튼들 생성
     * 2. 메뉴판 화면으로 이동
     **/
    public void goMain() {

        this.binding.layoutLeftMenus.setVisibility(View.VISIBLE);

        ///로그인 화면은 날려버리자
        NavigationUtils.removeAllFragment(getSupportFragmentManager());

        /**
         * 액티비티와 관련된 화면 리소스 교체
         * */
        this.binding.layoutLeftMenus.setBackgroundColor(Color.parseColor(ResourceUtil.getInstance().getcSideBackground()));
//        this.binding.buttonStart.setBackground(ResourceUtil.getInstance().getStartButton(this.binding.buttonStart.getLayoutParams().height));
        this.binding.buttonStart.setBackgroundResource(R.drawable.selector_btn_submit);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            this.binding.layoutTableNo.setBackground(ResourceUtil.getInstance().getTableNoBackground(this.binding.layoutTableNo.getLayoutParams().height) == null ? this.getResources().getDrawable(R.drawable.img_tableno) : ResourceUtil.getInstance().getTableNoBackground(this.binding.layoutTableNo.getLayoutParams().height));
        }
        else {
            this.binding.layoutTableNo.setBackgroundDrawable(ResourceUtil.getInstance().getTableNoBackground(this.binding.layoutTableNo.getLayoutParams().height) == null ? this.getResources().getDrawable(R.drawable.img_tableno) : ResourceUtil.getInstance().getTableNoBackground(this.binding.layoutTableNo.getLayoutParams().height));
        }
        this.binding.textTableNo.setTextColor(Color.parseColor("#ffffff"));

        ///인트로 배경
//        this.glide.asDrawable().load(ResourceUtil.getInstance().getiIntroBackground())
//                .into(new SimpleTarget<Drawable>() {
//                    @Override
//                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
//                        resource.setColorFilter(Color.parseColor("#90000000"), PorterDuff.Mode.DARKEN);
//                        binding.layoutIntro.setBackground(resource);
//
//                    }
//                });


        if (TabletSettingUtils.getInstacne().getBranchInfo() != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption.survey
                ) {

            ///혹시 멤버에 대한 정보가 찼는지 확인해봐야한다.


            ///TableSettingUtils 의 테이블 정보를 훑는다
            ///설문조사쪽
            if (TabletSettingUtils.getInstacne().getTableInfoData() == null
                    || (TabletSettingUtils.getInstacne().getTableInfoData().women == 0 && TabletSettingUtils.getInstacne().getTableInfoData().men == 0)
                    || TabletSettingUtils.getInstacne().getTableInfoData().age == 0) {
                this.binding.layoutIntro.setVisibility(View.VISIBLE);
                this.binding.layoutIntro.bringToFront();
                this.binding.textVersion.bringToFront();
            }
        } else {
            this.binding.layoutIntro.setVisibility(View.GONE);
        }


        ///로그인 정보를 보고서 주문현황 버튼을 표시할지 결정한다
        if (TabletSettingUtils.getInstacne().getBranchInfo() != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption.kitchenTab
                ) {
            this.binding.buttonSideMenuListItemOrderState.setVisibility(View.VISIBLE);
        } else {
            this.binding.buttonSideMenuListItemOrderState.setVisibility(View.GONE);
        }


        ////언어 선택모드를 사용중인지 확인한다.
        if (TabletSettingUtils.getInstacne().getBranchInfo() != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption.lang != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption.lang.size() > 0
        ) {
            this.binding.layoutLanguageButton.setVisibility(View.VISIBLE);
        } else {
            this.binding.layoutLanguageButton.setVisibility(View.GONE);
        }

        if(LoginUtil.getInstance().getSelectedLanguage() == null || LoginUtil.getInstance().getSelectedLanguage().equals("ko"))
        {
            this.binding.buttonSideMenuListItemReceipt.setTitle("계산서");
        }
        else
        {
            this.binding.buttonSideMenuListItemReceipt.setTitle("Bill");

        }


        ///로그인 정보를 보고서 광고 버튼을 표시할지 결정한다
        if (TabletSettingUtils.getInstacne().getBranchInfo() != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption.advertisement) {
            this.binding.layoutEventButton.setVisibility(View.VISIBLE);
            this.adManager.add(Observable.interval(1, 5, TimeUnit.SECONDS)
                            .subscribeOn(Schedulers.computation())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(aLong -> {
                                // here is the task that should repeat
                                ///광고를 띄우기위한 조건이 무엇일까?
                                //0. 광고를 띄우고 있을땐 굳이 확인할 필요가 없지?
                                //1. 마지막 터치로부터 checkInterval만큼의 시간이 지나야 가능
                                //2. 적어도 사용하기를 누른 이후. (즉, layoutIntro 가 Visible.GONE 상태인 경우)
                                //3. 마지막 광고 띄운 이후 (=? checkinterval)

                                if (this.binding.layoutIntro.getVisibility() == View.GONE
                                        && TabletSettingUtils.getInstacne().getOrderId() != null
                                        && lastTouchTime != null
                                        && Utils.timeToShowAD(lastTouchTime, TabletSettingUtils.getInstacne().getBranchInfo().storeOption.adShowInterval))
//                            NavigationUtils.openNewActivity(this, EventActivity.class, 2);

                                    if (ADDialog.getmInstanceWithoutInit() != null && ADDialog.getmInstanceWithoutInit().isShowing()) {

                                    } else {
//                                        NavigationUtils.openNewActivity(this, EventActivity.class, 2);
                                        this.adViewModel.isRandomMode.setValue(true);
                                        this.adViewModel.getAdList((TabletSettingUtils.getInstacne().getTableInfoData().men +
                                                        TabletSettingUtils.getInstacne().getTableInfoData().women)
                                                , (TabletSettingUtils.getInstacne().getTableInfoData().men != 0 && TabletSettingUtils.getInstacne().getTableInfoData().women != 0 ? "both" :
                                                        (TabletSettingUtils.getInstacne().getTableInfoData().men != 0 && TabletSettingUtils.getInstacne().getTableInfoData().women == 0 ? "male" :
                                                                (TabletSettingUtils.getInstacne().getTableInfoData().men == 0 && TabletSettingUtils.getInstacne().getTableInfoData().women != 0 ? "female" :
                                                                        "na")))
                                                , "screen"
                                        );
                                    }
                            })
            );
        } else {
            this.binding.layoutEventButton.setVisibility(View.GONE);
        }
        ///로그인 정보를 보고서 선물하기 버튼을 표시할지 결정한다
        if (TabletSettingUtils.getInstacne().getBranchInfo() != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption.present) {
            this.binding.layoutPresentButton.setVisibility(View.VISIBLE);
        } else {
            this.binding.layoutPresentButton.setVisibility(View.GONE);
        }


        ///로그인 정보에 logo 포함 여부에 따라 분기 처리
        LogUtil.d("로고 이미지 : " + TabletSettingUtils.getInstacne().getBranchInfo().univInfo.logo);
        if (TabletSettingUtils.getInstacne().getBranchInfo().univInfo.logo != null && TabletSettingUtils.getInstacne().getBranchInfo().univInfo.logo.length() > 0) {
            this.glide.load(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.logo)
                    .into(binding.imageLogo);
            this.glide.load(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.logo)
                    .into(binding.imageIntroLogo);
        } else {
            this.glide.load(R.drawable.icon_logo)
                    .into(binding.imageLogo);
            this.glide.load(R.drawable.icon_logo)
                    .into(binding.imageIntroLogo);
        }


        //테이블 번호 셋팅
        this.binding.textTableNo.setText(TabletSettingUtils.getInstacne().getTableNo());
        this.binding.layoutStatusBar.setVisibility(View.VISIBLE);

        //테이블셋팅 버튼 초기화
        if (TabletSettingUtils.getInstacne().getBranchInfo().storeOption.tableSetting) {


            if(LoginUtil.getInstance().getSelectedLanguage() == null || LoginUtil.getInstance().getSelectedLanguage().equals("ko"))
            {
                this.binding.buttonCall.setText("서비스요청");
            }
            else
            {
                this.binding.buttonCall.setText("Call Manager");
            }
            this.binding.buttonCall.setVisibility(View.VISIBLE);
        } else {
            this.binding.layoutButtonCall.setVisibility(View.GONE);
        }

        ///왼쪽 버튼 생성
        setSideMenuSubListViews();
        ///공지사항을 체크하고 띄워주는 부분(로그인 직후에는 resume이 동작안하니까 여기서 초기화해준다.
        if (TabletSettingUtils.getInstacne().getBranchInfo() != null) {
            ///notice 관련 셋팅
            if (LoginUtil.getInstance(this).getmNotice() != null && LoginUtil.getInstance(this).getmNotice().notice.size() > 0) {
                adManager.add(
                        this.observableNotice
                                .subscribe(aLong -> {
                                    // here is the task that should repeat
                                    this.binding.textNotice.setText(LoginUtil.getInstance(this).getmNotice().getRandomNotice());
                                })
                );
            }
        }

        ///채팅방 초기화

        if (!TabletSettingUtils.getInstacne().getBranchInfo().storeOption.storeChat
                && !TabletSettingUtils.getInstacne().getBranchInfo().storeOption.tableChat) {
            this.binding.layoutChatButton.setVisibility(View.GONE);
        } else {
            this.binding.layoutChatButton.setVisibility(View.VISIBLE);
        }

        setPrivateChatListener();
        this.binding.layoutChat.setup();
        this.binding.layoutChat.setCloseListener(() -> {
            closeChat(null);
        });

        ///시작화면 초기화
        this.binding.textIntroTableNo.setText("No. " + TabletSettingUtils.getInstacne().getTableNo());

        if(LoginUtil.getInstance().getSelectedLanguage() == null || LoginUtil.getInstance().getSelectedLanguage().equals("ko"))
        {

            this.binding.layoutSurvey1.setCheckBoxList(new String[]{
                    "0명"
                    , "1명"
                    , "2명"
                    , "3명"
                    , "4명"
                    , "5명"
                    , "6명+"

            });
            this.binding.layoutSurvey2.setCheckBoxList(new String[]{
                    "0명"
                    , "1명"
                    , "2명"
                    , "3명"
                    , "4명"
                    , "5명"
                    , "6명+"
            });

            this.binding.layoutSurvey3.setCheckBoxList(new String[]{
                    "10대"
                    , "20대"
                    , "30대"
                    , "40대"
                    , "50대+"
            });
            this.binding.textIntroTitle.setText(Html.fromHtml(
                    "안녕하세요 : )<br><br>" +
                            "<font color='#ff0000'><b>빠른 응대</b></font>와  <font color='#ff0000'><b>더 좋은 서비스</b></font>를 위해 " +
                            "정보를 입력해주세요"));
        }
        else
        {

            this.binding.layoutSurvey1.setTitle("Mens");

            this.binding.layoutSurvey1.setCheckBoxList(new String[]{
                    "0"
                    , "1"
                    , "2"
                    , "3"
                    , "4"
                    , "5"
                    , "6+"

            });
            this.binding.layoutSurvey2.setTitle("Womens");
            this.binding.layoutSurvey2.setCheckBoxList(new String[]{
                    "0"
                    , "1"
                    , "2"
                    , "3"
                    , "4"
                    , "5"
                    , "6+"
            });
            this.binding.layoutSurvey3.setTitle("Average age group");
            this.binding.layoutSurvey3.setCheckBoxList(new String[]{
                    "10"
                    , "20"
                    , "30"
                    , "40"
                    , "50+"
            });
            this.binding.textIntroTitle.setText(Html.fromHtml(
                    "Welcome : )<br><br>" +
                            "For the fast and good service, please select some questionaries"));
        }
    }


    private void setSideMenuSubListViews() {
        /**
         * 사이드 메뉴 뷰 셋팅
         * ***/
        ArrayList<OrderCategoryData.CategoryItem> buttons = new ArrayList<>();

        for (OrderCategoryData.CategoryItem category : TabletSetting.getInstance().mOrderMainCategoryList) {
            buttons.add(category);
            SideMenuListItem newItem = new SideMenuListItem(this);
            newItem.setTitle(category.categoryName);
            newItem.setOnClickListener(view -> {

                if (!view.isSelected()) {
                    view.setSelected(true);
                    settingSideMenuListSubState(view);
                    NavigationUtils.openOrderFragment(getSupportFragmentManager(), R.id.layoutFragment, category.categoryCode);
                    this.binding.setPresentButtonSelected(false);
                    settingSideMenuListState(newItem);
                }
            });
            this.binding.layoutOrderSub.addView(newItem);
        }

        if (this.binding.layoutOrderSub.getChildCount() > 0) {
            this.binding.layoutOrderSub.postDelayed(() -> {
                this.binding.layoutOrderSub.getChildAt(0).performClick();
            }, 500);
        }

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {

                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        ///fcm 키 등록
                        mainViewModel.registrationToken(
                                QueueUtils.registrationToken(TabletSettingUtils.getInstacne().getBranchInfo().posIp, TabletSettingUtils.getInstacne().getTableNo())
                                , token
                        );
                    }
                });

    }

    /**
     * 시작버튼 눌렀을때에 대한 처리
     **/
    public void goStart(View view) {


        int survey1 = 0;
        int survey2 = 0;
        int survey3 = 0;


        if (this.binding.layoutSurvey1.getSelectedItem() == null) {
            Toast.makeText(this, this.binding.layoutSurvey1.getTitle() + "을(를) 선택해주세요.", Toast.LENGTH_SHORT).show();
            return;
        }


        if (this.binding.layoutSurvey2.getSelectedItem() == null) {
            Toast.makeText(this, this.binding.layoutSurvey2.getTitle() + "을(를) 선택해주세요.", Toast.LENGTH_SHORT).show();
            return;
        }


        if (this.binding.layoutSurvey3.getSelectedItem() == null) {
            Toast.makeText(this, this.binding.layoutSurvey3.getTitle() + "을(를) 선택해주세요.", Toast.LENGTH_SHORT).show();
            return;
        }

        ////모두 선택되었으니 숫자를 파싱하자
        try {
            survey1 = Integer.parseInt(this.binding.layoutSurvey1.getSelectedItem().replace("명", "").replace("+", ""));
        } catch (Exception e) {

        }

        try {
            survey2 = Integer.parseInt(this.binding.layoutSurvey2.getSelectedItem().replace("명", "").replace("+", ""));
        } catch (Exception e) {

        }

        try {
            survey3 = Integer.parseInt(this.binding.layoutSurvey3.getSelectedItem().replace("대", "").replace("+", ""));
        } catch (Exception e) {

        }


        if (survey1 == 0 && survey2 == 0) {
            Toast.makeText(this, (LoginUtil.getInstance().getSelectedLanguage() == null || LoginUtil.getInstance().getSelectedLanguage().equals("ko") ? "인원을 선택해주세요." : "select member counts"), Toast.LENGTH_SHORT).show();
            return;
        }



        ///파싱된 숫자를 기반으로 체크하기
        TableInfoData newTableInfoData = new TableInfoData();
        newTableInfoData.men = survey1;
        newTableInfoData.women = survey2;
        newTableInfoData.age = survey3;
        TabletSettingUtils.getInstacne().setTableInfoData(newTableInfoData);

        LogUtil.d("사용을 시작합니다");
        this.mainViewModel.firstAccess(TabletSettingUtils.getInstacne().getBranchInfo().branchId, TabletSettingUtils.getInstacne().getTableNo());

        this.binding.layoutIntro.setVisibility(
                View.GONE
        );
        ///채팅 내역을 다 지워버릴까
    }

    /**
     * 이벤트 버튼을 눌러 선택할수있는 화면
     **/
    public void goADSelectionPage(View view) {
        LogUtil.d("이벤트창을 열어달라");
        if (ADDialog.getmInstanceWithoutInit() != null && ADDialog.getmInstanceWithoutInit().isShowing()) {
        } else {
//        NavigationUtils.openNewActivity(this, EventActivity.class, 0);
            this.adViewModel.isRandomMode.setValue(false);
            this.adViewModel.getAdList((TabletSettingUtils.getInstacne().getTableInfoData().men +
                            TabletSettingUtils.getInstacne().getTableInfoData().women)
                    , (TabletSettingUtils.getInstacne().getTableInfoData().men != 0 && TabletSettingUtils.getInstacne().getTableInfoData().women != 0 ? "both" :
                            (TabletSettingUtils.getInstacne().getTableInfoData().men != 0 && TabletSettingUtils.getInstacne().getTableInfoData().women == 0 ? "male" :
                                    (TabletSettingUtils.getInstacne().getTableInfoData().men == 0 && TabletSettingUtils.getInstacne().getTableInfoData().women != 0 ? "female" :
                                            "na")))
                    , "event"
            );
        }


    }


    /**
     * 버전코드
     **/
    public void updateVersionCode() {
        PackageInfo pi = null;
        //버전코드 등록
        try {
            pi = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (pi != null) {
            String versionCode = String.valueOf(pi.versionCode);
            String versionName = pi.versionName;
            this.binding.textVersion.setText(versionName + "(" + versionCode + ")");
        }
    }

    public void clickLeftSideMenuItem(View view) {
        switch (view.getId()) {
            case R.id.buttonSideMenuListItemReceipt:
                if (!this.binding.buttonSideMenuListItemReceipt.isSelected()) {
                    clearSelectionSideMenuListSub();
                    view.setSelected(true);
                    NavigationUtils.detailOpenFragment(getSupportFragmentManager(), R.id.layoutFragment, BillFragment.class.getSimpleName(), null);
                }
                break;
            case R.id.buttonSideMenuListItemOrderState:
                if (!this.binding.buttonSideMenuListItemOrderState.isSelected()) {
                    clearSelectionSideMenuListSub();
                    view.setSelected(true);
                    NavigationUtils.detailOpenFragment(getSupportFragmentManager(), R.id.layoutFragment, OrderStateFragment.class.getSimpleName(), null);
                }
                break;

        }
        settingSideMenuListState(view);
    }


    public void openBill() {
        if (!this.binding.buttonSideMenuListItemReceipt.isSelected()) {
            clearSelectionSideMenuListSub();
            this.binding.buttonSideMenuListItemReceipt.setSelected(true);
            NavigationUtils.detailOpenFragment(getSupportFragmentManager(), R.id.layoutFragment, BillFragment.class.getSimpleName(), null);
        }
        settingSideMenuListState(this.binding.buttonSideMenuListItemReceipt);

    }


    /**
     * 왼쪽 주문 하위버튼 모두 false로 바꾸는 기능
     */
    private void clearSelectionSideMenuListSub() {
        for (int i = 0; i < this.binding.layoutOrderSub.getChildCount(); i++) {
            this.binding.layoutOrderSub.getChildAt(i).setSelected(false);
        }
    }

    //
//    /***
//     * 사이브 메뉴 뷰 visible/invisible 상태에 따른 resize
//     * **/
    private void settingSideMenuListState(View v) {

        for (int i = 0; i < this.binding.layoutScrollView.getChildCount(); i++) {
            ///1. layoutOrderSub를 뒤진다
            if (this.binding.layoutScrollView.getChildAt(i).equals(this.binding.layoutOrderSub)) {
                for (int indexOrderSub = 0; indexOrderSub < this.binding.layoutOrderSub.getChildCount(); indexOrderSub++) {
                    if (!this.binding.layoutOrderSub.getChildAt(indexOrderSub).equals(v)) {
                        this.binding.layoutOrderSub.getChildAt(indexOrderSub).setSelected(false);
                    }
                }
            }
            ///2. 계산서, 주문서를 확인한다
            else {
                if (!this.binding.layoutScrollView.getChildAt(i).equals(v)) {
                    this.binding.layoutScrollView.getChildAt(i).setSelected(false);
                }
            }
        }
    }

    /***
     *
     * 사이브 메뉴 SUB 뷰 visible/invisible 상태에 따른 resize
     * **/
    private void settingSideMenuListSubState(View v) {

        for (int i = 0; i < this.binding.layoutOrderSub.getChildCount(); i++) {
            if (!this.binding.layoutOrderSub.getChildAt(i).equals(v)) {
                this.binding.layoutOrderSub.getChildAt(i).setSelected(false);
            }
        }

    }

    public void goChat(View view) {
//        if (TabletSettingUtils.getInstacne().getConvertedTableMaps() == null) {
//            this.loginViewModel.getAllMapInfo(TabletSettingUtils.getInstacne().getBranchInfo().storeBranchUid)
//                    .subscribeOn(Schedulers.computation())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(mapList -> {
////                        TabletSettingUtils.getInstacne().setTableMaps(mapList);
//                        TabletSettingUtils.getInstacne().setTableMaps(mapList);
//
//                        this.binding.layoutChatBack.setVisibility(View.VISIBLE);
//                        this.binding.layoutChat.setVisibility(View.VISIBLE);
//                        this.binding.layoutChatBack.bringToFront();
//
//                    }, throwable -> {
//                        LogUtil.d("테이블 선택 작업에 문제 : " + throwable.getMessage());
//
//                    });
//
//
//        } else {
//            this.binding.layoutChatBack.setVisibility(View.VISIBLE);
//            this.binding.layoutChat.setVisibility(View.VISIBLE);
//            this.binding.layoutChatBack.bringToFront();
//        }

        if(TabletSettingUtils.getInstacne().getOrderId() != null)
        {
            this.binding.layoutChatBack.setVisibility(View.VISIBLE);
            this.binding.layoutChat.setVisibility(View.VISIBLE);
            this.binding.layoutChatBack.bringToFront();
        }
        else
        {
            this.compositeSubscription.add(billViewModel.getOrderedList(TabletSettingUtils.getInstacne().getTableNo())
                .subscribe(response -> {
                    if (response != null && response.orderId != null) {
                        this.binding.layoutChatBack.setVisibility(View.VISIBLE);
                        this.binding.layoutChat.setVisibility(View.VISIBLE);
                        this.binding.layoutChatBack.bringToFront();
                    } else {
                        MessageDialogBuilder.getInstance(this)
                                .setTitle("알림")
                                .setContent("주문후 이용해주세요.")
                                .setDefaultButton()
                                .complete();
                    }

                }, error -> {
                    Toast.makeText(this, "지금은 이용할 수 없습니다.(e.na.Q)", Toast.LENGTH_SHORT).show();

                }, () -> {

                }));
        }


    }

    public void closeChat(View view) {
        this.binding.layoutChatBack.setVisibility(View.GONE);
        this.binding.layoutChat.setVisibility(View.GONE);

    }


    public void restarter() {

        MessageDialogBuilder.getInstance(this)
                .setContent("앱을 재시작하시겠습니까?")
                .setOkButton("확인", view -> {
                    MessageDialogBuilder.getInstance(this).dismiss();
                    throw new RestartExceptions("proccessRestart");
                })
                .setCancelButton("취소", view -> {
                    MessageDialogBuilder.getInstance(this).dismiss();
                })
                .complete();
    }


    /**
     * 채팅에 대한 알림 처리
     **/
    private void setPrivateChatListener() {
        if (TabletSettingUtils.getInstacne().getTableNo() != null) {
            FirebaseDatabase.getInstance().getReference()
                    .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
                    .child("chat")
                    .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
                    .child("personal")
                    .child(TabletSettingUtils.getInstacne().getTableNo())
                    .child("lastMessages")
                    .orderByChild("read")
                    .equalTo(false).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot != null && dataSnapshot.getChildrenCount() > 0) {
                        Boolean flag = false;
                        for (DataSnapshot item : dataSnapshot.getChildren()) {
                            if (!item.getValue(Message.class).sender.equals(TabletSettingUtils.getInstacne().getTableNo())) {
//                                binding.setIsNewChat(true);
                                flag = true;
                                break;
                            }
                        }
                        binding.setIsNewChat(flag);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            FirebaseDatabase.getInstance().getReference()
                    .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
                    .child("chat")
                    .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
                    .child("personal")
                    .child(TabletSettingUtils.getInstacne().getTableNo())
                    .child("lastMessages")
                    .orderByChild("read")
                    .equalTo(false)
                    .addChildEventListener(privateChatListener);
        }
    }

    ChildEventListener privateChatListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            LogUtil.d("채팅 onChildAdded : " + dataSnapshot);
            if (dataSnapshot != null) {
                if (!dataSnapshot.getValue(Message.class).sender.equals(TabletSettingUtils.getInstacne().getTableNo())
                        && !binding.layoutChat.isShowing(dataSnapshot.getValue(Message.class).sender)) {    //뒤 조건식은 내가 해당 유저랑 채팅중인지 아닌지 체크 하는것.
                    binding.setIsNewChat(true);
                    showNewMessageNoti();
                }
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            LogUtil.d("채팅 onChildChanged : " + dataSnapshot);
            if (dataSnapshot != null) {
                if (!dataSnapshot.getValue(Message.class).sender.equals(TabletSettingUtils.getInstacne().getTableNo()) && !binding.layoutChat.isShowing(dataSnapshot.getValue(Message.class).sender)) {
                    binding.setIsNewChat(true);
                    showNewMessageNoti();
                }
            }
        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            LogUtil.d("채팅 onChildRemoved : " + dataSnapshot);
            binding.setIsNewChat(false);

        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            LogUtil.d("채팅 onChildMoved : " + dataSnapshot);
            if (dataSnapshot != null) {
                if (!dataSnapshot.getValue(Message.class).sender.equals(TabletSettingUtils.getInstacne().getTableNo()) && !binding.layoutChat.isShowing(dataSnapshot.getValue(Message.class).sender)) {
                    binding.setIsNewChat(true);
                    showNewMessageNoti();
                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
//            binding.setIsNewChat(false);

        }
    };

    /**
     *
     * */
    public void goHome() {
        NavigationUtils.removeAllFragment(getSupportFragmentManager());
        this.binding.layoutOrderSub.getChildAt(0).performClick();

    }

    private boolean isShowingNewMessageNoti = false;

    private void showNewMessageNoti() {
        if (!isShowingNewMessageNoti && this.binding.layoutChatBack.getVisibility() != View.VISIBLE) {
            this.isShowingNewMessageNoti = true;
            this.binding.layoutNewMessage.bringToFront();
            CustomAnimationUtils.moveTopToBottom(this.binding.layoutNewMessage);

            handler.postDelayed(() -> {
                CustomAnimationUtils.moveBottomToTop(this.binding.layoutNewMessage);
                isShowingNewMessageNoti = false;
            }, 3500);

        }

    }


//    public void openTableMap()
//    {
//        if (TabletSettingUtils.getInstacne().getConvertedTableMaps() == null) {
//            this.loginViewModel.getAllMapInfo(TabletSettingUtils.getInstacne().getBranchInfo().storeBranchUid)
//                    .subscribeOn(Schedulers.computation())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(mapList -> {
////                        TabletSettingUtils.getInstacne().setTableMaps(mapList);
//                        TabletSettingUtils.getInstacne().setTableMaps(mapList);
//
//                        MapDialog.getInstance(this)
//                                .setMODE(MapDialog.MODE.VIEW)
//                                .setSelectedTableNo(null, null)
//                                .complete();
//
//                    }, throwable -> {
//                        LogUtil.d("테이블 선택 작업에 문제 : " + throwable.getMessage());
//
//                    });
//
//
//        } else {
//            MapDialog.getInstance(this)
//                    .setMODE(MapDialog.MODE.VIEW)
//                    .setSelectedTableNo(null, null)
//                    .complete();
//        }
//    }

    public void touchBlock()
    {

    }



    /**
     * 언어 변경
     **/
    public void goSelectLanguage(View view) {

        if (TabletSettingUtils.getInstacne().getBranchInfo() != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption.lang != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption.lang.size() > 0
        ) {
            LanguageDialog.getInstance(this)
                    .setSelectItemListener(language -> {
                        LogUtil.d("선택된 언어 : " + language);
                        LoginUtil.getInstance().setSelectedLanguage(this, language);
                        Toast.makeText(this, "[" + language + "] Language change. Application will restart", Toast.LENGTH_SHORT).show();
                        LanguageDialog.getInstance(this).dismiss();

                        throw new RestartExceptions("proccessRestart");
                    })
                    .complete();
        }

    }

}
