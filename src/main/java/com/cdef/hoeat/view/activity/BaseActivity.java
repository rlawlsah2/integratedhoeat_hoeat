package com.cdef.hoeat.view.activity;

import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

import com.cdef.commonmodule.utils.LogUtil;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by kimjinmo on 2018. 3. 26..
 */

public class BaseActivity extends AppCompatActivity {

    View decorView;
    int uiOption;
    CompositeSubscription compositeSubscription = new CompositeSubscription();


    /**
     * 소프트 버튼을 없앤다던지 UI와 관련된 작업을 처리한다.
     **/
    public void setUI() {

        if (decorView == null) {
            decorView = getWindow().getDecorView();
            uiOption = getWindow().getDecorView().getSystemUiVisibility();
            uiOption = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                uiOption |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                uiOption |= View.SYSTEM_UI_FLAG_FULLSCREEN;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                uiOption |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        LogUtil.d("소프트 버튼 상태 : " + decorView);
        decorView.setSystemUiVisibility(uiOption);
    }

    @Override
    protected void onDestroy() {
        if (this.compositeSubscription != null)
            this.compositeSubscription.clear();
        super.onDestroy();
    }
}
