package com.cdef.hoeat.view.viewPager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cdef.commonmodule.dataModel.BannerDefaultData;
import com.cdef.hoeat.R;

import java.util.ArrayList;
import java.util.Random;

public class EventViewPagerAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<BannerDefaultData> bannerList;
    private RequestManager glide;
    private RequestOptions requestOptions;
    Random random = new Random();


    public EventViewPagerAdapter(Context context, ArrayList<BannerDefaultData> bannerList, RequestManager glide) {
        mContext = context;
        this.bannerList = bannerList;
        this.requestOptions = new RequestOptions();
        this.requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(false);
        this.glide = glide;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        View imageLayout = LayoutInflater.from(mContext).inflate(R.layout.adapter_viewpager_ad, collection, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.image);


        this.glide.load(this.bannerList.get(position).files.get(
                random.nextInt(this.bannerList.get(position).files.size())
                ).fileUrl
        ).apply(this.requestOptions)
                .into(imageView);

        collection.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return this.bannerList.size();
//        return ModelObject.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
//
//    @Override
//    public CharSequence getPageTitle(int position) {
//        ModelObject customPagerEnum = ModelObject.values()[position];
//        return mContext.getString(customPagerEnum.getTitleResId());
//    }

}