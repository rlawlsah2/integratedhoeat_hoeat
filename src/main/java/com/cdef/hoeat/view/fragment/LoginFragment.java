package com.cdef.hoeat.view.fragment;


import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.cdef.commonmodule.exceptions.LoginInfoNotFoundException;
import com.cdef.commonmodule.utils.LoginUtil;
import com.cdef.hoeat.Exceptions.NotFoundStoreMenuListExceptions;
import com.cdef.hoeat.R;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.hoeat.view.activity.Splash;
import com.cdef.hoeat.view.dialog.loadingDialog.LoadingDialog;
import com.cdef.hoeat.viewModel.BillViewModel;
import com.cdef.hoeat.viewModel.LoginViewModel;
import com.cdef.hoeat.databinding.FragmentLoginBinding;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;


/**
 * Created by kimjinmo on 2017. 9. 18..
 */

public class LoginFragment extends BaseFragment {

    LoginViewModel loginViewModel;
    BillViewModel billViewModel;
    FragmentLoginBinding binding = null;

    public String token;
    public String tableNo;
    public String branchName;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        ////로컬에 저장된 데이터가 있는지 체크
        Bundle bundle = getArguments();
        if (bundle != null) {

            loadingDialog = new LoadingDialog(getContext(), getString(R.string.loadingDialog_title_login));
            loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
            loginViewModel.initRepository(getContext());
            billViewModel = ViewModelProviders.of(this).get(BillViewModel.class);
            billViewModel.initRepository(getContext());


            LogUtil.d("주문앱 로그인 bundle : " + bundle);

            this.token = bundle.getString("apiKey");
            this.tableNo = bundle.getString("tableNo");
            this.branchName = bundle.getString("branchName");

            LogUtil.d("주문앱 로그인 token : " + token);
            LogUtil.d("주문앱 로그인 tableNo : " + tableNo);
            LogUtil.d("주문앱 로그인 branchName : " + branchName);
            if (token != null && tableNo != null) {
                return;
            }

        }


        Toast.makeText(getContext(), "매니저를 통해 실행해주세요", Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding.setLoginViewModel(this.loginViewModel);


        if (token != null && tableNo != null) {
            this.binding.setTableNo(tableNo);
            this.binding.setBranchName(branchName);
            if(LoginUtil.getInstance(getContext()).getSelectedLanguage() == null || LoginUtil.getInstance(getContext()).getSelectedLanguage().equals("ko"))
            {
                this.binding.buttonLogin.setText("자동 로그인중..");
            }else
            {
                this.binding.buttonLogin.setText("Logining..");
            }
            goLogin(token, tableNo);
            return;
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view

        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_login, container, false);
        binding.setFragment(this);
        //here data must be an instance of the class MarsDataProvider
        return binding.getRoot();
    }


    public void goLogin(String token, String tableNo) {
        this.loadingDialog.show();
        this.compositeSubscription.add(
                this.loginViewModel.getLoginInfoAsObservable(token, tableNo)
                        .doOnSubscribe(() -> this.loadingDialog.show())
                        .flatMap(loginData -> {
                            if (loginData != null)   //로그인 성공
                            {
                                ///fcm키를 table 정보에 저장
                                LoginUtil.getInstance(getContext()).loadLocalSelectedLanguage(getContext());
                                return loginViewModel.getMainCategoryListAsObservable(getContext());
                            } else {
                                ///로그인 실패
//                        Toast.makeText(getContext(), "로그인 실패\n입력된 정보를 확인해주세요.", Toast.LENGTH_SHORT).show();
                                return Observable.error(new LoginInfoNotFoundException("login info not found"));
                            }
                        })
                        ///여기까지는 로그인 & 메뉴가져오기 성공
                        .flatMap(result -> {
                            if (result) {
                                return this.billViewModel.getOrderListAsObservable(
                                        tableNo
                                );

                            } else {
                                return Observable.error(new NotFoundStoreMenuListExceptions("not found store menulist"));
                            }

                        })
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .flatMap(result -> {
                            this.binding.buttonLogin.setText(R.string.button_login_success);
                            if (result) {

                            } else {
                                Toast.makeText(getContext(), "일시적으로 테이블 정보를 불러올 수 없습니다.(E0002)", Toast.LENGTH_SHORT).show();
                            }
                            this.loadingDialog.dismiss();
                            return Observable.just(true);
                        })
                        .delay(200, TimeUnit.MILLISECONDS)
                        .subscribeOn(AndroidSchedulers.mainThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(result -> {
                            ((Splash) getActivity()).goMain();
                        }, error -> {
                            LogUtil.d("로그인 완전 실패 : " + error);
                            error.printStackTrace();
                            this.loadingDialog.dismiss();
                            this.binding.buttonLogin.setText(R.string.button_login_fail);

                        }, () -> {

                        })
        );
    }


    public void goLogin(View view) {
        goLogin(token, tableNo);

    }

    /**
     * 바탕화면 눌렀을때 키 입력창 사라지게
     **/
    public void clickOUT(View v) {
        if (getActivity().getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) (getContext().getSystemService(Context.INPUT_METHOD_SERVICE));
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}
