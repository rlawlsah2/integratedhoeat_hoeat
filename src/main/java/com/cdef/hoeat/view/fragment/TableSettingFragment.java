package com.cdef.hoeat.view.fragment;

import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.widget.TextViewCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cdef.commonmodule.dataModel.firebaseDataSet.Notification;
import com.cdef.commonmodule.dataModel.TableSettingData;
import com.cdef.hoeat.R;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.hoeat.utils.QueueUtils;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.cdef.hoeat.view.adapter.TableSettingAdapter;
import com.cdef.hoeat.view.customView.TableSettingItem;
import com.cdef.hoeat.view.dialog.loadingDialog.LoadingDialog;
import com.cdef.hoeat.view.dialog.MessageDialogBuilder;
import com.cdef.hoeat.viewModel.OrderViewModel;
import com.cdef.hoeat.databinding.FragmentTablesettingBinding;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by kimjinmo on 2018. 2. 8..
 */

public class TableSettingFragment extends BaseFragment {


    ArrayList<TableSettingItem> mTableSettingItemList = new ArrayList<>();
    FragmentTablesettingBinding binding;

    OrderViewModel orderViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.orderViewModel = ViewModelProviders.of(this).get(OrderViewModel.class);
        this.orderViewModel.initRepository(getContext());
        this.orderViewModel.queueState.observe(this, queueState -> {
            switch (queueState) {
                case NONE:
                    this.loadingDialog.dismiss();
                    break;
                case LOADING:
                    this.loadingDialog.show();
                    break;
                case ERROR:
                    this.loadingDialog.dismiss();
                    Toast.makeText(getContext(), "주문 큐 상태를 확인해주세요", Toast.LENGTH_SHORT).show();
                    this.orderViewModel.queueState.setValue(OrderViewModel.QUEUESTATE.NONE);
                    break;
                case COMPLETE:
                    this.loadingDialog.dismiss();
                    this.orderViewModel.queueState.setValue(OrderViewModel.QUEUESTATE.NONE);
                    adapter.clearSelectedList();
                    binding.recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    MessageDialogBuilder.getInstance(getContext())
                            .setContent("직원에게 전달하였습니다.\n잠시만 기다려주세요 :)")
                            .setDefaultButton()
                            .complete();
                    break;
            }
        });


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        loadingDialog = new LoadingDialog(getContext(), "요청중입니다..");


        LogUtil.d("테이블 callOptions : " + TabletSettingUtils.getInstacne().getBranchInfo().callOptions);

        Iterator<Map.Entry<String, JsonElement>> items = TabletSettingUtils.getInstacne().getBranchInfo().callOptions.entrySet().iterator();
        Gson gson = new Gson();

        while (items.hasNext()) {
            list.add(gson.fromJson(TabletSettingUtils.getInstacne().getBranchInfo().callOptions.get(items.next().getKey()), TableSettingData.class));
        }

        this.adapter = new TableSettingAdapter(getContext(), Glide.with(getContext()));
        this.adapter.setmListData(list);
        this.binding.recyclerView.setAdapter(this.adapter);

    }


    public void clickCall(View view) {

        if (TabletSettingUtils.getInstacne().getBranchInfo() != null) {
            MessageDialogBuilder.getInstance(getContext())
                    .setContent("직원을 호출하시겠습니까?")
                    .setDefaultButtons(view1 -> {
                        MessageDialogBuilder.getInstance(getContext()).dismiss();
                        loadingDialog.show();
                        Notification notification = new Notification();
                        notification.content = "직원 호출입니다.";
                        notification.read = false;
                        notification.tableNo = TabletSettingUtils.getInstacne().getTableNo();
                        notification.title = getString(R.string.notificationTitleServiceCall);
                        notification.type = getString(R.string.notificationTypeServiceCall);
                        notification.time = Utils.getDate("yyyy-MM-dd HH:mm:ss");
                        this.orderViewModel.request(QueueUtils.request(TabletSettingUtils.getInstacne().getBranchInfo().posIp), notification);
                    })
                    .complete();
        }
    }


    public void requestTableSettingItemSelection(View view) {


        if (this.adapter.getSelectedList().size() > 0) {
            this.loadingDialog.show();
            String message = "";

            for (String key : this.adapter.getSelectedList().keySet()) {
                message += key + ": " + this.adapter.getSelectedList().get(key) + " / ";

            }

            if (message != null && !message.equals("")) {
                Notification notification = new Notification();
                notification.content = message;
                notification.read = false;
                notification.tableNo = TabletSettingUtils.getInstacne().getTableNo();
                notification.title = getContext().getString(R.string.notificationTitleService);
                notification.type = getContext().getString(R.string.notificationTypeService);
                notification.time = Utils.getDate("yyyy-MM-dd HH:mm:ss");

                this.orderViewModel.request(QueueUtils.request(TabletSettingUtils.getInstacne().getBranchInfo().posIp), notification);


            } else {
                loadingDialog.dismiss();
            }

        } else {
            Toast.makeText(getContext(), "원하시는 항목과 수량을 선택해주세요.", Toast.LENGTH_SHORT).show();
        }

    }

    GridLayoutManager manager;

    public ArrayList<TableSettingData> list = new ArrayList<>();
    public TableSettingAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view

        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_tablesetting, container, false);
        binding.setFragment(this);
        binding.textIntroTop.setText(Html.fromHtml("필요하신 서비스를 누른 후 <font color=\"#CF4247\"><b>서비스 요청</b></font>버튼을 누르세요"));
        binding.textIntroBottom.setText(Html.fromHtml("그 외 <font color=\"#8A5D3B\"><b>직원 호출</b></font>을 원하시는 경우 호출버튼을 눌러주세요"));


        TextViewCompat.setAutoSizeTextTypeWithDefaults(binding.textIntroTop, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        TextViewCompat.setAutoSizeTextTypeWithDefaults(binding.textIntroBottom, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);

        if(this.manager == null)
        {
            manager = new GridLayoutManager(getContext(), 7);
            this.binding.recyclerView.setLayoutManager(manager);
        }



        //here data must be an instance of the class MarsDataProvider
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        if(this.manager != null)
        {
            this.manager = null;
        }

        if(this.adapter != null)
        {
            this.adapter.onDestroy();
            this.adapter = null;
        }

        if (mTableSettingItemList != null) {
            mTableSettingItemList.clear();
            mTableSettingItemList = null;
        }
        super.onDestroyView();
    }


}
