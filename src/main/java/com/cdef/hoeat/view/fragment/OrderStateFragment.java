package com.cdef.hoeat.view.fragment;


import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;

import com.cdef.commonmodule.dataModel.firebaseDataSet.Order;
import com.cdef.hoeat.R;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.cdef.hoeat.view.adapter.OrderStateListAdapter;
import com.cdef.hoeat.view.dialog.loadingDialog.LoadingDialog;
import com.cdef.hoeat.viewModel.OrderStateViewModel;
import com.cdef.hoeat.databinding.FragmentOrderStateBinding;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.FirebaseDatabase;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

/**
 * Created by kimjinmo on 2017. 9. 18..
 * 주문 상태 확인창
 */

public class OrderStateFragment extends BaseFragment {


    FragmentOrderStateBinding binding;
    OrderStateViewModel orderStateViewModel;
    OrderStateListAdapter orderStateListAdapter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.loadingDialog = new LoadingDialog(getContext(), getString(R.string.loadingDialog_title_ordering));
        orderStateViewModel = new OrderStateViewModel();

        this.orderStateViewModel.initSetting.observe(this, initsetting -> {

            switch (initsetting) {
                case NONE:
                    this.loadingDialog.dismiss();
                    break;
                case LOADING:
                    this.loadingDialog.show();

                    break;
                case ERROR:
                    this.loadingDialog.dismiss();

                    break;
                case COMPLETE:
                    this.loadingDialog.dismiss();
                    break;
            }

        });
        this.orderStateViewModel.mOrderId.observe(this, orderId -> {

            if (orderId != null) { ///cart 리스트 처리
                FirebaseRecyclerOptions<Order> tmp = new FirebaseRecyclerOptions.Builder().setQuery(FirebaseDatabase.getInstance().getReference()
                        .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
                        .child("orderData")
                        .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
                        .child(orderId)
                        .child("orders")
                        .orderByChild("state"), Order.class).build();

                orderStateListAdapter = new OrderStateListAdapter(tmp);
//                orderStateListAdapter = new OrderStateListAdapter(Order.class, R.layout.adapter_order_state_list, OrderStateListAdapterViewHolder.class,
//                        FirebaseDatabase.getInstance().getReference()
//                                .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                .child("orderData")
//                                .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                .child(orderId)
//                                .child("orders")
//                                .orderByChild("state")
//                );
                orderStateListAdapter.registerAdapterDataObserver(adapterDataObserver);
                AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(orderStateListAdapter);
                alphaAdapter.setInterpolator(new OvershootInterpolator());
                this.binding.recyclerView.setAdapter(alphaAdapter);
            } else {

            }
        });
        this.binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        this.orderStateViewModel.getOrderId(
                (TabletSettingUtils.getInstacne().getBranchInfo().branchId)
                , (TabletSettingUtils.getInstacne().getTableNo())
        );


    }


    private RecyclerView.AdapterDataObserver adapterDataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            LogUtil.d("메뉴 adapterDataObserver : ");

        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            super.onItemRangeChanged(positionStart, itemCount);
            LogUtil.d("메뉴 onItemRangeChanged : ");

        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            super.onItemRangeInserted(positionStart, itemCount);
            LogUtil.d("메뉴 onItemRangeInserted : ");

        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            super.onItemRangeRemoved(positionStart, itemCount);
            LogUtil.d("메뉴 onItemRangeRemoved : ");

        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view

        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_order_state, container, false);
        binding.setFragment(this);

        //here data must be an instance of the class MarsDataProvider
        return binding.getRoot();
    }


}
