package com.cdef.hoeat.view.fragment;


import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.integration.recyclerview.RecyclerViewPreloader;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.util.ViewPreloadSizeProvider;
import com.cdef.commonmodule.dataModel.firebaseDataSet.Present;
import com.cdef.commonmodule.dataModel.MenuItemData;
import com.cdef.hoeat.OrderAppApplication;
import com.cdef.hoeat.R;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.hoeat.utils.QueueUtils;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.cdef.hoeat.view.adapter.MenuAdapter;
import com.cdef.hoeat.view.dialog.loadingDialog.LoadingDialog;
import com.cdef.hoeat.view.dialog.MapDialog;
import com.cdef.hoeat.view.dialog.MessageDialogBuilder;
import com.cdef.hoeat.viewModel.BillViewModel;
import com.cdef.hoeat.viewModel.LoginViewModel;
import com.cdef.hoeat.viewModel.MainViewModel;
import com.cdef.hoeat.viewModel.OrderViewModel;
import com.cdef.hoeat.databinding.FragmentPresentBinding;

import org.json.JSONException;
import org.json.JSONObject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kimjinmo on 2017. 9. 18..
 */

public class PresentFragment extends BaseFragment {

    MenuAdapter mAdapter;
    private boolean isOrdering = false;
    ViewPreloadSizeProvider<MenuItemData> preloadSizeProvider;


    FragmentPresentBinding binding;
    OrderViewModel orderViewModel = null;
    MainViewModel mainViewModel = null;
    LoginViewModel loginViewModel = null;
    BillViewModel billViewModel = null;
    RequestOptions requestOptions;

    private int totalPrice_textview = 0;
    private int totalPrice_pos = 0;
    private boolean isFirstOrder = false;
    private Present mPresent = null;

    ///선물하기 메뉴
    MenuItemData menuFrom = null;
    MenuItemData menuTo = null;

    @Override
    public void onStart() {
        super.onStart();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String category = bundle.getString("category");
            bundle.remove("category");
            LogUtil.d("카테고리 : " + category);
//            this.orderViewModel.getMenuList(category, LoginUtil.getInstance(getContext()).getsBranchUid());

        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.loadingDialog = new LoadingDialog(getContext(), getString(R.string.loadingDialog_title_ordering));
        this.orderViewModel = ViewModelProviders.of(this).get(OrderViewModel.class);
        this.orderViewModel.initRepository(getContext());
        this.orderViewModel.menuList.observe(this, menuList -> {
            if (this.mAdapter == null) {
                LogUtil.d("주문 받아온거 fragment에서 출력  menuList : " + menuList.size());
                RecyclerViewPreloader<MenuItemData> preloader = new RecyclerViewPreloader<MenuItemData>(Glide.with(this), this.mAdapter, this.preloadSizeProvider, 0);
                this.mAdapter = new MenuAdapter(getContext(), Glide.with(this), this.preloadSizeProvider);
                this.mAdapter.setListener((v, itemData, position) -> {
//                    Toast.makeText(getContext(), "선택한 메뉴 이름은 : " + itemData.foodName, Toast.LENGTH_SHORT).show();

//                    orderViewModel.addCart(TabletSettingUtils.getInstacne().getBranchInfo().branchId
//                            , TabletSettingUtils.getInstacne().getTableNo()
//                            , itemData
//                    );
                    this.binding.setSelectedMenu(itemData);
                    Glide.with(getContext()).load(itemData.foodImage).apply(this.requestOptions).into(this.binding.imageSelectedMenu);
                });

                this.binding.recyclerView.addOnScrollListener(preloader);
                this.binding.recyclerView.setAdapter(mAdapter);
//                this.binding.recyclerView.setItemViewCacheSize(0);

            }
            mAdapter.setmListData(menuList);
            mAdapter.notifyDataSetChanged();
        });

        this.loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        this.loginViewModel.initRepository(getContext());
        this.billViewModel = ViewModelProviders.of(this).get(BillViewModel.class);
        this.billViewModel.initRepository(getContext());

        this.mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        this.mainViewModel.initRepository(getContext());

        this.requestOptions = new RequestOptions();
        this.requestOptions.override(Utils.dpToPx(getContext(), 300), Utils.dpToPx(getContext(), 200)).placeholder(R.drawable.img_sample_menu);
        LogUtil.d("주문 프래그먼트 상태  onActivityCreated");


        ///메뉴 로딩 관련 상태
        this.orderViewModel.state.observe(this, state -> {

            switch (state) {
                case NONE:
                    this.loadingDialog.dismiss();
                    break;
                case LOADING:
                    this.loadingDialog.setTitleNew(getResources().getString(R.string.loadingDialog_title_loading));
                    this.loadingDialog.show();
                    break;
                case ERROR:
                    this.loadingDialog.dismiss();
                    break;
                case COMPLETE:
                    this.loadingDialog.dismiss();
                    break;
            }
        });

        ///주문상황표시
        this.orderViewModel.orderState.observe(this, orderState -> {
            switch (orderState) {
                case NONE:
                    this.loadingDialog.dismiss();
                    break;
                case NOCART:
                    Toast.makeText(OrderAppApplication.mContext, "주문할 메뉴를 선택해주세요.", Toast.LENGTH_SHORT).show();
                    this.loadingDialog.dismiss();
                    break;
                case LOADING:
                    this.loadingDialog.setTitleNew(getResources().getString(R.string.loadingDialog_title_ordering));
                    this.loadingDialog.show();
                    break;
                case ERROR:
                    Toast.makeText(OrderAppApplication.mContext, "일시적인 오류가 발생했습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                    this.loadingDialog.dismiss();
                    break;
                case COMPLETE:
                    this.loadingDialog.dismiss();
                    ///선물하기 관련 데이터 올리기
//                    if (menuFrom != null)    ///from만 처리된 경우
//                    {
//                        menuFrom = null;
//                        ArrayList<MenuItemData> list = new ArrayList<>();
//                        list.add(menuTo);
//                        this.orderViewModel.orderForEvent(this.binding.getSelectedTableNo(), list);
//
//                    } else {
//                        menuTo = null;
//                        presentComplete();
//
//                    }
                    break;
                case COMPLETEWITHERROR1:
                    this.loadingDialog.dismiss();
                    ///선물하기 관련 데이터 올리기
//                    presentComplete();
//
//                    MessageDialogBuilder.getInstance(getContext())
//                            .setContent("주문이 완료되었습니다.(E1)")
//                            .setDefaultButton()
//                            .complete();
                    break;
                case COMPLETEWITHERROR2:
                    this.loadingDialog.dismiss();
                    ///선물하기 관련 데이터 올리기
//                    presentComplete();
//                    MessageDialogBuilder.getInstance(getContext())
//                            .setContent("주문이 완료되었습니다.(E2)")
//                            .setDefaultButton()
//                            .complete();
                    break;
            }
        });


        ///큐 상태 체크
        this.orderViewModel.queueState.observe(this, queueState -> {
            switch (queueState) {
                case NONE:
                    this.loadingDialog.dismiss();
                    break;
                case LOADING:
                    this.loadingDialog.show();
                    break;
                case ERROR:
                    this.loadingDialog.dismiss();
                    Toast.makeText(getContext(), "주문 큐 상태를 확인해주세요", Toast.LENGTH_SHORT).show();
                    this.orderViewModel.queueState.setValue(OrderViewModel.QUEUESTATE.NONE);
                    break;
                case COMPLETE:
                    this.loadingDialog.dismiss();
                    this.binding.setSelectedTableNo(null);
                    this.binding.setSelectedMenu(null);
                    this.binding.inputMessage.setText("");
                    Glide.with(getContext()).clear(this.binding.imageSelectedMenu);
                    this.orderViewModel.queueState.setValue(OrderViewModel.QUEUESTATE.NONE);
                    MessageDialogBuilder.getInstance(getContext()).setTitle("선물하기")
                            .setContent("선물하였습니다. 채팅창에서 확인할 수 있습니다.")
                            .setDefaultButton()
                            .complete();

                    break;
            }
        });


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(preloadSizeProvider == null)
        {
            this.preloadSizeProvider = new ViewPreloadSizeProvider<>();
        }

        this.binding.setOrderViewModel(this.orderViewModel);
        this.binding.recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));

        this.binding.setInputCount(0);
        this.binding.inputMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.setInputCount(charSequence.length());

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        this.binding.imageSelectedMenu.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                LogUtil.d("선택 이미지 가로 : " + binding.imageSelectedMenu.getWidth());
                LogUtil.d("선택 이미지 가로 : " + binding.imageSelectedMenu.getMeasuredWidth());
                if (binding.imageSelectedMenu.getMeasuredWidth() != 0) {
                    binding.imageSelectedMenu.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) binding.imageSelectedMenu.getLayoutParams();
                    lp.height = (int) (binding.imageSelectedMenu.getMeasuredWidth() * (2 / 3.0));
                    binding.imageSelectedMenu.setLayoutParams(lp);
                }
            }
        });
        this.orderViewModel.getMenuFlagship(getContext(), "present", TabletSettingUtils.getInstacne().getBranchInfo().storeBranchUid);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        LogUtil.d("주문 프래그먼트 상태  onCreateView");
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_present, container, false);
        binding.setFragment(this);


        //here data must be an instance of the class MarsDataProvider
        return binding.getRoot();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        ///adapter 해제
        if (this.mAdapter != null) {
            this.mAdapter.onDestroy();
            this.mAdapter = null;
        }

        if(this.requestOptions != null)
        {
            this.requestOptions = null;
        }

        ///바인딩 확인후 recyclerView 해제
        if (this.binding != null && this.binding.recyclerView != null) {
            this.binding.recyclerView.setAdapter(null);
        }

    }

    public void goPresent() {


        this.compositeSubscription.add(billViewModel.getOrderedList(TabletSettingUtils.getInstacne().getTableNo())
                .subscribe(response -> {
                    if (response != null && response.orderId != null) {


                        if (this.binding.getSelectedMenu() == null) {
                            Toast.makeText(getContext(), "선물할 메뉴를 선택해주세요.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (this.binding.getSelectedTableNo() == null || this.binding.getSelectedTableNo().length() == 0) {
                            Toast.makeText(getContext(), "선물할 대상 테이블을 선택해주세요.", Toast.LENGTH_SHORT).show();
                            return;
                        }


                        MessageDialogBuilder.getInstance(getContext())
                                .setTitle("선물하기")
                                .setContent("[" + this.binding.getSelectedTableNo() + "]번 테이블에 " +
                                        "[" + this.binding.getSelectedMenu().foodName + "]메뉴를 선물하시겠습니까?")
                                .setDefaultButtons((view) -> {
                                    if (!view.isSelected()) {
                                        view.setSelected(true);
                                        JSONObject jsonObject = new JSONObject();
                                        try {
                                            jsonObject.put("type", "present");
                                            jsonObject.put("from", TabletSettingUtils.getInstacne().getTableNo() + "");
                                            jsonObject.put("to", this.binding.getSelectedTableNo() + "");
                                            jsonObject.put("foodUid", this.binding.getSelectedMenu().foodUid);
                                            if (this.binding.inputMessage.getText() != null) {
                                                jsonObject.put("message", this.binding.inputMessage.getText().toString());
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        this.orderViewModel.requestQueueEvent(QueueUtils.requestQueueEvent(TabletSettingUtils.getInstacne().getBranchInfo().posIp)
                                                , jsonObject.toString()
                                        );

                                        if (MessageDialogBuilder.getmInstanceWithoutInit() != null)
                                            MessageDialogBuilder.getmInstanceWithoutInit().dismiss();
                                    }

                                })
                                .complete();

                    } else {
                        MessageDialogBuilder.getInstance(getContext())
                                .setTitle("알림")
                                .setContent("주문후 이용해주세요.")
                                .setDefaultButton()
                                .complete();
                    }

                }, error -> {
                    Toast.makeText(getContext(), "지금은 이용할 수 없습니다.(e.na.Q)", Toast.LENGTH_SHORT).show();

                }, () -> {

                })
        );
    }

    public void goSelectTableNo(View view) {


        this.compositeSubscription.add(this.loginViewModel.getAllMapInfo(TabletSettingUtils.getInstacne().getBranchInfo().storeBranchUid)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mapList -> {
                    TabletSettingUtils.getInstacne().setTableMaps(mapList);
                    this.mainViewModel.getTableInfoList(QueueUtils.getTableInfoList(TabletSettingUtils.getInstacne().getBranchInfo().posIp))
                            .subscribeOn(Schedulers.computation())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(result -> {
                                TabletSettingUtils.getInstacne().setTableInfoList(result);
                            }, error -> {
                                LogUtil.d("왜 매장 테이블 정보가 없냐 : " + error);
                                Toast.makeText(getContext(), "일시적으로 매장 테이블 상태를 가져올 수 없습니다.", Toast.LENGTH_SHORT).show();

                            }, () -> {
                                MapDialog.getInstance(getContext())
                                        .setMODE(MapDialog.MODE.CHAT)
                                        .setSelectedTableNo(null, null)
                                        .setListener(
                                                (hallName, selectedTable) -> {
                                                    TabletSettingUtils.getInstacne().setSelectedTableNo(selectedTable.tNo);
                                                    MapDialog.getInstance(getContext()).dismiss();
                                                    binding.setSelectedTableNo(selectedTable.tNo);
                                                }).complete();
                            });


                }, throwable -> {
                    LogUtil.d("테이블 선택 작업에 문제 : " + throwable.getMessage());

                })
        );
    }
}
