package com.cdef.hoeat.view.fragment;


import androidx.databinding.ViewDataBinding;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.hoeat.view.dialog.loadingDialog.LoadingDialog;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by kimjinmo on 2017. 9. 18..
 */

public class BaseFragment<T extends ViewDataBinding> extends Fragment {
    protected LoadingDialog loadingDialog = null;
    public CompositeSubscription compositeSubscription = new CompositeSubscription();
    public T binding;

    public void close() {
        getActivity().onBackPressed();
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtil.d(this + "프래그먼트 시작");
    }

    public Boolean isLoading() {
        if (loadingDialog == null)
            return false;

        return loadingDialog.isShowing();
    }

    @Override
    public void onDestroy() {
        if (isLoading())
        {
            this.loadingDialog.dismiss();
            this.loadingDialog = null;
        }
        if(compositeSubscription != null)
        {
            compositeSubscription.clear();
        }
        super.onDestroy();
    }
}
