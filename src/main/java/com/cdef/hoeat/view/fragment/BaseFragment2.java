package com.cdef.hoeat.view.fragment;


import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.hoeat.view.dialog.loadingDialog.LoadingDialog;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by kimjinmo on 2017. 9. 18..
 */

public abstract class BaseFragment2<T extends ViewDataBinding> extends Fragment {
    protected LoadingDialog loadingDialog = null;
    public CompositeSubscription compositeSubscription = new CompositeSubscription();
    public T binding;


    public void close() {
        getActivity().onBackPressed();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtil.d(this + "프래그먼트 시작");
    }

    public Boolean isLoading() {
        if (loadingDialog == null)
            return false;

        return loadingDialog.isShowing();
    }

    @Override
    public void onDestroy() {
        if (isLoading())
        {
            this.loadingDialog.dismiss();
            this.loadingDialog = null;
        }
        if(compositeSubscription != null)
        {
            compositeSubscription.clear();
        }
        super.onDestroy();
    }
    public abstract int getLayout();


    //view inflate 진행
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        binding = DataBindingUtil.inflate(
                inflater, getLayout(), container, false);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.loadingDialog = new LoadingDialog(getContext(), "잠시만 기다려주세요..");
        } else {
            this.loadingDialog = new LoadingDialog(getActivity(), "잠시만 기다려주세요..");
        }
        LogUtil.d(this + "프래그먼트라이프사이클 onCreateView");
        return binding.getRoot();
    }

}

