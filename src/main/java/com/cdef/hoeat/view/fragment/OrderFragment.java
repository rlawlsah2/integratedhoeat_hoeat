package com.cdef.hoeat.view.fragment;


import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.integration.recyclerview.RecyclerViewPreloader;
import com.bumptech.glide.util.ViewPreloadSizeProvider;
import com.cdef.commonmodule.utils.LoginUtil;
import com.cdef.commonmodule.dataModel.firebaseDataSet.Order;
import com.cdef.commonmodule.dataModel.MenuItemData;
import com.cdef.commonmodule.dataModel.OrderCategoryData;
import com.cdef.hoeat.OrderAppApplication;
import com.cdef.hoeat.R;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.TabletSetting;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.hoeat.utils.ExToast;
import com.cdef.hoeat.utils.QueueUtils;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.cdef.hoeat.view.adapter.CartListAdapter3;
import com.cdef.hoeat.view.adapter.MenuAdapter;
import com.cdef.hoeat.view.dialog.BillDialog;
import com.cdef.hoeat.view.dialog.MenuDetailType2Dialog;
import com.cdef.hoeat.view.dialog.loadingDialog.LoadingDialog;
import com.cdef.hoeat.view.dialog.MenuDetailDialog;
import com.cdef.hoeat.view.dialog.MessageDialogBuilder;
import com.cdef.hoeat.viewModel.BaseViewModel;
import com.cdef.hoeat.viewModel.BillViewModel;
import com.cdef.hoeat.viewModel.OrderViewModel;
import com.cdef.hoeat.databinding.FragmentOrderBinding;

import java.util.ArrayList;

/**
 * Created by kimjinmo on 2017. 9. 18..
 */

public class OrderFragment extends BaseFragment {

    MenuAdapter mAdapter;
    private boolean isOrdering = false;

    ViewPreloadSizeProvider<MenuItemData> preloadSizeProvider;

    FragmentOrderBinding binding;
    OrderViewModel orderViewModel = null;
    BillViewModel billViewModel;    ///빌지를 띄우기위해 필요함


    CartListAdapter3 mCartListAdapter;
    private int totalPrice_textview = 0;
    private int totalPrice_pos = 0;
    private boolean isFirstOrder = false;

    ArrayList<MenuItemData> orderList;

    private boolean isOpenBill = false;
    int selectedSubcategoryIndex = 0;

    public String parentCategoryId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.loadingDialog = new LoadingDialog(getContext(), getString(R.string.loadingDialog_title_ordering));
        this.billViewModel = ViewModelProviders.of(this).get(BillViewModel.class);
        this.billViewModel.initRepository(getContext());

        this.orderViewModel = ViewModelProviders.of(this).get(OrderViewModel.class);
        this.orderViewModel.initRepository(getContext());
        this.orderViewModel.menuList.observe(this, menuList -> {

            if (menuList == null || menuList.size() == 0) {
                binding.recyclerView.setVisibility(View.GONE);
                binding.textEmptyMenu.setVisibility(View.VISIBLE);
            } else {
                binding.recyclerView.setVisibility(View.VISIBLE);
                binding.textEmptyMenu.setVisibility(View.GONE);
            }


            if (this.mAdapter == null) {
                this.mAdapter = new MenuAdapter(getContext(), Glide.with(this), this.preloadSizeProvider);

                RecyclerViewPreloader<MenuItemData> preloader = new RecyclerViewPreloader<MenuItemData>(Glide.with(this), this.mAdapter, this.preloadSizeProvider, 6);
                this.mAdapter.setListener((v, itemData, position) -> {

                    if (itemData.showDetail != 1 && itemData.isSet != 1) {
                        this.mCartListAdapter.addCart(itemData);
                        setCartTotalPrice();
                        ExToast.setToast(getContext(), (LoginUtil.getInstance().getSelectedLanguage() == null || LoginUtil.getInstance().getSelectedLanguage().equals("ko") ? "장바구니에 추가되었습니다." : "inserted in cart"), Toast.LENGTH_SHORT);


                    } else {


                        if (itemData.isSet == 1) {
//                            MenuDetailDialog.getInstance(getContext())
//                                    .setData(getCurrentCategoryName(), mAdapter.getmListData(), position)
//                                    .setCart(this.mCartListAdapter.getAllItem())
//                                    .setClickAddCartListener(selectedMenu -> {
//                                        LogUtil.d("옵션 목록의 정체를 밝혀라 2 : " + selectedMenu.selectedSideOptions);
//
//                                        this.mCartListAdapter.addCart(selectedMenu);
//                                        setCartTotalPrice();
//
//                                        ExToast.setToast(getContext(), "장바구니에 추가되었습니다.", Toast.LENGTH_SHORT);
//
//                                        MenuDetailDialog.getmInstance().reset();
//
//                                    })
//                                    .setClickOrderDirectListener(() -> {
////                                    goOrderDirect(selectedMenu);
//                                        goOrder();
//                                    })
//                                    .setClickCartListButtonsListener(new CartListAdapter3.OnItemClickListener() {
//                                        @Override
//                                        public void onClickCountButton(boolean isMinus, int position) {
//                                            mCartListAdapter.changeQuantity(isMinus, position);
//                                            setCartTotalPrice();
//                                            MenuDetailDialog.getmInstance().setCart(mCartListAdapter.getAllItem());
//
//                                        }
//
//                                        @Override
//                                        public void onClickDeleteButton(int position) {
//                                            mCartListAdapter.deleteCart(position);
//                                            setCartTotalPrice();
//                                            MenuDetailDialog.getmInstance().setCart(mCartListAdapter.getAllItem());
//
//
//                                        }
//                                    })
//                                    .complete();
                            MenuDetailType2Dialog
                                    .getInstance(getContext())
                                    .setData(itemData)
                                    .setClickAddCartListener(selectedMenu -> {
                                        this.mCartListAdapter.addCart(selectedMenu);
                                        setCartTotalPrice();
                                        ExToast.setToast(getContext(), (LoginUtil.getInstance().getSelectedLanguage() == null || LoginUtil.getInstance().getSelectedLanguage().equals("ko") ? "장바구니에 추가되었습니다." : "inserted in cart"), Toast.LENGTH_SHORT);
                                        MenuDetailType2Dialog.getmInstanceWithoutInit().dismiss();
                                    })
                                    .complete();
                        } else {
                            this.binding.layoutSelectedMenu.setVisibility(View.VISIBLE);
                            this.binding.layoutSelectedMenu.bringToFront();

                            this.binding.setSelectedMenu(itemData);

//                            this.binding.textSelectedMenuPrice.setText(Utils.setComma(itemData.price) + "원");
//                            this.binding.textSelectedMenuTitle.setText(itemData.foodName);
//                            this.binding.textSelectedMenuIntro.setText(itemData.description+itemData.description+itemData.description+itemData.description+itemData.description);


                            Glide.with(this).load(itemData.foodImage)
                                    .into(this.binding.imageSelectedMenu);


                        }
                    }
                });
                this.binding.recyclerView.setAdapter(this.mAdapter);
                this.binding.recyclerView.addOnScrollListener(preloader);

            }
            mAdapter.setmListData(menuList);
            closeSelectedMenu(null);
            mAdapter.notifyDataSetChanged();
            try {
                this.binding.recyclerView.getLayoutManager().scrollToPosition(0);
            } catch (Exception e) {
            }
        });


        if (preloadSizeProvider == null) {
            this.preloadSizeProvider = new ViewPreloadSizeProvider<>();
        }


        ///메뉴 로딩 관련 상태
        this.orderViewModel.state.observe(this, state -> {

            switch (state) {
                case NONE:
                    this.loadingDialog.dismiss();
                    break;
                case LOADING:
                    this.loadingDialog.setTitleNew(getResources().getString(R.string.loadingDialog_title_loading));
                    this.loadingDialog.show();
                    break;
                case ERROR:
                    this.loadingDialog.dismiss();
                    break;
                case COMPLETE:
                    this.loadingDialog.dismiss();
                    break;
            }
        });

        ///큐 상태 체크
        this.orderViewModel.queueState.observe(this, queueState -> {
            switch (queueState) {
                case NONE:
                    this.loadingDialog.dismiss();
                    break;
                case LOADING:
                    this.loadingDialog.show();
                    break;
                case ERROR:
                    this.loadingDialog.dismiss();
                    Toast.makeText(getContext(), "주문 큐 상태를 확인해주세요", Toast.LENGTH_SHORT).show();
                    this.orderViewModel.queueState.setValue(OrderViewModel.QUEUESTATE.NONE);
                    break;
                case COMPLETE:
                    this.loadingDialog.dismiss();
//                    PosCheckerForOrderQueue posCheckerForOrderQueue = new PosCheckerForOrderQueue(this.mCartListAdapter.getAllItem());
//                    posCheckerForOrderQueue.execute();
                    this.orderViewModel.orderToQueue(TabletSettingUtils.getInstacne().getTableNo(), this.mCartListAdapter.getAllItem());
                    this.billViewModel.getOrderList(
                            TabletSettingUtils.getInstacne().getTableNo()
                    );
                    this.orderViewModel.queueState.setValue(OrderViewModel.QUEUESTATE.NONE);
                    break;
            }
        });
        ///장바구니 담기 진행상황표시
        this.orderViewModel.addCartProgress.observe(this, addcartprofress -> {
            switch (addcartprofress) {
                case NONE:
                    this.loadingDialog.dismiss();
                    break;
                case LOADING:
                    this.loadingDialog.show();
                    break;
                case ERROR:
                    this.loadingDialog.dismiss();
                    break;
                case COMPLETE:
                    this.loadingDialog.dismiss();
                    Toast.makeText(getContext(), (LoginUtil.getInstance().getSelectedLanguage() == null || LoginUtil.getInstance().getSelectedLanguage().equals("ko") ? "장바구니에 추가되었습니다." : "inserted in cart"), Toast.LENGTH_SHORT).show();
                    break;
            }
        });
        ///주문상황표시
        this.orderViewModel.orderState.observe(this, orderState -> {
            switch (orderState) {
                case NONE:
                    this.loadingDialog.dismiss();
                    break;
                case NOCART:
                    Toast.makeText(OrderAppApplication.mContext, (LoginUtil.getInstance().getSelectedLanguage() == null || LoginUtil.getInstance().getSelectedLanguage().equals("ko") ? "주문할 메뉴를 선택해주세요." : "oops, cart is empty. please select menu"), Toast.LENGTH_SHORT).show();
                    this.loadingDialog.dismiss();
                    break;
                case LOADING:
                    this.loadingDialog.setTitleNew(getResources().getString(R.string.loadingDialog_title_ordering));
                    this.loadingDialog.show();
                    break;
                case ERROR:
                    Toast.makeText(OrderAppApplication.mContext, "일시적인 오류가 발생했습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                    this.loadingDialog.dismiss();
                    break;
                case COMPLETE:
                    this.loadingDialog.dismiss();
                    MessageDialogBuilder.getInstance(getContext()).dismiss();
                    MessageDialogBuilder.getInstance(getContext())
                            .setContent((LoginUtil.getInstance().getSelectedLanguage() == null || LoginUtil.getInstance().getSelectedLanguage().equals("ko") ? "주문이 완료되었습니다.\n왼쪽에 있는 [계산서]를 누르시면 지금까지 주문된 목록을 확인하실 수 있습니다 :)" : "Order Complete.\nYou can check ordered list by touching [Bill] laying left side"))
                            .setDefaultButton()
                            .complete();
                    if (MenuDetailDialog.getmInstance() != null) {
                        MenuDetailDialog.getmInstance().dismiss();
                    }
                    mCartListAdapter.clear();
                    setCartTotalPrice();
                    break;
                case COMPLETEWITHERROR1:
                    this.loadingDialog.dismiss();

                    MessageDialogBuilder.getInstance(getContext())
                            .setContent((LoginUtil.getInstance().getSelectedLanguage() == null || LoginUtil.getInstance().getSelectedLanguage().equals("ko") ? "주문이 완료되었습니다.\n왼쪽에 있는 [계산서]를 누르시면 지금까지 주문된 목록을 확인하실 수 있습니다 :)(E1)" : "Order Complete.\nYou can check ordered list by touching [Bill] laying left side(E1)"))
                            .setDefaultButton()
                            .complete();
                    if (MenuDetailDialog.getmInstance() != null) {
                        MenuDetailDialog.getmInstance().dismiss();
                    }
                    mCartListAdapter.clear();
                    setCartTotalPrice();
                    break;
                case COMPLETEWITHERROR2:
                    this.loadingDialog.dismiss();
                    MessageDialogBuilder.getInstance(getContext())
                            .setContent((LoginUtil.getInstance().getSelectedLanguage() == null || LoginUtil.getInstance().getSelectedLanguage().equals("ko") ? "주문이 완료되었습니다.\n왼쪽에 있는 [계산서]를 누르시면 지금까지 주문된 목록을 확인하실 수 있습니다 :)(E2)" : "Order Complete.\nYou can check ordered list by touching [Bill] laying left side(E2)"))
                            .setDefaultButton()
                            .complete();
                    if (MenuDetailDialog.getmInstance() != null) {
                        MenuDetailDialog.getmInstance().dismiss();
                    }
                    mCartListAdapter.clear();
                    setCartTotalPrice();
                    break;
            }
        });


        ///빌지 띄우는 용도
        billViewModel = ViewModelProviders.of(this).get(BillViewModel.class);
        billViewModel.initRepository(getContext());

        ///큐 상태 체크
        this.billViewModel.state.observe(this, state -> {
            switch (state) {
                case NONE:
                    this.loadingDialog.dismiss();
                    break;
                case LOADING:
                    this.loadingDialog.show();
                    break;
                case ERROR:
                    this.loadingDialog.dismiss();
                    Toast.makeText(getContext(), "일시적으로 계산서를 불러올 수 없습니다.(E0001)", Toast.LENGTH_SHORT).show();
                    break;
                case COMPLETE:
                    this.loadingDialog.dismiss();
                    break;
            }
        });
//        this.binding.setBillViewModel(billViewModel);
        billViewModel.mPagingOrderList.observe(this, orders -> {
            if (orders != null) {
                LogUtil.d("----------------------------------------------------------");

                for (Order item : orders) {
                    LogUtil.d("중간점검 " + item.foodName + " : " + item.quantity);
                }

                if (isOpenBill) {

                    if (BillDialog.checkShowing()) {
                        BillDialog.getmInstance()
                                .setBillViewModel(this.billViewModel)
                                .setCloseListener(() -> {

                                    this.isOpenBill = false;

                                })
                                .complete();
                    } else {

                        BillDialog.getInstance(getContext())
                                .setBillViewModel(this.billViewModel)
                                .setCloseListener(() -> {

                                    this.isOpenBill = false;
                                })
                                .complete();
                    }

                }

            }

//            this.binding.setOrderList(orders);

        });
        billViewModel.mOrderedTotalPrice.observe(this, totalPrice -> {
//            this.binding.setTotalPrice(totalPrice);

        });
        billViewModel.mOrderListPageIndex.observe(this, currentPage -> {
//            this.binding.setCurrentPage(currentPage);
        });
        billViewModel.mTotalPage.observe(this, totalPage -> {
//            this.binding.setTotalPage(totalPage);

        });
        billViewModel.mPersonCount.observe(this, personCount -> {
//            this.binding.setPersonCount(personCount);
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        if(this.mCartListAdapter != null)
        {
            this.mCartListAdapter.unregisterAdapterDataObserver(this.adapterDataObserver);
        }

        ///view 관련된 리소스 해제
        int total = this.binding.layoutSubCategory.getChildCount();
        for (int index = 0; index < total; index++) {
            if (this.binding.layoutSubCategory.getChildAt(index) != null) {
                this.binding.layoutSubCategory.getChildAt(index).setTag(null);
                this.binding.layoutSubCategory.getChildAt(index).setOnClickListener(null);
                this.binding.layoutSubCategory.getChildAt(index).setBackgroundResource(android.R.color.transparent);
            }
        }

        ///adapter 해제
        if (this.mAdapter != null) {
            this.mAdapter.onDestroy();
            this.mAdapter = null;
        }

        ///
        if (this.mCartListAdapter != null) {
            this.mCartListAdapter = null;
        }

        ///바인딩 확인후 recyclerView 해제
        if (this.binding != null && this.binding.recyclerView != null) {
            this.binding.recyclerView.setAdapter(null);
        }

    }


    //이 카테고리 아이디는 main임. sub첫번째꺼 가져와야함.
    public void setCategory(String parentCategoryId) {
        LogUtil.d("주문 프래그먼트 상태  setCategory");
        LogUtil.d("임시 카테고리 저장ㄹ : " + parentCategoryId);
        this.parentCategoryId = parentCategoryId;
        if (binding != null) {

            ////1. 서브 카테고리 버튼을 셋팅
            if (parentCategoryId != null) {
                int currentSubCategorySize = TabletSetting.getInstance().mOrderSubCategoryList.get(parentCategoryId).size();
                ///(1) 기존 셋팅된 버튼을 전부 초기화
                int total = this.binding.layoutSubCategory.getChildCount();
                OrderCategoryData.CategoryItem tmp = null;

                for (int i = 0; i < total; i++) {
                    this.binding.layoutSubCategory.getChildAt(i).setSelected(false);
                    this.binding.layoutSubCategory.getChildAt(i).setOnClickListener(null);
                    if (i >= currentSubCategorySize) {
                        this.binding.layoutSubCategory.getChildAt(i).setVisibility(View.GONE);
                    } else {
                        tmp = TabletSetting.getInstance().mOrderSubCategoryList.get(parentCategoryId).get(i);
                        ((Button) this.binding.layoutSubCategory.getChildAt(i)).setText(tmp.categoryName);
                        this.binding.layoutSubCategory.getChildAt(i).setTag(null);
                        this.binding.layoutSubCategory.getChildAt(i).setTag(tmp);
                        this.binding.layoutSubCategory.getChildAt(i).setOnClickListener(clickSubCategory);
                        this.binding.layoutSubCategory.getChildAt(i).setVisibility(View.VISIBLE);
                    }
                }

            }


            //3. 눌린게 없는경우 첫번째 버튼을 눌러줘야함.
            if (this.isMenuCategoryButtonSelected() == -1) {
                this.binding.layoutSubCategory.getChildAt(0).performClick();
            } else {
                this.binding.layoutSubCategory.getChildAt(this.isMenuCategoryButtonSelected()).setSelected(false);
                this.binding.layoutSubCategory.getChildAt(this.isMenuCategoryButtonSelected()).performClick();
            }


            ///스크롤을 맨 위로 올린다.
            this.binding.recyclerView.scrollTo(0, 0);
        }

    }

    private String getCurrentCategoryName() {
        for (int i = 0; i < this.binding.layoutSubCategory.getChildCount(); i++) {
            if (this.binding.layoutSubCategory.getChildAt(i).isSelected()) {
                return ((Button) this.binding.layoutSubCategory.getChildAt(i)).getText().toString();
            }
        }

        return null;
    }

    private View.OnClickListener clickSubCategory = view -> {
        if (!view.isSelected()) {
            this.loadingDialog.setTitleNew(getResources().getString(R.string.loadingDialog_title_loading));
            setMenuCategoryButtonSelection(view);

            if (view.getTag() != null) {
                this.binding.recyclerView.setVisibility(View.GONE);
                getMenuList(((OrderCategoryData.CategoryItem) view.getTag()).categoryCode);
            }
        }
    };

    private void getMenuList(String subCategoryId) {
        ////2. api 요청
        if (this.orderViewModel != null) {
            if (subCategoryId != null) {
                this.orderViewModel.getMenuList(getContext(), subCategoryId, TabletSettingUtils.getInstacne().getBranchInfo().storeBranchUid);
            }
        }
    }


    //서브 카테고리 선택셋팅
    private void setMenuCategoryButtonSelection(View selected) {
        for (int i = 0; i < this.binding.layoutSubCategory.getChildCount(); i++) {

            if (this.binding.layoutSubCategory.getChildAt(i).equals(selected)) {
                selectedSubcategoryIndex = i;
                this.binding.layoutSubCategory.getChildAt(i).setSelected(true);
            } else {
                this.binding.layoutSubCategory.getChildAt(i).setSelected(false);
            }

        }
    }

    private int isMenuCategoryButtonSelected() {
        int tmp = -1;
        for (int i = 0; i < this.binding.layoutSubCategory.getChildCount(); i++) {
            if (this.binding.layoutSubCategory.getChildAt(i).isSelected()) {
                tmp = i;
                break;
            }
        }
        return tmp;
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String category = bundle.getString("category");
            bundle.remove("category");
            LogUtil.d("카테고리 : " + category);
            if (category != null)
                this.setCategory(category);
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LogUtil.d("주문 프래그먼트 상태  onActivityCreated");

        this.binding.recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int width = binding.recyclerView.getWidth();
                LogUtil.d("선택된 이미지 영역 사이즈 : " + width);
                if (width > 3) {
                    binding.recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    RelativeLayout.LayoutParams ilp = (RelativeLayout.LayoutParams) binding.imageSelectedMenu.getLayoutParams();
                    ilp.height = (int) (width * 0.6);
                    binding.imageSelectedMenu.setLayoutParams(ilp);


                }
            }
        });


        this.binding.textSelectedMenuIntro.setMovementMethod(ScrollingMovementMethod.getInstance());
        this.binding.setOrderViewModel(this.orderViewModel);

        this.binding.recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        this.binding.recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

            float x1, x2;
            float y1, y2;
            float min_distance = 150;//(float) (binding.recyclerView.getMeasuredWidth() * 0.2);

            @Override
            public boolean onInterceptTouchEvent(@NonNull RecyclerView recyclerView, @NonNull MotionEvent motionEvent) {

                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        x1 = motionEvent.getX();
                        y1 = motionEvent.getY();
                        //                        x1 = event.getX();
                        //                        LogUtil.d("메뉴쪽 스와이프 x1 : " + x1);
                        //                        x2 = 0;
                        break;
                    case MotionEvent.ACTION_UP:
                        x2 = motionEvent.getX();
                        y2 = motionEvent.getY();
                        float deltaY = y2 - y1;
                        float deltaX = x2 - x1;
                        //어느정도 위로 올렸다는건 위아래 스크롤일 가능성이 더 크다는 얘기
                        if (Math.abs(deltaY) > 80) {
                            break;
                        }

                        if (Math.abs(deltaX) > this.min_distance) {

                            // Left to Right swipe action
                            if (x2 > x1) {

                                if (selectedSubcategoryIndex <= 0) {
                                    //1개이므로 변화 없음.

                                } else {
                                    (binding.layoutSubCategory.getChildAt(selectedSubcategoryIndex - 1)).performClick();
                                }


                            }

                            // Right to left swipe action
                            else {
                                //                                Toast.makeText(getContext(), "Right to Left swipe [Previous]", Toast.LENGTH_SHORT).show ();
                                if (selectedSubcategoryIndex == 0) {
                                    //두개 이상인가?
                                    if (TabletSetting.getInstance().mOrderSubCategoryList.get(parentCategoryId).size() > 1)//카테고리 사이즈
                                    {
                                        (binding.layoutSubCategory.getChildAt(1)).performClick();
                                    } else {
                                        //1개이므로 변화 없음.
                                    }
                                } else {
                                    //현재 있는 버튼이 오른쪽 끝자락인지 확인?
                                    if (TabletSetting.getInstance().mOrderSubCategoryList.get(parentCategoryId).size() - 1 == selectedSubcategoryIndex)//카테고리 사이즈
                                    {

                                    } else {
                                        //1칸 이동해야함
                                        (binding.layoutSubCategory.getChildAt(selectedSubcategoryIndex + 1)).performClick();
                                    }
                                }
                            }

                        }
                        break;

                }
                return false;
            }

            @Override
            public void onTouchEvent(@NonNull RecyclerView recyclerView, @NonNull MotionEvent motionEvent) {
                LogUtil.d("리스트뷰 테스트중 onTouchEvent : " + motionEvent.getAction());

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean b) {
                LogUtil.d("리스트뷰 테스트중 onRequestDisallowInterceptTouchEvent : " + b);

            }
        });
        this.binding.recyclerView.setItemViewCacheSize(30);
        this.mCartListAdapter = new CartListAdapter3();
        this.mCartListAdapter.registerAdapterDataObserver(this.adapterDataObserver);
        this.mCartListAdapter.setListener(new CartListAdapter3.OnItemClickListener() {
            @Override
            public void onClickCountButton(boolean isMinus, int position) {
                mCartListAdapter.changeQuantity(isMinus, position);
                setCartTotalPrice();

            }

            @Override
            public void onClickDeleteButton(int position) {
                mCartListAdapter.deleteCart(position);
                setCartTotalPrice();


            }
        });
        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        lm.setItemPrefetchEnabled(true);
        this.binding.recyclerViewCart.setLayoutManager(lm);
        this.binding.recyclerViewCart.setAdapter(mCartListAdapter);
        this.binding.recyclerViewCart.addOnScrollListener(new RecyclerView.OnScrollListener() {

            int findFirstVisibleItemPosition = 0;
            int findFirstCompletelyVisibleItemPosition = 0;
            int findLastVisibleItemPosition = 0;
            int findLastCompletelyVisibleItemPosition = 0;
            int totalItem = 0;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


                ///위쪽이 보일때
                this.findFirstVisibleItemPosition = ((LinearLayoutManager) binding.recyclerViewCart.getLayoutManager()).findFirstVisibleItemPosition();
                this.findFirstCompletelyVisibleItemPosition = ((LinearLayoutManager) binding.recyclerViewCart.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
                this.findLastVisibleItemPosition = ((LinearLayoutManager) binding.recyclerViewCart.getLayoutManager()).findLastVisibleItemPosition();
                this.findLastCompletelyVisibleItemPosition = ((LinearLayoutManager) binding.recyclerViewCart.getLayoutManager()).findLastCompletelyVisibleItemPosition();
                if (mCartListAdapter != null)
                    this.totalItem = mCartListAdapter.getItemCount();

//                ///reverse가 적용되었음.
//                ///top
//                if ((totalItem != 0 && this.findLastVisibleItemPosition != 0 && findLastCompletelyVisibleItemPosition != 0 && findLastVisibleItemPosition != findLastCompletelyVisibleItemPosition)) {
//                    binding.viewCartShadowTop.setVisibility(View.VISIBLE);
//
//                } else {
//                    binding.viewCartShadowTop.setVisibility(View.GONE);
//                }
//
//
//                if ((totalItem != 0 && this.findLastVisibleItemPosition != 0 && findLastCompletelyVisibleItemPosition != 0)
//                        && this.findFirstCompletelyVisibleItemPosition != 0
//                        ) {
//                    binding.viewCartShadowBottom.setVisibility(View.VISIBLE);
//                } else {
//                    binding.viewCartShadowBottom.setVisibility(View.GONE);
//                }


                ///reverse 없앰.
                ///top
                if ((totalItem != 0 && this.findLastVisibleItemPosition != 0 && findLastCompletelyVisibleItemPosition != 0 && findLastVisibleItemPosition != findLastCompletelyVisibleItemPosition)) {
                    binding.viewCartShadowBottom.setVisibility(View.VISIBLE);

                } else {
                    binding.viewCartShadowBottom.setVisibility(View.GONE);
                }


                if ((totalItem != 0 && this.findLastVisibleItemPosition != 0 && findLastCompletelyVisibleItemPosition != 0)
                        && this.findFirstCompletelyVisibleItemPosition != 0
                ) {
                    binding.viewCartShadowTop.setVisibility(View.VISIBLE);
                } else {
                    binding.viewCartShadowTop.setVisibility(View.GONE);
                }


                LogUtil.d("recyclerViewCart findFirstVisibleItemPosition : " + ((LinearLayoutManager) binding.recyclerViewCart.getLayoutManager()).findFirstVisibleItemPosition());
                LogUtil.d("recyclerViewCart findFirstCompletelyVisibleItemPosition : " + ((LinearLayoutManager) binding.recyclerViewCart.getLayoutManager()).findFirstCompletelyVisibleItemPosition());
                LogUtil.d("recyclerViewCart findLastVisibleItemPosition : " + ((LinearLayoutManager) binding.recyclerViewCart.getLayoutManager()).findLastVisibleItemPosition());
                LogUtil.d("recyclerViewCart findLastCompletelyVisibleItemPosition : " + ((LinearLayoutManager) binding.recyclerViewCart.getLayoutManager()).findLastCompletelyVisibleItemPosition());
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        LogUtil.d("주문 프래그먼트 상태  onCreateView");
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_order, container, false);
        binding.setFragment(this);

        ///불러온 리소스 기반으로 뷰 셋팅
        this.binding.sideMenuCategoryButtons1.setBackgroundResource(R.drawable.selector_btn_sub_category);
        this.binding.sideMenuCategoryButtons2.setBackgroundResource(R.drawable.selector_btn_sub_category);
        this.binding.sideMenuCategoryButtons3.setBackgroundResource(R.drawable.selector_btn_sub_category);
        this.binding.sideMenuCategoryButtons4.setBackgroundResource(R.drawable.selector_btn_sub_category);
        this.binding.sideMenuCategoryButtons5.setBackgroundResource(R.drawable.selector_btn_sub_category);
        this.binding.sideMenuCategoryButtons6.setBackgroundResource(R.drawable.selector_btn_sub_category);
        this.binding.sideMenuCategoryButtons7.setBackgroundResource(R.drawable.selector_btn_sub_category);
        this.binding.sideMenuCategoryButtons8.setBackgroundResource(R.drawable.selector_btn_sub_category);
        this.binding.sideMenuCategoryButtons9.setBackgroundResource(R.drawable.selector_btn_sub_category);
        this.binding.sideMenuCategoryButtons10.setBackgroundResource(R.drawable.selector_btn_sub_category);
        this.binding.textCartTotalPrice.setTextColor(getResources().getColor(R.color.login));

        //here data must be an instance of the class MarsDataProvider
        return binding.getRoot();
    }


    private RecyclerView.AdapterDataObserver adapterDataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            setCartTotalPrice();
            LogUtil.d("카트 adapterDataObserver onChanged: ");
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            super.onItemRangeChanged(positionStart, itemCount);
            setCartTotalPrice();
            LogUtil.d("카트 adapterDataObserver onItemRangeChanged: " + positionStart + "/" + itemCount);
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            super.onItemRangeInserted(positionStart, itemCount);
            setCartTotalPrice();

//            LogUtil.d("카트 추가시 layoutManager height1 : " + binding.recyclerViewCart.getLayoutManager().getHeight());
//            LogUtil.d("카트 추가시 layoutManager height2 : " + binding.recyclerViewCart.computeVerticalScrollRange());

            binding.recyclerViewCart.scrollToPosition(positionStart);
            LogUtil.d("카트 adapterDataObserver onItemRangeInserted: " + positionStart + "/" + itemCount);
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            super.onItemRangeRemoved(positionStart, itemCount);
            setCartTotalPrice();
            LogUtil.d("카트 adapterDataObserver onItemRangeRemoved: " + positionStart + "/" + itemCount);
        }
    };

    private void setCartTotalPrice() {
        totalPrice_textview = 0;
        if (mCartListAdapter != null) {

            for (MenuItemData item : mCartListAdapter.getAllItem()) {
                if (item.isSet == 0) {
                    totalPrice_textview += item.price * item.quantity;
                } else {
                    totalPrice_textview += (com.cdef.hoeat.utils.Utils.getSelectedOptionsTotalPrice(item.selectedSideOptions) + item.price) * item.quantity;
                }

            }
            this.binding.setCartItemCount(mCartListAdapter.getItemCount());
        }
        if (this.binding.textCartTotalPrice != null)
            this.binding.textCartTotalPrice.setText(Utils.setComma(totalPrice_textview) + "원");
    }


    public void goOrder() {
//        posChecker = new PosChecker();
//        posChecker.execute();
//        queueOrder();
//        if (OrderAppApplication.getUseQueue(getContext()) == null) {
////            기존방식
//            posChecker = new PosChecker();
//            posChecker.execute();
////            Toast.makeText(getContext(), "이전방식 주문", Toast.LENGTH_SHORT).show();
//
//        } else {
//        PosCheckerForOrderQueue posCheckerForOrderQueue = new PosCheckerForOrderQueue();
//        posCheckerForOrderQueue.execute();
//            Toast.makeText(getContext(), "큐 주문", Toast.LENGTH_SHORT).show();

//        }


        if (this.binding.getSelectedMenu() != null && this.binding.layoutSelectedMenu.getVisibility() == (View.VISIBLE)) {
            this.mCartListAdapter.addCart(this.binding.getSelectedMenu());
            setCartTotalPrice();
            ExToast.setToast(getContext(), (LoginUtil.getInstance().getSelectedLanguage() == null || LoginUtil.getInstance().getSelectedLanguage().equals("ko") ? "장바구니에 추가되었습니다." : "inserted in cart"), Toast.LENGTH_SHORT);
            closeSelectedMenu(this.binding.buttonOrder);
        } else {
            if (mCartListAdapter == null || mCartListAdapter.getItemCount() == 0) {

                Toast.makeText(getContext(), (LoginUtil.getInstance().getSelectedLanguage() == null || LoginUtil.getInstance().getSelectedLanguage().equals("ko") ? "주문할 메뉴를 1개이상 담아주세요." : "oops, cart is empty. please select menu"), Toast.LENGTH_SHORT).show();
            } else {

                ///매장에서 최소주문금액을 정해놨으면
                if (TabletSettingUtils.getInstacne().getBranchInfo() != null
                        && TabletSettingUtils.getInstacne().getBranchInfo().storeOption != null
                        && TabletSettingUtils.getInstacne().getBranchInfo().storeOption.minOrderPrice != 0) {

                    if (totalPrice_textview < TabletSettingUtils.getInstacne().getBranchInfo().storeOption.minOrderPrice) {
                        this.billViewModel.getOrderListAsObservableTotalPrice(TabletSettingUtils.getInstacne().getTableNo())
                                .subscribe(currentTotalPrice -> {
                                            if (currentTotalPrice > 0 && (currentTotalPrice + totalPrice_textview) >= TabletSettingUtils.getInstacne().getBranchInfo().storeOption.minOrderPrice) {
                                                orderViewModel.checkQueueState(QueueUtils.checkQueueState(TabletSettingUtils.getInstacne().getBranchInfo().posIp));
                                            } else {
                                                MessageDialogBuilder.getInstance(getContext())
                                                        .setContent((LoginUtil.getInstance().getSelectedLanguage() == null || LoginUtil.getInstance().getSelectedLanguage().equals("ko") ? "첫 주문은 " + Utils.setComma(TabletSettingUtils.getInstacne().getBranchInfo().storeOption.minOrderPrice) + "원 이상 주문하셔야합니다." : "You must order at least "+ Utils.setComma(TabletSettingUtils.getInstacne().getBranchInfo().storeOption.minOrderPrice)+ "  won for the first order."))
                                                        .setDefaultButton()
                                                        .complete();
                                            }

                                        }, error -> {
                                            Toast.makeText(getContext(), "일시적인 오류로 주문내역을 불러올 수 없습니다. e11 " + Utils.setComma(TabletSettingUtils.getInstacne().getBranchInfo().storeOption.minOrderPrice) + "원 이상 주문하셔야합니다.", Toast.LENGTH_LONG).show();
                                        }
                                        , () -> {
                                            this.billViewModel.state.setValue(BaseViewModel.STATE.NONE);
                                        }
                                );
                        return;

                    }
                }
                if (orderViewModel.queueState.getValue() != null && orderViewModel.queueState.getValue() != OrderViewModel.QUEUESTATE.LOADING) {
                    orderViewModel.queueState.setValue(OrderViewModel.QUEUESTATE.LOADING);
                    orderViewModel.checkQueueState(QueueUtils.checkQueueState(TabletSettingUtils.getInstacne().getBranchInfo().posIp));
                } else {
                    Toast.makeText(getContext(), "주문 처리중입니다.", Toast.LENGTH_LONG).show();
                }
            }
        }

    }


    public void closeSelectedMenu(View view) {
        LogUtil.d("닫기를 누른거라고 : " + view);
        this.binding.layoutSelectedMenu.setVisibility(View.GONE);
        this.binding.setSelectedMenu(null);
    }

    public void goBill() {

        isOpenBill = true;

        billViewModel.getOrderList(
                TabletSettingUtils.getInstacne().getTableNo()
        );
    }

    public void touchSelectedMenuLayout() {

    }
}
