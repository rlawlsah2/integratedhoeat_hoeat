package com.cdef.hoeat.view.customView;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.cdef.commonmodule.dataModel.firebaseDataSet.Order;
import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.LayoutOrderBillBinding;

import java.util.ArrayList;
import java.util.Currency;
import java.util.Locale;


/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class BillListView extends LinearLayout {

    LayoutOrderBillBinding binding;
    int totalPrice = 0;
    private final int PageCount = 5;


    private int CurrentPage = 0;
    private int TotalPage = 0;

    String won = Currency.getInstance(Locale.KOREA).getSymbol();


    ArrayList<Order> mOrderList = new ArrayList<>();


    Context mContext;

    public BillListView(Context context) {
        super(context);
        this.mContext = context;
        registerHandler();
    }

    public BillListView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        registerHandler();

    }

    public BillListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        registerHandler();
    }

    private void registerHandler() {
        this.binding  = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.layout_order_bill, this, true);

//        LayoutInflater.from(getContext()).inflate(R.layout.layout_order_bill, this, true);
//        ButterKnife.bind(this);
        this.binding.textDivideTitle.setSelected(true);
    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        this.mOrderList.clear();
        this.CurrentPage = 0;
        totalPrice = 0;

//        if (visibility == View.VISIBLE) {
//            ///데이터 가져옴
//            /**계산서 데이터 셋팅**/
//            FirebaseDatabase.getInstance().getReference().child(BuildConfig.Brand)
//                    .child("tables")
//                    .child(LoginUtil.getInstance(getContext()).branchInfo.branchId)
//                    .child(LoginUtil.getInstance(getContext()).getsTableNo())
//                    .child("orders")
//                    .addListenerForSingleValueEvent(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(DataSnapshot dataSnapshot) {
//                            LogUtil.d("계산서 항목 : " + dataSnapshot.getChildrenCount());
//                            for (DataSnapshot item : dataSnapshot.getChildren()) {
//                                LogUtil.d("계산서 상세 항목 : " + item);
//                                Order order = item.getValue(Order.class);
//                                if (order != null) {
//                                    mOrderList.add(order);
//                                    int price;
//                                    int quantity;
//                                    try {
//                                        price = Integer.parseInt(order.price);
//
//                                    } catch (NumberFormatException e) {
//                                        price = -9999999;
//                                    }
//                                    try {
//                                        quantity = Integer.parseInt(order.quantity);
//
//                                    } catch (NumberFormatException e) {
//                                        quantity = 1;
//                                    }
//
//
//                                    totalPrice += (price * quantity);
//                                }
//
//                            }
//                            textTotalPrice.setText(Utils.setComma(totalPrice) + "원");
//                            textDivideTotalPrice.setText(Utils.setComma(totalPrice) + "원");
//
//                            TotalPage = (int) ((dataSnapshot.getChildrenCount() - 1) / PageCount);
//                            LogUtil.d("계산서 항목 TotalPage: " + TotalPage);
//
//                            setListView(CurrentPage);
//                        }
//
//                        @Override
//                        public void onCancelled(DatabaseError databaseError) {
//
//                        }
//                    });
//        }
    }


//    private void setListView(int page) {
//        LogUtil.d("계산서 항목 setListView size: " + this.mOrderList.size());
//        LogUtil.d("계산서 항목 setListView page: " + page);
//
//        int lastIndex = ((page + 1) * PageCount < this.mOrderList.size() ? (page + 1) * PageCount : (this.mOrderList.size() == 1) ? 1 : this.mOrderList.size());
//        LogUtil.d("계산서 항목 setListView lastIndex: " + lastIndex);
//        LogUtil.d("계산서 항목 setListView startIndex: " + (page * PageCount));
//
//        if ((page * PageCount) < lastIndex) {
//            mOrderList.subList(page * PageCount, lastIndex);    //필요한 만큼 얻옴.
//            int i = 0;
//            LogUtil.d("계산서 항목 setListView subList: " + mOrderList.subList(page * PageCount, lastIndex).size());
//            for (Order item : mOrderList.subList(page * PageCount, lastIndex)) {
//                this.layoutBillList.getChildAt(i).setVisibility(VISIBLE);
//                ((BillListItem) this.layoutBillList.getChildAt(i)).setData(
//                        item.foodName,
//                        item.quantity,
//                        won + Utils.setComma(item.price)
//                );
//                i++;
//            }
//
//            for (int j = i; j < PageCount; j++) {
//                this.layoutBillList.getChildAt(j).setVisibility(INVISIBLE);
//            }
//        } else {
//
//        }
//        LogUtil.d("계산서 항목 setListView CurrentPage: " + CurrentPage);
//        LogUtil.d("계산서 항목 setListView TotalPage: " + TotalPage);
//
//        ////prev next 버튼 셋팅
//        if (CurrentPage == 0) {
//            this.buttonPrev.setVisibility(View.GONE);
//        } else if (CurrentPage > 0) {
//            this.buttonPrev.setVisibility(View.VISIBLE);
//        }
//
//
//        if (CurrentPage == TotalPage) {
//            this.buttonNext.setVisibility(View.GONE);
//        } else if (CurrentPage < TotalPage) {
//            this.buttonNext.setVisibility(View.VISIBLE);
//        }
//
//
//    }
//
//
//    private void calcDividePrice()
//    {
//        int personCount = 1;
//
//        try {
//            personCount = Integer.parseInt(this.textPersonCount.getText().toString());
//        }
//        catch (NumberFormatException e)
//        {
//            personCount = 1;
//        }
//
//        if(totalPrice != 0)
//        {
//            int result = (totalPrice / personCount);
//
//            this.textDividePrice.setText(Utils.setComma(result) + "원");
//        }
//
//
//    }

    @Override
    protected void onDetachedFromWindow() {
        if(mOrderList != null)
        {
            mOrderList.clear();
            mOrderList = null;
        }
        super.onDetachedFromWindow();
    }

}
