package com.cdef.hoeat.view.customView;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.cdef.commonmodule.dataModel.firebaseDataSet.Order;
import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.LayoutBillListItemBinding;


/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class BillListItem extends RelativeLayout {

    LayoutBillListItemBinding binding;

    Context mContext;

    public BillListItem(Context context) {
        super(context);
        this.mContext = context;
        registerHandler(null);
    }

    public BillListItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.ButtonTableSettingItem);
        registerHandler(typedArray);

    }

    public BillListItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.ButtonTableSettingItem);
        registerHandler(typedArray);
    }

    private void registerHandler(TypedArray typedArray) {
        this.binding  = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.layout_bill_list_item, this, true);

    }

    public void setData(Order order)
    {
//        this.binding.setOrder(order);
//        this.binding.textMenuName.setText(name);
//        this.binding.textMenuCount.setText(count);
//        this.binding.textMenuPrice.setText(price);
    }


}
