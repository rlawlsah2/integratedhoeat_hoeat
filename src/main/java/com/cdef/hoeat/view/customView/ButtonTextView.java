package com.cdef.hoeat.view.customView;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.cdef.hoeat.R;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.Utils;

/**
 * Created by kimjinmo on 2017. 9. 25..
 */

public class ButtonTextView extends androidx.appcompat.widget.AppCompatTextView
{

    int strokeColor = 0;
    int unSelectedColor = 0;
    float originTextSize = 0;
    boolean isDirect;
    public ButtonTextView(Context context) {
        super(context);
        LogUtil.d("초기화 방식 1");
        registerHandler(null);

    }

    public ButtonTextView(Context context, int strokeColor) {
        super(context);
        LogUtil.d("초기화 방식 1");
        this.strokeColor = strokeColor;
        isDirect = true;
        registerHandler(null);
    }

    public ButtonTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        LogUtil.d("초기화 방식 2");
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.ButtonTextView);
        registerHandler(typedArray);

    }

    public ButtonTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LogUtil.d("초기화 방식 3");
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.ButtonTextView, defStyleAttr, 0);
        registerHandler(typedArray);
    }


    private void registerHandler(TypedArray typedArray) {

        /////attrs를 기반으로 뷰를 셋팅한다
        if (typedArray != null) {
            this.strokeColor = typedArray.getColor(R.styleable.ButtonTextView_strokeColor, getContext().getResources().getColor(R.color.white));
            LogUtil.d("받아온 색상1 : " + this.strokeColor);
        }
        this.unSelectedColor = getContext().getResources().getColor(R.color.darkGray);


        LogUtil.d("원본 사이즈 : " + (getTextSize() / getResources().getDisplayMetrics().scaledDensity));

        this.originTextSize = (getTextSize() / getResources().getDisplayMetrics().scaledDensity);
    }

    public void setOriginTextSize(int unit, float size) {
        this.setTextSize(unit, size);
        this.originTextSize = (getTextSize() / getResources().getDisplayMetrics().scaledDensity);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);

        if(isSelected())
        {
            this.setTextSize(TypedValue.COMPLEX_UNIT_SP,originTextSize+8);
            this.setTextColor(getContext().getResources().getColor(R.color.white));
            this.setShadowLayer(Utils.dpToPx(getContext(), 40), 0, 0, this.strokeColor);

        }
        else
        {
            this.setTextSize(TypedValue.COMPLEX_UNIT_SP,originTextSize);
            this.setTextColor(this.unSelectedColor);
            this.setShadowLayer(0,0,0,strokeColor);
        }

    }
}