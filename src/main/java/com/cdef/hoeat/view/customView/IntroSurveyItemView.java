package com.cdef.hoeat.view.customView;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.GradientDrawable;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.LayoutIntroSurveyItemBinding;

/**
 * Created by kimjinmo on 2017. 8. 23..
 * 정의되어야 할 항목 정리
 * <p>
 * 인덱스 번호 : int
 * 타이틀 : String
 * 버튼 목록 : ArrayList<String>
 */

public class IntroSurveyItemView extends LinearLayout {


    LayoutIntroSurveyItemBinding binding;
    Context mContext;

    public IntroSurveyItemView(Context context) {
        super(context);
        this.mContext = context;
        LogUtil.d("SideMenuListItem1");
        registerHandler(null);
    }

    public IntroSurveyItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        LogUtil.d("SideMenuListItem2");
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.IntroSurveyItemView);
        registerHandler(typedArray);

    }

    public IntroSurveyItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        LogUtil.d("SideMenuListItem3");
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.IntroSurveyItemView);
        registerHandler(typedArray);
    }

    private void registerHandler(TypedArray typedArray) {

        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.layout_intro_survey_item, this, true);
        this.binding.textIndex.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                int height = binding.textIndex.getMeasuredHeight();
                if (height != 0) {
                    binding.textIndex.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    LinearLayout.LayoutParams lp = (LayoutParams) binding.textIndex.getLayoutParams();
                    lp.width = height;
                    binding.textIndex.setLayoutParams(lp);
                    LogUtil.d("서베이 쪽 인덱스 높이 조절 : " + height);
                }
            }
        });
        if (typedArray != null) {
            String title = typedArray.getString(R.styleable.IntroSurveyItemView_IntroSurveyItemViewTitle);
            String index = typedArray.getString(R.styleable.IntroSurveyItemView_IntroSurveyItemViewIndex);
            int color = typedArray.getColor(R.styleable.IntroSurveyItemView_titleColor, getResources().getColor(R.color.introTextColor));
            this.binding.textIndex.setText(index);
            this.binding.textTitle.setText(title);
            this.binding.textIndex.setBackground(setIndexBound(color));
        }
    }

    public void setCheckBoxList(String[] list) {
        this.binding.layoutCheckBoxList.removeAllViews();

        for (String title : list) {
            SelectBoxWithTextView item = new SelectBoxWithTextView(getContext());
            item.setting(title, getResources().getColor(R.color.tableNo));
            item.setOnClickListener(view -> {
                item.setSelected(true);


                for (int i = 0; i < this.binding.layoutCheckBoxList.getChildCount(); i++) {
                    this.binding.layoutCheckBoxList.getChildAt(i).setSelected(this.binding.layoutCheckBoxList.getChildAt(i).equals(view));
                }

                ///다른 아이템들 해제하는 방법도 생각해야함
            });
            this.binding.layoutCheckBoxList.addView(item);
        }
    }


    public String getSelectedItem() {
        for (int i = 0; i < this.binding.layoutCheckBoxList.getChildCount(); i++) {
            if (this.binding.layoutCheckBoxList.getChildAt(i).isSelected()) {
                return ((SelectBoxWithTextView) this.binding.layoutCheckBoxList.getChildAt(i)).getTitle();
            }
        }
        return null;
    }


    public String getTitle() {
        return this.binding.textTitle.getText().toString();
    }

    public void setTitle(String title) {
        this.binding.textTitle.setText(title);
    }

    public GradientDrawable setIndexBound(int color) {
        GradientDrawable tmp = new GradientDrawable();
        tmp.setShape(GradientDrawable.RECTANGLE);
        tmp.setColor(getResources().getColor(R.color.transparent));
        tmp.setStroke(Utils.dpToPx(getContext(), 1), color);
        return tmp;
    }
}
