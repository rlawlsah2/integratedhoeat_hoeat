package com.cdef.hoeat.view.customView;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.cdef.commonmodule.utils.Utils;
import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.LayoutSelectBoxWithTextBinding;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class SelectBoxWithTextView extends RelativeLayout {


    LayoutSelectBoxWithTextBinding binding;
    Context mContext;

    public SelectBoxWithTextView(Context context) {
        super(context);
        this.mContext = context;
        registerHandler(null);
    }

    public SelectBoxWithTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SelectBoxWithTextView);
        registerHandler(typedArray);

    }

    public SelectBoxWithTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SelectBoxWithTextView);
        registerHandler(typedArray);
    }

    private void registerHandler(TypedArray typedArray) {

        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.layout_select_box_with_text, this, true);
        if (typedArray != null) {
            String title = typedArray.getString(R.styleable.SelectBoxWithTextView_SelectBoxWithTextViewTitle);
            int color = typedArray.getColor(R.styleable.SelectBoxWithTextView_selectedColor, Color.parseColor("#000000"));

            this.binding.setTitle(title);
            this.binding.textTitle.setTextColor(getColorStateList(color));
            this.binding.textTitle.setBackground(getSelectorDrawable(color));
        }
    }

    public void setting(String title, int color) {
        this.binding.setTitle(title);
        this.binding.textTitle.setTextColor(getColorStateList(color));
        this.binding.textTitle.setBackground(getSelectorDrawable(color));
    }

    public StateListDrawable getSelectorDrawable(int color) {
        StateListDrawable out = new StateListDrawable();
        out.addState(new int[]{android.R.attr.state_pressed}, setSelectedBackbroud(color));
        out.addState(new int[]{android.R.attr.state_selected}, setSelectedBackbroud(color));
        out.addState(new int[]{-android.R.attr.state_pressed, -android.R.attr.state_selected}, setUnSelectedBackbroud());
        return out;
    }

    GradientDrawable selectedBackground;
    GradientDrawable unSelectedBackground;

    ////선택 되었을때 박스 뷰셋팅
    public GradientDrawable setSelectedBackbroud(int color) {

        if (this.selectedBackground == null) {
            this.selectedBackground = new GradientDrawable();
            this.selectedBackground.setShape(GradientDrawable.RECTANGLE);
            this.selectedBackground.setCornerRadius(Utils.dpToPx(getContext(), 10));
            this.selectedBackground.setColor(Color.parseColor("#ffffff"));
            this.selectedBackground.setStroke(Utils.dpToPx(getContext(), 5), color);
        }
        return this.selectedBackground;
    }

    ////선택 해제 되었을때 박스 뷰셋팅
    public GradientDrawable setUnSelectedBackbroud() {

        if (this.unSelectedBackground == null) {
            this.unSelectedBackground = new GradientDrawable();
            this.unSelectedBackground.setShape(GradientDrawable.RECTANGLE);
            this.unSelectedBackground.setCornerRadius(Utils.dpToPx(getContext(), 10));
            this.unSelectedBackground.setColor(Color.parseColor("#ffffff"));
            this.unSelectedBackground.setStroke(Utils.dpToPx(getContext(), 1), Color.parseColor("#b8b8b8"));
        }
        return this.unSelectedBackground;
    }

    public String getTitle() {
        return this.binding.getTitle();
    }

    public ColorStateList getColorStateList(int color) {
        return new ColorStateList(
                new int[][]{
                        new int[]{-android.R.attr.state_selected, -android.R.attr.state_pressed} //1
                        , new int[]{android.R.attr.state_selected} //2
                        , new int[]{android.R.attr.state_pressed} //3

                },
                new int[]{
                        Color.parseColor("#a8a8a8"), //1
                        color, //2
                        color //3
                }
        );
    }


    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        this.binding.textTitle.setSelected(selected);
        this.binding.imageSelectionMark.setVisibility((selected ? View.VISIBLE : View.GONE));
    }
}
