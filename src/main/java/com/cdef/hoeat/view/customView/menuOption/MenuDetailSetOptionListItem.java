package com.cdef.hoeat.view.customView.menuOption;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.cdef.commonmodule.dataModel.MenuSideOptionItemData;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.LayoutSetOptionListItemBinding;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class MenuDetailSetOptionListItem extends LinearLayout {


    LayoutSetOptionListItemBinding binding;
    Context mContext;
    RequestOptions requestOptions;

    public static MenuDetailSetOptionListItem getInstance(Context context) {
        return new MenuDetailSetOptionListItem(context);
    }

    public MenuDetailSetOptionListItem setData(MenuSideOptionItemData data) {
        this.binding.setOptionItem(data);
        if (data != null && data.foodImage != null && this.requestManager != null) {
            this.requestManager
//                .load("https://cdefi-psp.s3.amazonaws.com/menus/image_3356121d-e82d-4ff1-aeae-d174dab736bc.png")
                    .load(data.foodImage)
                    .apply(this.requestOptions).into(this.binding.imageCheckBox);
        }
        return this;
    }

    RequestManager requestManager;
    public MenuDetailSetOptionListItem(Context context) {
        super(context);
        this.mContext = context;
        registerHandler(null);
    }

    public MenuDetailSetOptionListItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

    }

    public MenuDetailSetOptionListItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void registerHandler(TypedArray typedArray) {
        this.requestManager = Glide.with(this);

        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.layout_set_option_list_item, this, true);
        this.binding.setItemView(this);
        this.requestOptions = new RequestOptions();
        this.requestOptions.override(100, 100);
        this.requestOptions.circleCrop();

    }

    public MenuDetailSetOptionListItem setInitSelection(boolean isSelected) {

        LogUtil.d("해당 아이템 선택된거냐 setInitSelection: " + isSelected);
        if (isSelected) {
            this.setSelection(this);
//            this.setSelected(true);
        }
        return this;
    }

    public void setSelection(View view) {
        this.setSelected(!isSelected());
//        this.itemSelectionListener.clickItem(this);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        this.binding.imageCheckBox.setSelected(selected);
        this.binding.setIsSelected(selected);
        if (!selected) {
            MenuSideOptionItemData tmp = binding.getOptionItem();
            tmp.quantity = 1;
            this.binding.setOptionItem(tmp);
        }

        try {
            ///M보다 낮은버전일때 foreground가 제대로 동작하지 않기 때문에, 직접 이미지를 변경해줘야한다.
            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M) {
                // Do something for lollipop and above versions
                if(this.binding.getIsSelected())
                {
                    this.requestManager
                            .load(R.drawable.selector_state_checkbox_circle)
                            .apply(this.requestOptions).into(this.binding.imageCheckBox);
                }
                else
                {
                    this.requestManager
                            .load(this.binding.getOptionItem().foodImage)
                            .apply(this.requestOptions).into(this.binding.imageCheckBox);
                }
            }
        }
        catch (Exception e)
        {

        }
    }

    public void changeQuantity(View view, boolean isMinus) {
        MenuSideOptionItemData tmp = binding.getOptionItem();

        if (isMinus) {
            if (tmp.quantity > 1) {
                tmp.quantity--;
            } else {
                ///1로 초기화 하고, selection해제
                tmp.quantity = 0;
            }

        } else {
            if (tmp.maxQuantity > tmp.quantity) {
                tmp.quantity++;
            }
        }

        if (tmp.quantity == 0) {
            tmp.quantity = 1;
            this.setSelected(false);
        } else if (tmp.quantity > 0) {
            this.setSelected(true);
        }

        this.binding.setOptionItem(tmp);
//        this.itemChangeQuantityListener.changeQuantity();

    }

    public MenuSideOptionItemData getItem() {
        try {
            return (MenuSideOptionItemData) this.binding.getOptionItem().getClone();
        } catch (CloneNotSupportedException e) {
            return this.binding.getOptionItem();
        }
    }


    ItemSelectionListener itemSelectionListener;

    public MenuDetailSetOptionListItem setItemSelectionListener(ItemSelectionListener itemSelectionListener) {
        this.itemSelectionListener = itemSelectionListener;
        this.binding.setItemSelectionListener(this.itemSelectionListener);
        return this;
    }

    public MenuDetailSetOptionListItem setItemChangeQuantityListener(ItemChangeQuantityListener itemChangeQuantityListener) {
        this.itemChangeQuantityListener = itemChangeQuantityListener;
        this.binding.setItemChangeQuantityListener(this.itemChangeQuantityListener);
        return this;
    }

    ItemChangeQuantityListener itemChangeQuantityListener;


    public interface ItemSelectionListener {
        void clickItem(MenuDetailSetOptionListItem view);
    }

    public interface ItemChangeQuantityListener {
        void changeQuantity(MenuDetailSetOptionListItem view, boolean isMinus);
    }

}
