package com.cdef.hoeat.view.customView.type2;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.core.widget.TextViewCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.dataModel.OrderCategoryData;
import com.cdef.hoeat.R;
import com.cdef.hoeat.utils.Utils;
import com.cdef.hoeat.databinding.Type2LayoutCategory1Binding;

import java.util.ArrayList;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class Category1 extends LinearLayout {


    Type2LayoutCategory1Binding binding;
    Context mContext;
    int[] sizeList = {8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19};

    public void setTitle(String title) {
        this.binding.textTitle.setText(title);
    }

    public Category1(Context context) {
        super(context);
        this.mContext = context;
        LogUtil.d("SideMenuListItem1");
        registerHandler(null);
    }

    public Category1(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        LogUtil.d("SideMenuListItem2");
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SideMenuListItem);
        registerHandler(typedArray);

    }

    public Category1(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        LogUtil.d("SideMenuListItem3");
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SideMenuListItem);
        registerHandler(typedArray);
    }

    private void registerHandler(TypedArray typedArray) {

        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.type2_layout_category1, this, true);
        if (typedArray != null) {
            String title = typedArray.getString(R.styleable.SideMenuListItem_menuTitle);
            this.binding.textTitle.setText(title);
        }
        TextViewCompat.setAutoSizeTextTypeWithDefaults(this.binding.textTitle, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        TextViewCompat.setAutoSizeTextTypeUniformWithPresetSizes(this.binding.textTitle,
                sizeList, TypedValue.COMPLEX_UNIT_SP);
        this.setSelected(false);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        this.binding.textTitle.setSelected(selected);
//        if(selected)
//        {
//            binding.layoutSubCategory.setVisibility(View.VISIBLE);
//        }
//        else
//        {
//            binding.layoutSubCategory.setVisibility(View.GONE);
//        }
//        this.requestLayout();
        if (selected) {
            this.changeMargin(Utils.dpToPx(getContext(), 0));
        } else {
            if (this.getMeasuredWidth() == 0) {
                this.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        LogUtil.d("선택안된 뷰 가로 : " + getMeasuredWidth());

                        if (getMeasuredWidth() > 2) {

                            LogUtil.d("@@선택안된 뷰 가로 : " + getMeasuredWidth());
                            getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            changeMargin(Utils.dpToPx(getContext(), (int) (getMeasuredWidth() * 0.3)));
                        }
                    }
                });
            } else {
                this.changeMargin(Utils.dpToPx(getContext(), (int) (this.getMeasuredWidth() * 0.3)));
            }
        }
    }


    public void changeMargin(int margin) {

        if (margin == 0) {
            binding.layoutSubCategory.setVisibility(View.VISIBLE);
        } else {
            binding.layoutSubCategory.setVisibility(View.GONE);
        }


        LinearLayout.LayoutParams params = (LayoutParams) binding.layoutMain.getLayoutParams();
        params.rightMargin = (int) (margin);
        binding.layoutMain.setLayoutParams(params);

//        Animation a = new Animation() {
//
//            @Override
//            protected void applyTransformation(float interpolatedTime, Transformation t) {
//                LinearLayout.LayoutParams params = (LayoutParams) binding.layoutMain.getLayoutParams();
//                params.rightMargin = (int) (margin * interpolatedTime);
//                binding.layoutMain.setLayoutParams(params);
//
//            }
//        };
//
//        if (margin == 0) {
//            a.setDuration(0); // in ms
//        } else {
//            a.setDuration(0); // in ms
//        }
//
//        this.binding.layoutMain.startAnimation(a);
    }

    public interface SubCategoryClickItemListener {
        void click(OrderCategoryData.CategoryItem item);

    }

    public void setSubCategoryClickItemListener(SubCategoryClickItemListener clickItemListener) {
        this.subCategoryClickItemListener = clickItemListener;

    }

    public SubCategoryClickItemListener subCategoryClickItemListener;

    public void subCategoryClick() {
        if (this.binding.layoutSubCategory.getChildCount() > 0) {
            this.binding.layoutSubCategory.getChildAt(0).performClick();
        }
    }


    public void setSubCategories(ArrayList<OrderCategoryData.CategoryItem> subList) {
        this.binding.layoutSubCategory.removeAllViews();
        for (OrderCategoryData.CategoryItem item : subList) {
            Category2 category2 = new Category2(getContext());
            category2.setSelected(false);
            category2.setTitle(item.categoryName);
            category2.setOnClickListener(v -> {

                this.sideCategoryInit();
                ((Category2) v).setSelected(true);
                if (this.subCategoryClickItemListener != null) {
                    this.subCategoryClickItemListener.click(item);
                }
            });
            this.binding.layoutSubCategory.addView(category2);
        }

    }


    private void sideCategoryInit() {
        int childCount = this.binding.layoutSubCategory.getChildCount();
        for (int count = 0; count < childCount; count++) {
            ((Category2) this.binding.layoutSubCategory.getChildAt(count)).setSelected(false);
        }
    }


}
