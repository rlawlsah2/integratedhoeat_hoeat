package com.cdef.hoeat.view.customView.type2;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.core.widget.TextViewCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.dataModel.MenuSideOptionItemData;
import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.Type2AdapterSetMenuDetailSelectedOptionListItemBinding;

/**
 * 계산서 상세보기에서 선택된 메뉴의 옵션을 표시하기 위한 뷰
 */

public class MenuDetailResultOptionListItem extends LinearLayout {


    Type2AdapterSetMenuDetailSelectedOptionListItemBinding  binding;
    Context mContext;
    int[] sizeList = {8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19};

    public void setTitle(String title) {
        this.binding.textTitle.setText(title);
    }

    public MenuDetailResultOptionListItem setOption(MenuSideOptionItemData itemData)
    {
        this.binding.setOption(itemData);
        return this;
    }

    public MenuSideOptionItemData getOption()
    {
        return this.binding.getOption();
    }




    public static MenuDetailResultOptionListItem getInstance(Context context) {
        return new MenuDetailResultOptionListItem(context);
    }
    private MenuDetailResultOptionListItem(Context context) {
        super(context);
        this.mContext = context;
        LogUtil.d("SideMenuListItem1");
        registerHandler(null);
    }

    public MenuDetailResultOptionListItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        LogUtil.d("SideMenuListItem2");
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SideMenuListItem);
        registerHandler(typedArray);

    }

    public MenuDetailResultOptionListItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        LogUtil.d("SideMenuListItem3");
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SideMenuListItem);
        registerHandler(typedArray);
    }

    private void registerHandler(TypedArray typedArray) {

        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.type2_adapter_set_menu_detail_selected_option_list_item, this, true);
        if (typedArray != null) {
            String title = typedArray.getString(R.styleable.SideMenuListItem_menuTitle);
            this.binding.textTitle.setText(title);
        }
        TextViewCompat.setAutoSizeTextTypeWithDefaults(this.binding.textTitle, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        TextViewCompat.setAutoSizeTextTypeUniformWithPresetSizes(this.binding.textTitle,
                sizeList, TypedValue.COMPLEX_UNIT_SP);
        this.setSelected(false);
    }


}
