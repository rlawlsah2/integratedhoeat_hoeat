package com.cdef.hoeat.view.customView.menuOption;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.cdef.commonmodule.dataModel.MenuSideGroupItemData;
import com.cdef.commonmodule.dataModel.MenuSideOptionItemData;
import com.cdef.hoeat.R;
import com.cdef.hoeat.utils.Utils;
import com.cdef.hoeat.databinding.LayoutSetGroupListItemBinding;

import java.util.ArrayList;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class MenuDetailSetGroupListItem extends LinearLayout {


    LayoutSetGroupListItemBinding binding;
    Context mContext;
    OptionClickListener clickListener;

    public ArrayList<MenuSideOptionItemData> getSelectedOptionList() {
        return selectedOptionList;
    }

    public void setSelectedOptionList(ArrayList<MenuSideOptionItemData> selectedOptionList) {
        this.selectedOptionList = selectedOptionList;
        this.binding.setSelectedOptionList(this.selectedOptionList);
        this.binding.setSelectedOptionListStr(Utils.getSelectedItemListStr(this.selectedOptionList));
    }

    ArrayList<MenuSideOptionItemData> selectedOptionList;

    public static MenuDetailSetGroupListItem getInstance(Context context) {
        return new MenuDetailSetGroupListItem(context);
    }

    public MenuDetailSetGroupListItem setData(MenuSideGroupItemData data) {
        this.binding.setGroupItem(data);
        ////맨처음 접근할때 초기 선택값들 체크해주려는 로직
        if (this.selectedOptionList == null || this.selectedOptionList.size() == 0) {
            ///선택된게 하나도 없는경우
            if (data != null && data.sideOptions != null && data.sideOptions.size() > 0) {
                ArrayList<MenuSideOptionItemData> tmp = new ArrayList<>();
                for (MenuSideOptionItemData item : data.sideOptions) {
                    if (item.defaultItem == 1) {
                        tmp.add(item);
                    }
                }
                if (tmp.size() > 0) {
                    setSelectedOptionList(tmp);
                }
            }
        }
        return this;
    }

    public MenuSideGroupItemData getData() {
        return this.binding.getGroupItem();
    }

    public MenuDetailSetGroupListItem(Context context) {
        super(context);
        this.mContext = context;
        registerHandler(null);
    }

    public MenuDetailSetGroupListItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

    }

    public MenuDetailSetGroupListItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void registerHandler(TypedArray typedArray) {
        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.layout_set_group_list_item, this, true);
        this.binding.setMenuDetailSetOptionListItem(this);
    }

    public MenuDetailSetGroupListItem setClickListener(OptionClickListener clickListener) {
        this.clickListener = clickListener;
        this.binding.setClickListener(this.clickListener);
        return this;
    }


    public interface OptionClickListener {
        void openOptionDialog(ArrayList<MenuSideOptionItemData> selectedOptions, MenuDetailSetGroupListItem view);
    }

    /**
     * 필수 항목인데 선택 안했는지 확인
     * true : 문제없음
     * false : 안됨
     */
    public boolean isNecessaryAndSelect() {
        return (this.binding.getGroupItem().isNecessary == 0) || (this.binding.getGroupItem().isNecessary == 1 && (this.selectedOptionList != null && this.selectedOptionList.size() > 0));

    }

    /**
     * 선택된 항목을 넘겨줌
     **/
    public ArrayList<MenuSideOptionItemData> getSelectedOptions() {
        return this.selectedOptionList;
    }
}
