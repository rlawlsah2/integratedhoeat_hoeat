package com.cdef.hoeat.view.customView;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Build;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.cdef.hoeat.R;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.ResourceUtil;
import com.cdef.hoeat.databinding.LayoutSideMenuListItemBinding;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class SideMenuListItem extends LinearLayout {


    LayoutSideMenuListItemBinding binding;
    Context mContext;

    public void setTitle(String title) {
        this.binding.textTitle.setText(title);
    }

    public SideMenuListItem(Context context) {
        super(context);
        this.mContext = context;
        LogUtil.d("SideMenuListItem1");
        registerHandler(null);
    }

    public SideMenuListItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        LogUtil.d("SideMenuListItem2");
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SideMenuListItem);
        registerHandler(typedArray);

    }

    public SideMenuListItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        LogUtil.d("SideMenuListItem3");
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SideMenuListItem);
        registerHandler(typedArray);
    }

    private void registerHandler(TypedArray typedArray) {

        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.layout_side_menu_list_item, this, true);
        this.setSelected(false);
//        this.textTitle.setSelected(false);
        /////attrs를 기반으로 뷰를 셋팅한다
        if (typedArray != null) {
            String title = typedArray.getString(R.styleable.SideMenuListItem_menuTitle);
            this.binding.textTitle.setText(title);
        }
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        this.binding.textTitle.setSelected(selected);
        if (selected) {
//            this.binding.viewState.setBackgroundColor(Color.parseColor(ResourceUtil.getInstance().getcButtonSelected()));
            this.binding.textTitle.setTextColor(Color.parseColor(ResourceUtil.getInstance().getcButtonSelected()));
        } else {
//            this.binding.viewState.setBackgroundColor(Color.TRANSPARENT);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                this.binding.textTitle.setTextColor(mContext.getColor(R.color.sideMenuTextOff_));
            } else {
                this.binding.textTitle.setTextColor(getResources().getColor(R.color.sideMenuTextOff_));
            }
        }


    }
}
