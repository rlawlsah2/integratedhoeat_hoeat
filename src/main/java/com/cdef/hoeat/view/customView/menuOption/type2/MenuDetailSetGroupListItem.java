package com.cdef.hoeat.view.customView.menuOption.type2;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.transition.Transition;
import androidx.transition.TransitionSet;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.dataModel.MenuSideGroupItemData;
import com.cdef.commonmodule.dataModel.MenuSideOptionItemData;
import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.Type2LayoutSetGroupListItemBinding;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorListenerAdapter;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ArgbEvaluator;
import com.nineoldandroids.animation.FloatEvaluator;
import com.nineoldandroids.animation.ValueAnimator;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class MenuDetailSetGroupListItem extends RelativeLayout {


    Type2LayoutSetGroupListItemBinding binding;
    Context mContext;
    OptionClickListener clickListener;

//    public ArrayList<MenuSideOptionItemData> getSelectedOptionList() {
//        return selectedOptionList;
//    }
//
//    public void setSelectedOptionList(ArrayList<MenuSideOptionItemData> selectedOptionList) {
//        this.selectedOptionList = selectedOptionList;
//        this.binding.setSelectedOptionList(this.selectedOptionList);
//        this.binding.setSelectedOptionListStr(Utils.getSelectedItemListStr(this.selectedOptionList));
//    }

//    ArrayList<MenuSideOptionItemData> selectedOptionList;

    public static MenuDetailSetGroupListItem getInstance(Context context) {
        return new MenuDetailSetGroupListItem(context);
    }

    public MenuDetailSetGroupListItem setData(MenuSideGroupItemData data) {
        this.binding.setGroupItem(data);
        this.binding.layoutSubOptions.removeAllViews();

//        LogUtil.d("row 셋팅 : " + (int) Math.ceil(data.sideOptions.size() / 3.0));
//        this.binding.layoutSubOptions.setRowCount((int) Math.ceil(data.sideOptions.size() / 3.0));
//        this.binding.layoutSubOptions.setColumnCount(3);
        for (MenuSideOptionItemData item : data.sideOptions) {
            this.binding.layoutSubOptions.addView(
                    MenuDetailSetOptionListItem.getInstance(getContext(), (data.maxGroupItem == 0 || data.maxGroupItem > 1))
                            .setData(item)
                            .setInitSelection(item.defaultItem == 1)
                            .setItemSelectionListener(view -> {
                                ///1개 항목만 선택할 수 있는지 확인. maxGroupItem의 갯수가 0이거나 선택된 갯수보다 크면 계속. 아니면 중지
                                if (this.binding.getGroupItem().maxGroupItem == 0 || (this.binding.getGroupItem().maxGroupItem > selectedItemCount() && this.binding.getGroupItem().maxGroupItem != 1)) {
                                    view.setSelection(view);
                                } else if (this.binding.getGroupItem().maxGroupItem == 1) {

                                    //다 지우고 진행
                                    ///필수 선택인 경우
                                    if (data.isNecessary == 1) {

                                        if (!view.isSelected()) {
                                            deSelectAll();
                                            view.setSelection(view);
                                        }
                                    } else {

                                        if (!view.isSelected()) {
                                            deSelectAll();
                                            view.setSelection(view);
                                        } else {
                                            deSelectAll();
                                        }
                                    }
                                } else {
                                    if (view.isSelected()) {
                                        view.setSelected(false);
                                    } else {
                                        Toast.makeText(getContext(), "최대 " + this.binding.getGroupItem().maxGroupItem + "종류까지만 선택가능합니다.", Toast.LENGTH_SHORT).show();
                                    }
                                }


                                if (this.clickListener != null) {
                                    this.clickListener.openOptionDialog(getSelectedOptions(), this);
                                }

                            })
                            .setItemChangeQuantityListener((view, isMinus) -> {
                                if (isMinus) {
                                    view.changeQuantity(view, isMinus); //마이너스면 딱히 상관없음
                                } else {
                                    ///1개 항목만 선택할 수 있는지 확인. maxGroupItem의 갯수가 0이거나 선택된 갯수보다 크면 계속. 아니면 중지
                                    if (this.binding.getGroupItem().maxGroupItem == 0 || (this.binding.getGroupItem().maxGroupItem > selectedItemCount() && this.binding.getGroupItem().maxGroupItem != 1)) {
                                        view.changeQuantity(view, isMinus);
                                    } else if (this.binding.getGroupItem().maxGroupItem == 1) {
                                        //다 지우고 진행
                                        if (!view.isSelected()) {
                                            deSelectAll();
                                            view.changeQuantity(view, isMinus);
                                        } else {
                                            view.changeQuantity(view, isMinus);
                                        }
                                    } else {
                                        Toast.makeText(getContext(), "최대 " + this.binding.getGroupItem().maxGroupItem + "종류까지만 선택가능합니다.", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                if (this.clickListener != null) {
                                    this.clickListener.openOptionDialog(getSelectedOptions(), this);
                                }

                            })
            );
        }
//        ////맨처음 접근할때 초기 선택값들 체크해주려는 로직
//        if (this.selectedOptionList == null || this.selectedOptionList.size() == 0) {
//            ///선택된게 하나도 없는경우
//
//            if (data != null && data.sideOptions != null && data.sideOptions.size() > 0) {
//                ArrayList<MenuSideOptionItemData> tmp = new ArrayList<>();
//                for (MenuSideOptionItemData item : data.sideOptions) {
//                    if (item.defaultItem == 1) {
//                        tmp.add(item);
//                    }
//                }
//                if (tmp.size() > 0) {
//                    setSelectedOptionList(tmp);
//                }
//
//            }
//        }
        return this;
    }

    public MenuSideGroupItemData getData() {
        return this.binding.getGroupItem();
    }

    public MenuDetailSetGroupListItem(Context context) {
        super(context);
        this.mContext = context;
        registerHandler(null);
    }

    public MenuDetailSetGroupListItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

    }

    public MenuDetailSetGroupListItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void registerHandler(TypedArray typedArray) {
        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.type2_layout_set_group_list_item, this, true);
//        this.binding.setMenuDetailSetOptionListItem(this);
    }

    public MenuDetailSetGroupListItem setClickListener(OptionClickListener clickListener) {
        this.clickListener = clickListener;
//        this.binding.setClickListener(this.clickListener);
        return this;
    }


    public interface OptionClickListener {
        void openOptionDialog(ArrayList<MenuSideOptionItemData> selectedOptions, MenuDetailSetGroupListItem view);
    }

//    /**
//     * 필수 항목인데 선택 안했는지 확인
//     * true : 문제없음
//     * false : 안됨
//     */
//    public boolean isNecessaryAndSelect() {
//        return (this.binding.getGroupItem().isNecessary == 0) || (this.binding.getGroupItem().isNecessary == 1 && (this.selectedOptionList != null && this.selectedOptionList.size() > 0));
//
//    }
//

    /**
     * 선택된 항목을 넘겨줌
     **/
    public ArrayList<MenuSideOptionItemData> getSelectedOptions() {
        ArrayList<MenuSideOptionItemData> selectedOptios = new ArrayList<>();
        for (int i = 0; i < this.binding.layoutSubOptions.getChildCount(); i++) {
            if (this.binding.layoutSubOptions.getChildAt(i).isSelected()) {
                selectedOptios.add(((MenuDetailSetOptionListItem) this.binding.layoutSubOptions.getChildAt(i)).getOption());
            }
        }
        return selectedOptios;
    }


    /**
     * 선택된 아이템 갯수 가져오는 함수
     */
    public int selectedItemCount() {
        int count = 0;
        for (int i = 0; i < this.binding.layoutSubOptions.getChildCount(); i++) {
            if (this.binding.layoutSubOptions.getChildAt(i).isSelected())
                count++;
        }
        return count;
    }

    /**
     * 모든 아이템 선택 해제
     */
    public void deSelectAll() {
        for (int i = 0; i < this.binding.layoutSubOptions.getChildCount(); i++) {
            this.binding.layoutSubOptions.getChildAt(i).setSelected(false);
        }
    }


    ///이부분을 누를 수 있도록 유도하는 애니메이션 처리
    public void showHint() {

        int colorFrom = getResources().getColor(R.color.login);
        int colorTo = getResources().getColor(R.color.loginSelected);
        WeakReference<ValueAnimator> colorAnimator = new WeakReference<>(ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo));
        colorAnimator.get().setStartDelay(100);
        colorAnimator.get().setDuration(500);
        colorAnimator.get().addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(com.nineoldandroids.animation.Animator animation) {
                super.onAnimationEnd(animation);
                colorAnimator.get().removeListener(this);
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
            }
        });
        colorAnimator.get().addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                binding.layoutHintSelect.setBackgroundColor(((int) colorAnimator.get().getAnimatedValue()));
            }
        });

        WeakReference<ValueAnimator> colorAnimatorReverse = new WeakReference<>(ValueAnimator.ofObject(new ArgbEvaluator(), colorTo, colorFrom));
        colorAnimatorReverse.get().setStartDelay(100);
        colorAnimatorReverse.get().setDuration(500);
        colorAnimatorReverse.get().addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(com.nineoldandroids.animation.Animator animation) {
                super.onAnimationEnd(animation);
                colorAnimatorReverse.get().removeListener(this);
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
            }
        });
        colorAnimatorReverse.get().addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                binding.layoutHintSelect.setBackgroundColor(((int) colorAnimatorReverse.get().getAnimatedValue()));
            }
        });


        WeakReference<ValueAnimator> alphaAnimator = new WeakReference<>(ValueAnimator.ofObject(new FloatEvaluator(), 0.0, 1.0));
        alphaAnimator.get().setStartDelay(100);
        alphaAnimator.get().setDuration(500);
        alphaAnimator.get().addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(com.nineoldandroids.animation.Animator animation) {
                super.onAnimationEnd(animation);
                alphaAnimator.get().removeListener(this);
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
            }
        });
        alphaAnimator.get().addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                LogUtil.d("알림 : " + alphaAnimator.get().getAnimatedValue());
                binding.layoutHintSelect.setAlpha(((float) alphaAnimator.get().getAnimatedValue()));
            }
        });


        WeakReference<ValueAnimator> alphaAnimatorReverse = new WeakReference<>(ValueAnimator.ofObject(new FloatEvaluator(), 1.0, 0.0));
        alphaAnimatorReverse.get().setStartDelay(100);
        alphaAnimatorReverse.get().setDuration(500);
        alphaAnimatorReverse.get().addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(com.nineoldandroids.animation.Animator animation) {
                super.onAnimationEnd(animation);
                alphaAnimatorReverse.get().removeListener(this);
            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
            }
        });
        alphaAnimatorReverse.get().addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                binding.layoutHintSelect.setAlpha(((float) alphaAnimatorReverse.get().getAnimatedValue()));
            }
        });


        WeakReference<AnimatorSet> animatorSet = new WeakReference<>(new AnimatorSet());
        animatorSet.get()
                .play(colorAnimator.get()).with(alphaAnimator.get())
                .after(colorAnimatorReverse.get()).with(alphaAnimatorReverse.get());


    }

}
