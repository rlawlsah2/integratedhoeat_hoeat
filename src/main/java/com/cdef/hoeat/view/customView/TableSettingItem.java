package com.cdef.hoeat.view.customView;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableField;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.TextViewCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.cdef.hoeat.R;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.hoeat.databinding.LayoutTableSettingItemBinding;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class TableSettingItem extends LinearLayout {

    static int width = 0;

    public static RequestOptions myOptions = null;
    LayoutTableSettingItemBinding binding;
    private eventListener listener;

    ObservableField<Integer> count = new ObservableField<>(0);


    public TableSettingItem(Context context) {
        super(context);
        registerHandler(null);
    }

    public TableSettingItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.ButtonTableSettingItem);
        registerHandler(typedArray);
    }

    public TableSettingItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.ButtonTableSettingItem);
        registerHandler(typedArray);
    }

    private void registerHandler(TypedArray typedArray) {
        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.layout_table_setting_item, this, true);
        this.binding.setItem(this);
        this.binding.setCount(this.count.get());

        if (myOptions == null) {
            myOptions = new RequestOptions()
                    .transform(new CircleCrop())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .skipMemoryCache(true);
        }
//        ViewTreeObserver vto = this.binding.image.getViewTreeObserver();
//        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                int layoutWidth = getMeasuredWidth();
//                LogUtil.d("이미지 사이즈 전  width: " + getMeasuredWidth());
//
//                if (layoutWidth != 0) {
//                    binding.image.getViewTreeObserver().removeGlobalOnLayoutListener(this);
//                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) binding.image.getLayoutParams();
//                    lp.height = layoutWidth- Utils.dpToPx(getContext(), 10);
//                    lp.width = layoutWidth- Utils.dpToPx(getContext(), 10);
//                    lp.setMargins(Utils.dpToPx(getContext(), 5),Utils.dpToPx(getContext(), 5),Utils.dpToPx(getContext(), 5),Utils.dpToPx(getContext(), 5));
//                    binding.image.setLayoutParams(lp);
//
//                    RelativeLayout.LayoutParams lp2 = (RelativeLayout.LayoutParams) binding.layoutCount.getLayoutParams();
//                    lp2.height = layoutWidth - Utils.dpToPx(getContext(), 23);
//                    lp2.width = layoutWidth - Utils.dpToPx(getContext(), 23);
//                    binding.layoutCount.setLayoutParams(lp2);
//                }
//                LogUtil.d("이미지 사이즈 후 width: " + getMeasuredWidth());
//            }
//        });

//
        if (typedArray != null) {

            int bg_resID = typedArray.getResourceId(R.styleable.ButtonTableSettingItem_image, 0);
            String title = typedArray.getString(R.styleable.ButtonTableSettingItem_itemTitle);

            Glide.with(getContext())
                    .load(bg_resID)
                    .apply(myOptions)
                    .into(binding.image);

            this.binding.textTitle.setText(title);
//            TextViewCompat.setAutoSizeTextTypeWithDefaults(this.binding.textTitle, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        }
    }

    public void setTitle(String title) {
        this.binding.textTitle.setText(title);
        this.binding.layoutTitle.setSelected(true);

    }

    public void setImage(String image) {


        Glide.with(getContext()).load(image)
                .apply(myOptions)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        binding.image.setBackground(resource);
                    }
                });
    }

    public void setListener(eventListener listener) {
        this.listener = listener;
    }


    public interface eventListener {
        void countChange(String title, Integer count);
    }


    public void deSelect() {
        this.binding.layoutCount.setVisibility(View.GONE);
//        this.binding.textCount.setText("0");
        this.count.set(0);
        this.binding.setCount(this.count.get());
        this.setSelected(false);

        if (this.listener != null)
            this.listener.countChange(this.getTitle(), this.count.get());
    }

    public void select(int currentCount) {
        this.binding.layoutCount.setVisibility(View.VISIBLE);
        this.count.set(currentCount);
        this.binding.setCount(this.count.get());
        this.setSelected(true);

        if (this.listener != null)
            this.listener.countChange(this.getTitle(), this.count.get());
    }

    private void select() {
        this.binding.layoutCount.setVisibility(View.VISIBLE);
//        this.binding.textCount.setText("1");
        this.count.set(1);
        this.binding.setCount(this.count.get());
        this.setSelected(true);
        this.binding.layoutTitle.setSelected(true);


        if (this.listener != null)
            this.listener.countChange(this.getTitle(), this.count.get());
    }

    public void goClickImage(View view) {
        if (!this.isSelected())
            select();
    }


    public String getTitle() {
        return this.binding.textTitle.getText().toString();
    }

    public String getCount() {
        return this.binding.textCount.getText().toString();
    }

    @Override
    protected void onDetachedFromWindow() {
        this.binding.image.setBackground(null);
        this.binding.layoutCount.setBackground(null);
        super.onDetachedFromWindow();
    }

    /***
     * 수량 조절 기능
     * **/
    public void setCount(View v) {
        LogUtil.d("눌리긴 눌렸냐?");
        LogUtil.d("눌리긴 눌렸냐? : " + this.count.get());
        int iCount = 0;

        switch (v.getId()) {
            case R.id.layoutPlus:
                if (count != null) {
                    iCount = this.count.get();
                    iCount++;
                    this.count.set(iCount);
                } else {
                    this.count.set(1);
                }
                break;

            case R.id.layoutMinus:

                if (count != null) {
                    iCount = this.count.get();
                    if (iCount > 1) {
                        iCount--;
                    } else if (iCount == 1) {
                        deSelect();
                        if (this.listener != null)
                            this.listener.countChange(this.getTitle(), this.count.get());
                        break;
                    }

                    this.count.set(iCount);
                } else {
                    this.count.set(1);
                }
                break;
        }


        this.binding.setCount(this.count.get());
        if (this.listener != null)
            this.listener.countChange(this.getTitle(), this.count.get());

    }
}
