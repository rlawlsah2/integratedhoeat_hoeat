package com.cdef.hoeat.view.customView.menuOption.type2;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;

import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.bumptech.glide.request.RequestOptions;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.dataModel.MenuSideOptionItemData;
import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.Type2LayoutSetOptionListItemBinding;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class MenuDetailSetOptionListItem extends LinearLayout {


    Type2LayoutSetOptionListItemBinding binding;
    Context mContext;
    RequestOptions requestOptions;

    public MenuSideOptionItemData getOption() {
        return this.binding.getOptionItem();
    }

    public static MenuDetailSetOptionListItem getInstance(Context context, boolean isMultiSelectable) {
        return new MenuDetailSetOptionListItem(context, isMultiSelectable);
    }

    public MenuDetailSetOptionListItem setData(MenuSideOptionItemData data) {
        this.binding.setOptionItem(data);
        this.binding.setIsMultiSelectable(this.isMultiSelectable);
//        if (data != null && data.foodImage != null) {
//            Glide.with(this)
////                .load("https://cdefi-psp.s3.amazonaws.com/menus/image_3356121d-e82d-4ff1-aeae-d174dab736bc.png")
//                    .load(data.foodImage)
//                    .apply(this.requestOptions).into(this.binding.imageCheckBox);
//        }
        return this;
    }

    public boolean isMultiSelectable = false;

    public MenuDetailSetOptionListItem(Context context, boolean isMultiSelectable) {
        super(context);
        this.mContext = context;
        this.isMultiSelectable = isMultiSelectable;
        registerHandler(null);
    }

    public MenuDetailSetOptionListItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

    }

    public MenuDetailSetOptionListItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void registerHandler(TypedArray typedArray) {
        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.type2_layout_set_option_list_item, this, true);

//        GridLayout.LayoutParams lp = (GridLayout.LayoutParams) getLayoutParams();
//        lp.columnSpec = GridLayout.spec(GridLayout.UNDEFINED, 1, GridLayout.FILL, 1);
//        setLayoutParams(lp);

//
//        this.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                if (getMeasuredWidth() > 3) {
//                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                    android.support.v7.widget.GridLayout.LayoutParams lp = (GridLayout.LayoutParams) getLayoutParams();
//                    lp.height = (int) (getMeasuredWidth() / 3.0);
//                    lp.width = lp.height;
//                    setLayoutParams(lp);
//                }
//            }
//        });
        this.binding.setItemView(this);
        this.requestOptions = new RequestOptions();
        this.requestOptions.override(100, 100);
        this.requestOptions.circleCrop();

    }

    public MenuDetailSetOptionListItem setInitSelection(boolean isSelected) {

        LogUtil.d("해당 아이템 선택된거냐 setInitSelection: " + isSelected);
        if (isSelected) {
            this.setSelection(this);
//            this.setSelected(true);
        }
        return this;
    }

    public void setSelection(View view) {
        this.setSelected(!isSelected());
//        this.itemSelectionListener.clickItem(this);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
//        this.binding.imageCheckBox.setSelected(selected);
        this.binding.setIsSelected(selected);
        if (!selected) {
            MenuSideOptionItemData tmp = binding.getOptionItem();
            tmp.quantity = 1;
            this.binding.setOptionItem(tmp);
        }
    }

    public void changeQuantity(View view, boolean isMinus) {
        MenuSideOptionItemData tmp = binding.getOptionItem();

        if (isMinus) {
            if (tmp.quantity > 1) {
                tmp.quantity--;
            } else {
                ///1로 초기화 하고, selection해제
                tmp.quantity = 0;
            }

        } else {
            if (tmp.maxQuantity > tmp.quantity) {
                tmp.quantity++;
            }
        }

        if (tmp.quantity == 0) {
            tmp.quantity = 1;
            this.setSelected(false);
        } else if (tmp.quantity > 0) {
            this.setSelected(true);
        }

        this.binding.setOptionItem(tmp);
//        this.itemChangeQuantityListener.changeQuantity();

    }

    public MenuSideOptionItemData getItem() {
        try {
            return (MenuSideOptionItemData) this.binding.getOptionItem().getClone();
        } catch (CloneNotSupportedException e) {
            return this.binding.getOptionItem();
        }
    }


    ItemSelectionListener itemSelectionListener;

    public MenuDetailSetOptionListItem setItemSelectionListener(ItemSelectionListener itemSelectionListener) {
        this.itemSelectionListener = itemSelectionListener;
        this.binding.setItemSelectionListener(this.itemSelectionListener);
        return this;
    }

    public MenuDetailSetOptionListItem setItemChangeQuantityListener(ItemChangeQuantityListener itemChangeQuantityListener) {
        this.itemChangeQuantityListener = itemChangeQuantityListener;
        this.binding.setItemChangeQuantityListener(this.itemChangeQuantityListener);
        return this;
    }

    ItemChangeQuantityListener itemChangeQuantityListener;


    public interface ItemSelectionListener {
        void clickItem(MenuDetailSetOptionListItem view);
    }

    public interface ItemChangeQuantityListener {
        void changeQuantity(MenuDetailSetOptionListItem view, boolean isMinus);
    }


}
