package com.cdef.hoeat.view.customView;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;

import java.util.ArrayList;
import java.util.List;

public class CTextView extends AppCompatTextView
{
    private int mAvailableWidth  = 0;
    private Paint mPaint         = null;
    private List<String> mCutStr = new ArrayList<String>();
 
    public CTextView(Context context)
    {
        super(context);
    }
 
    public CTextView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }
 
    private int setTextInfo(String text, int textWidth, int textHeight)
    {
        mPaint = getPaint();
        mPaint.setColor(getTextColors().getDefaultColor());
        mPaint.setTextSize(getTextSize());
//        
        int mTextHeight = textHeight;
// 
//        if (textWidth > 0) 
//        {
//        	mAvailableWidth = textWidth - this.getPaddingLeft() - this.getPaddingRight();
//        	mCutStr.clear();
//        	
//        	int end = 0;
//        	String[] textArr = text.split("\n");
//        	
////        	byte[] bt = null;
//        	for(int i=0; i<textArr.length; i++) 
//        	{
//        		if(textArr[i].length() == 0) textArr[i] = " ";
//        		do 
//        		{
//        			end = mPaint.breakText(textArr[i], true, mAvailableWidth, null);
//        			if (end > 0) 
//        			{
//        				mCutStr.add(textArr[i].substring(0, end));
//        				////////////////////////////////////////////
////        				bt = textArr[i].getBytes();
////            			Log.e("@@@TAG@@@","text char count ["+ i +"] " + end);
////        				Log.e("@@@TAG@@@","text line byte ["+ i +"]" + bt.length);
//        				////////////////////////////////////////////
//        				textArr[i] = textArr[i].substring(end);
//        				if (textHeight == 0) 
//        					mTextHeight += getLineHeight();
//        			}
//        		} while (end > 0);
//        	}
//        }
//        mTextHeight += getPaddingTop() + getPaddingBottom();
//        return mTextHeight;
    	
    	
    	if (textWidth > 0) {
    		  // 값 세팅
    		  mAvailableWidth = textWidth - this.getPaddingLeft() - this.getPaddingRight();

    		  mCutStr.clear();
    		  int end = 0;
    		  String[] textArr = text.split("\n");
    		  for(int i=0; i<textArr.length; i++) {
    		    if(textArr[i].length() == 0) textArr[i] = " ";
    		      do {
    		        // 글자가 width 보다 넘어가는지 체크
    		        end = mPaint.breakText(textArr[i], true, mAvailableWidth, null);
    		        if (end > 0) {
    		          // 자른 문자열을 문자열 배열에 담아 놓는다.
    		          mCutStr.add(textArr[i].substring(0, end));
    		          // 넘어간 글자 모두 잘라 다음에 사용하도록 세팅
    		          textArr[i] = textArr[i].substring(end);
    		          // 다음라인 높이 지정
    		          if (textHeight == 0) mTextHeight += getLineHeight();
    		        }
    		      } while (end > 0);
    		    }
    		}
		mTextHeight += getPaddingTop() + getPaddingBottom();
		return (int) (mTextHeight*(1.05));
    }
 
    @Override
    protected void onDraw(Canvas canvas)
    {
        // 글자 높이 지정
        float height = getPaddingTop() + getLineHeight();
        for (String text : mCutStr)
        {
            canvas.drawText(text, getPaddingLeft(), height, mPaint);
            height += getLineHeight();
        }
    }
 
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) 
    {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        
        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
        int height = setTextInfo(this.getText().toString(), parentWidth, parentHeight);
        
        if (parentHeight == 0)
            parentHeight = height;
        this.setMeasuredDimension(parentWidth, parentHeight);
    }
 
    @Override
    protected void onTextChanged(final CharSequence text, final int start, final int before, final int after)
    {
        setTextInfo(text.toString(), this.getWidth(), this.getHeight());
    }
 
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) 
    {
        if (w != oldw) 
        {
            setTextInfo(this.getText().toString(), w, h);
        }
    }
    
    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt)
    {
    	// TODO Auto-generated method stub
    	super.onScrollChanged(l, t, oldl, oldt);
    }
}
