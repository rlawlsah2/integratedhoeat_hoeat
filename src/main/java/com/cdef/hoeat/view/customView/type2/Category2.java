package com.cdef.hoeat.view.customView.type2;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.core.widget.TextViewCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.Type2LayoutCategory2Binding;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class Category2 extends LinearLayout {


    Type2LayoutCategory2Binding binding;
    Context mContext;
    int[] sizeList = {8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19};


    public void setTitle(String title) {
        this.binding.textTitle.setText(title);
    }

    public Category2(Context context) {
        super(context);
        this.mContext = context;
        LogUtil.d("SideMenuListItem1");
        registerHandler(null);
    }

    public Category2(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        LogUtil.d("SideMenuListItem2");
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SideMenuListItem);
        registerHandler(typedArray);

    }

    public Category2(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        LogUtil.d("SideMenuListItem3");
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SideMenuListItem);
        registerHandler(typedArray);
    }

    private void registerHandler(TypedArray typedArray) {

        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.type2_layout_category2, this, true);
        if (typedArray != null) {
            String title = typedArray.getString(R.styleable.SideMenuListItem_menuTitle);
            this.binding.textTitle.setText(title);
        }
        TextViewCompat.setAutoSizeTextTypeWithDefaults(this.binding.textTitle, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        TextViewCompat.setAutoSizeTextTypeUniformWithPresetSizes(this.binding.textTitle,
                sizeList, TypedValue.COMPLEX_UNIT_SP);
        this.setSelected(false);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        this.binding.textTitle.setSelected(selected);
    }


}
