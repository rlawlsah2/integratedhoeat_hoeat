package com.cdef.hoeat.view.customView;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Build;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.cdef.hoeat.R;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.ResourceUtil;
import com.cdef.hoeat.databinding.LayoutSideMenuListSubItemBinding;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class SideMenuListSubItem extends LinearLayout {


    LayoutSideMenuListSubItemBinding binding;
    Context mContext;

    public SideMenuListSubItem(Context context) {
        super(context);
        this.mContext = context;
        LogUtil.d("SideMenuListItem1");
        registerHandler(null);
    }

    public SideMenuListSubItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        LogUtil.d("SideMenuListItem2");
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SideMenuListSubItem);
        registerHandler(typedArray);

    }

    public SideMenuListSubItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        LogUtil.d("SideMenuListItem3");
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.SideMenuListSubItem);
        registerHandler(typedArray);
    }

    private void registerHandler(TypedArray typedArray) {
        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.layout_side_menu_list_sub_item, this, true);

        /////attrs를 기반으로 뷰를 셋팅한다
        if (typedArray != null) {
            String title = typedArray.getString(R.styleable.SideMenuListSubItem_menuSubTitle);
            this.binding.textTitle.setText(title);
        }
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        this.binding.textTitle.setSelected(selected);
        this.binding.viewState.setSelected(selected);

        if (selected)
            this.binding.textTitle.setTextColor(Color.parseColor(ResourceUtil.getInstance().getcButtonSubSelected()));
        else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.binding.textTitle.setTextColor(mContext.getColor(R.color.sideMenuTextOff_));
        }

    }

    public void setTitle(String text) {
        this.binding.textTitle.setText(text.replace("(경성)", ""));
    }

    public void setIcon(int id) {
        this.binding.viewState.setBackgroundResource(id);
    }


}
