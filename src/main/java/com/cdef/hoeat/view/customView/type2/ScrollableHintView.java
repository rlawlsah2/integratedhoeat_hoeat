package com.cdef.hoeat.view.customView.type2;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.LayoutScrollableHintViewBinding;
import com.cdef.hoeat.utils.Utils;

public class ScrollableHintView extends RelativeLayout {

    LayoutScrollableHintViewBinding binding;

    public ScrollableHintView(Context context) {
        super(context);
        this.initViews();
    }

    public ScrollableHintView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initViews();
    }

    public ScrollableHintView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.initViews();
    }

    public void initViews() {
        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.layout_scrollable_hint_view, this, true);
        this.binding.layoutMain.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                if (binding.layoutMain.getMeasuredWidth() > 2) {
                    binding.layoutMain.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int layoutMainWidth = binding.layoutMain.getMeasuredWidth();
                    int layoutMainHeight = binding.layoutMain.getMeasuredHeight();

                    LinearLayout.LayoutParams pointLP = (LinearLayout.LayoutParams) binding.imagePoint.getLayoutParams();
                    pointLP.height = (int) (layoutMainHeight * 0.15);
//                    pointLP.leftMargin = (int) (layoutMainWidth * 0.25);
                    binding.imagePoint.setLayoutParams(pointLP);


                    RelativeLayout.LayoutParams layoutArrowLP = (RelativeLayout.LayoutParams) binding.layoutArrow.getLayoutParams();
                    layoutArrowLP.width = (int) (layoutMainWidth * 0.8);
                    layoutArrowLP.height = (int) (layoutArrowLP.width * (272 / 180.0) * 2 + Utils.dpToPx(getContext(), 100));
                    binding.layoutArrow.setLayoutParams(layoutArrowLP);


                }

            }
        });

    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility == View.VISIBLE) {
            if (this.binding.imagePoint.getAnimation() == null)
                this.binding.imagePoint.setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.up_down_like_floating_400));
        } else {
            this.binding.imagePoint.clearAnimation();
        }
    }
}
