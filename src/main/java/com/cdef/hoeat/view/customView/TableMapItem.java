package com.cdef.hoeat.view.customView;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;

import com.cdef.hoeat.R;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.cdef.hoeat.view.dialog.MapDialog;
import com.cdef.hoeat.databinding.LayoutTableMapItemBinding;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */


public class TableMapItem extends RelativeLayout {

    LayoutTableMapItemBinding binding;

    Context mContext;

    private eventListener listener;


    private MapDialog.MODE mode = MapDialog.MODE.VIEW;

    public void setMODE(MapDialog.MODE mode) {
        this.mode = mode;
        this.binding.setMode(this.mode);
    }

    public TableMapItem(Context context) {
        super(context);
        this.mContext = context;
        registerHandler();
    }

    public TableMapItem(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        registerHandler();

    }

    public TableMapItem(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        registerHandler();

    }


    private void registerHandler() {
        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.layout_table_map_item, this, true);
        this.binding.setItemView(this);
        this.binding.imageMemberBackground.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                binding.imageMemberBackground.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int height = binding.imageMemberBackground.getMeasuredHeight();
                RelativeLayout.LayoutParams lp = (LayoutParams) binding.imageMemberBackground.getLayoutParams();
                lp.width = (int) (height * (52.4 / 58.8));
                binding.imageMemberBackground.setLayoutParams(lp);

            }
        });
    }

    public void setListener(eventListener listener) {
        this.listener = listener;
    }


    public interface eventListener {
        void countChange();
    }

    public void setData(com.cdef.commonmodule.login.TableMapItem data) {
        binding.setData(data);
        checkIsDisabled();
        checkMembers();

    }

    public com.cdef.commonmodule.login.TableMapItem getData() {
        return binding.getData();
    }

    public void refresh() {
        binding.setData(binding.getData());
        checkIsDisabled();
        checkMembers();
    }

    public void updateGameState(String gameID) {
        com.cdef.commonmodule.login.TableMapItem tmp = binding.getData();
        try {
            tmp.tableInfo.gameId = gameID;
            binding.setData(tmp);
        } catch (Exception e) {

        } finally {
            checkIsDisabled();
        }


    }

    private void checkMembers() {
        if (this.binding.getData() != null) {
            ///테이블에 대한 정보가 없으면 빈자리.
//            if (this.binding.getData().tableInfo == null
//                    || this.binding.getData().tableInfo.members == null
//                    || (this.binding.getData().tableInfo.members.men == 0 && this.binding.getData().tableInfo.members.women == 0)) {
//
//
//                ///-1. 내 테이블
//                if (this.binding.getData().tNo.equals(TabletSettingUtils.getInstacne().getTableNo())) {
//                    this.binding.imageMemberBackground.setBackgroundResource(R.drawable.img_tablemap_here);
//                    return;
//                }
//                this.binding.imageMemberBackground.setBackground(null);
//                return;
//            }

//            ///0. 내 테이블
//            if (this.binding.getData().tNo.equals(TabletSettingUtils.getInstacne().getTableNo())) {
//                this.binding.imageMemberBackground.setBackgroundResource(R.drawable.img_tablemap_here);
//                return;
//            }

//            ///1. 남자만 있는 경우
//            if (this.binding.getData().tableInfo.members.men != 0 && this.binding.getData().tableInfo.members.women == 0) {
//                this.binding.imageMemberBackground.setBackgroundResource(R.drawable.img_tablemap_member_men);
//                return;
//            }
//            ///2. 여자만 있는 경우
//            if (this.binding.getData().tableInfo.members.women != 0 && this.binding.getData().tableInfo.members.men == 0) {
//
//                this.binding.imageMemberBackground.setBackgroundResource(R.drawable.img_tablemap_member_women);
//                return;
//            }
//            ///3. 혼성인 경우
//            if (this.binding.getData().tableInfo.members.women != 0 && this.binding.getData().tableInfo.members.men != 0) {
//
//                this.binding.imageMemberBackground.setBackgroundResource(R.drawable.img_tablemap_member_mix);
//                return;
//            }


        } else {

            this.binding.imageMemberBackground.setBackground(null);
            return;
        }
    }

    private void checkIsDisabled() {
        if (this.binding.getData() != null) {
            ///테이블에 대한 정보가 없으면 빈자리.
//            if (this.binding.getData().tableInfo == null
//                    || this.binding.getData().tableInfo.members == null
//                    || (this.binding.getData().tableInfo.members.men == 0 && this.binding.getData().tableInfo.members.women == 0)) {
//                this.binding.imageDisable.setVisibility(View.VISIBLE);
//                this.binding.imageDisable.bringToFront();
//                return;
//            }
            ///주문내역이 없는 테이블은 못함
            if (this.binding.getData().tableInfo == null || this.binding.getData().tableInfo.IFSA_ORDER_ID == null) {
                this.binding.imageDisable.setVisibility(View.VISIBLE);
                this.binding.imageDisable.bringToFront();
                return;
            }

            ///게임중인경우 게임 못걸게 해야함. 단. 게임모드 뷰 일때만 해당됨.
            if ((this.binding.getMode() == MapDialog.MODE.GAME) && this.binding.getData().tableInfo.gameId != null) {
                this.binding.imageDisable.setVisibility(View.VISIBLE);
                this.binding.imageDisable.bringToFront();

                this.binding.imageGaming.setVisibility(View.VISIBLE);
                this.binding.imageGaming.bringToFront();
                return;
            }


            ///0. 내 테이블
            if (this.binding.getData().tNo.equals(TabletSettingUtils.getInstacne().getTableNo())) {
                this.binding.imageDisable.setVisibility(View.VISIBLE);
                this.binding.imageDisable.bringToFront();
                return;
            }


            this.binding.imageDisable.setVisibility(View.GONE);
            this.binding.imageGaming.setVisibility(View.GONE);
            return;

        } else {
            this.binding.imageDisable.setVisibility(View.GONE);
            this.binding.imageGaming.setVisibility(View.GONE);
            return;

        }


    }

    @Override
    public void setOnTouchListener(OnTouchListener l) {

        try {
            this.binding.imageDisable.setOnTouchListener(l);
            this.binding.imageGaming.setOnTouchListener(l);
            this.binding.imageSelection.setOnTouchListener(l);
        } catch (Exception e) {

        }

        super.setOnTouchListener(l);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        this.binding.title.setSelected(selected);
        this.binding.textTableNo.setSelected(selected);

//        if (selected) {
//            this.binding.imageSelection.setVisibility(View.VISIBLE);
//            this.binding.imageSelection.bringToFront();
//        } else {
//            this.binding.imageSelection.setVisibility(View.GONE);
//        }
    }

    public void clickImageDisable() {

    }
}
