package com.cdef.hoeat.view.customView.type2;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.core.widget.TextViewCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.Type2LayoutCheckboxWithTextBinding;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class CheckBoxWithText extends LinearLayout {


    public static int checkBoxHeight = 0;
    Type2LayoutCheckboxWithTextBinding binding;
    Context mContext;
    int[] sizeList = {8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19};

    public void setTitle(String title) {
        this.binding.setTitle(title);
    }

    public CheckBoxWithText(Context context) {
        super(context);
        this.mContext = context;
        registerHandler(null);
    }

    public CheckBoxWithText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CheckBoxWithText);
        registerHandler(typedArray);

    }

    public CheckBoxWithText(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CheckBoxWithText);
        registerHandler(typedArray);
    }


    private void registerHandler(TypedArray typedArray) {
        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.type2_layout_checkbox_with_text, this, true);
        if (typedArray != null) {
            String title = typedArray.getString(R.styleable.CheckBoxWithText_cbTitle);
//            ColorStateList titleColor = typedArray.getColorStateList(R.styleable.CheckBoxWithText_cbTitleColor);
            int checkBox = typedArray.getResourceId(R.styleable.CheckBoxWithText_cbImage, R.drawable.selector_icon_checkbox_with_text);

            this.binding.text.setTextColor(getResources().getColorStateList(R.color.selector_text_checkbox_with_text_title));
            this.binding.setTitle(title);
            this.binding.imageCheckBox.setBackgroundResource(checkBox);
        }
        TextViewCompat.setAutoSizeTextTypeWithDefaults(this.binding.text, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        TextViewCompat.setAutoSizeTextTypeUniformWithPresetSizes(this.binding.text,
                sizeList, TypedValue.COMPLEX_UNIT_SP);


        this.binding.text.postDelayed(() -> {
            if (CheckBoxWithText.checkBoxHeight == 0) {
                this.binding.text.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        int height = binding.text.getMeasuredHeight();
                        if (height > 2) {
                            binding.text.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            CheckBoxWithText.checkBoxHeight = (int) (height * 1.8f);
                            resizeCheckBox();
                        }
                    }
                });
            } else {
                this.resizeCheckBox();
            }
        }, 300);

        this.setSelected(false);
    }

    private void resizeCheckBox() {
        LinearLayout.LayoutParams lp = (LayoutParams) binding.imageCheckBox.getLayoutParams();
        lp.height = CheckBoxWithText.checkBoxHeight;
        lp.width = CheckBoxWithText.checkBoxHeight;
        binding.imageCheckBox.setLayoutParams(lp);
        binding.imageCheckBox.requestLayout();

    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        this.binding.text.setSelected(selected);
        this.binding.imageCheckBox.setSelected(selected);
//        this.binding.text.setVisibility(this.binding.text.isSelected() ? View.VISIBLE : View.GONE);
    }
}
