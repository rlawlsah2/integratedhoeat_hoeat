package com.cdef.hoeat.view.customView;

/**
 * Created by kimjinmo on 2018. 5. 3..
 */

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cdef.commonmodule.dataModel.firebaseDataSet.ExMessage;
import com.cdef.commonmodule.dataModel.firebaseDataSet.Message;
import com.cdef.hoeat.OrderAppApplication;
import com.cdef.hoeat.R;
import com.cdef.commonmodule.utils.FBPathBuilder;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.cdef.hoeat.view.activity.BaseActivity;
import com.cdef.hoeat.view.adapter.ChatListAdapter;
import com.cdef.hoeat.view.adapter.PrivateChatListAdapter;
import com.cdef.hoeat.view.dialog.MapDialog;
import com.cdef.hoeat.view.dialog.MessageDialogBuilder;
import com.cdef.hoeat.databinding.LayoutChatBinding;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kimjinmo on 2017. 8. 23..
 */

public class ChatLayout extends LinearLayout {

    LayoutChatBinding binding;


    public void close() {
        this.binding.buttonClose.performClick();
    }

    public boolean isViewing() {
        return isViewing;
    }

    public void setViewing(boolean viewing) {
        isViewing = viewing;
    }

    public CHATTYPE getChattype() {
        return this.chattype;
    }

    public EditText getEditText() {
        return this.binding.editInput;
    }


    private boolean isViewing = false;


    public void setMapListener(MapListener mapListener) {
        this.mapListener = mapListener;
        this.binding.setMapListener(this.mapListener);
    }

    MapListener mapListener;


    public CloseListener getCloseListener() {
        return closeListener;
    }

    public void setCloseListener(CloseListener closeListener) {
        this.closeListener = closeListener;
    }

    private CloseListener closeListener;


    Context mContext;
    FirebaseRecyclerAdapter chatListAdapter;
    PrivateChatListAdapter privateChatListAdapter;

    public void onViewClicked() {

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.chatListAdapter != null)
            this.chatListAdapter.startListening();
        if (this.privateChatListAdapter != null)
            this.privateChatListAdapter.startListening();
    }


    public enum CHATTYPE {
        ENTIRE, PRIVATE
    }

    private CHATTYPE chattype;

    public void onClickTotal() {
        if (chattype != CHATTYPE.ENTIRE)
            this.setChatTotal();
    }

    public void onClickPrivate() {
        if (chattype != CHATTYPE.PRIVATE)
            this.setChatPrivate();
    }


    String otherTableNo;    //1:1 채팅 상대의 테이블 번호. tno를 포함.


    public void sendMessage(View v) {
        if (chattype == CHATTYPE.PRIVATE) {
            if (this.otherTableNo == null || this.otherTableNo.length() == 0) {
                Toast.makeText(getContext(), "상대 테이블이 초기화 되었거나 비어있는 상태입니다. 나중에 시도해주세요.(1)", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TabletSettingUtils.getInstacne().getTableNo() == null || TabletSettingUtils.getInstacne().getTableNo().length() == 0) {
                Toast.makeText(getContext(), "상대 테이블이 초기화 되었거나 비어있는 상태입니다. 나중에 시도해주세요.(2)", Toast.LENGTH_SHORT).show();
                return;
            }
        }


        this.clickOUT();
        String message = this.binding.editInput.getText().toString();
        message.replaceAll("\n", "");
        LogUtil.d("메세지 보내기 : " + message);
        if (message != null && message.trim().length() > 0) {

            Map<String, Object> messageMap = new HashMap<>();
            Message messages = new Message();   //messages에 입력될 값
            messages.message = message;
            messages.date = Utils.getDate();
            messages.time = Utils.getTime();
            messages.timeStamp = -Utils.UnixTimeNow();
            LogUtil.d("메세지 보내기 chattype: " + chattype.name());

            Message lastMessages = new Message();   //lastMessages 내꺼 입력될 값
            lastMessages.message = message;
            lastMessages.date = Utils.getDate();
            lastMessages.time = Utils.getTime();
            lastMessages.read = false;
            lastMessages.timeStamp = messages.timeStamp;

            if (this.binding.getIsEntireChatMode())  ///전체채팅
            {

                String fbKey = FirebaseDatabase.getInstance().getReference().push().getKey();
                messages.sender = TabletSettingUtils.getInstacne().getTableNo(); //전체 채팅은 테이블번호 숫자만

                ///1. 상대방 데이터 넣기
                FirebaseDatabase.getInstance().getReference()
                        .child(FBPathBuilder.getInstance().init()
                                .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
                                .set("chat")
                                .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
                                .set("all")
                                .set(fbKey)
                                .complete())
                        .runTransaction(new Transaction.Handler() {
                            @Override
                            public Transaction.Result doTransaction(MutableData mutableData) {
                                LogUtil.d("메세지 보내기 doTransaction: " + messages);

                                if (messages != null) {
                                    mutableData.setValue(messages);
                                }
                                return Transaction.success(mutableData);

                            }

                            @Override
                            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

                                if (b) {
                                    //채팅 등록 성공
                                    binding.editInput.setText("");
                                    binding.buttonSend.setEnabled(false);
                                } else {
                                    Toast.makeText(getContext(), "잠시후 다시 시도해주세요", Toast.LENGTH_SHORT).show();
                                }

                            }
                        });
            } else            ///1:1채팅
            {

                String fbKey = FirebaseDatabase.getInstance().getReference().push().getKey();
                messages.sender = TabletSettingUtils.getInstacne().getTableNo();
                messages.receiver = otherTableNo;

                ///1-1. 내 messages 데이터 넣기
                messageMap.put(FBPathBuilder.getInstance().init()
                                .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
                                .set("chat")
                                .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
                                .set("personal")
                                .set(TabletSettingUtils.getInstacne().getTableNo())
                                .set("messages")
                                .set(otherTableNo)
                                .set(fbKey)
                                .complete()
                        ,
                        messages

                );
                ///1-2. 내 lastMessage 데이터 넣기
                lastMessages.sender = TabletSettingUtils.getInstacne().getTableNo();
                lastMessages.receiver = otherTableNo;
                messageMap.put(FBPathBuilder.getInstance().init()
                                .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
                                .set("chat")
                                .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
                                .set("personal")
                                .set(TabletSettingUtils.getInstacne().getTableNo())
                                .set("lastMessages")
                                .set(otherTableNo)
                                .complete()
                        ,
                        lastMessages

                );
                ///2. 상대방 데이터 넣기
                messageMap.put(FBPathBuilder.getInstance().init()
                                .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
                                .set("chat")
                                .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
                                .set("personal")
                                .set(otherTableNo)
                                .set("messages")
                                .set(TabletSettingUtils.getInstacne().getTableNo())
                                .set(fbKey)
                                .complete()
                        ,
                        messages

                );
                ///2-2. 상대방 lastMessage 데이터 넣기
                messageMap.put(FBPathBuilder.getInstance().init()
                                .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
                                .set("chat")
                                .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
                                .set("personal")
                                .set(otherTableNo)
                                .set("lastMessages")
                                .set(TabletSettingUtils.getInstacne().getTableNo())
                                .complete()
                        ,
                        lastMessages

                );

                FirebaseDatabase.getInstance().getReference().updateChildren(messageMap, (databaseError, databaseReference) -> {
                    if (databaseError == null) {
                        //채팅 등록 성공
                        this.binding.editInput.setText("");
                        this.binding.buttonSend.setEnabled(false);
                    } else {
                        Toast.makeText(getContext(), "잠시후 다시 시도해주세요", Toast.LENGTH_SHORT).show();
                    }
                });


            }
        } else {
            Toast.makeText(getContext(), "메세지를 입력하세요.", Toast.LENGTH_SHORT).show();
        }
    }

    public ChatLayout(Context context) {
        super(context);
        this.mContext = context;
        registerHandler();
    }

    public ChatLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        registerHandler();

    }

    public ChatLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        registerHandler();
    }

    private ButtonTextView buttonBestMenu;

    public PrivateChatListAdapter getPrivateChatListAdapter() {
        return this.privateChatListAdapter;
    }

    String input = null;

    private void registerHandler() {

        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.layout_chat, this, true);
        this.binding.setLayout(this);
        this.binding.setTitle(null);
        this.binding.setPrivateChatCounts(0);
        this.binding.setShowChatRoom(false);
        this.binding.setIsEntireChatMode(false);
        this.binding.buttonSend.setEnabled(false);
        this.binding.editInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                input = editable.toString();
                binding.buttonSend.setEnabled(input.length() > 0);
            }
        });
        this.binding.layoutButtonSend.setOnTouchListener((view, motionEvent) -> this.binding.buttonSend.dispatchTouchEvent(motionEvent));
        this.binding.recyclerViewPrivateChatList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        this.binding.recyclerViewChat.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, true));


    }

    @Override
    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility == View.VISIBLE && binding != null && privateChatListAdapter != null) {
            binding.setPrivateChatCounts(privateChatListAdapter.getItemCount());
//            clickGoChatMain(null);
//            setup();
        }
    }

    public void createNewPrivateChat(View view) {
        MapDialog.getInstance(getContext())
                .setSelectedTableNo(null, null)
                .setListener(((tableHallName, selectedTable) -> {

                    ///1:1 채팅창 열기


                    if (selectedTable != null && selectedTable.tNo != null) {
                        otherTableNo = selectedTable.tNo;
                        setChatAdapter(FirebaseDatabase.getInstance().getReference()
                                .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
                                .child("chat")
                                .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
                                .child("personal")
                                .child(TabletSettingUtils.getInstacne().getTableNo())
                                .child("messages")
                                .child(otherTableNo)
                                .orderByChild("timeStamp")
                        );

                        binding.setIsEntireChatMode(false);
                        binding.setShowChatRoom(true);

                        ///1:1 채팅방의 경우 테이블 번호를 타이틀로 띄워준다
                        binding.setTitle("No. " + otherTableNo);

                        if (MapDialog.getmInstance() != null)
                            MapDialog.getmInstance().dismiss();
                    } else {
                        Toast.makeText(getContext(), "해당 테이블은 채팅방에 참여할 수 없습니다.", Toast.LENGTH_SHORT).show();
                    }


                }))
                .complete();
//
//        MapDialog.getInstance(getContext())
//                .setMode(MapDialog.MAPMODE.Chat)
//                .setMapDialogListener(new MapDialog.MapDialogListener() {
//                    @Override
//                    public void selectTable(TableMapInfoAndMemberData data) {
//
//                    }
//
//                    @Override
//                    public void SuccessCreateNewChat(TableMapInfoAndMemberData data) {
//                        if (data != null) {
//
//                        }
//                    }
//                })
//                .complete();
    }

    public interface CloseListener {
        void close();
    }


    public void clickClose(View v) {
        this.binding.setIsEntireChatMode(false);
        this.binding.setShowChatRoom(false);
        this.binding.editInput.setText("");
        this.binding.setTitle(null);
        if (closeListener != null)
            closeListener.close();
    }

    public void clickGoChatMain(View view) {
        ///1:1채팅이 true여야만 메인화면으로 갈수 있음
        //1. 전체채팅일때 : 바로꺼짐
        //2. 1:1채팅중일때 : 애초에 불가능한 상황
        if (TabletSettingUtils.getInstacne().getBranchInfo().storeOption.tableChat) {
            this.binding.setIsEntireChatMode(false);
            this.binding.setShowChatRoom(false);
            this.binding.buttonPrivateChat.setSelected(true);
            this.binding.buttonEntireChat.setSelected(false);
            this.binding.editInput.setText("");
            this.binding.setTitle(null);
        } else {
//            close();
            clickOUT();
            this.binding.editInput.setText("");
            this.binding.setTitle(null);
            if (closeListener != null)
                closeListener.close();
        }
    }


    public void openEntireChat(View view) {
        if (TabletSettingUtils.getInstacne().getBranchInfo().storeOption.storeChat) {
            this.binding.setIsEntireChatMode(true);
            this.binding.buttonEntireChat.setSelected(true);
            this.binding.buttonPrivateChat.setSelected(false);
            this.binding.setShowChatRoom(true);
            setEntireChat();
        } else {
            Toast.makeText(getContext(), "해당 매장은 전체채팅을 운영하지 않습니다.", Toast.LENGTH_SHORT).show();
        }
    }

    public void openPrivateChat(View view) {
        if (TabletSettingUtils.getInstacne().getBranchInfo().storeOption.tableChat) {
            this.binding.setIsEntireChatMode(false);
            this.binding.buttonPrivateChat.setSelected(true);
            this.binding.buttonEntireChat.setSelected(false);
            setChatPrivate();
        } else {
            Toast.makeText(getContext(), "해당 매장은 1:1채팅을 운영하지 않습니다.", Toast.LENGTH_SHORT).show();
        }
    }

    public void setup() {
//        this.binding.buttonPrivateChat.performClick();
        ////앱 상태에 따른 분기 처리

        LogUtil.d("채팅 셋팅값 확인 storeChat: " + TabletSettingUtils.getInstacne().getBranchInfo().storeOption.storeChat);
        LogUtil.d("채팅 셋팅값 확인 tableChat: " + TabletSettingUtils.getInstacne().getBranchInfo().storeOption.tableChat);
        if (TabletSettingUtils.getInstacne().getBranchInfo().storeOption.storeChat) {
            if (TabletSettingUtils.getInstacne().getBranchInfo().storeOption.tableChat) {
                openPrivateChat(null);
            } else
                openEntireChat(null);
        } else {
            if (TabletSettingUtils.getInstacne().getBranchInfo().storeOption.tableChat) {
                openPrivateChat(null);
            } else {
                close();
            }
        }
    }


    private void setChatTotal() {
//        this.chattype = CHATTYPE.ENTIRE;
//
//        this.binding.textTitle.setText("전체 채팅");
//        this.binding.layoutRoomButtons.setVisibility(View.GONE);
//
//
//        if (this.binding.listview != null)
//            this.binding.listview.removeAllViews();
//
//        ///파이어베이스 셋팅
//        //1. allchat_startAt가져오기
//        //2. chat 리스너 추가
//        FirebaseDatabase.getInstance().getReference()
//                .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                .child("tables")
//                .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                .child(TabletSettingUtils.getInstacne().getTableNo())
//                .addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//
//
//                        TableInfo tableInfo = dataSnapshot.getValue(TableInfo.class);
//                        if (tableInfo != null) {
//                            setChatAdapter(FirebaseDatabase.getInstance().getReference()
//                                    .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                    .child("chat")
//                                    .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                    .child("all")
////                                    .orderByKey()
//                                    .startAt(tableInfo.allChat_startAt)
//                                    .limitToLast(200));
//
//
//                        }
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                });
    }

    private void setEntireChat() {
        this.chattype = CHATTYPE.ENTIRE;

        ///1:1 채팅창 열기
        setChatAdapter(FirebaseDatabase.getInstance().getReference()
                .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
                .child("chat")
                .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
                .child("all")
                .limitToFirst(100)
                .orderByChild("timeStamp")
        );

        binding.setIsEntireChatMode(true);
        binding.setShowChatRoom(true);

    }

    private void setChatPrivate() {
        this.chattype = CHATTYPE.PRIVATE;

        ///1. 1:1 채팅 목록 가져오기
        setPrivateChatAdapter();
        checkPrivateChatAlreadySelected();


        //1. chat 리스너 추가
//        setChatAdapter(FirebaseDatabase.getInstance().getReference()
//                .child("chat")
//                .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                .child("all2"));


//        ((MainActivity)getActivity()).readPrivateMessage();
    }

    private void checkPrivateChatAlreadySelected() {

//        if (PrivateChatListAdapter.selectedTableNo == null) {
//            // 선택된 이력이 없음.
//            this.binding.textNoSelectionCover.setVisibility(View.VISIBLE);
//        } else {
//            this.binding.textNoSelectionCover.setVisibility(View.GONE);
//
//        }
    }

    private void setChatAdapter(Query query) {

        if (chatListAdapter != null) {
            chatListAdapter = null;
        }


        FirebaseRecyclerOptions<Message> tmp = new FirebaseRecyclerOptions.Builder<Message>().setQuery(query, Message.class).build();

        chatListAdapter = new ChatListAdapter(tmp, TabletSettingUtils.getInstacne().getTableNo(), Glide.with(this));//new ChatListAdapter(Message.class, R.layout.adapter_chat, ChatListAdapterViewHolder.class, query, TabletSettingUtils.getInstacne().getTableNo(), Glide.with(this));
//        this.chatListAdapter = new FirebaseRecyclerAdapter<Message, ChatListAdapterViewHolder>(tmp) {
//
//            RequestManager glide;
//            RequestOptions requestOptions;
//
//            @NonNull
//            @Override
//            public ChatListAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//                // Create a new instance of the ViewHolder, in this case we are using a custom
//                // layout called R.layout.message for each item
//                View view = LayoutInflater.from(parent.getContext())
//                        .inflate(R.layout.adapter_chat, parent, false);
//
//                return new ChatListAdapterViewHolder(view);
//            }
//
//            @Override
//            protected void onBindViewHolder(@NonNull ChatListAdapterViewHolder holder, int position, @NonNull Message dataSet) {
//                if (requestOptions == null) {
//                    this.requestOptions = new RequestOptions();
//                    this.requestOptions.placeholder(R.drawable.img_sample_menu)
//                            .override(400, 266);
//                }
//                LogUtil.d("리스트 구성중 : " + position);
//                LogUtil.d("리스트 구성중 dataSet.sender: " + dataSet.sender);
//
//                holder.binding.setMyTableNo(TabletSettingUtils.getInstacne().getTableNo());
//                holder.binding.setMessage(dataSet);
//                try {
//                    holder.binding.setPrevMessage(getItem(position + 1));   //위에 채팅
//
//                } catch (Exception e) {
//
//                }
//                try {
//                    if (position >= 1) {
//                        holder.binding.setNextvMessage(getItem(position - 1));   //밑에 채팅
//                    }
//                } catch (Exception e) {
//
//                }
//
//                if (dataSet != null && dataSet.present != null && dataSet.present.menuImage != null) {
//                    Glide.with(getContext()).load(dataSet.present.menuImage).apply(this.requestOptions).into(holder.binding.imagePresent);
//                }
//            }
//        };
        chatListAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);

                LogUtil.d("채팅 여긴 오나? onItemRangeInserted");
                if (binding.recyclerViewChat != null) {
                    binding.recyclerViewChat.getLayoutManager().scrollToPosition(0);
                    chatListAdapter.notifyItemChanged(1);
                }

            }
        });
        this.chatListAdapter.startListening();

        this.binding.recyclerViewChat.removeAllViewsInLayout();
        this.binding.recyclerViewChat.setAdapter(chatListAdapter);
        chatListAdapter.notifyDataSetChanged();
    }

    /**
     * activity가 resume될때 사용
     **/
    public void onResume() {
        setChatPrivate();
    }


    private void setPrivateChatAdapter() {

        if (TabletSettingUtils.getInstacne().getBranchInfo() != null) {

            if (privateChatListAdapter == null) {
                FirebaseRecyclerOptions<ExMessage> tmp = new FirebaseRecyclerOptions.Builder().setQuery(FirebaseDatabase.getInstance().getReference()
                        .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
                        .child("chat")
                        .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
                        .child("personal")
                        .child(TabletSettingUtils.getInstacne().getTableNo())
                        .child("lastMessages")
                        .orderByChild("timeStamp"), ExMessage.class).build();

                privateChatListAdapter = new PrivateChatListAdapter(tmp, TabletSettingUtils.getInstacne().getTableNo());
                privateChatListAdapter.startListening();
                privateChatListAdapter.setCloseListener(((v, tableNo) -> {


                    MessageDialogBuilder.getInstance(getContext())
                            .setTitle("1:1 채팅방 나가기")
                            .setContent(tableNo + "테이블과의 대화방을 나가시겠습니까?")
                            .setDefaultButtons(view1 -> {


                                Map<String, Object> messageMap = new HashMap<>();

                                ///1-1. 내 messages 데이터 넣기
                                messageMap.put(FBPathBuilder.getInstance().init()
                                                .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
                                                .set("chat")
                                                .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
                                                .set("personal")
                                                .set(TabletSettingUtils.getInstacne().getTableNo())
                                                .set("messages")
                                                .set(tableNo)
                                                .complete()
                                        ,
                                        null

                                );

                                messageMap.put(FBPathBuilder.getInstance().init()
                                                .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
                                                .set("chat")
                                                .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
                                                .set("personal")
                                                .set(TabletSettingUtils.getInstacne().getTableNo())
                                                .set("lastMessages")
                                                .set(tableNo)
                                                .complete()
                                        ,
                                        null

                                );

                                FirebaseDatabase.getInstance().getReference().updateChildren(messageMap, (databaseError, databaseReference) -> {
                                    if (databaseError == null) {
                                        Toast.makeText(getContext(), "완료되었습니다.", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(getContext(), "잠시후 다시 시도해주세요", Toast.LENGTH_SHORT).show();
                                    }
                                    MessageDialogBuilder.getInstance(getContext()).dismiss();
                                });
                            })
                            .complete();


                }));
                privateChatListAdapter.setListener((v, tableNo) -> {

                    LogUtil.d("채팅 방 선택 : " + tableNo);
                    otherTableNo = tableNo;

                    ///1:1 채팅창 열기
                    setChatAdapter(FirebaseDatabase.getInstance().getReference()
                            .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
                            .child("chat")
                            .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
                            .child("personal")
                            .child(TabletSettingUtils.getInstacne().getTableNo())
                            .child("messages")
                            .child(otherTableNo)
                            .orderByChild("timeStamp")
                    );

                    binding.setIsEntireChatMode(false);
                    binding.setShowChatRoom(true);


                    ///해당 lastMessage의 read를 true로 바꾼다. 비동기로 처리
                    try {
//                        FirebaseDatabase.getInstance().getReference()
//                                .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                .child("chat")
//                                .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                .child("personal")
//                                .child(TabletSettingUtils.getInstacne().getTableNo())
//                                .child("lastMessages")
//                                .child(otherTableNo)
//                                .child("read")
//                                .setValue(true);
                    } catch (Exception e) {

                    }

                    ///1:1 채팅방의 경우 테이블 번호를 타이틀로 띄워준다
                    binding.setTitle("No. " + otherTableNo);
                });
                privateChatListAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                    @Override
                    public void onItemRangeInserted(int positionStart, int itemCount) {
                        super.onItemRangeInserted(positionStart, itemCount);
                        binding.recyclerViewPrivateChatList.getLayoutManager().smoothScrollToPosition(binding.recyclerViewPrivateChatList, null, 0);
                        binding.setPrivateChatCounts(privateChatListAdapter.getItemCount());

                    }
                });
                binding.recyclerViewPrivateChatList.setAdapter(privateChatListAdapter);
                privateChatListAdapter.notifyDataSetChanged();
            }
        }
    }


    /**
     * 해당 채팅방을 보고있는가?
     */
    public Boolean isShowing(String key) {
        return ((this.getVisibility() == View.VISIBLE) &&
                !this.binding.getIsEntireChatMode() && this.binding.getShowChatRoom() && key.equals(otherTableNo))
                ;
    }


    /**
     * 바탕화면 눌렀을때 키 입력창 사라지게
     **/
    public void clickOUT() {
        if (((BaseActivity) OrderAppApplication.mContext).getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) (mContext.getSystemService(Context.INPUT_METHOD_SERVICE));
            imm.hideSoftInputFromWindow(((BaseActivity) OrderAppApplication.mContext).getCurrentFocus().getWindowToken(), 0);
        }
    }

    public interface MapListener {
        void openMap();
    }


    public void exitPrivateChat(View view) {
        if (this.otherTableNo != null) {

            MessageDialogBuilder.getInstance(getContext())
                    .setTitle("1:1 채팅방 나가기")
                    .setContent(this.otherTableNo + "테이블과의 대화방을 나가시겠습니까?")
                    .setDefaultButtons(view1 -> {

                        ///나가기전에 현재 대화를 stop
                        this.chatListAdapter.stopListening();


                        Map<String, Object> messageMap = new HashMap<>();

                        ///1-1. 내 messages 데이터 넣기
                        messageMap.put(FBPathBuilder.getInstance().init()
                                        .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
                                        .set("chat")
                                        .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
                                        .set("personal")
                                        .set(TabletSettingUtils.getInstacne().getTableNo())
                                        .set("messages")
                                        .set(otherTableNo)
                                        .complete()
                                ,
                                null

                        );

                        messageMap.put(FBPathBuilder.getInstance().init()
                                        .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
                                        .set("chat")
                                        .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
                                        .set("personal")
                                        .set(TabletSettingUtils.getInstacne().getTableNo())
                                        .set("lastMessages")
                                        .set(otherTableNo)
                                        .complete()
                                ,
                                null

                        );

                        FirebaseDatabase.getInstance().getReference().updateChildren(messageMap, (databaseError, databaseReference) -> {
                            if (databaseError == null) {
                                //채팅 나가기 성공
                                clickGoChatMain(this.binding.buttonGoChatMain);
                                Toast.makeText(getContext(), otherTableNo + "테이블과의 채팅이 종료되었습니다.", Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(getContext(), "잠시후 다시 시도해주세요", Toast.LENGTH_SHORT).show();
                            }
                            MessageDialogBuilder.getInstance(getContext()).dismiss();
                        });
                    })
                    .complete();


        }

    }

    public void openNewPrivateChat() {
        MapDialog.getInstance(getContext())
                .setMODE(MapDialog.MODE.CHAT)
                .setSelectedTableNo(null, null)
                .setListener(((tableHallName, selectedTable) -> {
                    ///1:1 채팅창 열기
                    otherTableNo = selectedTable.tNo;
                    setChatAdapter(FirebaseDatabase.getInstance().getReference()
                            .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
                            .child("chat")
                            .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
                            .child("personal")
                            .child(TabletSettingUtils.getInstacne().getTableNo())
                            .child("messages")
                            .child(otherTableNo)
                            .orderByChild("timeStamp")
                    );

                    binding.setIsEntireChatMode(false);
                    binding.setShowChatRoom(true);

                    ///1:1 채팅방의 경우 테이블 번호를 타이틀로 띄워준다
                    binding.setTitle("No. " + otherTableNo);

                    if (MapDialog.getmInstance() != null)
                        MapDialog.getmInstance().dismiss();
                }))
                .complete();
    }


}

