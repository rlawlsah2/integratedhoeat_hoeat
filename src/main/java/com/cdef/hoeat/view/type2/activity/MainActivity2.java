package com.cdef.hoeat.view.type2.activity;

import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.commonmodule.utils.typekit.TypekitContextWrapper;
import com.cdef.commonmodule.dataModel.firebaseDataSet.Notification;
import com.cdef.hoeat.OrderAppApplication;
import com.cdef.hoeat.R;
import com.cdef.hoeat.utils.DefaultExceptionHandler;
import com.cdef.hoeat.utils.NavigationUtils;
import com.cdef.hoeat.databinding.ActivityMain2Binding;
import com.cdef.hoeat.utils.QueueUtils;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.cdef.hoeat.view.activity.BaseActivity;
import com.cdef.hoeat.view.dialog.MessageDialogBuilder;
import com.cdef.hoeat.view.dialog.loadingDialog.LoadingDialog;
import com.cdef.hoeat.view.fragment.BaseFragment;
import com.cdef.hoeat.view.fragment.BaseFragment2;
import com.cdef.hoeat.view.type2.dialog.TableSettingDialog;
import com.cdef.hoeat.view.type2.fragment.BillFragment2;
import com.cdef.hoeat.view.type2.fragment.OrderFragment;
import com.cdef.hoeat.viewModel.MainViewModel;
import com.cdef.hoeat.viewModel.OrderViewModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class MainActivity2 extends BaseActivity {


    ///키보드 view 상태를 체크하기위한 변수, 어디서든 접근하기 위해  static으로 사용
    ActivityMain2Binding binding = null;
    Date lastTouchTime = null;

    RequestManager glide;
    RequestOptions requestOptions;
    OrderViewModel orderViewModel;
    MainViewModel mainViewModel;

    LoadingDialog loadingDialog;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LogUtil.d("MainActivity2 onNewIntent : " + intent);
    }

    @Override
    public void onBackPressed() {
        setUI();
        if (getSupportFragmentManager().findFragmentById(R.id.layoutFragment) != null) {
            super.onBackPressed();
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.setUI();
        return super.onTouchEvent(event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.setUI();
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        ///로딩중일땐 터치 못하게
        if (getSupportFragmentManager().findFragmentById(R.id.layoutFragment) instanceof BaseFragment) {
            if (((BaseFragment) getSupportFragmentManager().findFragmentById(R.id.layoutFragment)).isLoading()) {
                return false;
            }
        }
        if (getSupportFragmentManager().findFragmentById(R.id.layoutFragment) instanceof BaseFragment2) {
            if (((BaseFragment2) getSupportFragmentManager().findFragmentById(R.id.layoutFragment)).isLoading()) {
                return false;
            }
        }

        ///마지막 터치 시간 갱신
        this.lastTouchTime = Calendar.getInstance().getTime();
        setUI();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            if (ev.getPointerCount() > 1) {
                LogUtil.d("Multitouch detected!");
                ev.setAction(MotionEvent.ACTION_CANCEL);
                return super.dispatchTouchEvent(ev);
            }
        }


        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.lastTouchTime = Calendar.getInstance().getTime(); //마지막 터치 시간값을 초기화
        this.loadingDialog = new LoadingDialog(this, "잠시만 기다려주세요 :)");

        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_main2);
        this.binding.setActivity(this);

        getSupportFragmentManager().addOnBackStackChangedListener(() -> {

            LogUtil.d("addOnBackStackChangedListener 동작 : " + getSupportFragmentManager().findFragmentById(R.id.layoutFragment));
            if (getSupportFragmentManager().findFragmentById(R.id.layoutFragment) != null) {
                this.binding.layoutFragment.setVisibility(View.VISIBLE);
                this.binding.layoutFragment.bringToFront();

            } else {
                this.binding.layoutFragment.setVisibility(View.GONE);
            }

        });


        if (this.glide == null) {
            this.glide = Glide.with(this);
            this.requestOptions = new RequestOptions();
            this.requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL).skipMemoryCache(false);
        }
        LogUtil.d("앱 시작");
        OrderAppApplication.mContext = this;

        ///앱에서 떨어진 Exception에 대해 자동 재시작 기능을 위해 추가한 코드
        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(this));
        this.setUI();
        this.initViewsLayout()
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(result -> this.initPromotionImage())
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {

                }, error -> {

                });


        if (this.orderViewModel == null) {
            this.orderViewModel = ViewModelProviders.of(this).get(OrderViewModel.class);
            this.orderViewModel.initRepository(this);
            this.orderViewModel.queueState.observe(this, queueState -> {
                switch (queueState) {
                    case NONE:
                        this.loadingDialog.dismiss();
                        break;
                    case LOADING:
                        this.loadingDialog.show();
                        break;
                    case ERROR:
                        this.loadingDialog.dismiss();
                        this.orderViewModel.queueState.setValue(OrderViewModel.QUEUESTATE.NONE);

                        MessageDialogBuilder.getInstance(this)
                                .setContent("일시적인 오류가 발생했습니다. 잠시후 다시 시도해주세요.")
                                .setDefaultButton()
                                .complete();
                        break;
                    case COMPLETE:
                        this.loadingDialog.dismiss();
                        if(TableSettingDialog.getmInstanceWithoutInit() != null)
                            TableSettingDialog.getmInstanceWithoutInit().dismiss();
                        this.orderViewModel.queueState.setValue(OrderViewModel.QUEUESTATE.NONE);
                        MessageDialogBuilder.getInstance(this)
                                .setContent("직원에게 전달하였습니다.\n잠시만 기다려주세요 :)")
                                .setDefaultButton()
                                .complete();
                        break;
                }
            });

        }
        this.mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        this.mainViewModel.initRepository(this);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {

                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        ///fcm 키 등록
                        mainViewModel.registrationToken(
                                QueueUtils.registrationToken(TabletSettingUtils.getInstacne().getBranchInfo().posIp, TabletSettingUtils.getInstacne().getTableNo())
                                , token
                        );
                    }
                });
    }

    public Observable<Boolean> initViewsLayout() {

        if (TabletSettingUtils.getInstacne().getBranchInfo() != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption.tableSetting) {
            this.binding.buttonTableSetting.setVisibility(View.VISIBLE);
        } else {
            this.binding.buttonTableSetting.setVisibility(View.GONE);
        }

        return Observable.just(true);
//        return Observable.create((Observable.OnSubscribe<Boolean>) subscriber -> {
//
//            this.binding.layoutPromotion.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                @Override
//                public void onGlobalLayout() {
//                    int height = binding.layoutPromotion.getMeasuredHeight();
//                    if (height > 1) {
//                        binding.layoutPromotion.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) binding.layoutPromotion.getLayoutParams();
//                        lp.width = (int) (height * 1.2);
////                        binding.layoutPromotion.setLayoutParams(lp);
//                        subscriber.onNext(true);
//                    }
//                }
//            });
//
//
//        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (routineCompositeSubscriptions != null)
            this.routineCompositeSubscriptions.clear();
    }


    public CompositeSubscription routineCompositeSubscriptions = new CompositeSubscription();

    public Observable<Boolean> initPromotionImage() {
        return Observable.create((Observable.OnSubscribe<Boolean>) subscriber -> {

            String url1 = "";
            String url2 = "";
            String url3 = "";
            String url4 = "";
            String url5 = "";
            ArrayList<String> urls = new ArrayList<>();
            try {
                url1 = TabletSettingUtils.getInstacne().getBranchInfo().univInfo.appLayout.orderApp.featuredImage1;
                if (url1 != null) urls.add(url1);
            } catch (Exception e) {
                url1 = "";
            }
            try {
                url2 = TabletSettingUtils.getInstacne().getBranchInfo().univInfo.appLayout.orderApp.featuredImage2;
                if (url2 != null) urls.add(url2);
            } catch (Exception e) {
                url2 = "";
            }
            try {
                url3 = TabletSettingUtils.getInstacne().getBranchInfo().univInfo.appLayout.orderApp.featuredImage3;
                if (url3 != null) urls.add(url3);
            } catch (Exception e) {
                url3 = "";
            }
            try {
                url4 = TabletSettingUtils.getInstacne().getBranchInfo().univInfo.appLayout.orderApp.featuredImage4;
                if (url4 != null) urls.add(url4);
            } catch (Exception e) {
                url4 = "";
            }
            try {
                url5 = TabletSettingUtils.getInstacne().getBranchInfo().univInfo.appLayout.orderApp.featuredImage5;
                if (url5 != null) urls.add(url5);
            } catch (Exception e) {
                url5 = "";
            }

            if (urls.size() > 1)
                routineCompositeSubscriptions.add(
                        Observable.interval(500, 5000, TimeUnit.MILLISECONDS)
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(AndroidSchedulers.mainThread())
                                .subscribe(count -> {
                                    String currentUrl = urls.get((int) (count % urls.size()));

//                                    if (currentUrl.endsWith(".gif")) {
//                                        this.glide.asGif().load(currentUrl)
//                                                .apply(this.requestOptions)
//                                                .into(ctGifDrawable);
//
//                                    } else {
                                        this.glide.load(currentUrl)
                                                .apply(this.requestOptions)
                                                .into(ctDrawable);
//                                    }

                                })
                );
            else {

                String currentUrl = urls.get(0);
//                if (currentUrl.endsWith(".gif")) {
//                    this.glide.asGif().load(currentUrl)
//                            .apply(this.requestOptions)
//                            .into(ctGifDrawable);
//
//                } else {
                    this.glide.load(currentUrl)
                            .apply(this.requestOptions)
                            .into(ctDrawable);
//                }
            }

            subscriber.onNext(true);

        });
    }

    CustomTarget<Drawable> ctDrawable = new CustomTarget<Drawable>() {
        @Override
        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
            glide.load(resource)
                    .apply(requestOptions)
                    .transition(com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade(500))
                    .into(binding.imagePromotion);
        }

        @Override
        public void onLoadCleared(@Nullable Drawable placeholder) {

        }
    };
    CustomTarget<GifDrawable> ctGifDrawable = new CustomTarget<GifDrawable>() {
        @Override
        public void onResourceReady(@NonNull GifDrawable resource, @Nullable Transition<? super GifDrawable> transition) {
            resource.setLoopCount(-1);
            resource.start();
            binding.imagePromotion.setImageDrawable(resource);
//            glide.asGif()
//                    .load(resource)
//                    .apply(requestOptions)
//                    .transition(com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade(500))
//                    .into(binding.imagePromotion);
        }

        @Override
        public void onLoadCleared(@Nullable Drawable placeholder) {

        }
    };

    public void goBill() {
        NavigationUtils.detailOpenFragment(getSupportFragmentManager(), R.id.layoutFragment, BillFragment2.class.getSimpleName(), null);
    }

    public void goOrder() {
        NavigationUtils.detailOpenFragment(getSupportFragmentManager(), R.id.layoutFragment, OrderFragment.class.getSimpleName(), null);
    }

    Observable observableNotice = Observable.interval(1, 10, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread());


    public void goTableSetting() {
        if (TabletSettingUtils.getInstacne().getBranchInfo().callOptions != null && TabletSettingUtils.getInstacne().getBranchInfo().callOptions.entrySet() != null) {
            TableSettingDialog
                    .getInstance(this)
                    .setRequestListener1(() -> {
                        TableSettingDialog.getmInstanceWithoutInit().dismiss();
                        loadingDialog.show();
                        Notification notification = new Notification();
                        notification.content = "직원 호출입니다.";
                        notification.read = false;
                        notification.tableNo = TabletSettingUtils.getInstacne().getTableNo();
                        notification.title = getString(R.string.notificationTitleServiceCall);
                        notification.type = getString(R.string.notificationTypeServiceCall);
                        notification.time = Utils.getDate("yyyy-MM-dd HH:mm:ss");
                        this.orderViewModel.request(QueueUtils.request(TabletSettingUtils.getInstacne().getBranchInfo().posIp), notification);
                    })
                    .setRequestListener2((selectedList) -> {
                        if (selectedList.size() > 0) {
                            this.loadingDialog.show();
                            String message = "";

                            for (String key : selectedList.keySet()) {
                                message += key + ": " + selectedList.get(key) + " / ";

                            }

                            if (message != null && !message.equals("")) {
                                Notification notification = new Notification();
                                notification.content = message;
                                notification.read = false;
                                notification.tableNo = TabletSettingUtils.getInstacne().getTableNo();
                                notification.title = getString(R.string.notificationTitleService);
                                notification.type = getString(R.string.notificationTypeService);
                                notification.time = Utils.getDate("yyyy-MM-dd HH:mm:ss");

                                this.orderViewModel.request(QueueUtils.request(TabletSettingUtils.getInstacne().getBranchInfo().posIp), notification);


                            } else {
                                loadingDialog.dismiss();
                            }

                        } else {
                            Toast.makeText(this, "원하시는 항목과 수량을 선택해주세요.", Toast.LENGTH_SHORT).show();
                        }


                    })
                    .complete();
        } else {
            MessageDialogBuilder.getInstance(this)
                    .setContent("직원의 도움이 필요하신가요?\n아래 확인을 눌러주시면 직원이 안내드리겠습니다 :)")
                    .setDefaultButtons((view) -> {
                        MessageDialogBuilder.getmInstanceWithoutInit().dismiss();
                        loadingDialog.show();
                        Notification notification = new Notification();
                        notification.content = "직원 호출입니다.";
                        notification.read = false;
                        notification.tableNo = TabletSettingUtils.getInstacne().getTableNo();
                        notification.title = getString(R.string.notificationTitleServiceCall);
                        notification.type = getString(R.string.notificationTypeServiceCall);
                        notification.time = Utils.getDate("yyyy-MM-dd HH:mm:ss");
                        this.orderViewModel.request(QueueUtils.request(TabletSettingUtils.getInstacne().getBranchInfo().posIp), notification);

                    }).complete();
        }
    }

    public void touchBlock() {

    }

}
