package com.cdef.hoeat.view.type2.dialog;

import android.app.Dialog;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.commonmodule.dataModel.TableSettingData;
import com.cdef.hoeat.OrderAppApplication;
import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.DialogTablesettingBinding;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.cdef.hoeat.view.adapter.TableSettingAdapter;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by kimjinmo on 2017. 1. 2..
 */

public class TableSettingDialog extends Dialog {

    static int initRecyclerViewH = 0;

    public ArrayList<TableSettingData> list = new ArrayList<>();
    public TableSettingAdapter adapter;

    ////멤버변수
    private static TableSettingDialog mInstance;
    private DialogTablesettingBinding binding;

    public static TableSettingDialog getmInstanceWithoutInit() {
        return TableSettingDialog.mInstance;
    }

    public void setLoadingState() {
        super.dismiss();
    }


    public static TableSettingDialog getInstance(Context context) {
        if (TableSettingDialog.mInstance != null) {
            TableSettingDialog.mInstance.dismiss();
        }

        if (TableSettingDialog.mInstance == null) {
            TableSettingDialog.mInstance = new TableSettingDialog(context);
        }

        TableSettingDialog.mInstance.init(context);
        return TableSettingDialog.mInstance;
    }

    public static void clearInstance() {
        TableSettingDialog.mInstance = null;
    }


    View decorView;
    int uiOption;

    /**
     * 소프트 버튼을 없앤다던지 UI와 관련된 작업을 처리한다.
     **/
    public void setUI() {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);

        if (decorView == null) {
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);

            decorView = getWindow().getDecorView();
            uiOption = getWindow().getDecorView().getSystemUiVisibility();
            uiOption = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_FULLSCREEN;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
                uiOption |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                uiOption |= View.SYSTEM_UI_FLAG_FULLSCREEN;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                uiOption |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }


        LogUtil.d("소프트 버튼 상태 : " + decorView);
        decorView.setSystemUiVisibility(uiOption);
        this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);

    }


    RequestManager glide;
    RequestOptions requestOptions;

    private TableSettingDialog(Context context) {
        super(context);
        glide = Glide.with(context);
        registerHandler();
    }

    private TableSettingDialog(Context context, int themeResId) {
        super(context, themeResId);
        glide = Glide.with(context);
        registerHandler();
    }

    private TableSettingDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        registerHandler();
    }

    public static boolean checkShowing() {
        return (mInstance != null && mInstance.isShowing());

    }


    /**
     * 초기 뷰 셋팅을 위한 부분
     **/
    private void registerHandler() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_tablesetting, null, false);
        this.binding.setDialog(this);
        setContentView(binding.getRoot());

        //안내문구 추가
        this.binding.textIntro.setText(Html.fromHtml("1.필요하신 것이 있으시다면 <font color='#ff0000'>선택하시고 수량을 조절</font>해주세요.<br>"
                + "2.선택이 끝나시면 하단의 <font color='#ff0000'>선택된 항목 요청하기</font> 버튼을 눌러주세요. 직원이 준비해서 가져다 드리겠습니다 :)<br>"
                + "*그 외에 필요하신 것은 <font color='#ff0000'>직원호출</font>을 눌러주시면 안내 도와드리겠습니다."
        ));

        ///2. 하위 뷰 셋팅
        Window window = this.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        this.binding.layoutMain.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (binding.layoutMain.getMeasuredWidth() > 2) {
                    binding.layoutMain.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) binding.layoutContent.getLayoutParams();
                    lp.width = (int) (binding.layoutMain.getMeasuredWidth() / 1.4);
                    lp.height = (int) (binding.layoutMain.getMeasuredHeight() / 1.6);
                    binding.layoutContent.setLayoutParams(lp);
                }
            }
        });
        Iterator<Map.Entry<String, JsonElement>> items = TabletSettingUtils.getInstacne().getBranchInfo().callOptions.entrySet().iterator();
        Gson gson = new Gson();
        while (items.hasNext()) {
            Map.Entry<String, JsonElement> item = items.next();
//            if(list.size() < 5)
            list.add(gson.fromJson(TabletSettingUtils.getInstacne().getBranchInfo().callOptions.get(item.getKey()), TableSettingData.class));
        }


        this.binding.recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            int recursiveCount = 0;

            @Override
            public void onGlobalLayout() {
                if (binding.recyclerView.getMeasuredWidth() > 2 && binding.recyclerView.getMeasuredHeight() > 2 && binding.layoutContent.getMeasuredHeight() > 2) {

                    LogUtil.d("테이블 셋팅 recursiveCount : " + recursiveCount);
                    //H 계산
                    int H = binding.recyclerView.getMeasuredHeight();
                    int W = binding.recyclerView.getMeasuredWidth();
                    if (initRecyclerViewH == 0) {
                        initRecyclerViewH = H;
                    }
                    LogUtil.d("테이블 셋팅 H : " + H);
                    LogUtil.d("테이블 셋팅 initRecyclerViewH : " + initRecyclerViewH);
                    LogUtil.d("테이블 셋팅 W : " + W);

                    ///한줄에 들어가기 적당한 크기
                    int n = (2 * W / initRecyclerViewH);
                    if (n > 7)
                        n = 7;
                    LogUtil.d("테이블 셋팅 n : " + n);
                    LogUtil.d("테이블 셋팅 분기 : " + (list.size() > n * (2 + recursiveCount)));
                    if (list.size() > n * (2 + recursiveCount)) //두줄 이상이 될것인가?
                    {
                        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) binding.layoutContent.getLayoutParams();
                        lp.height = binding.layoutContent.getMeasuredHeight() + (initRecyclerViewH / 2) + (Utils.dpToPx(getContext(), 40));
                        recursiveCount++;
                        binding.layoutContent.setLayoutParams(lp);
                        binding.layoutContent.requestLayout();
                    } else {
//                        아니면 그냥 그대로 진행
                        binding.recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        int paddingSide = (W - (initRecyclerViewH / 2) * n) / 2;
                        LogUtil.d("테이블 셋팅 paddingSide : " + paddingSide);
                        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) binding.layoutContent.getLayoutParams();
                        lp.height = binding.layoutContent.getMeasuredHeight() + (Utils.dpToPx(getContext(), 40) * 2) - (list.size() >= n ? initRecyclerViewH / 2 + Utils.dpToPx(getContext(), 40) : 0);
//                        recursiveCount++;
                        binding.layoutContent.setLayoutParams(lp);
                        binding.recyclerView.setPadding(paddingSide, 0, paddingSide, 0);
                        binding.recyclerView.requestLayout();
                        GridLayoutManager manager = new GridLayoutManager(getContext(), n);
                        binding.recyclerView.setLayoutManager(manager);
                        adapter = new TableSettingAdapter(OrderAppApplication.mContext, glide);
                        adapter.setListener(() -> {
                            if (adapter.getSelectedList().size() > 0) {
                                binding.buttonRequest2.setBackgroundResource(R.drawable.selector_submit1);
                                binding.buttonRequest2.setTextColor(getContext().getResources().getColor(R.color.white));
                            } else {
                                binding.buttonRequest2.setBackgroundResource(R.color.darkGray_);
                                binding.buttonRequest2.setTextColor(getContext().getResources().getColor(R.color.black));

                            }
                        });
                        binding.recyclerView.setAdapter(adapter);
                        adapter.setmListData(list);
                        binding.layoutContent.postDelayed(() -> {
                            binding.layoutContent.setVisibility(View.VISIBLE);
                        }, 250);

                    }
                }
            }
        });

        this.setUI();

    }

    @Override
    public boolean dispatchKeyEvent(@NonNull KeyEvent event) {
        return super.dispatchKeyEvent(event);
    }

    public TableSettingDialog init(Context context) {
        ///클린에 대한 메소드가 들어가야함

        if (this.isShowing())
            this.dismiss();
        return this;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (this.closeListener != null)
            this.closeListener.close();
        TableSettingDialog.mInstance = null;
    }

    ///현재 생성된 인스턴스가 있고 이것을 접근하려 할때 사용함
    public static TableSettingDialog getmInstance() {
        return TableSettingDialog.mInstance;
    }


    public void complete() {
//        if (!((Activity) this.mContext).isFinishing())
        this.show();
    }

    @Override
    public void show() {
        super.show();
        this.setUI();
    }

    public void clickMain() {

    }

    public CloseListener closeListener;

    public TableSettingDialog setCloseListener(CloseListener closeListener) {
        this.closeListener = closeListener;
        return this;
    }


    public TableSettingDialog setRequestListener1(RequestListener1 requestListener1) {
        this.requestListener1 = requestListener1;
        this.binding.setRequestListener1(this.requestListener1);
        return this;
    }

    public TableSettingDialog setRequestListener2(RequestListener2 requestListener2) {
        this.requestListener2 = requestListener2;
        this.binding.setRequestListener2(this.requestListener2);
        return this;
    }

    public RequestListener1 requestListener1;
    public RequestListener2 requestListener2;


    public interface RequestListener1 {
        void call();
    }

    public interface RequestListener2 {
        void call(HashMap<String, Integer> selectedItemList);
    }

    public interface CloseListener {
        void close();
    }

    public void request2()
    {
        if(this.requestListener2 != null)
            this.requestListener2.call(this.adapter.getSelectedList());
    }


}
