package com.cdef.hoeat.view.type2.fragment;


import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.dataModel.firebaseDataSet.Order;
import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.Type2FragmentBillBinding;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.cdef.hoeat.view.activity.MainActivity;
import com.cdef.hoeat.view.dialog.loadingDialog.LoadingDialog;
import com.cdef.hoeat.view.fragment.BaseFragment;
import com.cdef.hoeat.viewModel.BillViewModel;

/**
 * Created by kimjinmo on 2017. 9. 18..
 */

public class BillFragment2 extends BaseFragment {


    Type2FragmentBillBinding binding;
    BillViewModel billViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.loadingDialog = new LoadingDialog(getContext(), getString(R.string.loadingDialog_title_loading));
        billViewModel = ViewModelProviders.of(this).get(BillViewModel.class);
        billViewModel.initRepository(getContext());

        ///큐 상태 체크
        this.billViewModel.state.observe(this, state -> {
            switch (state) {
                case NONE:
                    this.loadingDialog.dismiss();
                    break;
                case LOADING:
                    this.loadingDialog.show();
                    break;
                case ERROR:
                    this.loadingDialog.dismiss();
                    Toast.makeText(getContext(), "일시적으로 계산서를 불러올 수 없습니다.(E0001)", Toast.LENGTH_SHORT).show();
                    break;
                case COMPLETE:
                    this.loadingDialog.dismiss();
                    break;
            }
        });
        billViewModel.mPagingOrderList.observe(this, orders -> {

            if (orders != null) {
                LogUtil.d("----------------------------------------------------------");

                for (Order item : orders) {
                    LogUtil.d("중간점검 " + item.foodName + " : " + item.quantity);
                }
            }

            this.binding.setOrderList(orders);


        });

        billViewModel.mOrderList.observe(this, totalOrderList -> {
            if (totalOrderList == null || totalOrderList.size() == 0) {
                this.binding.textBillEmpty.setVisibility(View.VISIBLE);
                this.binding.layoutBillList.setVisibility(View.GONE);
            } else {
                this.binding.textBillEmpty.setVisibility(View.GONE);
                this.binding.layoutBillList.setVisibility(View.VISIBLE);
            }
        });
        billViewModel.mOrderedTotalPrice.observe(this, totalPrice -> {
            this.binding.setTotalPrice(totalPrice);

        });
        billViewModel.mOrderListPageIndex.observe(this, currentPage -> {
            this.binding.setCurrentPage(currentPage);

            LogUtil.d("주문내역 가져오는 중간단계 1 mOrderListPageIndex : " + currentPage);
        });
        billViewModel.mTotalPage.observe(this, totalPage -> {
            this.binding.setTotalPage(totalPage);

        });
        billViewModel.mPersonCount.observe(this, personCount -> {
            this.binding.setPersonCount(personCount);
        });

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.binding.setBillViewModel(billViewModel);

        billViewModel.getOrderList(
                TabletSettingUtils.getInstacne().getTableNo()
        );

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view

        binding = DataBindingUtil.inflate(
                inflater, R.layout.type2_fragment_bill, container, false);
        binding.setFragment(this);
        if (TabletSettingUtils.getInstacne().getBranchInfo() != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption != null
                && TabletSettingUtils.getInstacne().getBranchInfo().storeOption.advertisement) {
            this.binding.setShowEvent(true);
        } else {
            this.binding.setShowEvent(false);
        }

        //here data must be an instance of the class MarsDataProvider
        return binding.getRoot();

    }


    public void goEvent(View view) {
        ((MainActivity) getActivity()).goADSelectionPage(null);
    }


    public void touchBlock() {

    }


}
