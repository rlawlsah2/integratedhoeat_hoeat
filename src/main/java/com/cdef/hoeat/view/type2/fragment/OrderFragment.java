package com.cdef.hoeat.view.type2.fragment;


import android.animation.Animator;

import androidx.lifecycle.ViewModelProviders;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.text.Html;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.integration.recyclerview.RecyclerViewPreloader;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.util.ViewPreloadSizeProvider;
import com.cdef.commonmodule.utils.ConstantsUtils;
import com.cdef.commonmodule.utils.CustomAnimationUtils;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.PreferenceUtils;
import com.cdef.commonmodule.utils.TabletSetting;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.commonmodule.dataModel.MenuItemData;
import com.cdef.commonmodule.dataModel.MenuSideGroupItemData;
import com.cdef.commonmodule.dataModel.MenuSideOptionItemData;
import com.cdef.commonmodule.dataModel.OrderCategoryData;
import com.cdef.hoeat.Exceptions.MinOrderPriceExceptions;
import com.cdef.hoeat.Exceptions.PosConnectionExceptions;
import com.cdef.hoeat.R;
import com.cdef.hoeat.utils.QueueUtils;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.cdef.hoeat.databinding.FragmentOrder2Binding;
import com.cdef.hoeat.view.adapter.type2.MenuAdapter;
import com.cdef.hoeat.view.adapter.type2.SetMenuSelectedOptionListAdapter;
import com.cdef.hoeat.view.adapter.type2.Type2CartListAdapter;
import com.cdef.hoeat.view.customView.menuOption.type2.MenuDetailSetGroupListItem;
import com.cdef.hoeat.view.customView.type2.Category1;
import com.cdef.hoeat.view.dialog.MessageDialogBuilder;
import com.cdef.hoeat.view.fragment.BaseFragment2;
import com.cdef.hoeat.viewModel.BillViewModel;
import com.cdef.hoeat.viewModel.OrderViewModel;
import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by kimjinmo on 2017. 9. 18..
 */

public class OrderFragment extends BaseFragment2<FragmentOrder2Binding> {


    RequestOptions requestOptions;
    MenuAdapter mAdapter;
    SetMenuSelectedOptionListAdapter selectedOptionListAdapter;

    ViewPreloadSizeProvider<MenuItemData> preloadSizeProvider;

    OrderViewModel orderViewModel = null;
    BillViewModel billViewModel;    ///빌지를 띄우기위해 필요함
    Type2CartListAdapter mCartListAdapter;
    public String parentCategoryId;
    RequestManager glide;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        this.glide = Glide.with(this);
        this.requestOptions = new RequestOptions();
        this.requestOptions.skipMemoryCache(false);
        this.requestOptions.placeholder(R.drawable.img_sample_menu);
        this.requestOptions.override(1024, 1024);
        this.billViewModel = ViewModelProviders.of(this).get(BillViewModel.class);
        this.billViewModel.initRepository(getContext());
        this.orderViewModel = ViewModelProviders.of(this).get(OrderViewModel.class);
        this.orderViewModel.initRepository(getContext());
        this.orderViewModel.menuList.observe(this, menuList -> {

            if (menuList == null || menuList.size() == 0) {
                binding.recyclerView.setVisibility(View.GONE);
                binding.textEmptyMenu.setVisibility(View.VISIBLE);
            } else {
                binding.recyclerView.setVisibility(View.VISIBLE);
                binding.textEmptyMenu.setVisibility(View.GONE);
            }

            if (this.mAdapter == null) {
                this.mAdapter = new MenuAdapter(getContext(), Glide.with(this),
                        this.preloadSizeProvider);
                this.mAdapter.setType(2);

                RecyclerViewPreloader<MenuItemData> preloader =
                        new RecyclerViewPreloader<MenuItemData>(Glide.with(this), this.mAdapter,
                                this.preloadSizeProvider, 6);
                this.binding.recyclerView.setAdapter(this.mAdapter);
                this.binding.recyclerView.addOnScrollListener(preloader);

            }
            mAdapter.setmListData(menuList);
            this.showScrollableHint();

            try {
                this.binding.recyclerView.getLayoutManager().scrollToPosition(0);
            } catch (Exception e) {
            }
        });
        ///큐 상태 체크
        this.orderViewModel.state.observe(this, queueState -> {
            switch (queueState) {
                case NONE:
                    this.loadingDialog.dismiss();
                    break;
                case LOADING:
                    this.loadingDialog.show();
                    break;
                case ERROR:
                    this.loadingDialog.dismiss();
                    break;
                case COMPLETE:
                    this.loadingDialog.dismiss();
                    this.orderViewModel.state.setValue(OrderViewModel.STATE.NONE);
                    break;
            }
        });
        ///큐 상태 체크
        this.orderViewModel.queueState.observe(this, queueState -> {
            switch (queueState) {
                case NONE:
                    this.loadingDialog.dismiss();
                    break;
                case LOADING:
                    this.loadingDialog.show();
                    break;
                case ERROR:
                    this.loadingDialog.dismiss();
                    Toast.makeText(getContext(), "주문 큐 상태를 확인해주세요", Toast.LENGTH_SHORT).show();
                    this.orderViewModel.queueState.setValue(OrderViewModel.QUEUESTATE.NONE);
                    break;
                case COMPLETE:
                    this.loadingDialog.dismiss();
//                    PosCheckerForOrderQueue posCheckerForOrderQueue = new
//                    PosCheckerForOrderQueue(this.mCartListAdapter.getAllItem());
//                    posCheckerForOrderQueue.execute();
                    this.orderViewModel.orderToQueue(TabletSettingUtils.getInstacne().getTableNo(),
                            this.mCartListAdapter.getAllItem());
                    this.billViewModel.getOrderList(
                            TabletSettingUtils.getInstacne().getTableNo()
                    );
                    this.orderViewModel.queueState.setValue(OrderViewModel.QUEUESTATE.NONE);
                    break;
            }
        });

        this.mCartListAdapter = new Type2CartListAdapter();
        this.mCartListAdapter.setHasStableIds(true);
        this.mCartListAdapter.setListener(new Type2CartListAdapter.OnItemClickListener() {
            @Override
            public void onClickCountButton(boolean isMinus, int position) {
                mCartListAdapter.changeQuantity(isMinus, position);
            }

            @Override
            public void onClickDeleteButton(int position) {
                mCartListAdapter.deleteCart(position);
            }
        });
        this.mCartListAdapter.setOnItemChangeListener(() -> {
            this.compositeSubscription.add(Observable.just(mCartListAdapter.getItemCount())
                    .observeOn(AndroidSchedulers.mainThread())
                    .flatMap(cartItemCount -> {
                        this.binding.setCartItemCounts(cartItemCount);
                        this.setCartTotalPrice();
                        if (cartItemCount > 0) {
                            this.binding.layoutCartOpen.setBackgroundResource(
                                    R.drawable.type2_gradient_cart_background);
                            this.binding.buttonBillGoOrder.setBackgroundResource(
                                    R.drawable.selector_btn_login);
                            if (this.binding.layoutCartAlert.getAnimation() == null) {
                                this.binding.layoutCartAlert.setAnimation(
                                        AnimationUtils.loadAnimation(getContext(),
                                                R.anim.up_down_like_floating));
                            }
                        } else {
                            this.binding.layoutCartOpen.setBackgroundResource(
                                    R.drawable.type2_gradient_cart_background_off);
                            this.binding.buttonBillGoOrder.setBackgroundResource(
                                    R.drawable.selector_btn_disable);
                            this.binding.layoutCartAlert.clearAnimation();
                        }
                        return Observable.just(true);
                    })
                    .subscribe(result -> {

                    }, error -> {

                    })
            );
//            this.binding.setCartItemCounts(mCartListAdapter.getItemCount());
//            if (this.mCartListAdapter.getItemCount() > 0) {
//                this.binding.layoutCartOpen.setBackgroundResource(R.drawable
//                .type2_gradient_cart_background);
//                this.binding.buttonBillGoOrder.setBackgroundResource(R.drawable
//                .selector_btn_login);
//                if (this.binding.layoutCartAlert.getAnimation() == null)
//                    this.binding.layoutCartAlert.setAnimation(AnimationUtils.loadAnimation
//                    (getContext(), R.anim.up_down_like_floating));
//            } else {
//                this.binding.layoutCartOpen.setBackgroundResource(R.drawable
//                .type2_gradient_cart_background_off);
//                this.binding.buttonBillGoOrder.setBackgroundResource(R.drawable
//                .selector_btn_disable);
//                this.binding.layoutCartAlert.clearAnimation();
//
//            }
        });


    }

    @Override
    public int getLayout() {
        return R.layout.fragment_order2;
    }

    @Override
    public void onStart() {
        super.onStart();
//        Bundle bundle = this.getArguments();
//        if (bundle != null) {
//            String category = bundle.getString("category");
//            bundle.remove("category");
//            LogUtil.d("카테고리 : " + category);
//            if (category != null)
//                this.setCategory(category);
//        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.binding.setFragment(this);

        this.binding.layoutOrder.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        if (binding.layoutOrder.getMeasuredWidth() > 2) {
                            binding.layoutOrder.getViewTreeObserver().removeOnGlobalLayoutListener(
                                    this);
                            RelativeLayout.LayoutParams lp =
                                    (RelativeLayout.LayoutParams) binding.layoutDetailMenuSub.getLayoutParams();
                            lp.width = (int) (binding.layoutOrder.getMeasuredWidth() * 0.75);
//                    lp.height = binding.layoutSetMenu.getMeasuredHeight();
                            binding.layoutDetailMenuSub.setLayoutParams(lp);
                        }
                    }
                });

        if (TabletSettingUtils.getInstacne().getBranchInfo().univInfo.logo != null
                && TabletSettingUtils.getInstacne().getBranchInfo().univInfo.logo.length() > 0) {
            this.glide.load(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.logo)
                    .into(binding.imageLogo);
        } else {
            this.glide.load(R.drawable.icon_logo_black)
                    .into(binding.imageLogo);
        }

        this.binding.setTotalCartPrice(0);
        this.binding.setCartItemCounts(0);
        this.binding.textCartAlert.setText(
                Html.fromHtml("<font color='#FF514A'>다 담으셨다면</font><br>눌러주세요"));
        this.binding.recyclerViewCart.getLayoutManager().setItemPrefetchEnabled(true);
        ((LinearLayoutManager) this.binding.recyclerViewCart.getLayoutManager()).setInitialPrefetchItemCount(
                10);

        ///계산서 셋팅
        this.binding.layoutBill.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        binding.layoutOrder.getViewTreeObserver().removeOnGlobalLayoutListener(
                                this);
                        binding.layoutBill.setY(-binding.layoutBill.getMeasuredHeight());
                        RelativeLayout.LayoutParams lp =
                                (RelativeLayout.LayoutParams) binding.layoutBillMain.getLayoutParams();
                        lp.width = (int) (binding.layoutBill.getMeasuredWidth() * 0.6);
                        binding.layoutBillMain.setLayoutParams(lp);
                    }
                });

        this.binding.recyclerViewCart.setAdapter(this.mCartListAdapter);

//
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            this.binding.layoutCartOpen.setY(-Utils.dpToPx(getContext(), (65)));
//            this.binding.layoutBill.setY(-Utils.dpToPx(getContext(), Utils.getWindosHeight
//            (getContext())));
        }
        this.openCart();
        this.binding.layoutOrderLeft.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        int height = binding.layoutOrderLeft.getHeight();
                        if (height > 3) {
                            binding.layoutOrderLeft.getViewTreeObserver().removeOnGlobalLayoutListener(
                                    this);
                            LinearLayout.LayoutParams tlp =
                                    (LinearLayout.LayoutParams) binding.layoutSelectedCatetory.getLayoutParams();
                            tlp.height = (int) (height * 0.2);
                            binding.layoutSelectedCatetory.setLayoutParams(tlp);
                            binding.layoutSelectedCatetory.setVisibility(View.VISIBLE);
                        }
                    }
                });
        this.binding.imageSelectedMenu.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        int width = binding.imageSelectedMenu.getMeasuredWidth();
                        LogUtil.d("선택된 이미지 영역 사이즈 : " + width);
                        if (width > 3) {
                            binding.imageSelectedMenu.getViewTreeObserver().removeOnGlobalLayoutListener(
                                    this);
                            RelativeLayout.LayoutParams ilp =
                                    (RelativeLayout.LayoutParams) binding.imageSelectedMenu.getLayoutParams();
                            ilp.height = (int) (width * 0.66);
                            binding.imageSelectedMenu.setLayoutParams(ilp);
                            binding.imageSelectedMenu.setVisibility(View.VISIBLE);
                        }
                    }
                });
        this.binding.imageSelectedDetailMenu.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        int width = binding.imageSelectedDetailMenu.getMeasuredWidth();
                        LogUtil.d("선택된 이미지 영역 사이즈 : " + width);
                        if (width > 3) {
                            binding.imageSelectedDetailMenu.getViewTreeObserver().removeOnGlobalLayoutListener(
                                    this);
                            RelativeLayout.LayoutParams ilp =
                                    (RelativeLayout.LayoutParams) binding.imageSelectedDetailMenu.getLayoutParams();
                            ilp.height = (int) (width * 0.66);
                            binding.imageSelectedDetailMenu.setLayoutParams(ilp);
                            binding.imageSelectedDetailMenu.setVisibility(View.VISIBLE);
                        }
                    }
                });
//        LogUtil.d("주문 프래그먼트 상태  onActivityCreated");
//
//        this.binding.recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new
//        ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                int width = binding.recyclerView.getWidth();
//                if (width > 3) {
//                    binding.recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
////                    RelativeLayout.LayoutParams ilp = (RelativeLayout.LayoutParams) binding
// .imageSelectedMenu.getLayoutParams();
////                    ilp.height = (int) (width * 0.6);
////                    binding.imageSelectedMenu.setLayoutParams(ilp);
//
//
//                }
//            }
//        });


        ///오른쪽 사이드 버튼 영역 재조정
        RelativeLayout.LayoutParams lp =
                (RelativeLayout.LayoutParams) this.binding.layoutSide.getLayoutParams();
        lp.width = (int) (Utils.getWindosWidth(getContext()) * 0.14);
        this.binding.layoutSide.setLayoutParams(lp);

        this.binding.recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        this.binding.recyclerView.setItemViewCacheSize(30);
//        this.binding.recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new
//        ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                if (binding.recyclerView.getMeasuredWidth() > 2) {
//                    binding.recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                    int x = (int) binding.recyclerView.getX();
//                    int y = (int) binding.recyclerView.getY();
//                    int width = (int) (binding.recyclerView.getMeasuredWidth() / 2.0);
//                    int height = binding.recyclerView.getMeasuredHeight();
//
//                    binding.viewTutorialMenu.setX(x);
//                    binding.viewTutorialMenu.setY(y);
//                    FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) binding
//                    .viewTutorialMenu.getLayoutParams();
//                    lp.width = width;
//                    lp.height = height;
//                    binding.viewTutorialMenu.setLayoutParams(lp);
//                }
//            }
//        });
        this.binding.recyclerView.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_MOVE) {
                if (binding.layoutScrollableHintView.getVisibility() == View.VISIBLE) {
                    binding.layoutScrollableHintView.setVisibility(View.INVISIBLE);
                }
            }

            return false;
        });

//        this.binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//            }
//        });
        this.binding.recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        if (binding.recyclerView.getMeasuredWidth() > 2) {
                            binding.recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(
                                    this);
                            RelativeLayout.LayoutParams lp =
                                    (RelativeLayout.LayoutParams) binding.layoutScrollableHintView.getLayoutParams();
                            lp.width = (int) (binding.recyclerView.getMeasuredWidth() * (1 / 5.5));
                            binding.layoutScrollableHintView.setLayoutParams(lp);
//                    binding.layoutScrollableHintView.setVisibility(View.VISIBLE);
                            showScrollableHint();
                        }
                    }
                });

        if (preloadSizeProvider == null) {
            this.preloadSizeProvider = new ViewPreloadSizeProvider<>();
        }

        if (this.mAdapter == null) {
            this.mAdapter = new MenuAdapter(getContext(), Glide.with(this),
                    this.preloadSizeProvider);
            this.mAdapter.setType(2);
            RecyclerViewPreloader<MenuItemData> preloader = new RecyclerViewPreloader<MenuItemData>(
                    Glide.with(this), this.mAdapter, this.preloadSizeProvider, 6);
            this.binding.recyclerView.setAdapter(this.mAdapter);
            this.binding.recyclerView.addOnScrollListener(preloader);
            this.mAdapter.setListener((v, itemData, position) -> {
//                this.binding.setSelectedSetMenu(itemData);

                if (itemData.showDetail != 1 && itemData.isSet != 1) {
//                    this.mCartListAdapter.addCart(itemData);
//                    setCartTotalPrice();
//                    ExToast.setToast(getContext(), "장바구니에 추가되었습니다.", Toast.LENGTH_SHORT);

                    CustomAnimationUtils.makeFlyAnimation(getActivity(), v,
                            this.binding.textCartCounts, new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
//                            mCartListAdapter.addCart(itemData);
//                            binding.textCartCounts.setText("1건");
                                    CustomAnimationUtils.impression(binding.textCartCounts,
                                            binding.textCartCounts.getTextSize(),
                                            new Animator.AnimatorListener() {
                                                @Override
                                                public void onAnimationStart(Animator animation) {

                                                }

                                                @Override
                                                public void onAnimationEnd(Animator animation) {
                                                    LogUtil.d("텍스트 크기 변경 end : "
                                                            + binding.textCartCounts.getTextSize());
                                                    binding.textCartCounts.setTextSize(
                                                            TypedValue.COMPLEX_UNIT_SP, 22);
                                                    mCartListAdapter.addCart(itemData);
                                                    mCartListAdapter.notifyDataSetChanged();

                                                }

                                                @Override
                                                public void onAnimationCancel(Animator animation) {

                                                }

                                                @Override
                                                public void onAnimationRepeat(Animator animation) {

                                                }
                                            });

                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {

                                }
                            });


                } else {

                    if (itemData.isSet == 1) {
                        this.binding.scrollViewOptions.scrollTo(0, 0);
                        this.binding.layoutSetMenu.bringToFront();
                    } else {
                        this.binding.layoutDetailMenu.bringToFront();
                    }
                    this.setSelectedMenuViews(itemData);


//                    if (itemData.isSet == 1) {
//                        MenuDetailDialog.getInstance(getContext())
//                                .setData(getCurrentCategoryName(), mAdapter.getmListData(),
//                                position)
//                                .setCart(this.mCartListAdapter.getAllItem())
//                                .setClickAddCartListener(selectedMenu -> {
//                                    LogUtil.d("옵션 목록의 정체를 밝혀라 2 : " + selectedMenu
//                                    .selectedSideOptions);
//
//                                    this.mCartListAdapter.addCart(selectedMenu);
//                                    setCartTotalPrice();
//
//                                    ExToast.setToast(getContext(), "장바구니에 추가되었습니다.", Toast
//                                    .LENGTH_SHORT);
//
//                                    MenuDetailDialog.getmInstance().reset();
//
//                                })
//                                .setClickOrderDirectListener(() -> {
////                                    goOrderDirect(selectedMenu);
//                                    goOrder();
//                                })
//                                .setClickCartListButtonsListener(new CartListAdapter3
//                                .OnItemClickListener() {
//                                    @Override
//                                    public void onClickCountButton(boolean isMinus, int
//                                    position) {
//                                        mCartListAdapter.changeQuantity(isMinus, position);
//                                        setCartTotalPrice();
//                                        MenuDetailDialog.getmInstance().setCart
//                                        (mCartListAdapter.getAllItem());
//
//                                    }
//
//                                    @Override
//                                    public void onClickDeleteButton(int position) {
//                                        mCartListAdapter.deleteCart(position);
//                                        setCartTotalPrice();
//                                        MenuDetailDialog.getmInstance().setCart
//                                        (mCartListAdapter.getAllItem());
//
//
//                                    }
//                                })
//                                .complete();
//                    } else {
//                        this.binding.layoutSelectedMenu.setVisibility(View.VISIBLE);
//                        this.binding.layoutSelectedMenu.bringToFront();
//
//                        this.binding.setSelectedMenu(itemData);
//
////                            this.binding.textSelectedMenuPrice.setText(Utils.setComma
// (itemData.price) + "원");
////                            this.binding.textSelectedMenuTitle.setText(itemData.foodName);
////                            this.binding.textSelectedMenuIntro.setText(itemData
// .description+itemData.description+itemData.description+itemData.description+itemData
// .description);
//
//
//                        Glide.with(this).load(itemData.foodImage)
//                                .into(this.binding.imageSelectedMenu);
//
//
//                    }
                }
            });
            this.mAdapter.setmListData(new ArrayList<>());
            this.showScrollableHint();

        }

        parentCategoryId = TabletSetting.getInstance().mOrderMainCategoryList.get(0).categoryCode;
        LogUtil.d("222 : " + parentCategoryId);


        for (OrderCategoryData.CategoryItem item :
                TabletSetting.getInstance().mOrderMainCategoryList) {
            LogUtil.d("-222 : " + item.categoryCode + " / " + item.categoryName + " / "
                    + item.parentCategory);
            Category1 category1 = new Category1(getContext());
            category1.setTitle("" + item.categoryName);
            category1.setSubCategories(
                    TabletSetting.getInstance().mOrderSubCategoryList.get(item.categoryCode));
            category1.setSubCategoryClickItemListener(subCategoryItem -> {
                this.orderViewModel.getMenuList(getContext(), subCategoryItem.categoryCode,
                        TabletSettingUtils.getInstacne().getBranchInfo().storeBranchUid);
                this.binding.setSelectedCategory(
                        item.categoryName + " - " + subCategoryItem.categoryName);
                this.binding.setSelectedCategoryDescription(subCategoryItem.categoryDescription);
            });
            category1.setOnClickListener(v -> {

                this.sideCategoryInit();
                ((Category1) v).setSelected(true);
                this.parentCategoryId = item.categoryCode;
                int currentSubCategorySize = TabletSetting.getInstance().mOrderSubCategoryList.get(
                        parentCategoryId).size();
                OrderCategoryData.CategoryItem item2 =
                        TabletSetting.getInstance().mOrderSubCategoryList.get(parentCategoryId).get(
                                0);
                LogUtil.d("222 : " + item2.categoryName);

                category1.subCategoryClick();

//                this.orderViewModel.getMenuList(getContext(), item2.categoryCode,
//                TabletSettingUtils.getInstacne().getBranchInfo().storeBranchUid);
//                this.orderViewModel.getMenuLists(getContext(), TabletSetting.getInstance()
//                .mOrderSubCategoryList.get(item.categoryCode), TabletSettingUtils.getInstacne()
//                .getBranchInfo().storeBranchUid)
//                        .subscribe(menuItemData -> this.orderViewModel.menuList.setValue
//                        (menuItemData), error -> {
//                        });
            });

            this.binding.layoutButtons.addView(category1);

        }

        this.binding.layoutButtons.setVisibility(View.VISIBLE);

        if (this.binding.layoutButtons.getChildCount() > 0) {
            new Handler().postDelayed(() -> {
                this.binding.layoutButtons.getChildAt(0).performClick();
                if (TabletSettingUtils.getInstacne() != null
                        && TabletSettingUtils.getInstacne().getBranchInfo() != null
                        && TabletSettingUtils.getInstacne().getBranchInfo().storeOption != null
                        && TabletSettingUtils.getInstacne().getBranchInfo().storeOption.tutorial) {
                    this.showTutorial();
                }
            }, 100);
        }


    }

    public void closeSetSelectionView() {
        this.glide.clear(this.binding.imageSelectedMenu);
        this.glide.clear(this.binding.imageSelectedDetailMenu);
        this.binding.setSelectedSetMenu(null);
        this.binding.layoutMenuSetOptions.removeAllViews();
        if (this.selectedOptionListAdapter != null) {
            this.selectedOptionListAdapter.removeAll();
        }
    }

    private void sideCategoryInit() {
        int childCount = this.binding.layoutButtons.getChildCount();
        for (int count = 0; count < childCount; count++) {
            ((Category1) this.binding.layoutButtons.getChildAt(count)).setSelected(false);
        }
    }


    private void setSelectedMenuViews(MenuItemData itemData) {
        this.binding.setSelectedSetMenu(itemData);


        if (itemData.isSet == 1) {
            this.glide.load(itemData.foodImage).apply(this.requestOptions)
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource,
                                @Nullable Transition<? super Drawable> transition) {
                            glide.load(resource).apply(requestOptions).into(
                                    binding.imageSelectedMenu);
                        }
                    });
            for (MenuSideGroupItemData groupItem : itemData.sideGroup) {
                this.binding.layoutMenuSetOptions.addView(
                        MenuDetailSetGroupListItem.getInstance(getContext())
                                .setData(groupItem)
                                .setClickListener((selectedOptions, menuDetailSetGroupListItem) -> {

                                    this.getSelectedOptions();
//                                    this.binding.textSelectedOptions.setText(com.cdef.hoeat
//                                    .utils.Utils.getSelectedItemListStr(selectedOptions));
//                                    MenuOptionDialog.getInstance(getContext())
//                                            .setInitSelectedItem(selectedOptions)
//                                            .setGroupItem(groupItem)
//                                            .setSubmitListener((selectedOptionList) -> {
//                                            })
//                                            .complete();
                                }));
            }

            this.getSelectedOptions();

        } else if (itemData.showDetail == 1) {
            this.glide.load(itemData.foodImage).apply(this.requestOptions).into(
                    new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource,
                                @Nullable Transition<? super Drawable> transition) {
                            glide.load(resource).apply(requestOptions).into(
                                    binding.imageSelectedDetailMenu);
                        }
                    });
        }

    }


    public ArrayList<MenuSideOptionItemData> selectedOptions = new ArrayList<>();

    private void getSelectedOptions() {

//        this.binding.layoutResultSelectedOptions.removeAllViews();
        int setOptionCount = this.binding.getSelectedSetMenu().sideGroup.size();
        this.selectedOptions.clear();

        for (int index = 0; index < setOptionCount; index++) {
            selectedOptions.addAll(
                    ((MenuDetailSetGroupListItem) this.binding.layoutMenuSetOptions.getChildAt(
                            index)).getSelectedOptions());
        }
////        this.binding.layoutResultSelectedOptions.removeAllViews();
//        for (int i = 0; i < tmp.size(); i++) {
//            this.selectedOptions.put(tmp.get(i).CMDTCD, tmp.get(i));
//            this.binding.layoutResultSelectedOptions.addView(
//                    MenuDetailResultOptionListItem.getInstance(getContext()).
//                            setOption(tmp.get(i))
//            );
//        }

        if (this.selectedOptionListAdapter == null) {
            this.selectedOptionListAdapter = new SetMenuSelectedOptionListAdapter();
            this.selectedOptionListAdapter.setmListData(this.selectedOptions);
            this.selectedOptionListAdapter.setHasStableIds(true);
            this.binding.recyclerViewSelectedOptions.setAdapter(this.selectedOptionListAdapter);
        } else {
            this.selectedOptionListAdapter.setmListData(this.selectedOptions);
        }
        this.selectedOptionListAdapter.notifyDataSetChanged();
        this.binding.textMenuResultTotalPrice.setText(
                Utils.setComma((com.cdef.hoeat.utils.Utils.getSelectedOptionsTotalPrice(
                        this.selectedOptions) + this.binding.getSelectedSetMenu().price)
                        * this.binding.getSelectedSetMenu().quantity) + "원"
        );

    }

    public void touchBlock() {

    }

    public void addCart() {

        ///주문서에 담기전에 필수 선택항목이 비어있진않은지 체크
        if (this.binding.getSelectedSetMenu().isSet == 1)    //세트 메뉴일때만
        {
            ///필수 항목 점검
            int optionCount = this.binding.getSelectedSetMenu().sideGroup.size();
            int index = 0;


            for (index = 0; index < optionCount; index++) {
                if (this.binding.getSelectedSetMenu().sideGroup.get(index).isNecessary == 1 &&
                        ((MenuDetailSetGroupListItem) this.binding.layoutMenuSetOptions.getChildAt(
                                index)).getSelectedOptions().size() == 0
                )  //필수인데 선택 안한경우
                {
                    this.binding.scrollViewOptions.scrollTo(0,
                            (int) this.binding.layoutMenuSetOptions.getChildAt(index).getY());
                    Toast.makeText(getContext(),
                            "[" + this.binding.getSelectedSetMenu().sideGroup.get(index).groupName
                                    + "] 를 선택해주세요.", Toast.LENGTH_SHORT).show();
                    ((MenuDetailSetGroupListItem) this.binding.layoutMenuSetOptions.getChildAt(
                            index)).showHint();
                    return;
                }
//                index++;
            }

            ///셋트메뉴 담는 로직 추가
            MenuItemData selectedMenuItemTemp = null;
            try {
                selectedMenuItemTemp = (MenuItemData) this.binding.getSelectedSetMenu().getClone();
                selectedMenuItemTemp.selectedSideOptions = new ArrayList<>();
                for (MenuSideOptionItemData item : selectedOptions) {
                    selectedMenuItemTemp.selectedSideOptions.add(
                            (MenuSideOptionItemData) item.getClone());
                }


//                this.mCartListAdapter.addCart(selectedMenuItemTemp);
//                this.mCartListAdapter.notifyDataSetChanged();

                ///이펙트 추가
                MenuItemData finalSelectedMenuItemTemp = selectedMenuItemTemp;
                CustomAnimationUtils.makeFlyAnimation(getActivity(),
                        this.binding.buttonSetMenuAddCart, this.binding.textCartCounts,
                        new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                                closeSetSelectionView();
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
//                            mCartListAdapter.addCart(itemData);
                                CustomAnimationUtils.impression(binding.textCartCounts,
                                        binding.textCartCounts.getTextSize(),
                                        new Animator.AnimatorListener() {
                                            @Override
                                            public void onAnimationStart(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationEnd(Animator animation) {
                                                LogUtil.d("텍스트 크기 변경 end : "
                                                        + binding.textCartCounts.getTextSize());
                                                binding.textCartCounts.setTextSize(
                                                        TypedValue.COMPLEX_UNIT_SP, 22);
                                                mCartListAdapter.addCart(finalSelectedMenuItemTemp);
                                                mCartListAdapter.notifyDataSetChanged();
//                                closeSetSelectionView();
                                            }

                                            @Override
                                            public void onAnimationCancel(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationRepeat(Animator animation) {

                                            }
                                        });

                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                return;
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                LogUtil.d("담기 버튼 눌렀다가 에러남 : " + e.getLocalizedMessage());
            }


        } else if (this.binding.getSelectedSetMenu().showDetail == 1)    //상세보기 메뉴일때만
        {

            ///셋트메뉴 담는 로직 추가
            MenuItemData selectedMenuItemTemp = null;
            try {
                selectedMenuItemTemp = (MenuItemData) this.binding.getSelectedSetMenu().getClone();
                ///이펙트 추가
                MenuItemData finalSelectedMenuItemTemp = selectedMenuItemTemp;
                CustomAnimationUtils.makeFlyAnimation(getActivity(),
                        this.binding.buttonDetailMenuAddCart, this.binding.textCartCounts,
                        new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                                closeSetSelectionView();
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
//                            mCartListAdapter.addCart(itemData);
                                CustomAnimationUtils.impression(binding.textCartCounts,
                                        binding.textCartCounts.getTextSize(),
                                        new Animator.AnimatorListener() {
                                            @Override
                                            public void onAnimationStart(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationEnd(Animator animation) {
                                                LogUtil.d("텍스트 크기 변경 end : "
                                                        + binding.textCartCounts.getTextSize());
                                                binding.textCartCounts.setTextSize(
                                                        TypedValue.COMPLEX_UNIT_SP, 22);
                                                mCartListAdapter.addCart(finalSelectedMenuItemTemp);
                                                mCartListAdapter.notifyDataSetChanged();
//                                closeSetSelectionView();
                                            }

                                            @Override
                                            public void onAnimationCancel(Animator animation) {

                                            }

                                            @Override
                                            public void onAnimationRepeat(Animator animation) {

                                            }
                                        });

                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });
                return;
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                LogUtil.d("담기 버튼 눌렀다가 에러남 : " + e.getLocalizedMessage());
            }
        } else {
            this.mCartListAdapter.addCart(this.binding.getSelectedSetMenu());
            this.mCartListAdapter.notifyDataSetChanged();
        }
    }

    public void openCartList(View view) {
        this.binding.recyclerViewCart.getLayoutManager().scrollToPosition(0);
        this.binding.layoutBill.bringToFront();
        CustomAnimationUtils.moveTopToBottom(this.binding.layoutBill);
        CustomAnimationUtils.backgroundChange(this.binding.layoutBill,
                getResources().getColor(R.color.transparent),
                getResources().getColor(R.color.moremoreDim));

    }


    private void openCart() {
//        if (!isShowingNewMessageNoti && this.binding.layoutChatBack.getVisibility() != View
//        .VISIBLE) {
//            this.isShowingNewMessageNoti = true;
        this.binding.layoutCartOpen.bringToFront();
        CustomAnimationUtils.moveTopToBottom(this.binding.layoutCartOpen);

//            handler.postDelayed(() -> {
//                CustomAnimationUtils.moveBottomToTop(this.binding.layoutNewMessage);
//                isShowingNewMessageNoti = false;
//            }, 3500);

//        }

    }


    private void setCartTotalPrice() {
        int total = 0;
        if (mCartListAdapter != null) {

            for (MenuItemData item : mCartListAdapter.getAllItem()) {
                if (item.isSet == 0) {
                    total += item.price * item.quantity;
                } else {
                    total += (com.cdef.hoeat.utils.Utils.getSelectedOptionsTotalPrice(
                            item.selectedSideOptions) + item.price) * item.quantity;
                }

            }
        }
        this.binding.setTotalCartPrice(total);
    }

    public void closeBill() {
        CustomAnimationUtils.moveBottomToTop(this.binding.layoutBill);
        CustomAnimationUtils.backgroundChange(this.binding.layoutBill,
                getResources().getColor(R.color.moremoreDim),
                getResources().getColor(R.color.transparent));
        if (this.tmpIntervalCompositeSubscription != null) {
            this.tmpIntervalCompositeSubscription.clear();
        }
        this.binding.setOrderResultTimeCount(0);

    }

    public void goOrder() {
//        posChecker = new PosChecker();
//        posChecker.execute();
//        queueOrder();
//        if (OrderAppApplication.getUseQueue(getContext()) == null) {
////            기존방식
//            posChecker = new PosChecker();
//            posChecker.execute();
////            Toast.makeText(getContext(), "이전방식 주문", Toast.LENGTH_SHORT).show();
//
//        } else {
//        PosCheckerForOrderQueue posCheckerForOrderQueue = new PosCheckerForOrderQueue();
//        posCheckerForOrderQueue.execute();
//            Toast.makeText(getContext(), "큐 주문", Toast.LENGTH_SHORT).show();

//        }

        if (mCartListAdapter == null || mCartListAdapter.getItemCount() == 0) {
            Toast.makeText(getContext(), "주문할 메뉴를 1개이상 담아주세요.", Toast.LENGTH_SHORT).show();
        } else {


            ///1.최소금액 확인
            this.compositeSubscription.add(Observable.just(
                    TabletSettingUtils.getInstacne().getBranchInfo().storeOption.minOrderPrice)
                            .onErrorResumeNext(throwable -> Observable.just(0))
                            .flatMap(minOrderPrice -> {
                                if (minOrderPrice > 0) {
                                    if (this.binding.getTotalCartPrice() < minOrderPrice) {
                                        this.loadingDialog.show();
                                        return this.billViewModel.getOrderListAsObservableTotalPrice(
                                                TabletSettingUtils.getInstacne().getTableNo())
                                                .subscribeOn(Schedulers.computation())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .flatMap(currentTotalPrice -> {
                                                    if (currentTotalPrice > 0 && (currentTotalPrice
                                                            + this.binding.getTotalCartPrice())
                                                            >= minOrderPrice) {
                                                        return orderViewModel.checkQueueStateAsObservable(
                                                                QueueUtils.checkQueueState(
                                                                        TabletSettingUtils.getInstacne().getBranchInfo().posIp))
                                                                .flatMap(result -> {
                                                                    if (result) {
                                                                        return Observable.just(
                                                                                true);
                                                                    } else {
                                                                        throw new PosConnectionExceptions(
                                                                                "주문을 받을 수 없다.");
                                                                    }
                                                                });
                                                    } else {
                                                        throw new MinOrderPriceExceptions(
                                                                "첫 주문은 " + minOrderPrice
                                                                        + "원 이상 주문하셔야합니다.");
                                                    }
                                                })
                                                .flatMap(orderAvailable -> {
                                                    if (!orderAvailable) {
                                                        throw new PosConnectionExceptions("주문을 받을"
                                                                + " 수 없다.");
                                                    } else {
                                                        return Observable.just(true);
                                                    }
                                                });

                                    } else {
                                        return Observable.just(true);
                                    }
                                } else {
                                    return Observable.just(true);
                                }
                            })
                            .observeOn(AndroidSchedulers.mainThread())
                            .flatMap(minOrderPriceOKFlag -> {
//                                this.loadingDialog.dismiss();
                                return Observable.just(minOrderPriceOKFlag);
                            })
                            .observeOn(AndroidSchedulers.mainThread())
                            ///이상없으니 주문넣기
                            .flatMap(minOrderPriceOKFlag -> {
                                this.loadingDialog.show();
                                return this.orderViewModel.orderToQueueAsObservable(
                                        TabletSettingUtils.getInstacne().getTableNo(),
                                        this.mCartListAdapter.getAllItem());
                            })
//                            .subscribeOn(AndroidSchedulers.mainThread())
                            .subscribe(orderResult -> {
                                this.loadingDialog.dismiss();
                                if (MessageDialogBuilder.getmInstanceWithoutInit() != null) {
                                    MessageDialogBuilder.getmInstanceWithoutInit().dismiss();
                                }

                                String message = "주문완료되었습니다.\n잠시만 기다려주세요 :)";
                                switch (orderResult) {
                                    case -2:    //사용자 취소
                                        break;
                                    case -1:    //주문 실패. 네트워크나 다른 장애로 인해 발생.
                                        MessageDialogBuilder.getInstance(getContext()).setContent(
                                                "일시적인 문제로 주문할 수 없습니다.\n직원에게 문의해주세요.").setDefaultButton().complete();
                                        break;
                                    case 0: //주문엔 성공하였으나 수기입력인경우
                                        message = "주문완료되었습니다.(e1)\n잠시만 기다려주세요 :)";
                                    case 1: //주문 성공
                                        this.orderComplete();
//                                MessageDialogBuilder.getInstance(getContext()).setContent
//                                (message).setDefaultButton().complete();
                                        ///result 보여주기
                                        break;
                                }

                            }, error -> {
                                LogUtil.d("오류남? : " + error);
                                this.loadingDialog.dismiss();
                                if (MessageDialogBuilder.getmInstanceWithoutInit() != null) {
                                    MessageDialogBuilder.getmInstanceWithoutInit().dismiss();
                                }

                                if (error instanceof MinOrderPriceExceptions) {
                                    MessageDialogBuilder.getInstance(getContext())
                                            .setContent(error.getMessage())
                                            .setDefaultButton()
                                            .complete();

                                } else if (error instanceof PosConnectionExceptions) {
                                    MessageDialogBuilder.getInstance(getContext())
                                            .setContent("(p1)일시적인 문제로 주문할 수 없습니다.\n직원에게 문의해주세요.")
                                            .setDefaultButton()
                                            .complete();

                                } else {
                                    error.printStackTrace();

                                    MessageDialogBuilder.getInstance(getContext())
                                            .setContent("(e2)일시적인 문제로 주문할 수 없습니다.\n직원에게 문의해주세요.")
                                            .setDefaultButton()
                                            .complete();
                                }

                            })
            );

            ///매장에서 최소주문금액을 정해놨으면
//            if (TabletSettingUtils.getInstacne().getBranchInfo() != null
//                    && TabletSettingUtils.getInstacne().getBranchInfo().storeOption != null
//                    && TabletSettingUtils.getInstacne().getBranchInfo().storeOption
//                    .minOrderPrice != 0) {
//
//                if (totalPrice_textview < TabletSettingUtils.getInstacne().getBranchInfo()
//                .storeOption.minOrderPrice) {
//                    this.billViewModel.getOrderListAsObservableTotalPrice(TabletSettingUtils
//                    .getInstacne().getTableNo())
//                            .subscribe(currentTotalPrice -> {
//                                        if (currentTotalPrice > 0 && (currentTotalPrice +
//                                        totalPrice_textview) >= TabletSettingUtils.getInstacne
//                                        ().getBranchInfo().storeOption.minOrderPrice) {
//                                            orderViewModel.checkQueueState(QueueUtils
//                                            .checkQueueState(TabletSettingUtils.getInstacne()
//                                            .getBranchInfo().posIp));
//                                        } else {
//                                            MessageDialogBuilder.getInstance(getContext())
//                                                    .setContent("첫 주문은 " + Utils.setComma
//                                                    (TabletSettingUtils.getInstacne()
//                                                    .getBranchInfo().storeOption.minOrderPrice)
//                                                    + "원 이상 주문하셔야합니다.")
//                                                    .setDefaultButton()
//                                                    .complete();
//                                        }
//
//                                    }, error -> {
//                                        Toast.makeText(getContext(), "일시적인 오류로 주문내역을 불러올 수 없습니다
//                                        . e11 " + Utils.setComma(TabletSettingUtils.getInstacne
//                                        ().getBranchInfo().storeOption.minOrderPrice) + "원 이상
//                                        주문하셔야합니다.", Toast.LENGTH_LONG).show();
//                                    }
//                                    , () -> {
//                                        this.billViewModel.state.setValue(BaseViewModel.STATE
//                                        .NONE);
//                                    }
//                            );
//                    return;
//
//                }
//            }
//            if (orderViewModel.queueState.getValue() != null && orderViewModel.queueState
//            .getValue() != OrderViewModel.QUEUESTATE.LOADING) {
//                orderViewModel.queueState.setValue(OrderViewModel.QUEUESTATE.LOADING);
//                orderViewModel.checkQueueState(QueueUtils.checkQueueState(TabletSettingUtils
//                .getInstacne().getBranchInfo().posIp));
//            } else {
//                Toast.makeText(getContext(), "주문 처리중입니다.", Toast.LENGTH_LONG).show();
//            }
        }

    }

    CompositeSubscription tmpIntervalCompositeSubscription;

    private void orderComplete() {
        this.mCartListAdapter.clear();
        this.mCartListAdapter.notifyDataSetChanged();
        this.binding.setCartItemCounts(mCartListAdapter.getItemCount());
        this.tmpIntervalCompositeSubscription = new CompositeSubscription();
        this.tmpIntervalCompositeSubscription.add(Observable.interval(0, 1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(interval -> {
                    if (interval >= 10) {
                        this.closeBill();
                        return;
                    }
                    this.binding.setOrderResultTimeCount((int) (10 - interval));

                }, error -> {
                    this.binding.setOrderResultTimeCount(0);
                })
        );

    }


    boolean alreadyShowTutorial2 = false;
    boolean alreadyShowTutorial3 = false;

    //recyclerView.computeHorizontalScrollRange() > recyclerView.getWidth() || recyclerView
    // .computeVerticalScrollRange() > recyclerView.getHeight()
    public void showScrollableHint() {
        LogUtil.d(
                "showScrollableHint : " + shouldShowScrollableHint + " / canScrollVertically : " + (
                        this.binding.recyclerView.computeVerticalScrollRange()
                                > this.binding.recyclerView.getMeasuredHeight()));
        if (this.shouldShowScrollableHint && this.binding.recyclerView != null) {
            this.binding.recyclerView.postDelayed(() -> {
                if (this.binding.recyclerView.computeVerticalScrollRange()
                        > this.binding.recyclerView.getMeasuredHeight()) {
                    this.shouldShowScrollableHint = false;
                    this.binding.layoutScrollableHintView.setVisibility(View.VISIBLE);
                } else {
                    this.binding.layoutScrollableHintView.setVisibility(View.INVISIBLE);
                }
            }, 50);
        } else {
            this.binding.layoutScrollableHintView.setVisibility(View.INVISIBLE);
        }
    }

    public boolean shouldShowScrollableHint = false;

    public void showTutorial() {


        try {
            if (!PreferenceUtils.getBooleanValuePref(getContext(),
                    ConstantsUtils.PREF_FILENAME_TUTORIAL, ConstantsUtils.PREF_KEY_TUTORIAL)) {
                shouldShowScrollableHint = true;
                PreferenceUtils.addBooleanValuePref(getContext(),
                        ConstantsUtils.PREF_FILENAME_TUTORIAL, ConstantsUtils.PREF_KEY_TUTORIAL,
                        true);
                ShowcaseView sv = new ShowcaseView.Builder(getActivity())
                        .setTarget(new ViewTarget(this.binding.layoutButtons.getChildAt(0)))
                        .setContentTitle("메뉴 카테고리")
                        .blockAllTouches()
                        .setContentText("메뉴 카테고리 중 하나를 누르시면 해당 카테고리의 메뉴가 나열됩니다.")
                        .setStyle(R.style.CustomShowcaseView)
                        .build();
                RelativeLayout.LayoutParams lpsv = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                lpsv.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                lpsv.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                lpsv.setMargins(0, 0, Utils.dpToPx(getContext(), 50),
                        Utils.dpToPx(getContext(), 50));
                sv.setButtonPosition(lpsv);
                sv.setOnShowcaseEventListener(new OnShowcaseEventListener() {
                    @Override
                    public void onShowcaseViewHide(ShowcaseView showcaseView) {

                    }

                    @Override
                    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
                        if (!alreadyShowTutorial2) {
                            alreadyShowTutorial2 = true;
                            try {
                                ShowcaseView sv2 = new ShowcaseView.Builder(getActivity())
                                        .setTarget(new ViewTarget(
                                                binding.recyclerView.findChildViewUnder(0, 0)))
                                        .setContentTitle("메뉴 선택").blockAllTouches()

                                        .setContentText(
                                                "원하시는 메뉴를 눌러주세요.\n단품은 바로 주문서에 추가되고, 옵션선택이 필요한 상품의 경우 상세보기가 뜹니다.")
                                        .setStyle(R.style.CustomShowcaseView)
                                        .build();

                                sv2.setButtonPosition(lpsv);
                                sv2.setOnShowcaseEventListener(new OnShowcaseEventListener() {
                                    @Override
                                    public void onShowcaseViewHide(ShowcaseView showcaseView) {

                                    }

                                    @Override
                                    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {

                                        if (!alreadyShowTutorial3) {
                                            alreadyShowTutorial3 = true;
                                            try {

                                                ShowcaseView sv3 = new ShowcaseView.Builder(
                                                        getActivity())
                                                        .setTarget(new ViewTarget(
                                                                binding.layoutCartOpen))
                                                        .setContentTitle("주문서 확인").blockAllTouches()

                                                        .setContentText(
                                                                "원하시는 메뉴를 다 담으셨으면 '주문서 확인/주문하기' 버튼을 누르신 후 \n담겨진 메뉴를 확인하시고 주문하기를 눌러주세요.")
                                                        .setStyle(R.style.CustomShowcaseView)
                                                        .build();


                                                sv3.setButtonPosition(lpsv);
                                                sv3.show();
                                            } catch (Exception e) {

                                            }
                                        }

                                    }

                                    @Override
                                    public void onShowcaseViewShow(ShowcaseView showcaseView) {

                                    }

                                    @Override
                                    public void onShowcaseViewTouchBlocked(
                                            MotionEvent motionEvent) {

                                    }
                                });
                                sv2.show();
                            } catch (Exception e) {

                            }
                        }

                    }

                    @Override
                    public void onShowcaseViewShow(ShowcaseView showcaseView) {

                    }

                    @Override
                    public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {

                    }
                });
                sv.show();

            }
        } catch (Exception e) {

        }


    }

    @Override
    public void close() {
        if (!CustomAnimationUtils.isRunning) {
            if (this.binding.getCartItemCounts() > 0) {
                this.openCartList(this.binding.layoutCartOpen);
                MessageDialogBuilder.getInstance(getContext())
                        .setContent("주문을 완료하지않은 내역이 있습니다.\n이대로 처음화면으로 돌아가시면 \n담아두신 내역이 취소됩니다.")
                        .setDefaultButtons((view) -> {
                            MessageDialogBuilder.getmInstanceWithoutInit().dismiss();
                            super.close();
                        })
                        .complete();
            } else {
                super.close();
            }
        }
    }
}
