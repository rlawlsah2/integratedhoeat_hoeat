package com.cdef.hoeat.view.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.cdef.commonmodule.dataModel.TableSettingData;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.hoeat.OrderAppApplication;
import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.AdapterTableSettingBinding;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by kimjinmo on 2018. 8 .23
 * 테이블 셋팅 동적으로 처리한다.
 */

public class TableSettingAdapter extends BaseListAdapter<TableSettingAdapter.ViewHolder, TableSettingData> {

    RequestManager glide;
    RequestOptions myOptions = null;

    private HashMap<String, Integer> selectedList = new HashMap<>();
    int[] sizeList = {11, 12, 13, 14, 15, 16, 17, 18, 19};
    public TableSettingAdapter.OnItemClickListener listener;

    public void setListener(TableSettingAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick();
    }


    public void setmListData(ArrayList<TableSettingData> data) {

        this.mListData.clear();
        this.mListData.addAll(data);
    }

    public void clearSelectedList() {
        this.selectedList.clear();
    }

    public HashMap<String, Integer> getSelectedList() {
        return selectedList;
    }

    public ArrayList<TableSettingData> getmListData() {
        return this.mListData;
    }


    public TableSettingAdapter(Context context, RequestManager manager) {
        super();
        this.glide = manager;
        myOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new CircleCrop())
//                .transform(new RoundedCornersTransformation(20, 0, RoundedCornersTransformation.CornerType.TOP))
                .skipMemoryCache(false)
                .placeholder(R.drawable.img_sample_menu)
                .override(Utils.dpToPx(context, 228), Utils.dpToPx(context, 218));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder vh = new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_table_setting, parent, false));

        this.vhList.add(vh);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ///content 셋팅
        TableSettingData data = mListData.get(position);
        holder.binding.setMenu(data);
        holder.binding.setPosition(position);
        holder.binding.setListener(listener);
        if (data != null) {
            holder.binding.button.setTitle(data.value);
            holder.binding.button.setListener((title, count) -> {
                LogUtil.d(title + "의 수량 : " + count);
                ////count가 0이면 해제한거니 없애야 함.
                ///1 이상이면 갱신. 단 이미 있는지 체크후 처리
                if (count > 0) {
                    this.selectedList.put(title, count);
                } else {
                    this.selectedList.remove(title);
                }
                if(listener != null)
                    listener.onItemClick();
            });

            holder.binding.button.setImage(data.imageUrl);
//            holder.binding.button.setImage("https://s3.ap-northeast-2.amazonaws.com/cdefi-psp/menus/GyeongSeong/%E1%84%80%E1%85%A7%E1%86%BC%E1%84%89%E1%85%A5%E1%86%BC%E1%84%89%E1%85%B3%E1%84%90%E1%85%A6%E1%84%8B%E1%85%B5%E1%84%8F%E1%85%B3.png");

//            holder.binding.layoutMain.setTag(data);
//            TextViewCompat.setAutoSizeTextTypeWithDefaults(holder.binding.textMenuTitle, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
//            TextViewCompat.setAutoSizeTextTypeUniformWithPresetSizes(holder.binding.textMenuTitle,
//                    sizeList, TypedValue.COMPLEX_UNIT_SP);
//
//            holder.binding.textMenuPrice.setTextColor(OrderAppApplication.mContext.getResources().getColor(R.color.yellowMore));
//
//            if (data.foodImage != null && data.foodImage.length() > 5) {
//                glide.load(data.foodImage)
//                        .apply(myOptions.signature(new ObjectKey(data.foodImage)))
//                        .into(holder.binding.imageMenu);
//            } else {
//                glide.load(R.drawable.img_sample_menu)
//                        .apply(myOptions)
//                        .into(holder.binding.imageMenu);
//            }
        }
    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        static int width = 0;
        AdapterTableSettingBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);

            if (ViewHolder.width == 0)
                this.binding.button.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        int layoutWidth = binding.button.getMeasuredWidth();
                        int layoutHeight = binding.button.getMeasuredHeight();
                        if (layoutWidth > 2 && layoutHeight > 2) {
                            ViewHolder.width = layoutWidth;
                            binding.button.getViewTreeObserver().removeGlobalOnLayoutListener(this);
//                    RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) binding.image.getLayoutParams();
//                    lp.height = layoutWidth;
//                    lp.width = layoutWidth;
//                    binding.image.setLayoutParams(lp);
//
//                    RelativeLayout.LayoutParams lp2 = (RelativeLayout.LayoutParams) binding.layoutCount.getLayoutParams();
//                    lp2.height = layoutWidth;
//                    lp2.width = layoutWidth;
//                    binding.layoutCount.setLayoutParams(lp2);

                            LinearLayout.LayoutParams lpMain = (LinearLayout.LayoutParams) binding.button.getLayoutParams();
                            lpMain.width = ViewHolder.width;
                            lpMain.height = ViewHolder.width + Utils.dpToPx(OrderAppApplication.mContext, 40);
                            binding.button.setLayoutParams(lpMain);
                        }
                    }
                });
            else {
                LinearLayout.LayoutParams lpMain = (LinearLayout.LayoutParams) binding.button.getLayoutParams();
                lpMain.width = ViewHolder.width;
                lpMain.height = ViewHolder.width + Utils.dpToPx(OrderAppApplication.mContext, 40);
                binding.button.setLayoutParams(lpMain);
            }
        }
    }


    @Override
    public void onDestroy() {
        ///vh 처리
        for (ViewHolder vh : vhList) {
            if (vh != null) {
//                vh.binding.layoutMain.setBackgroundResource(android.R.color.transparent);
//                vh.binding.layoutMain.setBackgroundResource(android.R.color.transparent);
//                vh.binding.layoutBottom.setBackgroundResource(android.R.color.transparent);
//                vh.binding.textSoldOut.setBackgroundResource(android.R.color.transparent);
//                vh.binding.imageMenu.setBackgroundResource(android.R.color.transparent);
            }
        }

        ///기타 변수 초기화
        this.sizeList = null;
        this.myOptions = null;
        if (this.mListData != null)
            this.mListData.clear();
        this.mListData = null;
        this.listener = null;
    }


}
