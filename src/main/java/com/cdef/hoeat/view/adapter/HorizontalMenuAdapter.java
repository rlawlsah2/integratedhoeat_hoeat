package com.cdef.hoeat.view.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.core.widget.TextViewCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.cdef.commonmodule.dataModel.MenuItemData;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.AdapterMenuListHorizontalBinding;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;


/**
 * Created by kimjinmo on 2017. 9. 25..
 */

public class HorizontalMenuAdapter extends BaseListAdapter<HorizontalMenuAdapter.ViewHolder, MenuItemData> {

    RequestManager glide;
    RequestOptions myOptions = null;

    int[] sizeList = {11, 12, 13, 14, 15, 16, 17, 18, 19};
    public HorizontalMenuAdapter.OnItemClickListener listener;

    public void setListener(HorizontalMenuAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(View v, MenuItemData itemData, int currentPosition, int prevPosition);
    }


    public void setmListData(ArrayList<MenuItemData> data) {

        this.mListData.clear();
        this.mListData.addAll(data);
    }

    public ArrayList<MenuItemData> getmListData() {
        return this.mListData;
    }


    public static int SelectedPosition = 0;

    public HorizontalMenuAdapter(Context context, RequestManager manager, int selectedPosition) {
        super();
        this.glide = manager;
        this.SelectedPosition = selectedPosition;
        myOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new RoundedCornersTransformation(20, 0, RoundedCornersTransformation.CornerType.TOP))
//                .skipMemoryCache(true)
                .placeholder(R.drawable.img_sample_menu)
                .override(Utils.dpToPx(context, 228), Utils.dpToPx(context, 218));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder vh = new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_menu_list_horizontal, parent, false));

        this.vhList.add(vh);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ///content 셋팅
        MenuItemData data = mListData.get(position);
        holder.binding.setAdapter(this);
        holder.binding.setMenu(data);
        holder.binding.setPosition(position);
        holder.binding.setIsSelected(position == SelectedPosition);
        holder.binding.setListener(listener);
        if (data != null) {
//            holder.binding.layoutMain.setTag(data);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(holder.binding.textMenuTitle, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            TextViewCompat.setAutoSizeTextTypeUniformWithPresetSizes(holder.binding.textMenuTitle,
                    sizeList, TypedValue.COMPLEX_UNIT_SP);

            if (data.foodImage != null && data.foodImage.length() > 5) {
                glide.load(data.foodImage)
                        .apply(myOptions.signature(new ObjectKey(data.foodImage)))
                        .into(holder.binding.imageMenu);
            } else {
                glide.load(R.drawable.img_sample_menu)
                        .apply(myOptions)
                        .into(holder.binding.imageMenu);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        AdapterMenuListHorizontalBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
//            binding.layoutMain.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                @Override
//                public void onGlobalLayout() {
//                    binding.layoutMain.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//
//                    int height = binding.layoutMain.getHeight();
////                    int width = binding.layoutMain.getWidth();  //가로 사이즈
////                    int height = (int) (width * 1.01);
//
//                    RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) binding.layoutMain.getLayoutParams();
//                    lp.width = height;
//
//
//                    LogUtil.d("가로 스크롤뷰 사이즈 width : " + lp.width);
//                    LogUtil.d("가로 스크롤뷰 사이즈 lp.height : " + lp.height);
//                    LogUtil.d("가로 스크롤뷰 사이즈 height : " + height);
//                    binding.layoutMain.setLayoutParams(lp);
//                }
//            });
        }
    }


    @Override
    public void onDestroy() {
        ///vh 처리
        for (ViewHolder vh : vhList) {
            if (vh != null) {
                vh.binding.layoutMain.setBackgroundResource(android.R.color.transparent);
                vh.binding.layoutMain.setBackgroundResource(android.R.color.transparent);
                vh.binding.imageMenu.setBackgroundResource(android.R.color.transparent);
            }
        }

        ///기타 변수 초기화
        this.sizeList = null;
        this.myOptions = null;
        if (this.mListData != null)
            this.mListData.clear();
        this.mListData = null;
        this.listener = null;
    }

    public void SelectItem(View view, MenuItemData selectedItem, int position)
    {
        if(this.listener != null)
        {
            this.listener.onItemClick(view, selectedItem, position, HorizontalMenuAdapter.SelectedPosition);
            HorizontalMenuAdapter.SelectedPosition = position;
        }
    }


}
