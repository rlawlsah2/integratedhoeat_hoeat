package com.cdef.hoeat.view.adapter;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.cdef.commonmodule.dataModel.firebaseDataSet.Order;
import com.cdef.hoeat.R;
import com.cdef.hoeat.view.viewHolder.OrderStateListAdapterViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.ObservableSnapshotArray;


/**
 * Created by kimjinmo on 2016. 11. 16..
 * <p>
 * #최적화 작업완료
 */

public class OrderStateListAdapter extends FirebaseRecyclerAdapter<Order, OrderStateListAdapterViewHolder> {
    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirebaseRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public OrderStateListAdapter(@NonNull FirebaseRecyclerOptions<Order> options) {
        super(options);
    }

//    public OrderStateListAdapter(ObservableSnapshotArray<Order> dataSnapshots, @LayoutRes int modelLayout, Class<OrderStateListAdapterViewHolder> viewHolderClass, LifecycleOwner owner) {
//        super(dataSnapshots, modelLayout, viewHolderClass, owner);
//    }
//
//    public OrderStateListAdapter(ObservableSnapshotArray<Order> dataSnapshots, @LayoutRes int modelLayout, Class<OrderStateListAdapterViewHolder> viewHolderClass) {
//        super(dataSnapshots, modelLayout, viewHolderClass);
//    }
//
//    public OrderStateListAdapter(SnapshotParser<Order> parser, @LayoutRes int modelLayout, Class<OrderStateListAdapterViewHolder> viewHolderClass, Query query) {
//        super(parser, modelLayout, viewHolderClass, query);
//    }
//
//    public OrderStateListAdapter(SnapshotParser<Order> parser, @LayoutRes int modelLayout, Class<OrderStateListAdapterViewHolder> viewHolderClass, Query query, LifecycleOwner owner) {
//        super(parser, modelLayout, viewHolderClass, query, owner);
//    }
//
//    public OrderStateListAdapter(Class<Order> modelClass, @LayoutRes int modelLayout, Class<OrderStateListAdapterViewHolder> viewHolderClass, Query query) {
//        super(modelClass, modelLayout, viewHolderClass, query);
//    }
//
//    public OrderStateListAdapter(Class<Order> modelClass, @LayoutRes int modelLayout, Class<OrderStateListAdapterViewHolder> viewHolderClass, Query query, LifecycleOwner owner) {
//        super(modelClass, modelLayout, viewHolderClass, query, owner);
//    }

    public ObservableSnapshotArray<Order> getData() {
        return this.getSnapshots();
    }
//    public ObservableSnapshotArray<Order> getData() {
//        return this.mSnapshots;
//    }

//
//    @Override
//    protected void populateViewHolder(OrderStateListAdapterViewHolder holder, Order dataSet, int position) {
//        LogUtil.d("리스트 구성중 : " + position);
//        holder.binding.setItem(dataSet);
//    }


    @Override
    protected void onBindViewHolder(@NonNull OrderStateListAdapterViewHolder holder, int position, @NonNull Order dataSet) {
        holder.binding.setItem(dataSet);

    }

    @NonNull
    @Override
    public OrderStateListAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OrderStateListAdapterViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_order_state_list, parent, false));
    }
}
