package com.cdef.hoeat.view.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.TextViewCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.bumptech.glide.ListPreloader;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.util.ViewPreloadSizeProvider;
import com.cdef.commonmodule.dataModel.MenuItemData;
import com.cdef.hoeat.R;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.hoeat.databinding.AdapterMenuListBinding;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;


/**
 * Created by kimjinmo on 2017. 9. 25..
 */

public class MenuAdapter extends BaseListAdapter<MenuAdapter.ViewHolder, MenuItemData> implements ListPreloader.PreloadModelProvider<MenuItemData> {

    RequestManager glide;
    RequestOptions myOptions = null;

    ViewPreloadSizeProvider<MenuItemData> preloadSizeProvider;

    int[] sizeList = {11, 12, 13, 14, 15, 16, 17, 18, 19};
    public MenuAdapter.OnItemClickListener listener;

    public void setListener(MenuAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public List<MenuItemData> getPreloadItems(int position) {
        return this.mListData.subList(position, position + 1);
    }

    @Nullable
    @Override
    public RequestBuilder<?> getPreloadRequestBuilder(@NonNull MenuItemData data) {
//        return this.glide.load(item.foodImage);
        if (data.foodImage != null && data.foodImage.length() > 5) {
            return glide.load(data.foodImage)
                    .apply(myOptions);
        } else {
            return glide.load(R.drawable.img_sample_menu)
                    .apply(myOptions);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View v, MenuItemData itemData, int position);
    }


    public void setmListData(ArrayList<MenuItemData> data) {

        this.mListData.clear();
        if (data != null)
            this.mListData.addAll(data);
    }

    public ArrayList<MenuItemData> getmListData() {
        return this.mListData;
    }


    public MenuAdapter(Context context, RequestManager manager, ViewPreloadSizeProvider<MenuItemData> preloadSizeProvider) {
        super();
        this.glide = manager;
        this.preloadSizeProvider = preloadSizeProvider;
        myOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new RoundedCornersTransformation(18, 0, RoundedCornersTransformation.CornerType.TOP))
                .skipMemoryCache(false)
                .placeholder(R.drawable.img_sample_menu)
                .override(Utils.dpToPx(context, 228), Utils.dpToPx(context, 218));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder vh = new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_menu_list, parent, false));

        this.vhList.add(vh);
        this.preloadSizeProvider.setView(vh.binding.imageMenu);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ///content 셋팅
        MenuItemData data = mListData.get(position);
        holder.binding.setMenu(data);
        holder.binding.setPosition(position);
        holder.binding.setListener(listener);
        if (data != null) {
//            holder.binding.layoutMain.setTag(data);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(holder.binding.textMenuTitle, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            TextViewCompat.setAutoSizeTextTypeUniformWithPresetSizes(holder.binding.textMenuTitle,
                    sizeList, TypedValue.COMPLEX_UNIT_SP);

//            holder.binding.textMenuPrice.setTextColor(OrderAppApplication.mContext.getResources().getColor(R.color.yellowMore));

            if (data.foodImage != null && data.foodImage.length() > 5) {
//                glide.asGif().load("https://media.giphy.com/media/l3q2yYNt8DXoyKRdm/giphy.gif")

                glide.load(data.foodImage)
//                        .apply(myOptions.signature(new ObjectKey(data.foodImage)))
                        .apply(myOptions)

                        .into(holder.binding.imageMenu);
            } else {
                glide.load(R.drawable.img_sample_menu)
                        .apply(myOptions)
                        .into(holder.binding.imageMenu);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        AdapterMenuListBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            binding.layoutMain.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    binding.layoutMain.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int width = binding.layoutMain.getWidth();  //가로 사이즈


                    int height = (int) (width * 1.01);

                    GridLayoutManager.LayoutParams lp = (GridLayoutManager.LayoutParams) binding.layoutMain.getLayoutParams();
                    lp.height = height;
                    binding.layoutMain.setLayoutParams(lp);
                }
            });
        }
    }

    @Override
    public void onDestroy() {
        ///vh 처리
        for (ViewHolder vh : vhList) {
            if (vh != null) {
                vh.binding.layoutMain.setBackgroundResource(android.R.color.transparent);
                vh.binding.layoutMain.setBackgroundResource(android.R.color.transparent);
                vh.binding.layoutBottom.setBackgroundResource(android.R.color.transparent);
                vh.binding.textSoldOut.setBackgroundResource(android.R.color.transparent);
                vh.binding.imageMenu.setBackgroundResource(android.R.color.transparent);
            }
        }

        if(this.glide != null)
        {
            this.glide = null;
        }

        if(this.myOptions != null)
        {
            this.myOptions = null;
        }

        ///기타 변수 초기화
        this.sizeList = null;
        this.myOptions = null;
        if (this.mListData != null)
            this.mListData.clear();
        this.mListData = null;
        this.listener = null;
    }


}
