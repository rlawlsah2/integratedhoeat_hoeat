package com.cdef.hoeat.view.adapter.type2;

import androidx.databinding.DataBindingUtil;
import androidx.core.widget.TextViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cdef.commonmodule.dataModel.MenuSideOptionItemData;
import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.Type2AdapterSetMenuDetailSelectedOptionListItemBinding;
import com.cdef.hoeat.view.adapter.BaseListAdapter;

import java.util.ArrayList;


/**
 * Created by kimjinmo on 2017. 9. 25..
 */

public class SetMenuSelectedOptionListAdapter extends BaseListAdapter<SetMenuSelectedOptionListAdapter.ViewHolder, MenuSideOptionItemData> {


    public void setmListData(ArrayList<MenuSideOptionItemData> data) {
        this.mListData.clear();
        if (data != null)
            this.mListData.addAll(data);
    }

    public ArrayList<MenuSideOptionItemData> getmListData() {
        return this.mListData;
    }


    public SetMenuSelectedOptionListAdapter() {
        super();

    }

    @Override
    public long getItemId(int position) {
        return this.mListData.get(position).CMDTCD.hashCode();
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder;
        View rowView;
        rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.type2_adapter_set_menu_detail_selected_option_list_item, parent, false);
        viewHolder = new ViewHolder(rowView);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ///content 셋팅
        MenuSideOptionItemData data = mListData.get(position);
        holder.binding.setOption(data);


    }


    @Override
    public int getItemCount() {
        return mListData.size();
    }

    public void removeAll()
    {
        this.mListData.clear();
        this.notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        Type2AdapterSetMenuDetailSelectedOptionListItemBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }

}
