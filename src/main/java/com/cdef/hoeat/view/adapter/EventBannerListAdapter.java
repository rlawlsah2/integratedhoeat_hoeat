package com.cdef.hoeat.view.adapter;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.cdef.commonmodule.dataModel.BannerDefaultData;
import com.cdef.hoeat.R;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.hoeat.databinding.AdapterEventBannerListBinding;

import java.util.ArrayList;


/**
 * Created by kimjinmo on 2017. 9. 25..
 */

public class EventBannerListAdapter extends RecyclerView.Adapter<EventBannerListAdapter.ViewHolder> {

    ArrayList<BannerDefaultData> mListData = new ArrayList<>();
    RequestManager glide;
    RequestOptions myOptions = null;

    public EventBannerListAdapter.OnItemClickListener listener;

    public void setListener(EventBannerListAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(View v, BannerDefaultData itemData);
    }


    public void setmListData(ArrayList<BannerDefaultData> data) {

        this.mListData.clear();
        this.mListData.addAll(data);
    }

    public EventBannerListAdapter(Context context, RequestManager manager) {
        this.glide = manager;
        myOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(false)
                .placeholder(R.drawable.img_ad_placeholder);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder vh = new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_event_banner_list, parent, false));

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ///content 셋팅
        BannerDefaultData data = mListData.get(position);
        holder.binding.setListener(this.listener);
        holder.binding.setBanner(data);

        if (data != null) {
            if (data.bannerImage != null && data.bannerImage.length() > 5) {
                LogUtil.d("불러올 이미지 주소 : " + data.bannerImage);
                glide.load(data.bannerImage)
                        .apply(myOptions)
                        .into(holder.binding.image);
            } else
                glide.load(R.drawable.img_ad_placeholder)
                        .apply(myOptions)
                        .into(holder.binding.image);
        }
    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        AdapterEventBannerListBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
//            binding.layoutMain.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                @Override
//                public void onGlobalLayout() {
//                    binding.layoutMain.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//
//
//                    int width = binding.layoutMain.getWidth();  //가로 사이즈
//                    int height = (int) (width * 1.01);
//
//                    GridLayoutManager.LayoutParams lp = (GridLayoutManager.LayoutParams) binding.layoutMain.getLayoutParams();
//                    lp.height = height;
//                    binding.layoutMain.setLayoutParams(lp);
//                }
//            });
        }
    }


}
