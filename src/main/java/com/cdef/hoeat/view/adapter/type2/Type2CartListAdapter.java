package com.cdef.hoeat.view.adapter.type2;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.dataModel.MenuItemData;
import com.cdef.hoeat.R;
import com.cdef.hoeat.utils.Utils;
import com.cdef.hoeat.view.adapter.BaseListAdapter;
import com.cdef.hoeat.view.viewHolder.CartListAdapterViewHolder;
import com.cdef.hoeat.view.viewHolder.Type2CartListAdapterViewHolder;

import java.util.ArrayList;


/**
 * Created by kimjinmo on 2018. 8 .23
 * type2 카트 어댑터
 */

public class Type2CartListAdapter extends BaseListAdapter<Type2CartListAdapterViewHolder, MenuItemData> {


    public void deleteCart(int position) {
        try {
            this.mListData.remove(position);
        } catch (Exception e) {
        } finally {
            this.notifyDataSetChanged();
            if (this.onItemChangeListener != null)
                this.onItemChangeListener.onItemChange();
        }
    }

    public void addCart(MenuItemData data) {
        //혹시 이전에 포함된 데이터는 아닌지 파악
        try {
            ///셋트여부
            if (data.isSet == 0) {
                checkContainAndAddOne((MenuItemData) data.getClone());
            } else {
                this.mListData.add(0, (MenuItemData) data.getClone());
                notifyDataSetChanged();
            }

            if (this.onItemChangeListener != null)
                this.onItemChangeListener.onItemChange();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }


    }

    @Override
    public long getItemId(int position) {
        if (this.mListData.get(position).isSet == 1) {
            return (this.mListData.get(position).CMDTCD + Utils.getSelectedOptionsName(this.mListData.get(position).selectedSideOptions)).hashCode();
        } else {
            return (this.mListData.get(position).CMDTCD + this.mListData.get(position).quantity).hashCode();

        }
    }

    public void checkContainAndAddOne(MenuItemData data) {

        int position = 0;

        boolean isContained = false;
        for (MenuItemData item : this.mListData) {
            if (item.foodUid == data.foodUid) {
                isContained = true;
                break;
            }
            position++;
        }

        if (isContained) {
            //지우고 맨위로 추가
            try {
                MenuItemData tmp = (MenuItemData) this.mListData.get(position).getClone();
                tmp.quantity++;

                this.mListData.remove(position);
                this.mListData.add(0, tmp);

            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            } finally {
                notifyDataSetChanged();
                return;
            }
        }

        ///처음일 경우
        this.mListData.add(0, data);
        notifyDataSetChanged();

        if (this.onItemChangeListener != null)
            this.onItemChangeListener.onItemChange();
    }


    public MenuItemData getItem(int index) {
        return this.mListData.get(index);
    }

    public ArrayList<MenuItemData> getAllItem() {
        return this.mListData;
    }

    public void clear() {
        this.mListData.clear();
        notifyDataSetChanged();
        if (this.onItemChangeListener != null)
            this.onItemChangeListener.onItemChange();
    }

    public void changeQuantity(boolean isMinus, int position) {

        if (isMinus) {
            if (this.mListData.get(position).quantity > 1) {
                this.mListData.get(position).quantity--;
            } else if (this.mListData.get(position).quantity == 1) {
                deleteCart(position);
            }
        } else {
            this.mListData.get(position).quantity++;
        }
        this.notifyDataSetChanged();

        if (this.onItemChangeListener != null)
            this.onItemChangeListener.onItemChange();
    }

    @Override
    public void onDestroy() {

    }


    @Override
    public Type2CartListAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Type2CartListAdapterViewHolder vh = new Type2CartListAdapterViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.type2_adapter_cart_list, parent, false));
        return vh;
    }

    @Override
    public void onBindViewHolder(Type2CartListAdapterViewHolder holder, int position) {
        holder.binding.setListener(listener);
        holder.binding.setPosition(position);
        holder.binding.setMenu(getItem(position));
    }

    @Override
    public int getItemCount() {
        LogUtil.d("");
        return mListData.size();
    }


    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public OnItemClickListener listener;

    public interface OnItemClickListener {
        void onClickCountButton(boolean isMinus, int position);

        void onClickDeleteButton(int position);
    }

    OnItemChangeListener onItemChangeListener;

    public void setOnItemChangeListener(OnItemChangeListener listener) {
        this.onItemChangeListener = listener;
    }

    public interface OnItemChangeListener {
        void onItemChange();
    }


}
