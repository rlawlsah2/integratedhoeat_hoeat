package com.cdef.hoeat.view.adapter.type2;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.TextViewCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import com.bumptech.glide.ListPreloader;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.util.ViewPreloadSizeProvider;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.commonmodule.dataModel.MenuItemData;
import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.AdapterMenuList2xWideBinding;
import com.cdef.hoeat.databinding.AdapterMenuList3xBinding;
import com.cdef.hoeat.view.adapter.BaseListAdapter;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by kimjinmo on 2017. 9. 25..
 */

public class MenuAdapter extends BaseListAdapter<RecyclerView.ViewHolder, MenuItemData> implements ListPreloader.PreloadModelProvider<MenuItemData> {

    RequestManager glide;
    RequestOptions myOptions = null;

    ViewPreloadSizeProvider<MenuItemData> preloadSizeProvider;

    int[] sizeList = {11, 12, 13, 14, 15, 16, 17, 18, 19};
    public MenuAdapter.OnItemClickListener listener;

    public void setListener(MenuAdapter.OnItemClickListener listener) {
        this.listener = listener;
    }

    int type = 1;  /// 1: 3x, 2: 2x_wide

    public void setType(int type) {
        this.type = type;
    }


    @NonNull
    @Override
    public List<MenuItemData> getPreloadItems(int position) {
        return this.mListData.subList(position, position + 1);
    }

    @Nullable
    @Override
    public RequestBuilder<?> getPreloadRequestBuilder(@NonNull MenuItemData data) {
//        return this.glide.load(item.foodImage);
        if (data.foodImage != null && data.foodImage.length() > 5) {
            return glide.load(data.foodImage)
                    .apply(myOptions);
        } else {
            return glide.load(R.drawable.img_sample_menu)
                    .apply(myOptions);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View v, MenuItemData itemData, int position);
    }


    public void setmListData(ArrayList<MenuItemData> data) {

        this.mListData.clear();
        if (data != null)
            this.mListData.addAll(data);
        this.notifyDataSetChanged();
    }

    public ArrayList<MenuItemData> getmListData() {
        return this.mListData;
    }


    public MenuAdapter(Context context, RequestManager manager, ViewPreloadSizeProvider<MenuItemData> preloadSizeProvider) {
        super();
        this.glide = manager;
        this.preloadSizeProvider = preloadSizeProvider;
        myOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .transform(new RoundedCornersTransformation(18, 0, RoundedCornersTransformation.CornerType.TOP))
                .skipMemoryCache(false)
                .placeholder(R.drawable.img_sample_menu)
                .override(Utils.dpToPx(context, 210), Utils.dpToPx(context, 140));
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View rowView;
        switch (viewType) {
            case VIEW_TYPES.x3:
                rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_menu_list_3x, parent, false);
                viewHolder = new x3ViewHolder(rowView);
                this.preloadSizeProvider.setView(((x3ViewHolder) viewHolder).binding.imageMenu);
                LogUtil.d("메뉴 어댑터 : viewHolder :" + viewHolder + " / type" + type + " / viewType : " + viewType);
                return viewHolder;

            case VIEW_TYPES.x2_wide:
                rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_menu_list_2x_wide, parent, false);
                viewHolder = new x2WideViewHolder(rowView);
                this.preloadSizeProvider.setView(((x2WideViewHolder) viewHolder).binding.imageMenu);
                LogUtil.d("메뉴 어댑터 : viewHolder :" + viewHolder + " / type" + type + " / viewType : " + viewType);
                return viewHolder;

            default:
                rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_menu_list_3x, parent, false);
                viewHolder = new x3ViewHolder(rowView);
                this.preloadSizeProvider.setView(((x3ViewHolder) viewHolder).binding.imageMenu);
                LogUtil.d("메뉴 어댑터 : viewHolder :" + viewHolder + " / type" + type + " / viewType : " + viewType);
                return viewHolder;

        }

    }

    @Override
    public int getItemViewType(int position) {
//        return super.getItemViewType(position);
        if (this.type == VIEW_TYPES.x3) {
            return VIEW_TYPES.x3;
        } else {
            return VIEW_TYPES.x2_wide;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ///content 셋팅
        MenuItemData data = mListData.get(position);

        if (holder instanceof x3ViewHolder) {

            ((x3ViewHolder) holder).binding.setMenu(data);
            ((x3ViewHolder) holder).binding.setPosition(position);
            ((x3ViewHolder) holder).binding.setListener(listener);
            if (data != null) {
                TextViewCompat.setAutoSizeTextTypeWithDefaults(((x3ViewHolder) holder).binding.textMenuTitle, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
                TextViewCompat.setAutoSizeTextTypeUniformWithPresetSizes(((x3ViewHolder) holder).binding.textMenuTitle,
                        sizeList, TypedValue.COMPLEX_UNIT_SP);

                if (data.foodImage != null && data.foodImage.length() > 5) {
                    glide.load(data.foodImage)
                            .apply(myOptions)
                            .into(((x3ViewHolder) holder).binding.imageMenu);
                } else {
                    glide.load(R.drawable.img_sample_menu)
                            .apply(myOptions)
                            .into(((x3ViewHolder) holder).binding.imageMenu);
                }
            }

        } else {
            ((x2WideViewHolder) holder).binding.textDescription.setLines(10);
            ((x2WideViewHolder) holder).binding.setAdapter(this);
            ((x2WideViewHolder) holder).binding.setMenu(data);
            ((x2WideViewHolder) holder).binding.setPosition(position);
            ((x2WideViewHolder) holder).binding.setListener(listener);
            ((x2WideViewHolder) holder).binding.checkboxRecommand.setSelected(data.recommendFlag == 1);

            if (data.foodImage != null && data.foodImage.length() > 5) {
                glide.load(data.foodImage)
                        .apply(myOptions)
                        .into(((x2WideViewHolder) holder).binding.imageMenu);
            } else {
                glide.load(R.drawable.img_sample_menu)
                        .apply(myOptions)
                        .into(((x2WideViewHolder) holder).binding.imageMenu);
            }


            ((x2WideViewHolder) holder).binding.textDescription.postDelayed(() -> {
                int height = (int) (
                        ((x2WideViewHolder) holder).binding.textDescription.getPaint().getFontMetrics().descent
                                - ((x2WideViewHolder) holder).binding.textDescription.getPaint().getFontMetrics().ascent
                                + ((x2WideViewHolder) holder).binding.textDescription.getPaint().getFontMetrics().leading);
                int viewHeight = ((x2WideViewHolder) holder).binding.textDescription.getMeasuredHeight();
                int lineCount = ((x2WideViewHolder) holder).binding.textDescription.getLineCount();

                LogUtil.d("textDescription viewHeight : " + viewHeight);
                LogUtil.d("textDescription realHeight : " + height);
                LogUtil.d("textDescription realLineCount : " + lineCount);

                int removeLineCount = 0;

                while (viewHeight <= height * (lineCount - removeLineCount)) {
                    removeLineCount++;
                }
                LogUtil.d("textDescription removeLineCount : " + removeLineCount);
                if (removeLineCount > 0) {
                    ((x2WideViewHolder) holder).binding.textDescription.setLines(lineCount - removeLineCount);
                }
            }, 50);


        }
    }


    public void touchBlock() {

    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    public static class x3ViewHolder extends RecyclerView.ViewHolder {

        AdapterMenuList3xBinding binding;

        public x3ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            binding.layoutMain.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    binding.layoutMain.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int width = binding.layoutMain.getWidth();  //가로 사이즈


                    int height = (int) (width * 1.01);

                    GridLayoutManager.LayoutParams lp = (GridLayoutManager.LayoutParams) binding.layoutMain.getLayoutParams();
                    lp.height = height;
                    binding.layoutMain.setLayoutParams(lp);
                }
            });
        }
    }

    public static class x2WideViewHolder extends RecyclerView.ViewHolder {

        AdapterMenuList2xWideBinding binding;
        static int width = 0;

        public x2WideViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);

            /// 뷰 사이즈
            if (width < 3) {
                binding.imageMenu.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        binding.imageMenu.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        int mainWidth = binding.layoutMain.getWidth();  //가로 사이즈
                        x2WideViewHolder.width = (int) (mainWidth * 0.4);
                        int height = (int) (x2WideViewHolder.width * 0.66);

                        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) binding.imageMenu.getLayoutParams();
                        lp.width = x2WideViewHolder.width;
                        lp.height = height;
                        binding.imageMenu.setLayoutParams(lp);
                    }
                });
            } else {
                int height = (int) (x2WideViewHolder.width * 0.66);
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) binding.imageMenu.getLayoutParams();
                lp.width = x2WideViewHolder.width;
                lp.height = height;
                binding.imageMenu.setLayoutParams(lp);
            }
        }
    }

    @Override
    public void onDestroy() {
        ///vh 처리
//        for (ViewHolder vh : vhList) {
//            if (vh != null) {
//                vh.binding.layoutMain.setBackgroundResource(android.R.color.transparent);
//                vh.binding.layoutMain.setBackgroundResource(android.R.color.transparent);
//                vh.binding.layoutBottom.setBackgroundResource(android.R.color.transparent);
//                vh.binding.textSoldOut.setBackgroundResource(android.R.color.transparent);
//                vh.binding.imageMenu.setBackgroundResource(android.R.color.transparent);
//            }
//        }

        if (this.glide != null) {
            this.glide = null;
        }

        if (this.myOptions != null) {
            this.myOptions = null;
        }

        ///기타 변수 초기화
        this.sizeList = null;
        this.myOptions = null;
        if (this.mListData != null)
            this.mListData.clear();
        this.mListData = null;
        this.listener = null;
    }


    private class VIEW_TYPES {
        public static final int x3 = 1;
        public static final int x2_wide = 2;
    }


}
