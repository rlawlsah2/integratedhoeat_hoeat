package com.cdef.hoeat.view.adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.TextViewCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.ListPreloader;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.util.ViewPreloadSizeProvider;
import com.cdef.commonmodule.utils.Utils;
import com.cdef.commonmodule.dataModel.MenuItemData;
import com.cdef.hoeat.R;
import com.cdef.hoeat.databinding.AdapterLanguageListBinding;
import com.cdef.hoeat.databinding.AdapterMenuListBinding;
import com.cdef.hoeat.view.dialog.LanguageDialog;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;


/**
 * Created by kimjinmo on 2017. 9. 25..
 */

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.ViewHolder> {


    RequestManager glide;
    RequestOptions myOptions = null;

    public ArrayList<String> mListData = new ArrayList<>();

    int[] sizeList = {11, 12, 13, 14, 15, 16, 17, 18, 19};

    public void setmListData(ArrayList<String> data) {

        this.mListData.clear();
        if (data != null)
            this.mListData.addAll(data);
    }

    public ArrayList<String> getmListData() {
        return this.mListData;
    }


    public LanguageAdapter(Context context, RequestManager manager) {
        super();
        this.glide = manager;
        myOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .skipMemoryCache(false);
    }

    public void setSelectItemListener(SelectItemListener selectItemListener) {
        this.selectItemListener = selectItemListener;
    }

    SelectItemListener selectItemListener;
    public interface SelectItemListener{
        void click(String select);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder vh = new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_language_list, parent, false));

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ///content 셋팅
        String data = mListData.get(position);
        holder.binding.setLanguage(data);
        holder.binding.setListener(this.selectItemListener);
        this.glide.load("https://d1pchlk7cbrs1q.cloudfront.net/langFlags/"+ data + ".png")
                .apply(this.myOptions)
                .into(holder.binding.image);
    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        AdapterLanguageListBinding binding;


        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);

        }
    }

}
