package com.cdef.hoeat.view.adapter;

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.cdef.commonmodule.dataModel.firebaseDataSet.ExMessage;
import com.cdef.hoeat.R;
import com.cdef.hoeat.view.viewHolder.PrivateChatListAdapterViewHolder;
import com.firebase.ui.common.ChangeEventType;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;

import java.util.HashSet;

/**
 * Created by kimjinmo on 2016. 11. 16..
 * <p>
 * #최적화 작업완료
 */

public class PrivateChatListAdapter extends FirebaseRecyclerAdapter<ExMessage, PrivateChatListAdapterViewHolder> {

    public static HashSet<String> newMessageList = new HashSet<>();

    public static String selectedTableNo = null;
    public static OnItemClickListener listener;
    public static OnItemCloseClickListener closeClickListener;
    public String tableNo;

    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirebaseRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public PrivateChatListAdapter(@NonNull FirebaseRecyclerOptions<ExMessage> options, String tableNo) {
        super(options);
        this.tableNo = tableNo;

    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

//    public PrivateChatListAdapter(ObservableSnapshotArray<ExMessage> dataSnapshots, @LayoutRes int modelLayout, Class<PrivateChatListAdapterViewHolder> viewHolderClass, LifecycleOwner owner) {
//        super(dataSnapshots, modelLayout, viewHolderClass, owner);
//    }
//
//    public PrivateChatListAdapter(ObservableSnapshotArray<ExMessage> dataSnapshots, @LayoutRes int modelLayout, Class<PrivateChatListAdapterViewHolder> viewHolderClass) {
//        super(dataSnapshots, modelLayout, viewHolderClass);
//    }
//
//    public PrivateChatListAdapter(SnapshotParser<ExMessage> parser, @LayoutRes int modelLayout, Class<PrivateChatListAdapterViewHolder> viewHolderClass, Query query) {
//        super(parser, modelLayout, viewHolderClass, query);
//    }
//
//    public PrivateChatListAdapter(SnapshotParser<ExMessage> parser, @LayoutRes int modelLayout, Class<PrivateChatListAdapterViewHolder> viewHolderClass, Query query, LifecycleOwner owner) {
//        super(parser, modelLayout, viewHolderClass, query, owner);
//    }
//
//    public PrivateChatListAdapter(Class<ExMessage> modelClass, @LayoutRes int modelLayout, Class<PrivateChatListAdapterViewHolder> viewHolderClass, Query query) {
//        super(modelClass, modelLayout, viewHolderClass, query);
//    }
//
//    public PrivateChatListAdapter(Class<ExMessage> modelClass, @LayoutRes int modelLayout, Class<PrivateChatListAdapterViewHolder> viewHolderClass, Query query, String tableNo) {
//        super(modelClass, modelLayout, viewHolderClass, query);
//        this.tableNo = tableNo;
//    }
//
//    public PrivateChatListAdapter(Class<ExMessage> modelClass, @LayoutRes int modelLayout, Class<PrivateChatListAdapterViewHolder> viewHolderClass, Query query, LifecycleOwner owner) {
//        super(modelClass, modelLayout, viewHolderClass, query, owner);
//    }

    public void setNewFlag(String newMessageSender) {
        if (newMessageSender != null) {
            PrivateChatListAdapter.newMessageList.add(newMessageSender);
        }
    }

    /**
     * 선택한 대화방의 경우 리스트에서 삭제해야하므로
     **/
    public void deleteNewFlag(String str) {
        if (str != null) {
            PrivateChatListAdapter.newMessageList.remove(str);
        }

    }


    /**
     * 거꾸로 하기 위함.
     */

    @Override
    public void onChildChanged(@NonNull ChangeEventType type, @NonNull DataSnapshot snapshot, int index, int oldIndex) {
//        super.onChildChanged(type, snapshot, newIndex, oldIndex);
        switch (type) {

            case ADDED:
                notifyItemInserted(getItemCount() - 1 - index);
                break;
            case CHANGED:
                notifyItemChanged(getItemCount() - 1 - index);
                break;
            case REMOVED:
                notifyItemRemoved(getItemCount() - 1 - index);
                break;
            case MOVED:
                notifyItemMoved(getItemCount() - 1 - oldIndex, getItemCount() - 1 - index);
                break;
            default:
                throw new IllegalStateException("Incomplete case statement");
        }

    }

    /**
     * 거꾸로 하기 위함.
     */
    @Override
    public ExMessage getItem(int pos) {
        return super.getItem(getItemCount() - 1 - pos);
    }

    @Override
    protected void onBindViewHolder(@NonNull PrivateChatListAdapterViewHolder holder, int position, @NonNull ExMessage dataSet) {


        if (dataSet != null) {
            if (dataSet.sender != null) {
                if (!dataSet.sender.equals(tableNo)) {  ///타인이 보낸경우
                    dataSet.key = dataSet.sender;
                } else {
                    dataSet.key = dataSet.receiver;
                }
            } else {
                dataSet.key = dataSet.receiver;
                if (dataSet.key == null || dataSet.key.equals("null"))
                    dataSet.key = "";
            }
        }
        holder.binding.setMessage(dataSet);
        holder.binding.setListener(this.listener);
        holder.binding.setCloseListener(this.closeClickListener);
        holder.binding.setTableNo(dataSet.key);
    }

    @NonNull
    @Override
    public PrivateChatListAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PrivateChatListAdapterViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_private_chat_list, parent, false));
    }


    public interface OnItemClickListener {
        void onItemClick(View v, String tableNo);
    }


    public void setCloseListener(OnItemCloseClickListener listener) {
        this.closeClickListener = listener;
    }


    public interface OnItemCloseClickListener {
        void close(View v, String tableNo);
    }


}
