package com.cdef.hoeat.view.adapter;

/**
 * Created by kimjinmo on 2018. 5. 3..
 */

import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;
import com.cdef.commonmodule.dataModel.firebaseDataSet.Message;
import com.cdef.hoeat.R;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.hoeat.view.viewHolder.ChatListAdapterViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseError;

/**
 * Created by kimjinmo on 2018.5.4
 * <p>
 * 채팅 아이템 작업완료
 */

public class ChatListAdapter extends FirebaseRecyclerAdapter<Message, ChatListAdapterViewHolder> {

    String tableNo;
    RequestManager glide;

    RequestOptions requestOptions;

    /**
     * Initialize a {@link RecyclerView.Adapter} that listens to a Firebase query. See
     * {@link FirebaseRecyclerOptions} for configuration options.
     *
     * @param options
     */
    public ChatListAdapter(@NonNull FirebaseRecyclerOptions<Message> options, String tableNo, RequestManager glide) {
        super(options);
        this.tableNo = tableNo;
        this.glide = glide;
        this.requestOptions = new RequestOptions();
        this.requestOptions.placeholder(R.drawable.img_sample_menu)
                .override(400, 266);
    }
//
//    public ChatListAdapter(Class<Message> modelClass, @LayoutRes int modelLayout, Class<ChatListAdapterViewHolder> viewHolderClass, Query query, String tableNo, RequestManager glide) {
//        super(modelClass, modelLayout, viewHolderClass, query);
//        this.tableNo = tableNo;
//        this.glide = glide;
//        this.requestOptions = new RequestOptions();
//        this.requestOptions.placeholder(R.drawable.img_sample_menu)
//                .override(400, 266);
//    }

    /**
     * 거꾸로 하기 위함.
     */

//    @Override
//    public void onChildChanged(@NonNull ChangeEventType type, @NonNull DataSnapshot snapshot, int index, int oldIndex) {
////        super.onChildChanged(type, snapshot, newIndex, oldIndex);
//        switch (type) {
//
//            case ADDED:
//                notifyItemInserted(getItemCount() - 1 - index);
//                break;
//            case CHANGED:
//                notifyItemChanged(getItemCount() - 1 - index);
//                break;
//            case REMOVED:
//                notifyItemRemoved(getItemCount() - 1 - index);
//                break;
//            case MOVED:
//                notifyItemMoved(getItemCount() - 1 - oldIndex, getItemCount() - 1 - index);
//                break;
//            default:
//                throw new IllegalStateException("Incomplete case statement");
//        }
//
//    }
//
//    /**
//     * 거꾸로 하기 위함.
//     */
//    @Override
//    public Message getItem(int pos) {
//        return super.getItem(getItemCount() - 1 - pos);
//    }

    @Override
    public void onError(@NonNull DatabaseError error) {
        super.onError(error);
        LogUtil.d("채팅 error : " + error);
    }

    @Override
    protected void onBindViewHolder(@NonNull ChatListAdapterViewHolder holder, int position, @NonNull Message dataSet) {
        LogUtil.d("리스트 구성중 : " + position);
        LogUtil.d("리스트 구성중 dataSet.sender: " + dataSet.sender);

        holder.binding.setMyTableNo(this.tableNo);
        holder.binding.setMessage(dataSet);
        try {
            holder.binding.setPrevMessage(getItem(position + 1));   //위에 채팅

        } catch (Exception e) {

        }
        try {
            if (position >= 1) {
                holder.binding.setNextvMessage(getItem(position - 1));   //밑에 채팅
            }
        } catch (Exception e) {

        }

        if (dataSet != null && dataSet.present != null && dataSet.present.menuImage != null) {
            if(this.tableNo.equals(dataSet.sender))
            {
                this.glide.load(dataSet.present.menuImage).apply(this.requestOptions).into(holder.binding.imagePresent2);
            }
            else
            {
                this.glide.load(dataSet.present.menuImage).apply(this.requestOptions).into(holder.binding.imagePresent);
            }
        }
    }

    @NonNull
    @Override
    public ChatListAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ChatListAdapterViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_chat, parent, false));
    }

    @Override
    public void onDataChanged() {
        super.onDataChanged();
        LogUtil.d("채팅 뭐 있나");
    }

    public interface OnItemClickListener {
        void onItemClick(View v, String tableNo);
    }

}
