package com.cdef.hoeat.view.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.ViewGroup;

import com.cdef.commonmodule.dataModel.BaseData;

import java.util.ArrayList;
import java.util.HashSet;


/**
 * Created by kimjinmo on 2017. 9. 25..
 */

public abstract class BaseListAdapter<T1 extends RecyclerView.ViewHolder, T2 extends BaseData> extends RecyclerView.Adapter<T1> {

    public ArrayList<T2> mListData = new ArrayList<>();

    public HashSet<T1> vhList = new HashSet<>();

    public void setmListData(ArrayList<T2> data) {

        this.mListData.clear();
        this.mListData.addAll(data);
    }

    public BaseListAdapter() {

    }

    public abstract void onDestroy();


    @NonNull
    @Override
    public T1 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull T1 holder, int position) {

    }

}
