package com.cdef.hoeat.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.hoeat.view.activity.EventActivity;
import com.cdef.hoeat.view.fragment.BillFragment;
import com.cdef.hoeat.view.fragment.LoginFragment;
import com.cdef.hoeat.view.fragment.OrderFragment;
import com.cdef.hoeat.view.fragment.OrderStateFragment;
import com.cdef.hoeat.view.fragment.PresentFragment;
import com.cdef.hoeat.view.fragment.TableSettingFragment;
import com.cdef.hoeat.view.type2.fragment.BillFragment2;

/**
 * Created by kimjinmo on 2016. 11. 16..
 */

public class NavigationUtils {

    public static final String BACKSTACKID = "fragments";


    public static void removeAllFragment(FragmentManager fragmentManager) {
        fragmentManager.popBackStack();
    }

    public static void openOrderFragment(FragmentManager fragmentManager, int layoutFragment, String categoryId) {


        Fragment currentFragment = fragmentManager.findFragmentById(layoutFragment);
        OrderFragment newFragment = null;
        String tag = OrderFragment.class.getSimpleName();

        //열려고하는 fragment가 존재하지않으면 생성해야지
        if (currentFragment == null) {

            Bundle bundle = new Bundle();
            bundle.putString("category", categoryId);
            newFragment = new OrderFragment();
            newFragment.setArguments(bundle);

            fragmentManager.beginTransaction()
                    .add(layoutFragment, newFragment, OrderFragment.class.getSimpleName())
                    .addToBackStack(NavigationUtils.BACKSTACKID)
                    .setTransition(FragmentTransaction.TRANSIT_NONE)
                    .commit();
            newFragment.setCategory(categoryId);
        } else {
            if (currentFragment instanceof OrderFragment) {
                ((OrderFragment) currentFragment).setCategory(categoryId);
            } else {
                removeAllFragment(fragmentManager);
                Bundle bundle = new Bundle();
                bundle.putString("category", categoryId);
                newFragment = new OrderFragment();
                newFragment.setArguments(bundle);
                fragmentManager.beginTransaction()
                        .add(layoutFragment, newFragment, tag)
                        .addToBackStack(NavigationUtils.BACKSTACKID)
                        .setTransition(FragmentTransaction.TRANSIT_NONE)
                        .commit();
                newFragment.setCategory(categoryId);
                //지워버려
            }
        }
    }


    /***
     * fragment를 새로열때(단, 돌아올 때 새로 로딩 안됨) + 애니메이션 처리 없이
     * **/
    public static void detailOpenFragment(FragmentManager fragmentManager, int layoutFragment, String openFragment, Bundle bundle) {
        LogUtil.d("NavigationUtils_performance start : ");


        Fragment currentFragment = fragmentManager.findFragmentById(layoutFragment);
        Fragment checkAlreadyIncludedFragment = fragmentManager.findFragmentByTag(openFragment);

        LogUtil.d("NavigationUtils detailOpenFragment : " + openFragment);
        LogUtil.d("NavigationUtils detailOpenFragment currentFragment : " + currentFragment);
        LogUtil.d("NavigationUtils detailOpenFragment checkAlreadyIncludedFragment : " + checkAlreadyIncludedFragment);

        Fragment newFragment = null;

        //열려고하는 fragment가 존재하지않으면 생성해야지
        if (checkAlreadyIncludedFragment == null) {


            if (openFragment.equals(LoginFragment.class.getSimpleName())) {
                newFragment = new LoginFragment();
            } else if (openFragment.equals(BillFragment.class.getSimpleName())) {
                newFragment = new BillFragment();
            } else if (openFragment.equals(BillFragment2.class.getSimpleName())) {
                newFragment = new BillFragment2();
            } else if (openFragment.equals(TableSettingFragment.class.getSimpleName())) {
                newFragment = new TableSettingFragment();
            } else if (openFragment.equals(OrderStateFragment.class.getSimpleName())) {
                newFragment = new OrderStateFragment();
            } else if (openFragment.equals(PresentFragment.class.getSimpleName())) {
                newFragment = new PresentFragment();
            } else if (openFragment.equals(com.cdef.hoeat.view.type2.fragment.OrderFragment.class.getSimpleName())) {
                newFragment = new com.cdef.hoeat.view.type2.fragment.OrderFragment();
            }
        }

        if (newFragment == null)
            return;

        if (bundle != null && newFragment != null)
            newFragment.setArguments(bundle);

        if (currentFragment == null) //열린 화면이 하나도 없는 경우
        {
            fragmentManager.beginTransaction()
                    .add(layoutFragment, newFragment, openFragment)
                    .addToBackStack(NavigationUtils.BACKSTACKID)
                    .setTransition(FragmentTransaction.TRANSIT_NONE)
                    .commit();

            LogUtil.d("NavigationUtils_performance end1 : ");
        } else {
            if (checkAlreadyIncludedFragment == null)    //열린 화면은 존재하는데, 열려고 하는 화면이 manager에 없을때
            {
                removeAllFragment(fragmentManager);
                fragmentManager.beginTransaction()
                        .hide(currentFragment)
                        .add(layoutFragment, newFragment, openFragment)
                        .addToBackStack(NavigationUtils.BACKSTACKID)
                        .setTransition(FragmentTransaction.TRANSIT_NONE)
                        .commit();
                LogUtil.d("NavigationUtils_performance end2 : ");
            } else {
                if (currentFragment != checkAlreadyIncludedFragment)     //열린 화면이 존재하고, 열려고 하는 화면이 열린것과는 다른경우. 즉 back단에 있을때
                {
                    removeAllFragment(fragmentManager);
                    fragmentManager.beginTransaction()
                            .hide(currentFragment)
                            .add(layoutFragment, newFragment, openFragment)
                            .addToBackStack(NavigationUtils.BACKSTACKID)
                            .setTransition(FragmentTransaction.TRANSIT_NONE)
                            .commit();
                } else    //열린화면이 딱 한개인경우에 해당.
                {

                }

                LogUtil.d("NavigationUtils_performance end3 : ");
            }
        }
    }


    public static void openNewActivity(Context context, Class activity, int isRandomEvent) {
        if (activity.getSimpleName().equals(EventActivity.class.getSimpleName())) {

            Intent intent = new Intent(context, EventActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            if (isRandomEvent > 0) {
                intent.putExtra("isRandomEvent", isRandomEvent);
                ///시간이 되면 뜨는 자동 광고
            }
            context.startActivity(intent);
            ((Activity) context).overridePendingTransition(0, 0);
        }
    }
}
