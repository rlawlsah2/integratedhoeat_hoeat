package com.cdef.hoeat.utils;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.hoeat.Exceptions.RestartExceptions;
import com.cdef.hoeat.view.activity.MainActivity;
import com.cdef.hoeat.view.activity.Splash;
import com.crashlytics.android.Crashlytics;

import java.lang.Thread.UncaughtExceptionHandler;

/**
 * This custom class is used to handle exception.
 *
 * @author Chintan Rathod (http://www.chintanrathod.com)
 */
public class DefaultExceptionHandler implements UncaughtExceptionHandler {


    private Activity activity;

    public DefaultExceptionHandler(Activity a) {
        activity = a;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {

        ///오류 보고 추가를 위한 코드
        if(ex instanceof RestartExceptions)
        {

        }
        else {
            if (TabletSettingUtils.getInstacne().getBranchInfo() != null) {
                Crashlytics.setUserIdentifier(TabletSettingUtils.getInstacne().getBranchInfo().branchId);
                Crashlytics.setString("tableNo", TabletSettingUtils.getInstacne().getSelectedTableNo());
                Crashlytics.logException(ex);

            }
        }
        LogUtil.d("에러가 터졌네여 1: " + ex);

        Intent intent = new Intent(activity, Splash.class);
        intent.putExtra("crash", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);


        ///현재 로그인 정보를 가져와서 인텐트로 추가해줘야 함.
        String apiKey = TabletSettingUtils.getInstacne().getApiKey();
        String tableNo = TabletSettingUtils.getInstacne().getTableNo();
        String branchName = TabletSettingUtils.getInstacne().getBranchInfo().branchName;

        if (apiKey != null
                && tableNo != null
                && branchName != null) {

            intent.putExtra("apiKey", apiKey);
            intent.putExtra("tableNo", tableNo);
            intent.putExtra("branchName", branchName);

        }


        LogUtil.d("에러가 터졌네여 2: " + ex);

        PendingIntent pendingIntent = PendingIntent.getActivity(activity, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        LogUtil.d("에러가 터졌네여 3: " + ex);

        AlarmManager mgr = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
        LogUtil.d("에러가 터졌네여 4: " + ex);

        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 300, pendingIntent);
        LogUtil.d("에러가 터졌네여 5: " + ex);

        activity.finish();
        System.exit(2);
    }
}