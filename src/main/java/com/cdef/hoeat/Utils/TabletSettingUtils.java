package com.cdef.hoeat.utils;


import com.cdef.commonmodule.dataModel.firebaseDataSet.TableInfo;
import com.cdef.commonmodule.dataModel.TableInfoData;
import com.cdef.commonmodule.login.BranchInfo;
import com.cdef.commonmodule.login.TableMap;
import com.cdef.commonmodule.login.TableMapItem;
import com.cdef.commonmodule.utils.ConstantsUtils;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.LoginUtil;
import com.cdef.commonmodule.utils.PreferenceUtils;
import com.cdef.hoeat.OrderAppApplication;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

public class TabletSettingUtils {


    private String apiKey;
    private String tableNo;
    private ArrayList<TableMap> tableMaps;
    private HashMap<String, ArrayList<TableMapItem>> convertedTableMaps;
    private HashMap<String, TableMapItem> tableInfoMap; //각 테이블의 상태값을 저장하는 변수


    public ArrayList<TableMap> getTableMaps() {
        return tableMaps;
    }

    public void setTableMaps(ArrayList<TableMap> tableMaps) {
        this.tableMaps = tableMaps;
        ///세부 분류 하자
        ///convertedTableMaps를 초기화한다.
        if (this.convertedTableMaps == null)
            this.convertedTableMaps = new HashMap<>();
        this.convertedTableMaps.clear();
        for (TableMap item : tableMaps) {
            this.convertedTableMaps.put(item.hallName, item.talbeMapJson);
        }
    }


    public HashMap<String, ArrayList<TableMapItem>> getConvertedTableMaps() {
        return convertedTableMaps;
    }

    public String getSelectedTableNo() {
        return selectedTableNo;
    }

    public void setSelectedTableNo(String selectedTableNo) {
        this.selectedTableNo = selectedTableNo;
    }

    private String selectedTableNo; //선택된 테이블 번호. 직원용 전용
    private BranchInfo branchInfo;
    private String IFSA_ORDER_ID;
    private String STBL_CD;

    public TableInfoData getTableInfoData() {
        if (tableInfoData == null)
            tableInfoData = new TableInfoData();
        return tableInfoData;
    }

    public void setTableInfoData(TableInfoData tableInfoData) {
        this.tableInfoData = tableInfoData;
    }

    private TableInfoData tableInfoData;

    public void setOrderId(String orderId) {
        this.IFSA_ORDER_ID = orderId;
    }

    public String getOrderId()
    {
        return this.IFSA_ORDER_ID;
    }
    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getTableNo() {
        if (tableNo == null && OrderAppApplication.mContext != null) {
            tableNo = PreferenceUtils.getStringValuePref(OrderAppApplication.mContext, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_TABLENO);
        }
        return tableNo;
    }

    public void setTableNo(String tableNo) {
        this.tableNo = tableNo;
        if (tableNo != null)
        {
            LogUtil.d("setTableNo OrderAppApplication.mContext : " + OrderAppApplication.mContext);

            PreferenceUtils.addStringValuePref(OrderAppApplication.mContext, ConstantsUtils.PREF_FILENAME_LOGIN, ConstantsUtils.PREF_KEY_LOGIN_TABLENO, this.tableNo);
        }
    }

    public BranchInfo getBranchInfo() {
        return branchInfo;
    }

    public void setBranchInfo(BranchInfo branchInfo) {
        this.branchInfo = branchInfo;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        LoginUtil.getInstance().setsAccessToken(accessToken);
    }

    private String accessToken;


    synchronized public static TabletSettingUtils getInstacne() {
        if (TabletSettingUtils.instance == null)
            TabletSettingUtils.instance = new TabletSettingUtils();

        return TabletSettingUtils.instance;
    }

    private TabletSettingUtils() {

    }

    public void setTableInfoList(JsonObject str) {
        if (this.tableInfoMap == null)
            this.tableInfoMap = new HashMap<>();
        this.tableInfoMap.clear();
        if (str != null) {
            Gson gson = new Gson();
            LogUtil.d("매장 테이블 정보 : " + str.toString());
            for (String key : str.keySet()) {
                LogUtil.d(key + " 테이블 정보 : " + str.get(key));
                ///큐에서 가져온 경우 서버에서 가져올 경우랑 약간 다름
                ///40={tNo=40, x=300.0, width=50.0, tableId=, y=250.0, height=50.0, members={men=0.0, women=0.0, age=0.0}, IFSA_ORDER_ID=null}
                TableMapItem tableMapItem = gson.fromJson(str.get(key), TableMapItem.class);
                tableMapItem.tableInfo = new TableInfo();
                tableMapItem.tableInfo.members = tableMapItem.members;
                tableMapItem.tableInfo.IFSA_ORDER_ID = tableMapItem.IFSA_ORDER_ID;
                this.tableInfoMap.put(key, tableMapItem);
            }
        }
    }

    public HashMap<String, TableMapItem> getTableInfoList() {
        if (this.tableInfoMap == null)
            this.tableInfoMap = new HashMap<>();
        return this.tableInfoMap;
    }


    private static TabletSettingUtils instance;


}
