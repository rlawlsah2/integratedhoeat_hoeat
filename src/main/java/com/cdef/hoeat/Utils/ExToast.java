package com.cdef.hoeat.utils;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cdef.hoeat.R;

import java.lang.ref.WeakReference;

public class ExToast {

    public static void setToast(Context context, String message, int duration)
    {

        LayoutInflater inflate = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

// Custom layout
        View view = inflate.inflate(R.layout.layout_extoast, null);
        TextView tv = (TextView) view.findViewById(R.id.textTitle);
        tv.setText(message);

// Toast
        Toast toast = new Toast(context);
        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 60);
        toast.setDuration(duration);

// Set the Toast custom layout
        toast.setView(view);
        toast.show();

    }
}
