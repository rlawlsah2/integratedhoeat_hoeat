package com.cdef.commonmodule.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.cdef.hoeat.Exceptions.RestartExceptions;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.cdef.hoeat.view.activity.MainActivity;
import com.crashlytics.android.Crashlytics;

import java.lang.Thread.UncaughtExceptionHandler;

/**
 * This custom class is used to handle exception.
 *
 * @author Chintan Rathod (http://www.chintanrathod.com)
 * 앱이 켜지지않은 상태일 경우에 대한 핸들링 처리
 */
public class DefaultExceptionHandlerForBackground implements UncaughtExceptionHandler {


    private Context context;

    public DefaultExceptionHandlerForBackground(Context a) {
        context = a;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        ///오류 보고 추가를 위한 코드
        if (ex instanceof RestartExceptions) {

        } else {
            if (TabletSettingUtils.getInstacne().getBranchInfo() != null) {
                Crashlytics.setUserIdentifier(TabletSettingUtils.getInstacne().getBranchInfo().branchId);
                Crashlytics.setString("tableNo", TabletSettingUtils.getInstacne().getSelectedTableNo());
                Crashlytics.logException(ex);

            }
        }
        LogUtil.d("에러가 터졌네여 1: " + ex);

        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("crash", true);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);

        ///현재 로그인 정보를 가져와서 인텐트로 추가해줘야 함.
        String apiKey = TabletSettingUtils.getInstacne().getApiKey();
        String tableNo = TabletSettingUtils.getInstacne().getTableNo();
        String branchName = TabletSettingUtils.getInstacne().getBranchInfo().fbBrandName;

        if (apiKey != null
                && tableNo != null
                && branchName != null) {

            intent.putExtra("apiKey", apiKey);
            intent.putExtra("tableNo", tableNo);
            intent.putExtra("branchName", branchName);

        }

        LogUtil.d("에러가 터졌네여 2: " + ex);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        LogUtil.d("에러가 터졌네여 3: " + ex);

        AlarmManager mgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        LogUtil.d("에러가 터졌네여 4: " + ex);

        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 300, pendingIntent);
        LogUtil.d("에러가 터졌네여 5: " + ex);

//        if(context)
//        context.finish();
        System.exit(2);
    }
}