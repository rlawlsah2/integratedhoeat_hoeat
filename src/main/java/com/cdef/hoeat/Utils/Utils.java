package com.cdef.hoeat.utils;

import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.dataModel.firebaseDataSet.Order;
import com.cdef.commonmodule.dataModel.MenuSideOptionItemData;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

public class Utils extends com.cdef.commonmodule.utils.Utils {


    /**
     * 선택된 옵션 목록
     **/
    public static String getSelectedItemListStr(ArrayList<MenuSideOptionItemData> selectedItemList) {
        if (selectedItemList == null || selectedItemList.size() == 0) {
            return null;
        }
        StringBuilder str = new StringBuilder();
        int itemCount = 0;
        for (MenuSideOptionItemData item : selectedItemList) {
            itemCount++;
            str.append(item.foodName.trim())
                    .append(" (")
                    .append(item.quantity)
                    .append(")");
            if (itemCount < selectedItemList.size())
                str.append(", ");

        }

        return str.toString();
    }


    /**
     * 선택된 옵션 중 원하는 항목만 따로 리스트로 빼내기
     **/
    public static String getSelectedItemOptionListStr(String typeName, String divider, ArrayList<MenuSideOptionItemData> selectedItemList) {
        if (selectedItemList == null || selectedItemList.size() == 0) {
            return "선택된 옵션없음";
        }

        StringBuilder str = new StringBuilder();
        int itemCount = 0;
        for (MenuSideOptionItemData item : selectedItemList) {


            if (typeName.equals("foodName")) {
                str.append(item.foodName);
            } else if (typeName.equals("quantity")) {
                str.append(item.quantity + "");
            } else if (typeName.equals("price")) {
                str.append(Utils.setComma(item.quantity * item.price) + "원");
            }

            if (itemCount < selectedItemList.size())
                str.append(divider);
            itemCount++;

        }
        return str.toString();
    }


    /**
     * 선택된 옵션 목록
     **/
    public static String getSelectedItemListStrFromOrder(HashMap<String, Order> selectedItemList) {
        if (selectedItemList == null || selectedItemList.size() == 0) {
            return null;
        }
        StringBuilder str = new StringBuilder();
        int itemCount = 0;
        for (String key : selectedItemList.keySet()) {
            itemCount++;
            str.append(selectedItemList.get(key).foodName.trim())
                    .append(" (")
                    .append(selectedItemList.get(key).quantity)
                    .append(")");
            if (itemCount < selectedItemList.size())
                str.append(", ");

        }

        return str.toString();
    }


    /**
     * 선택된 옵션 가격 합산값.
     **/
    public static int getSelectedOptionsTotalPrice(ArrayList<MenuSideOptionItemData> selectedItemList) {
        if (selectedItemList == null || selectedItemList.size() == 0) {
            return 0;
        }
        int totalPrice = 0;
        for (MenuSideOptionItemData item : selectedItemList) {

            totalPrice += item.price * item.quantity;
        }

        return totalPrice;
    }

    /**
     * 선택된 옵션 가격 합산값.
     **/
    public static String getSelectedOptionsName(ArrayList<MenuSideOptionItemData> selectedItemList) {
        if (selectedItemList == null || selectedItemList.size() == 0) {
            return "name";
        }
        String result = "";
        for (MenuSideOptionItemData item : selectedItemList) {
            result += item.foodName + item.quantity;
        }

        return result;
    }

}
