package com.cdef.hoeat.utils;

public class QueueUtils {


    private static String baseUrl(String posIp) {
        return "http://" + posIp + ":9626";
    }

    /**
     * 주문 큐 상태 체크
     **/
    public static String checkQueueState(String posIp) {
        return baseUrl(posIp) + "/health";
    }

    /**
     * 주문
     **/
    public static String order(String posIp) {
        return baseUrl(posIp) + "/api/remote/order";
    }


    /**
     * 테이블 정보 가져오기
     **/
    public static String requestQueueEvent(String posIp) {
        return baseUrl(posIp) + "/api/remote/event";
    }


    /**
     * 매장 모든 테이블 정보 가져오기
     **/
    public static String getTableInfoList(String posIp) {
        return baseUrl(posIp) + "/data/tables";
    }


    /**
     * 테이블 셋팅 요청
     **/
    public static String request(String posIp) {
        return baseUrl(posIp) + "/data/notification";
    }

    /**
     * fcm 토큰 등록
     **/
    public static String registrationToken(String posIp, String tableNo) {
        return baseUrl(posIp) + "/data/tables/" + tableNo + "/registrationToken";
    }

    /**
     * 테이블 정보 가져오기
     **/
    public static String getTableInfo(String posIp, String tableNo) {
        return baseUrl(posIp) + "/data/orders/" + tableNo;
    }

}
