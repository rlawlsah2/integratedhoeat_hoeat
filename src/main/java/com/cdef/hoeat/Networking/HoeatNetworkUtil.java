package com.cdef.hoeat.Networking;

import android.content.Context;

import com.cdef.commonmodule.Networking.NetworkUtil;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.hoeat.OrderAppApplication;

/**
 * Created by kimjinmo on 2018. 7. 5..
 *
 *
 *
 */

public class HoeatNetworkUtil extends NetworkUtil {
    protected static HoeatNetworkUtil instance = null;

    public static HoeatNetworkUtil getInstance(Context context) {
        if (HoeatNetworkUtil.instance == null)
            instance = new HoeatNetworkUtil(context);
        return instance;
    }



    private HoeatNetworkUtil(Context context) {
        super(context);

        LogUtil.d("홈 네트워크 유틸 생성자");
    }

    @Override
    public void inject(Context context) {
        super.inject(context);
        LogUtil.d("홈 네트워크 유틸 inject");

        OrderAppApplication
                .getNetworkComponent(context)
                .inject(this);

    }
}
