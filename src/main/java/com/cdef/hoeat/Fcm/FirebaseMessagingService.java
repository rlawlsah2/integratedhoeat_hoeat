package com.cdef.hoeat.Fcm;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.bumptech.glide.Glide;
import com.cdef.commonmodule.utils.DefaultExceptionHandlerForBackground;
import com.cdef.commonmodule.utils.LoginUtil;
import com.cdef.hoeat.OrderAppApplication;
import com.cdef.hoeat.R;
import com.cdef.commonmodule.utils.LogUtil;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;


public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    private static final String TAG = "FirebaseMsgService";


    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (OrderAppApplication.mContext == null) {
            Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandlerForBackground(this));
        }

//        Toast.makeText(getApplicationContext(), "주문앱 fcm 수신 : " + remoteMessage.getData(), Toast.LENGTH_SHORT).show();
        LogUtil.d("fcm 들어온다1 : " + remoteMessage);
        LogUtil.d("fcm 들어온다2 : " + remoteMessage.getMessageId());
        LogUtil.d("fcm 들어온다3 : " + remoteMessage.getData());
        LogUtil.d("fcm 들어온다4 : " + remoteMessage.getFrom());
        LogUtil.d("fcm 들어온다5 : " + remoteMessage.getNotification());
        for (String item : remoteMessage.getData().keySet()) {
            LogUtil.d("fcm 들어온다5 : " + item);
        }

        if (remoteMessage.getData() != null) {
            String type = remoteMessage.getData().get("type");
            if (type != null) {
                switch (type) {

                    case "restart":
//                        this.clearAppData();
                        this.clearGlideData();
                        proccessRestart();
                        break;
                    case "light":
                        String lightType = remoteMessage.getData().get("lightType");
                        Intent light = new Intent();
                        light.setAction("com.cdef.orderappmanager.LIGHT");
                        light.putExtra("type", lightType);
                        sendBroadcast(light);
                        break;
                    case "notice":
                        String message = remoteMessage.getData().get("message");
                        notice(message);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private void notice(String message) {
        sendNotification(message);
    }

//    private void checkBTModule()
//    {
//        if(getApplicationContext() != null && LoginUtil.getInstance(getApplicationContext()) != null && LoginUtil.getInstance(getApplicationContext()).getBTModuleName() != null)
//        {
//            boolean bool = BTModule.getInstance(getApplicationContext(), LoginUtil.getInstance(getApplicationContext()).getBTModuleName()).isConnected();
//
//            LogUtil.d("fcm checkBTModule : " + (bool? "이상없음." : "연결 시도중..."));
//
//
//        }
//    }

    private boolean isAppRunning(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        for (int i = 0; i < procInfos.size(); i++) {
            if (procInfos.get(i).processName.equals(context.getPackageName())) {
                LogUtil.d("패키지 : " + procInfos.get(i).processName);
                return true;
            }
        }

        return false;
    }

    private void proccessRestart() {

//        try {
//            throw new NullPointerException("proccessRestart");
//        } catch (NullPointerException e) {
//            startActivity(new Intent(this, MainActivity.class));
//        }

//        System.exit(0);


        if(OrderAppApplication.mContext != null) {
            ///언어 선택 초기화
            LoginUtil.getInstance().setSelectedLanguage(OrderAppApplication.mContext, null);
            ActivityCompat.finishAffinity((Activity) OrderAppApplication.mContext);
        }
        System.runFinalizersOnExit(true);
        System.exit(0);
    }

    private void sendNotification(String messageBody) {
//        Intent intent = new Intent(this, MainActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("카운터")
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
//                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_MAX); //** MAX 나 HIGH로 줘야 가능함


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }



    ///애플리케이션 로컬 데이터 모두 삭제하기
    private void clearAppData() {
        try {
            // clearing app data
            if (Build.VERSION_CODES.KITKAT <= Build.VERSION.SDK_INT) {
                ((ActivityManager)getSystemService(ACTIVITY_SERVICE)).clearApplicationUserData(); // note: it has a return value!
            } else {
                String packageName = getApplicationContext().getPackageName();
                Runtime runtime = Runtime.getRuntime();
                runtime.exec("pm clear "+packageName);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void clearGlideData()
    {
        try {
            Glide.get(getApplicationContext()).clearDiskCache();
        }catch (Exception e)
        {

        }

        try {
            Glide.get(getApplicationContext()).clearMemory();
        }catch (Exception e)
        {

        }
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
    }
}

