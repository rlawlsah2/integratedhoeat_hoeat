package com.cdef.hoeat.Exceptions;

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

public class NotFoundStoreMenuListExceptions extends RuntimeException {

    public NotFoundStoreMenuListExceptions(String message)
    {
        super(message);
    }
}
