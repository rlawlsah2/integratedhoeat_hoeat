package com.cdef.hoeat.Exceptions;

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

public class PosConnectionExceptions extends RuntimeException {

    public PosConnectionExceptions(String message)
    {
        super(message);
    }
}
