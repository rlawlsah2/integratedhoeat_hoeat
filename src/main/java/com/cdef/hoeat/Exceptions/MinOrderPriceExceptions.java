package com.cdef.hoeat.Exceptions;

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

public class MinOrderPriceExceptions extends RuntimeException {

    public MinOrderPriceExceptions(String message)
    {
        super(message);
    }
}
