package com.cdef.hoeat.Exceptions;

/**
 * Created by kimjinmo on 2018. 8. 1..
 */

public class RestartExceptions extends RuntimeException {

    public RestartExceptions(String message)
    {
        super(message);
    }
}
