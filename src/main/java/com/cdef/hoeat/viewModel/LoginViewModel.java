package com.cdef.hoeat.viewModel;

import androidx.lifecycle.MutableLiveData;
import android.content.Context;
import android.widget.Toast;

import com.cdef.commonmodule.login.TableMap;
import com.cdef.commonmodule.Networking.Responses.LoginResponse;
import com.cdef.hoeat.BuildConfig;
import com.cdef.commonmodule.dataModel.LoginData;
import com.cdef.hoeat.Networking.HoeatNetworkUtil;
import com.cdef.hoeat.OrderAppApplication;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.TabletSetting;
import com.cdef.commonmodule.exceptions.LoginInfoNotFoundException;
import com.cdef.commonmodule.Repository.LoginRepository;
import com.cdef.commonmodule.Repository.OrderRepository;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class LoginViewModel extends BaseViewModel {



    public MutableLiveData<LoginData> loginInfo = new MutableLiveData<LoginData>() {
    };
    public MutableLiveData<INITSETTING> initSetting = new MutableLiveData<INITSETTING>() {
    };

    public MutableLiveData<ArrayList<TableMap>> tableMapList = new MutableLiveData<>();
    private LoginRepository loginRepository;
    private OrderRepository orderRepository;

    private String mBranchID = null;
    private String mBranchPW = null;
    private String mBranchTableNo = null;

    public enum INITSETTING {
        NONE, LOADING, ERROR, COMPLETE
    }

    public LoginViewModel() {

    }

    public void initRepository(Context context) {
        this.loginRepository = new LoginRepository(HoeatNetworkUtil.getInstance(context).getService());
        this.orderRepository = new OrderRepository(HoeatNetworkUtil.getInstance(context).getService());
        this.initSetting.setValue(INITSETTING.NONE);

    }

    public void getMapInfo(String branchUid) {
        this.compositeSubscription.add(this.loginRepository.getAllMapInfo(branchUid)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(BuildConfig.APIRequestRetryLimit)
                .subscribe(
                        response -> {
                            LogUtil.d("테이블 맵 가져오기 스트림 subscribe success: " + response);
                            this.tableMapList.setValue(response);

                        },
                        error -> {
                            LogUtil.d("테이블 맵 가져오기 스트림 subscribe error: " + error);
                            this.tableMapList.setValue(null);

                        },
                        () -> {

                        }));

    }

    public void getMainCategoryList(Context context) {
        this.initSetting.setValue(INITSETTING.LOADING);
        this.compositeSubscription.add(this.loginRepository.initSetting()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
//                .retry(BuildConfig.APIRequestRetryLimit)
                .subscribe(
                        response -> {
                            LogUtil.d("로그인 스트림 subscribe success1: " + response);

                            TabletSetting.getInstance().setOrderMainCategoryList(loginRepository.mainCategories);

                            for (String key : loginRepository.subCategories.keySet()) {
                                TabletSetting.getInstance().setOrderSubCategoryList(key, loginRepository.subCategories.get(key));
                            }
                            LogUtil.d("프래그먼트에서 보는 초기화 셋팅 스트림 현황 COMPLETE 모델쪽1: " + response);


                            this.initSetting.setValue(INITSETTING.COMPLETE);

                        },
                        error -> {
                            if (error instanceof LoginInfoNotFoundException) {
                                orderRepository.updateAccessToken(TabletSettingUtils.getInstacne().getApiKey(),
                                        TabletSettingUtils.getInstacne().getTableNo())
                                        .subscribeOn(Schedulers.computation())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .onErrorResumeNext(cannotLogin -> Observable.error(cannotLogin))
                                        .subscribe(
                                                updateTokenSuccess -> {

                                                    TabletSettingUtils.getInstacne().setAccessToken(((LoginResponse) updateTokenSuccess).result.accessToken);
                                                    TabletSettingUtils.getInstacne().setBranchInfo(((LoginResponse) updateTokenSuccess).result.branchInfo);

                                                    this.loginRepository.initSetting()
                                                            .subscribeOn(Schedulers.computation())
                                                            .observeOn(AndroidSchedulers.mainThread())
                                                            .subscribe(
                                                                    response2 -> {
                                                                        LogUtil.d("로그인 스트림 subscribe success2: " + response2);

                                                                        TabletSetting.getInstance().setOrderMainCategoryList(loginRepository.mainCategories);

                                                                        for (String key : loginRepository.subCategories.keySet()) {
                                                                            TabletSetting.getInstance().setOrderSubCategoryList(key, loginRepository.subCategories.get(key));
                                                                        }

                                                                        LogUtil.d("프래그먼트에서 보는 초기화 셋팅 스트림 현황 COMPLETE 모델쪽2: " + response2);

                                                                        this.initSetting.setValue(INITSETTING.COMPLETE);

                                                                    },
                                                                    error2 -> {
                                                                        LogUtil.d("로그인 스트림 subscribe error2: " + error);
                                                                        this.initSetting.setValue(INITSETTING.ERROR);
                                                                    },
                                                                    () -> {
                                                                    }
                                                            );
                                                },
                                                updateTokenError -> {
                                                    Toast.makeText(OrderAppApplication.mContext, "로그인 정보 초기화를 위해 재시작합니다.", Toast.LENGTH_SHORT).show();
                                                    this.initSetting.setValue(INITSETTING.ERROR);
                                                },
                                                () -> {
                                                }
                                        );
                            } else {
                                LogUtil.d("로그인 스트림 subscribe error: " + error);
                                this.initSetting.setValue(INITSETTING.ERROR);
                            }


                        },
                        () -> {
                        }
                ));

    }

    public void getLoginInfo(String branchId, String pw, String tableNo) {
        this.compositeSubscription.add(loginRepository.getLoginInfo(branchId, pw, tableNo)
                .subscribe(
                        response -> {
                            LogUtil.d("로그인 modelView response : " + response.getValue());

                            mBranchID = branchId;
                            mBranchPW = pw;
                            mBranchTableNo = tableNo;
                            loginInfo.setValue(response.getValue());

                        },
                        error -> {
                            LogUtil.d("로그인 modelView error : " + error.getStackTrace().toString());
                            loginInfo.setValue(null);

                        },
                        () -> {

                            LogUtil.d("로그인 modelView complete : ");

                        }
                ));
    }

    /**
     * token + tableNo 방식 로그인
     */

    public void getLoginInfo(String token, String tableNo) {
        this.compositeSubscription.add(loginRepository.getLoginInfo(token, tableNo)
                .subscribe(
                        response -> {
                            LogUtil.d("로그인 modelView response : " + response.getValue());
                            mBranchTableNo = tableNo;
                            TabletSettingUtils.getInstacne().setApiKey(token);
                            TabletSettingUtils.getInstacne().setTableNo(tableNo);
                            loginInfo.setValue(response.getValue());

                        },
                        error -> {
                            LogUtil.d("로그인 modelView error : " + error);
                            loginInfo.setValue(null);

                        },
                        () -> {

                            LogUtil.d("로그인 modelView complete : ");

                        }
                ));
    }


    public void saveDefaultLoginInfo(Context context) {
        LogUtil.d("로그인 성공 이후 로컬 저장 saveDefaultLoginInfo : " + mBranchID);
        LogUtil.d("로그인 성공 이후 로컬 저장 saveDefaultLoginInfo : " + mBranchPW);
        LogUtil.d("로그인 성공 이후 로컬 저장 saveDefaultLoginInfo : " + mBranchTableNo);

        FirebaseDatabase.getInstance().getReference()
                .child(loginInfo.getValue().branchInfo.fbBrandName)
                .child("tables")
                .child(loginInfo.getValue().branchInfo.branchId)
                .child(mBranchTableNo)
                .child("registrationToken").setValue(FirebaseInstanceId.getInstance().getToken().toString());

        TabletSettingUtils.getInstacne().setAccessToken(this.loginInfo.getValue().accessToken);
        TabletSettingUtils.getInstacne().setBranchInfo(this.loginInfo.getValue().branchInfo);
        this.getMainCategoryList(context);
    }



    public Observable<ArrayList<TableMap>> getAllMapInfo(String branchUid) {
        return this.loginRepository.getAllMapInfo(branchUid);

    }

    @Override
    protected void onCleared() {

        LogUtil.d("로그인 뷰모델  onCleared");
        if(this.compositeSubscription != null)
        {
            this.compositeSubscription.clear();
        }
        super.onCleared();
    }



    ////개편된 코드


    /**
     * token + tableNo 방식 로그인
     */

    public Observable<LoginData> getLoginInfoAsObservable(String token, String tableNo) {
        return (loginRepository.getLoginInfo(token, tableNo)
                .flatMap(response -> {
                    mBranchTableNo = tableNo;
                    TabletSettingUtils.getInstacne().setApiKey(token);
                    TabletSettingUtils.getInstacne().setTableNo(tableNo);


                    TabletSettingUtils.getInstacne().setAccessToken(response.getValue().accessToken);
                    TabletSettingUtils.getInstacne().setBranchInfo(response.getValue().branchInfo);



                    return Observable.just(response.getValue());

                }));
    }


    public Observable<Boolean> getMainCategoryListAsObservable(Context context) {

        return this.loginRepository.initSetting()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())

                .flatMap(response -> {
                    LogUtil.d("로그인 스트림 subscribe success1: " + response);

                    TabletSetting.getInstance().setOrderMainCategoryList(loginRepository.mainCategories);

                    for (String key : loginRepository.subCategories.keySet()) {
                        TabletSetting.getInstance().setOrderSubCategoryList(key, loginRepository.subCategories.get(key));
                    }
                    LogUtil.d("프래그먼트에서 보는 초기화 셋팅 스트림 현황 COMPLETE 모델쪽1: " + response);

                    return Observable.just(true);
                });
    }

//    //로그인 성공후 데이터 저장
//    public void saveDefaultLoginInfo(Context context) {
//        LogUtil.d("로그인 성공 이후 로컬 저장 saveDefaultLoginInfo : " + mBranchID);
//        LogUtil.d("로그인 성공 이후 로컬 저장 saveDefaultLoginInfo : " + mBranchPW);
//        LogUtil.d("로그인 성공 이후 로컬 저장 saveDefaultLoginInfo : " + mBranchTableNo);
//
//        FirebaseDatabase.getInstance().getReference()
//                .child(loginInfo.getValue().branchInfo.fbBrandName)
//                .child("tables")
//                .child(loginInfo.getValue().branchInfo.branchId)
//                .child(mBranchTableNo)
//                .child("registrationToken").setValue(FirebaseInstanceId.getInstance().getToken().toString());
//
//        this.getMainCategoryList(context);
//    }

}
