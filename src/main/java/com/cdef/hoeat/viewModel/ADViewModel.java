package com.cdef.hoeat.viewModel;

import androidx.lifecycle.MutableLiveData;
import android.content.Context;

import com.cdef.commonmodule.dataModel.ADActionData;
import com.cdef.commonmodule.dataModel.BannerDefaultData;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.Repository.ADRepository;
import com.cdef.hoeat.Networking.HoeatNetworkUtil;
import com.cdef.hoeat.BuildConfig;

import java.util.ArrayList;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class ADViewModel extends BaseViewModel {

    MutableLiveData<ADRepository> adRepository = new MutableLiveData<>();
    public MutableLiveData<ArrayList<BannerDefaultData>> bannerList = new MutableLiveData<>();
    public MutableLiveData<BannerDefaultData> randomBanner = new MutableLiveData<>();
    public MutableLiveData<BannerDefaultData> mSelectedBanner = new MutableLiveData<>();
    public MutableLiveData<Boolean> isRandomMode = new MutableLiveData<>();

    public ADViewModel() {

    }

    public void initRepository(Context context) {
        this.adRepository.setValue(new ADRepository(HoeatNetworkUtil.getInstance(context).getService()));
        this.isRandomMode.setValue(false);
    }


    /**
     * 배너 목록 불러오기
     *
     * @param members 인원수. 디폴트 2
     * @param sex     테이블 구성원 표시. male|female|both. 디폴트 both
     **/
    public void getAdList(int members, String sex, String section) {
        this.compositeSubscription.add(this.adRepository.getValue().getAdList(members, sex, section)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(BuildConfig.APIRequestRetryLimit)
                .subscribe(
                        response -> {
                            LogUtil.d("주문 modelView response : " + response);
                            this.bannerList.setValue(new ArrayList<>());
                            this.bannerList.setValue(response.getValue());
                        },
                        error -> {
//                            state.setValue(STATE.ERROR);
                            LogUtil.d("주문 modelView error : " + error);
                            this.bannerList.setValue(new ArrayList<>());
                        },
                        () -> {

//                            LogUtil.d("주문 modelView complete : ");
//                            state.setValue(STATE.COMPLETE);

                        }));
    }

    /**
     * 배너 목록 불러오기
     *
     * @param members 인원수. 디폴트 2
     * @param sex     테이블 구성원 표시. male|female|both. 디폴트 both
     **/
    public void getBannerList(int members, String sex) {
        this.compositeSubscription.add(this.adRepository.getValue().getBannerList(members, sex)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(BuildConfig.APIRequestRetryLimit)
                .subscribe(
                        response -> {
                            LogUtil.d("주문 modelView response : " + response);
                            this.bannerList.setValue(new ArrayList<>());
                            this.bannerList.setValue(response.getValue());
                        },
                        error -> {
//                            state.setValue(STATE.ERROR);
                            LogUtil.d("주문 modelView error : " + error);
                            this.bannerList.setValue(new ArrayList<>());
                        },
                        () -> {

//                            LogUtil.d("주문 modelView complete : ");
//                            state.setValue(STATE.COMPLETE);

                        }));
    }

    /**
     * 랜덤으로 배너 광고 하나 가져오기
     *
     * @param members 인원수. 디폴트 2
     * @param sex     테이블 구성원 표시. male|female|both. 디폴트 both
     * @param section 광고의 유형. event | screen | launcher
     **/
    public void getRandomBanner(String tableNo, int members, String sex, String section) {
        this.state.setValue(STATE.NONE);
        this.compositeSubscription.add(this.adRepository.getValue().getRandomBanner(tableNo, members, sex, section)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(BuildConfig.APIRequestRetryLimit)
                .subscribe(
                        response -> {
                            randomBanner.setValue(response.adInfo);
                            this.state.setValue(STATE.COMPLETE);

                        },
                        error -> {
                            randomBanner.setValue(null);

                            this.state.setValue(STATE.ERROR);
                        },
                        () -> {

                        }));

    }

    /**
     * 단일 배너를 조회할때 호출하는 API. 서버측 뷰수 집계를 위해 필요하다.
     *
     * @param uuid    광고UUID
     * @param tableNo 테이블번호
     * @param members 인원수. 디폴트 0
     * @param sex     테이블 구성원 표시. male|female|both. 디폴트 na
     **/

    public void getBannerDetail(String uuid, String tableNo, int members, String sex) {
        this.state.setValue(STATE.NONE);
        this.compositeSubscription.add(this.adRepository.getValue().getBannerDetail(uuid, tableNo, members, sex)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(BuildConfig.APIRequestRetryLimit)
                .subscribe(
                        response -> {
                            this.state.setValue(STATE.COMPLETE);
                        },
                        error -> {
                            this.state.setValue(STATE.ERROR);
                        },
                        () -> {

                        }));

    }


    public MutableLiveData<ADActionData> getADActionResult = new MutableLiveData<>();

    /**
     * 광고 참여 결과
     */
    public void getADAction(String uuid, String actionParam, String input, String tableNo, int members, String sex) {
        this.state.setValue(STATE.NONE);
        this.compositeSubscription.add(this.adRepository.getValue().getADAction(uuid, actionParam, input, tableNo, members, sex)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(BuildConfig.APIRequestRetryLimit)
                .subscribe(
                        response -> {
                            getADActionResult.setValue(response);
                            this.state.setValue(STATE.COMPLETE);

                        },
                        error -> {

                            this.state.setValue(STATE.ERROR);
                        },
                        () -> {

                        }));
    }
}
