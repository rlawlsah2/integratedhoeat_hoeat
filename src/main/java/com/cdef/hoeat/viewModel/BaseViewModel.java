package com.cdef.hoeat.viewModel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.cdef.commonmodule.utils.LogUtil;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class BaseViewModel extends ViewModel {

    public MutableLiveData<STATE> state = new MutableLiveData<>();
    public CompositeSubscription compositeSubscription = new CompositeSubscription();


    public enum STATE {
        NONE, LOADING, ERROR, COMPLETE
    }


    @Override
    protected void onCleared() {

        LogUtil.d(this + " 뷰모델  onCleared");
        if(this.compositeSubscription != null)
        {
            this.compositeSubscription.clear();
        }
        super.onCleared();
    }

}
