package com.cdef.hoeat.viewModel;

import androidx.lifecycle.MutableLiveData;
import android.content.Context;

import com.cdef.commonmodule.dataModel.firebaseDataSet.Order;
import com.cdef.commonmodule.Networking.Responses.TableInfoResponse;
import com.cdef.commonmodule.Repository.BillRepository;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.utils.OrderSort;
import com.cdef.hoeat.Networking.HoeatNetworkUtil;
import com.cdef.hoeat.utils.QueueUtils;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import com.cdef.hoeat.BuildConfig;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class BillViewModel extends BaseViewModel {
    private final int PageCount = 5;
    public MutableLiveData<ArrayList<Order>> mOrderList = new MutableLiveData<>();
    public MutableLiveData<List<Order>> mPagingOrderList = new MutableLiveData<>();
    public MutableLiveData<Integer> mOrderedTotalPrice = new MutableLiveData<>();
    public MutableLiveData<Integer> mOrderListPageIndex = new MutableLiveData<>();
    public MutableLiveData<Integer> mPersonCount = new MutableLiveData<>();

    public MutableLiveData<Integer> mTotalPage = new MutableLiveData<>();
    private MutableLiveData<BillRepository> billRepository = new MutableLiveData<>();

    OrderSort orderSort = new OrderSort();

    public BillViewModel() {
        mOrderedTotalPrice.setValue(0);
        mOrderListPageIndex.setValue(0);
        mTotalPage.setValue(0);
        mPersonCount.setValue(1);
    }


    public void initRepository(Context context) {
        this.billRepository.setValue(new BillRepository(HoeatNetworkUtil.getInstance(context).getService()));
    }


    private void getOrderList(int index) {
        if (mOrderList != null
                && mOrderList.getValue() != null
                && mOrderList.getValue().size() > 0) {
            ///주문내역이 최소 1개라는 이야기.
            ///page index에 맞춰 해당 count만큼 넘겨준다. 기본값 5
            int totalSize = mOrderList.getValue().size();
            mOrderListPageIndex.setValue(index);

            LogUtil.d("주문내역 가져오는 중간단계 index : " + index);
            LogUtil.d("주문내역 가져오는 중간단계 startIndex : " + (index * 5));
            LogUtil.d("주문내역 가져오는 중간단계 totalSize : " + totalSize);
            if ((index * 5) + 5 < totalSize) {
                mPagingOrderList.setValue(
                        mOrderList.getValue().subList((index * 5), (index * 5) + 5));
            } else {
                mPagingOrderList.setValue(
                        mOrderList.getValue().subList((index * 5), totalSize));
            }


        } else {
            mPagingOrderList.setValue(null);
        }

    }

    /**
     * 개편된 api 기반 계산서 조회
     */
    public void getOrderList(String tableNo) {


        this.state.setValue(STATE.LOADING);
        this.compositeSubscription.add(
                this.billRepository.getValue()
                        .getTableInfo(QueueUtils.getTableInfo(TabletSettingUtils.getInstacne().getBranchInfo().posIp, tableNo))
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(tableInfoResponse -> {

                            ArrayList<Order> tempList = new ArrayList<>();
                            int totalPrice = 0;
                            Gson gson = new Gson();
                            Order tmpOrder;

                            for (Map.Entry<String, JsonElement> item : tableInfoResponse.orders.entrySet()) {
                                LogUtil.d("주문내역 orders item : " + item.getKey());
                                tmpOrder = gson.fromJson(gson.toJsonTree(item.getValue()), Order.class);
                                totalPrice += (tmpOrder.price * tmpOrder.quantity);
                                tempList.add(tmpOrder);
                            }

                            Collections.sort(tempList, orderSort);

                            this.mOrderList.setValue(tempList);
                            this.mOrderedTotalPrice.setValue(totalPrice);
                            this.mTotalPage.setValue((int) ((tableInfoResponse.orders.entrySet().size() - 1) / PageCount));
                            getOrderList(mOrderListPageIndex.getValue());
                            TabletSettingUtils.getInstacne().setTableInfoData(tableInfoResponse.members);
                            this.state.setValue(STATE.COMPLETE);


                        }, throwable -> {
                            this.state.setValue(STATE.ERROR);
                            LogUtil.d("주문내역 가져올때 문제 발생 : " + throwable);
                            LogUtil.d("주문내역 가져올때 문제 발생 : " + throwable.getStackTrace().toString());

                        }));

    }


    /**
     * 개편된 api 기반 이전 주문내역 가져오기 Observable
     */
    public Observable<TableInfoResponse> getOrderedList(String tableNo) {


        this.state.setValue(STATE.LOADING);
        return this.billRepository.getValue().getTableInfo(QueueUtils.getTableInfo(TabletSettingUtils.getInstacne().getBranchInfo().posIp, tableNo))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(tableInfoResponse -> {
                    if (tableInfoResponse != null)
                        TabletSettingUtils.getInstacne().setOrderId(tableInfoResponse.orderId);

                    return Observable.just(tableInfoResponse);
                });

//                .subscribe(tableInfoResponse -> {
//
////                    LogUtil.d("주문내역 orders : " + tableInfoResponse.orders.entrySet());
//
//                    ArrayList<Order> tempList = new ArrayList<>();
//                    int totalPrice = 0;
//                    Gson gson = new Gson();
//                    Order tmpOrder;
//
//                    for (Map.Entry<String, JsonElement> item : tableInfoResponse.orders.entrySet()) {
//
//                        LogUtil.d("주문내역 orders item : " + item.getKey());
//                        tmpOrder = gson.fromJson(gson.toJsonTree(item.getValue()), Order.class);
//                        totalPrice += (tmpOrder.price * tmpOrder.quantity);
//                        tempList.add(tmpOrder);
//                    }
//
//                    Collections.sort(tempList, orderSort);
//
//                    this.mOrderList.setValue(tempList);
//                    this.mOrderedTotalPrice.setValue(totalPrice);
//                    this.mTotalPage.setValue((int) ((tableInfoResponse.orders.entrySet().size() - 1) / PageCount));
//                    getOrderList(mOrderListPageIndex.getValue());
//                    TabletSettingUtils.getInstacne().setTableInfoData(tableInfoResponse.members);
//                    this.state.setValue(STATE.COMPLETE);
//
//
//                }, throwable -> {
//                    this.state.setValue(STATE.ERROR);
//
//                });

    }


    /**
     * 개편된 api 기반 계산서 조회 (Observable로 리턴받기)
     */
    public Observable<Integer> getOrderListAsObservableTotalPrice(String tableNo) {


        this.state.setValue(STATE.LOADING);
        return this.billRepository.getValue()
                .getTableInfo(QueueUtils.getTableInfo(TabletSettingUtils.getInstacne().getBranchInfo().posIp, tableNo))
                .flatMap(tableInfoResponse -> {

//                    LogUtil.d("주문내역 orders : " + tableInfoResponse.orders.entrySet());
                    if (tableInfoResponse != null)
                        TabletSettingUtils.getInstacne().setOrderId(tableInfoResponse.orderId);
                    ArrayList<Order> tempList = new ArrayList<>();
                    int totalPrice = 0;
                    Gson gson = new Gson();
                    Order tmpOrder;

                    for (Map.Entry<String, JsonElement> item : tableInfoResponse.orders.entrySet()) {
                        tmpOrder = gson.fromJson(gson.toJsonTree(item.getValue()), Order.class);
                        totalPrice += (tmpOrder.price * tmpOrder.quantity);
                        tempList.add(tmpOrder);
                    }

                    Collections.sort(tempList, orderSort);

                    this.mOrderList.setValue(tempList);
                    this.mOrderedTotalPrice.setValue(totalPrice);
                    this.mTotalPage.setValue((int) ((tableInfoResponse.orders.entrySet().size() - 1) / PageCount));
                    getOrderList(mOrderListPageIndex.getValue());
                    TabletSettingUtils.getInstacne().setTableInfoData(tableInfoResponse.members);

//                    if (mOrderedTotalPrice.getValue() != 0)
//                        return Observable.just(true);
//                    else
//                        return Observable.just(false);

                    return Observable.just(mOrderedTotalPrice.getValue());

                });


    }


    public void goPage(int index) {
        getOrderList(index);
    }


    public void goPlus(boolean flag) {
        if (flag) {
            this.mPersonCount.setValue((this.mPersonCount.getValue() + 1));
        } else {
            if (this.mPersonCount.getValue() > 1) {
                this.mPersonCount.setValue((this.mPersonCount.getValue() - 1));
            }
        }
    }


    /**
     * 개편된 api 기반 계산서 조회
     */
    public Observable<Boolean> getOrderListAsObservable(String tableNo) {

        return this.billRepository.getValue()
                .getTableInfo(QueueUtils.getTableInfo(TabletSettingUtils.getInstacne().getBranchInfo().posIp, tableNo))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(error -> {
                    LogUtil.d("getOrderListAsObservable doOnError : " + error);
                    return Observable.just(null);
                })
                .flatMap(tableInfoResponse -> {
                    LogUtil.d("getOrderListAsObservable result : " + tableInfoResponse);
                    if (tableInfoResponse == null) {
                        return Observable.just(false);
                    } else {
                        ArrayList<Order> tempList = new ArrayList<>();
                        int totalPrice = 0;
                        Gson gson = new Gson();
                        Order tmpOrder;
                        try {

                            for (Map.Entry<String, JsonElement> item : tableInfoResponse.orders.entrySet()) {

                                LogUtil.d("주문내역 orders item : " + item.getKey());
                                tmpOrder = gson.fromJson(gson.toJsonTree(item.getValue()), Order.class);
                                totalPrice += (tmpOrder.price * tmpOrder.quantity);
                                tempList.add(tmpOrder);
                            }
                        }
                        catch (Exception e)
                        {

                        }

                        Collections.sort(tempList, orderSort);

                        this.mOrderList.setValue(tempList);
                        this.mOrderedTotalPrice.setValue(totalPrice);
                        this.mTotalPage.setValue((int) ((tableInfoResponse.orders.entrySet().size() - 1) / PageCount));
                        getOrderList(mOrderListPageIndex.getValue());
                        TabletSettingUtils.getInstacne().setTableInfoData(tableInfoResponse.members);
                        TabletSettingUtils.getInstacne().setOrderId(tableInfoResponse.orderId);

                        return Observable.just(true);
                    }
                });
    }
}
