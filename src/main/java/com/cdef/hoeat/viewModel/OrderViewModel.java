package com.cdef.hoeat.viewModel;

import androidx.lifecycle.MutableLiveData;
import android.content.Context;
import android.widget.Toast;

import com.cdef.commonmodule.Networking.Responses.OrderResponse;
import com.cdef.commonmodule.dataModel.MenuItemData;
import com.cdef.commonmodule.dataModel.MenuSideOptionItemData;
import com.cdef.commonmodule.Networking.Responses.LoginResponse;
import com.cdef.commonmodule.dataModel.OrderCategoryData;
import com.cdef.hoeat.BuildConfig;
import com.cdef.commonmodule.dataModel.firebaseDataSet.Notification;
import com.cdef.hoeat.Networking.HoeatNetworkUtil;
import com.cdef.hoeat.OrderAppApplication;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.hoeat.utils.QueueUtils;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.cdef.hoeat.view.dialog.MessageDialogBuilder;
import com.cdef.commonmodule.exceptions.LoginInfoNotFoundException;
import com.cdef.commonmodule.Repository.OrderRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class OrderViewModel extends BaseViewModel {


    public MutableLiveData<ArrayList<MenuItemData>> menuList = new MutableLiveData<>();
    private MutableLiveData<OrderRepository> orderRepository = new MutableLiveData<>();
    public MutableLiveData<ADDCARTPROFRESS> addCartProgress = new MutableLiveData<>();
    public MutableLiveData<STATE> state = new MutableLiveData<>();
    public MutableLiveData<QUEUESTATE> queueState = new MutableLiveData<>();
    public MutableLiveData<String> exTotalPrice = new MutableLiveData<>();

    public MutableLiveData<ArrayList<MenuItemData>> cartList = new MutableLiveData<>();


    int historyPrice;
//    POSOrderProccessor posOrderProccessor;

    public enum ADDCARTPROFRESS {
        NONE, LOADING, ERROR, COMPLETE
    }

    public enum QUEUESTATE {
        NONE, LOADING, ERROR, COMPLETE
    }


    public OrderViewModel() {
        isOrdering.setValue(false);
        isFirstOrder.setValue(true);
        orderState.setValue(ORDERSTATE.NONE);
        queueState.setValue(QUEUESTATE.NONE);


    }

    public void initRepository(Context context) {
        this.orderRepository.setValue(new OrderRepository(HoeatNetworkUtil.getInstance(context).getService()));
    }


    /**
     * 큐 상태 체크
     **/
    public synchronized void checkQueueState(String url) {
        queueState.setValue(QUEUESTATE.LOADING);
        this.compositeSubscription.add(this.orderRepository.getValue().checkQueueState(url)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        response -> {
                            if (response) {
                                queueState.setValue(QUEUESTATE.COMPLETE);
                            } else {
                                queueState.setValue(QUEUESTATE.ERROR);
                            }
                        },
                        error -> {
                            queueState.setValue(QUEUESTATE.ERROR);
                        },
                        () -> {

                        }));
    }

    /**
     * 큐 상태 체크
     **/
    public synchronized Observable<Boolean> checkQueueStateAsObservable(String url) {
        return this.orderRepository.getValue().checkQueueState(url)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
//                .flatMap(response -> {
//                    response
//
//                });
//                .subscribe(
//                        response -> {
//                            if (response) {
//                                queueState.setValue(QUEUESTATE.COMPLETE);
//                            } else {
//                                queueState.setValue(QUEUESTATE.ERROR);
//                            }
//                        },
//                        error -> {
//                            queueState.setValue(QUEUESTATE.ERROR);
//                        },
//                        () -> {
//
//                        }));
    }

    /**
     * 서비스 요청 보내기
     **/
    public void request(String url, Notification notification) {
        queueState.setValue(QUEUESTATE.LOADING);
        this.compositeSubscription.add(this.orderRepository.getValue().request(url, notification)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        response -> {
                            if (response) {
                                queueState.setValue(QUEUESTATE.COMPLETE);
                            } else {
                                queueState.setValue(QUEUESTATE.ERROR);
                            }
                        },
                        error -> {
                            queueState.setValue(QUEUESTATE.ERROR);
                            LogUtil.d("서비스 요청 에러 : " + error);

                        },
                        () -> {

                        }));
    }


    ///한개의 카테고리에 대해서 메뉴코드 가져오기
    public void getMenuList(Context context, String categoryId, String storeBranchUid) {
        state.setValue(STATE.LOADING);
        this.compositeSubscription.add(this.orderRepository.getValue().getMenuList(categoryId, storeBranchUid)
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                response -> {
                                    LogUtil.d("주문 modelView response1 : " + response);
                                    this.menuList.setValue(new ArrayList<>());
                                    this.menuList.setValue(response);
                                    state.setValue(STATE.COMPLETE);
                                },
                                error -> {
                                    if (error instanceof LoginInfoNotFoundException) {
                                        orderRepository.getValue().updateAccessToken(TabletSettingUtils.getInstacne().getApiKey(),
                                                TabletSettingUtils.getInstacne().getTableNo())
                                                .subscribeOn(Schedulers.computation())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .onErrorResumeNext(cannotLogin -> Observable.error(cannotLogin))
                                                .subscribe(
                                                        updateTokenSuccess -> {

                                                            TabletSettingUtils.getInstacne().setAccessToken(((LoginResponse) updateTokenSuccess).result.accessToken);
                                                            TabletSettingUtils.getInstacne().setBranchInfo(((LoginResponse) updateTokenSuccess).result.branchInfo);

                                                            this.orderRepository.getValue().getMenuList(categoryId, storeBranchUid)
                                                                    .subscribeOn(Schedulers.computation())
                                                                    .observeOn(AndroidSchedulers.mainThread())
                                                                    .subscribe(
                                                                            response2 -> {
                                                                                LogUtil.d("주문 modelView response2 : " + response2);
                                                                                this.menuList.setValue(new ArrayList<>());
                                                                                this.menuList.setValue(response2);
                                                                                state.setValue(STATE.COMPLETE);

                                                                            },
                                                                            error2 -> {
                                                                                LogUtil.d("주문 modelView error2 : " + error);
                                                                                this.menuList.setValue(null);
                                                                                state.setValue(STATE.ERROR);

                                                                            },
                                                                            () -> {

                                                                                LogUtil.d("주문 modelView complete2 : ");

                                                                            });
                                                        },
                                                        updateTokenError -> {
                                                            Toast.makeText(OrderAppApplication.mContext, "로그인 정보 초기화를 위해 재시작합니다.", Toast.LENGTH_SHORT).show();
                                                            state.setValue(STATE.ERROR);
                                                        },
                                                        () -> {
//                                                    state.setValue(STATE.ERROR);
                                                        }
                                                );
                                    } else {
                                        LogUtil.d("주문 modelView error1 : " + error);
                                        this.menuList.setValue(null);
                                        state.setValue(STATE.ERROR);
                                    }

                                },
                                () -> {

                                    LogUtil.d("주문 modelView complete1 : ");
//                            state.setValue(STATE.COMPLETE);

                                })
        );
    }

    //여러개의 카테고리에 대해서 메뉴코드 가져오기
    public Observable<ArrayList<MenuItemData>> getMenuLists(Context context, ArrayList<OrderCategoryData.CategoryItem> categoryItems, String storeBranchUid) {
        return Observable.from(categoryItems)
                .flatMap(categoryItem -> {
                    return this.orderRepository.getValue().getMenuList(categoryItem.categoryCode, storeBranchUid);

                })
                .flatMap(response -> {
                    return Observable.just(response);

                })
                .toList()
                .flatMap(menuList -> {
                    ArrayList<MenuItemData> tmp = new ArrayList<>();


                    for (ArrayList<MenuItemData> item : menuList) {
//                        int remain = tmp.size() % 3;
//                        for (int count = 0; count < remain; count++) {
//                            item.add(0,new MenuItemData(false, false));
//                        }
//                        item.add(0, new MenuItemData(false, true));
                        tmp.addAll(item);
                    }

                    return Observable.just(tmp);
                });

//
//                this.orderRepository.getValue().getMenuList(categoryId, storeBranchUid)
//                        .subscribeOn(Schedulers.computation())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .subscribe(
//                                response -> {
//                                    LogUtil.d("주문 modelView response1 : " + response.getValue());
//                                    this.menuList.setValue(new ArrayList<>());
//                                    this.menuList.setValue(response.getValue());
//                                    state.setValue(STATE.COMPLETE);
//                                },
//                                error -> {
//                                    if (error instanceof LoginInfoNotFoundException) {
//                                        orderRepository.getValue().updateAccessToken(TabletSettingUtils.getInstacne().getApiKey(),
//                                                TabletSettingUtils.getInstacne().getTableNo())
//                                                .subscribeOn(Schedulers.computation())
//                                                .observeOn(AndroidSchedulers.mainThread())
//                                                .onErrorResumeNext(cannotLogin -> Observable.error(cannotLogin))
//                                                .subscribe(
//                                                        updateTokenSuccess -> {
//
//                                                            TabletSettingUtils.getInstacne().setAccessToken(((LoginResponse) updateTokenSuccess).result.accessToken);
//                                                            TabletSettingUtils.getInstacne().setBranchInfo(((LoginResponse) updateTokenSuccess).result.branchInfo);
//
//                                                            this.orderRepository.getValue().getMenuList(categoryId, storeBranchUid)
//                                                                    .subscribeOn(Schedulers.computation())
//                                                                    .observeOn(AndroidSchedulers.mainThread())
//                                                                    .subscribe(
//                                                                            response2 -> {
//                                                                                LogUtil.d("주문 modelView response2 : " + response2.getValue());
//                                                                                this.menuList.setValue(new ArrayList<>());
//                                                                                this.menuList.setValue(response2.getValue());
//                                                                                state.setValue(STATE.COMPLETE);
//
//                                                                            },
//                                                                            error2 -> {
//                                                                                LogUtil.d("주문 modelView error2 : " + error);
//                                                                                this.menuList.setValue(null);
//                                                                                state.setValue(STATE.ERROR);
//
//                                                                            },
//                                                                            () -> {
//
//                                                                                LogUtil.d("주문 modelView complete2 : ");
//
//                                                                            });
//                                                        },
//                                                        updateTokenError -> {
//                                                            Toast.makeText(OrderAppApplication.mContext, "로그인 정보 초기화를 위해 재시작합니다.", Toast.LENGTH_SHORT).show();
//                                                            state.setValue(STATE.ERROR);
//                                                        },
//                                                        () -> {
////                                                    state.setValue(STATE.ERROR);
//                                                        }
//                                                );
//                                    } else {
//                                        LogUtil.d("주문 modelView error1 : " + error);
//                                        this.menuList.setValue(null);
//                                        state.setValue(STATE.ERROR);
//                                    }
//
//                                },
//                                () -> {
//
//                                    LogUtil.d("주문 modelView complete1 : ");
////                            state.setValue(STATE.COMPLETE);
//
//                                })
//        );
    }


    /**
     * 특수 메뉴 리스트 가져오기
     **/
    public void getMenuFlagship(Context context, String categoryId, String storeBranchUid) {
        state.setValue(STATE.LOADING);
        this.compositeSubscription.add(this.orderRepository.getValue().getMenuFlagship(categoryId, storeBranchUid)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(BuildConfig.APIRequestRetryLimit)
                .subscribe(
                        response -> {
                            state.setValue(STATE.COMPLETE);
                            LogUtil.d("주문 modelView response : " + response);
//                            this.menuList.setValue(new ArrayList<>());
                            this.menuList.setValue(response);
                        },
                        error -> {
                            if (error instanceof LoginInfoNotFoundException) {
                                orderRepository.getValue().updateAccessToken(TabletSettingUtils.getInstacne().getApiKey(),
                                        TabletSettingUtils.getInstacne().getTableNo())
                                        .subscribeOn(Schedulers.computation())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .onErrorResumeNext(cannotLogin -> Observable.error(cannotLogin))
                                        .subscribe(
                                                updateTokenSuccess -> {

                                                    TabletSettingUtils.getInstacne().setAccessToken(((LoginResponse) updateTokenSuccess).result.accessToken);
                                                    TabletSettingUtils.getInstacne().setBranchInfo(((LoginResponse) updateTokenSuccess).result.branchInfo);

                                                    this.orderRepository.getValue().getMenuFlagship(categoryId, storeBranchUid)
                                                            .subscribeOn(Schedulers.computation())
                                                            .observeOn(AndroidSchedulers.mainThread())
                                                            .retry(BuildConfig.APIRequestRetryLimit)
                                                            .subscribe(
                                                                    response2 -> {
                                                                        state.setValue(STATE.COMPLETE);
                                                                        LogUtil.d("주문 modelView response : " + response2);
                                                                        this.menuList.setValue(new ArrayList<>());
                                                                        this.menuList.setValue(response2);
                                                                    },
                                                                    error2 -> {
                                                                        state.setValue(STATE.ERROR);
                                                                        LogUtil.d("주문 modelView error : " + error);
                                                                        this.menuList.setValue(null);
                                                                    },
                                                                    () -> {

                                                                        LogUtil.d("주문 modelView complete : ");
                                                                        state.setValue(STATE.COMPLETE);

                                                                    });
                                                },
                                                updateTokenError -> {
                                                    Toast.makeText(OrderAppApplication.mContext, "로그인 정보 초기화를 위해 재시작합니다.", Toast.LENGTH_SHORT).show();
                                                    state.setValue(STATE.ERROR);
                                                },
                                                () -> {
                                                    state.setValue(STATE.ERROR);
                                                }
                                        );
                            } else {
                                state.setValue(STATE.ERROR);
                                LogUtil.d("주문 modelView error : " + error);
                                this.menuList.setValue(null);
                            }
                        },
                        () -> {

                            LogUtil.d("주문 modelView complete : ");
                            state.setValue(STATE.COMPLETE);

                        }));
    }

    public Observable<OrderResponse> goOrder(String url, String orderJson) {
        return this.orderRepository.getValue().order(url, orderJson)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());

    }

    /**
     * pos 주문 관련한 코드는 아래에
     **/
    MutableLiveData<Boolean> isFirstOrder = new MutableLiveData<>();    //첫 주문여부 판단
    MutableLiveData<Boolean> isOrdering = new MutableLiveData<>();

    public MutableLiveData<ORDERSTATE> orderState = new MutableLiveData<>();


    ///리뉴얼 된 주문 로직.
    public void orderToQueue(String tableNo, ArrayList<MenuItemData> orderList) {
        if (orderList == null || orderList.size() == 0) {
            orderState.setValue(ORDERSTATE.NOCART);
            return;
        }


        ///카트에 담긴 아이템들을 불러온다
        JSONObject orderObject = new JSONObject();
        JSONObject member = new JSONObject();

        try {


            orderObject.put("userId", tableNo);
            orderObject.put("brandName", TabletSettingUtils.getInstacne().getBranchInfo().fbBrandName);
            orderObject.put("branchId", TabletSettingUtils.getInstacne().getBranchInfo().branchId);
            orderObject.put("tableNo", tableNo);
            orderObject.put("orderType", "H");

            member.put("men", TabletSettingUtils.getInstacne().getTableInfoData().men);
            member.put("women", TabletSettingUtils.getInstacne().getTableInfoData().women);
            member.put("age", TabletSettingUtils.getInstacne().getTableInfoData().age);

            orderObject.put("members", member);


            int totalPrice = 0;

            JSONArray orders = new JSONArray();
            for (MenuItemData item : orderList) {
//                            Order item = snapshot.getValue(Order.class);
                JSONObject ordersItem = new JSONObject();   ///orders에 들어갈 각 항목
                JSONArray ordersItemSideOptions = new JSONArray();
                ordersItem.put("foodUid", item.foodUid);
                ordersItem.put("foodName", item.foodName);
                ordersItem.put("quantity", item.quantity);
                ordersItem.put("CMDT_CD", item.CMDTCD);
                ordersItem.put("categoryName", item.categoryName);

                int currentOptionPrice = 0; //현재 옵션 총 합산 가격
                ///옵션 여부 체크
                if (item.isSet == 1) {

                    for (MenuSideOptionItemData optionItem : item.selectedSideOptions) {
                        JSONObject ordersItemSideOptionItem = new JSONObject();   ///orders에 들어갈 각 항목의 옵션
                        ordersItemSideOptionItem.put("foodUid", optionItem.foodUid);
                        ordersItemSideOptionItem.put("foodName", optionItem.foodName);
                        ordersItemSideOptionItem.put("price", optionItem.price);
                        ordersItemSideOptionItem.put("quantity", optionItem.quantity);
                        ordersItemSideOptionItem.put("CMDT_CD", optionItem.CMDTCD);

                        currentOptionPrice += (optionItem.price * optionItem.quantity);
                        ordersItemSideOptions.put(ordersItemSideOptionItem);
                    }

                    if (ordersItemSideOptions.length() > 0) {
                        ordersItem.put("price", (currentOptionPrice + item.price));
                        ordersItem.put("options", ordersItemSideOptions);
                    }

                    ///해당 옵션 총 가격 * 수량
                    totalPrice += (currentOptionPrice + item.price) * item.quantity;
                } else {
                    ///해당 단품 가격 * 수량
                    totalPrice += item.price * item.quantity;
                    ordersItem.put("price", item.price);
                }

                orders.put(ordersItem);

            }

            orderObject.put("orders", orders);
            orderObject.put("totalPrice", totalPrice);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        LogUtil.d("주문 최종 내역 : " + orderObject);

        MessageDialogBuilder.getInstance(OrderAppApplication.mContext)
                .setContent("주문내역을 확인해주세요.\n주문후 취소할 수 없습니다.\n주문하시겠습니까?")
                .setOnDismissListener_(dialogInterface -> {
                })
                .setCancelButton("취소", view -> {
                    MessageDialogBuilder.getInstance(OrderAppApplication.mContext)
                            .dismiss();
                })
                .setOkButton("확인", view -> {
                    LogUtil.d("확인 버튼의 selected 상태값 : " + view.isSelected());

                    if (!view.isSelected()) {
                        view.setSelected(true);
                        orderState.setValue(ORDERSTATE.LOADING);
                        goOrder(QueueUtils.order(TabletSettingUtils.getInstacne().getBranchInfo().posIp), orderObject.toString())
                                .subscribe(
                                        response -> {
                                            orderState.setValue(ORDERSTATE.COMPLETE);


//                                            MessageDialogBuilder.getInstance(getContext()).dismiss();
//                                            if (MenuDetailDialog.getmInstance() != null) {
//                                                MenuDetailDialog.getmInstance().dismiss();
//                                            }
//                                            Toast.makeText(getContext(), "주문이 완료되었습니다", Toast.LENGTH_LONG).show();

                                        },
                                        error -> {
                                            orderState.setValue(ORDERSTATE.ERROR);

//                                            LogUtil.d("주문 modelView error : " + error);
//                                            loadingDialog.dismiss();
//                                            isOrdering.setValue(false);
//                                            MessageDialogBuilder.getInstance(getContext()).dismiss();
//                                            Toast.makeText(getContext(), "포스기 주문 프로그램을 실행해주세요.", Toast.LENGTH_SHORT).show();

                                        },
                                        () -> {

//                                            LogUtil.d("주문 modelView complete : ");
//                                            loadingDialog.dismiss();
//                                            isOrdering.setValue(false);
//                                            MessageDialogBuilder.getInstance(getContext()).dismiss();

                                        });
                    } else {
                        Toast.makeText(OrderAppApplication.mContext, "주문중입니다.", Toast.LENGTH_SHORT).show();
                    }
                })
                .complete();


    }

    ///리뉴얼 된 주문 로직.
    //결과 -2:취소, -1: 주문실패, 0: 주문엔 성공했으나 수기입력인경우?, 1: 주문성공
    public Observable<Integer> orderToQueueAsObservable(String tableNo, ArrayList<MenuItemData> orderList) {

        ///카트에 담긴 아이템들을 불러온다
        JSONObject orderObject = new JSONObject();
        JSONObject member = new JSONObject();

        try {
            orderObject.put("userId", tableNo);
            orderObject.put("brandName", TabletSettingUtils.getInstacne().getBranchInfo().fbBrandName);
            orderObject.put("branchId", TabletSettingUtils.getInstacne().getBranchInfo().branchId);
            orderObject.put("tableNo", tableNo);
            orderObject.put("orderType", "H");

            member.put("men", TabletSettingUtils.getInstacne().getTableInfoData().men);
            member.put("women", TabletSettingUtils.getInstacne().getTableInfoData().women);
            member.put("age", TabletSettingUtils.getInstacne().getTableInfoData().age);

            orderObject.put("members", member);


            int totalPrice = 0;

            JSONArray orders = new JSONArray();
            for (MenuItemData item : orderList) {
//                            Order item = snapshot.getValue(Order.class);
                JSONObject ordersItem = new JSONObject();   ///orders에 들어갈 각 항목
                JSONArray ordersItemSideOptions = new JSONArray();
                ordersItem.put("foodUid", item.foodUid);
                ordersItem.put("foodName", item.foodName);
                ordersItem.put("quantity", item.quantity);
                ordersItem.put("CMDT_CD", item.CMDTCD);
                ordersItem.put("categoryName", item.categoryName);

                int currentOptionPrice = 0; //현재 옵션 총 합산 가격
                ///옵션 여부 체크
                if (item.isSet == 1) {

                    for (MenuSideOptionItemData optionItem : item.selectedSideOptions) {
                        JSONObject ordersItemSideOptionItem = new JSONObject();   ///orders에 들어갈 각 항목의 옵션
                        ordersItemSideOptionItem.put("foodUid", optionItem.foodUid);
                        ordersItemSideOptionItem.put("foodName", optionItem.foodName);
                        ordersItemSideOptionItem.put("price", optionItem.price);
                        ordersItemSideOptionItem.put("quantity", optionItem.quantity);
                        ordersItemSideOptionItem.put("CMDT_CD", optionItem.CMDTCD);

                        currentOptionPrice += (optionItem.price * optionItem.quantity);
                        ordersItemSideOptions.put(ordersItemSideOptionItem);
                    }

                    if (ordersItemSideOptions.length() > 0) {
                        ordersItem.put("price", (currentOptionPrice + item.price));
                        ordersItem.put("options", ordersItemSideOptions);
                    }

                    ///해당 옵션 총 가격 * 수량
                    totalPrice += (currentOptionPrice + item.price) * item.quantity;
                } else {
                    ///해당 단품 가격 * 수량
                    totalPrice += item.price * item.quantity;
                    ordersItem.put("price", item.price);
                }

                orders.put(ordersItem);

            }

            orderObject.put("orders", orders);
            orderObject.put("totalPrice", totalPrice);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        LogUtil.d("주문 최종 내역 : " + orderObject);

        return Observable
                .create((Observable.OnSubscribe<Boolean>) subscriber ->
                        MessageDialogBuilder.getInstance(OrderAppApplication.mContext)
                                .setContent("주문내역을 확인해주세요.\n주문후 취소할 수 없습니다.\n주문하시겠습니까?")
                                .setCancelButton("취소", view -> {
                                    subscriber.onNext(false);
                                })
                                .setOkButton("확인", view -> {
                                    if (!view.isSelected()) {
                                        view.setSelected(true);
                                        subscriber.onNext(true);
                                    }
                                })
                                .complete())
                .flatMap(select -> {
                    MessageDialogBuilder.getmInstanceWithoutInit().dismiss();
                    if (!select) {
                        return Observable.just(-2);
                    } else {
                        this.state.setValue(STATE.LOADING);
                        return goOrder(QueueUtils.order(TabletSettingUtils.getInstacne().getBranchInfo().posIp), orderObject.toString())
                                .onErrorResumeNext(throwable -> Observable.just(null))
                                .flatMap(response -> {
                                    if (response == null) {
                                        this.state.setValue(STATE.COMPLETE);
                                        return Observable.just(-1);
                                    } else {

                                        this.state.setValue(STATE.COMPLETE);

                                        if(response.errCode != null)
                                        {
                                            if (response.errCode.equals("0")) {
                                                return Observable.just(1);
                                            } else {
                                                return Observable.just(0);
                                            }
                                        }
                                        else
                                        {
                                            if (response.orderId != null) {
                                                return Observable.just(1);
                                            } else {
                                                return Observable.just(0);
                                            }
                                        }

                                    }


                                });
                    }
                });


    }


    /**
     * orderForEvent
     **/
    public void orderForEvent(String tableNo, ArrayList<MenuItemData> orderList) {
        if (orderList == null || orderList.size() == 0) {
            orderState.setValue(ORDERSTATE.NOCART);
            return;
        }


        ///카트에 담긴 아이템들을 불러온다
        JSONObject orderObject = new JSONObject();
        JSONObject member = new JSONObject();

        try {


            orderObject.put("userId", tableNo);
            orderObject.put("brandName", TabletSettingUtils.getInstacne().getBranchInfo().fbBrandName);
            orderObject.put("branchId", TabletSettingUtils.getInstacne().getBranchInfo().branchId);
            orderObject.put("tableNo", tableNo);
            orderObject.put("orderType", "H");

            member.put("men", TabletSettingUtils.getInstacne().getTableInfoData().men);
            member.put("women", TabletSettingUtils.getInstacne().getTableInfoData().women);
            member.put("age", TabletSettingUtils.getInstacne().getTableInfoData().age);

            orderObject.put("members", member);


            int totalPrice = 0;

            JSONArray orders = new JSONArray();
            for (MenuItemData item : orderList) {
//                            Order item = snapshot.getValue(Order.class);
                JSONObject ordersItem = new JSONObject();   ///orders에 들어갈 각 항목
                JSONArray ordersItemSideOptions = new JSONArray();
                ordersItem.put("foodUid", item.foodUid);
                ordersItem.put("foodName", item.foodName);
                ordersItem.put("quantity", item.quantity);
                ordersItem.put("CMDT_CD", item.CMDTCD);
                ordersItem.put("categoryName", item.categoryName);

                int currentOptionPrice = 0; //현재 옵션 총 합산 가격
                ///옵션 여부 체크
                if (item.isSet == 1) {

                    for (MenuSideOptionItemData optionItem : item.selectedSideOptions) {
                        JSONObject ordersItemSideOptionItem = new JSONObject();   ///orders에 들어갈 각 항목의 옵션
                        ordersItemSideOptionItem.put("foodUid", optionItem.foodUid);
                        ordersItemSideOptionItem.put("foodName", optionItem.foodName);
                        ordersItemSideOptionItem.put("price", optionItem.price);
                        ordersItemSideOptionItem.put("quantity", optionItem.quantity);
                        ordersItemSideOptionItem.put("CMDT_CD", optionItem.CMDTCD);

                        currentOptionPrice += (optionItem.price * optionItem.quantity);
                        ordersItemSideOptions.put(ordersItemSideOptionItem);
                    }

                    if (ordersItemSideOptions.length() > 0) {
                        ordersItem.put("price", currentOptionPrice);
                        ordersItem.put("options", ordersItemSideOptions);
                    }
                    ///해당 옵션 총 가격 * 수량
                    totalPrice += currentOptionPrice * item.quantity;
                } else {
                    ///해당 단품 가격 * 수량
                    totalPrice += item.price * item.quantity;
                    ordersItem.put("price", item.price);
                }

                orders.put(ordersItem);

            }

            orderObject.put("orders", orders);
            orderObject.put("totalPrice", totalPrice);


        } catch (JSONException e) {
            e.printStackTrace();
        }


        LogUtil.d("주문 최종 내역 : " + orderObject);

        orderState.setValue(ORDERSTATE.LOADING);
        goOrder(QueueUtils.order(TabletSettingUtils.getInstacne().getBranchInfo().posIp), orderObject.toString())
                .subscribe(
                        response -> {
                            orderState.setValue(ORDERSTATE.COMPLETE);


//                                            MessageDialogBuilder.getInstance(getContext()).dismiss();
//                                            if (MenuDetailDialog.getmInstance() != null) {
//                                                MenuDetailDialog.getmInstance().dismiss();
//                                            }
//                                            Toast.makeText(getContext(), "주문이 완료되었습니다", Toast.LENGTH_LONG).show();

                        },
                        error -> {
                            orderState.setValue(ORDERSTATE.ERROR);

//                                            LogUtil.d("주문 modelView error : " + error);
//                                            loadingDialog.dismiss();
//                                            isOrdering.setValue(false);
//                                            MessageDialogBuilder.getInstance(getContext()).dismiss();
//                                            Toast.makeText(getContext(), "포스기 주문 프로그램을 실행해주세요.", Toast.LENGTH_SHORT).show();

                        },
                        () -> {

//                                            LogUtil.d("주문 modelView complete : ");
//                                            loadingDialog.dismiss();
//                                            isOrdering.setValue(false);
//                                            MessageDialogBuilder.getInstance(getContext()).dismiss();

                        });


    }


    /**
     * cart를 이용해서
     **/
//    public void OrderPos() {
//
//        this.isFirstOrder.setValue(true);
//
//        if (!isOrdering.getValue())  //주문중이 아닐경우
//        {
//            isOrdering.setValue(true);
//            FirebaseDatabase.getInstance().getReference().child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                    .child("tables")
//                    .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                    .child(TabletSettingUtils.getInstacne().getTableNo())
//                    .child("cart")
//                    .addListenerForSingleValueEvent(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(DataSnapshot cartResult) {
//
//                            if (cartResult != null && cartResult.getChildrenCount() > 0) {
//                                MessageDialogBuilder.getInstance(OrderAppApplication.mContext)
//                                        .setContent("주문내역을 확인해주세요.\n주문후 취소할 수 없습니다.\n주문하시겠습니까?")
//                                        .setOnDismissListener_(dialogInterface -> {
//                                            isOrdering.setValue(false);
//                                        })
//                                        .setCancelButton("취소", view -> {
//                                            MessageDialogBuilder.getInstance(OrderAppApplication.mContext)
//                                                    .dismiss();
////                                            loadingDialog.dismiss();
//                                            orderState.setValue(ORDERSTATE.NONE);
//                                            isOrdering.setValue(false);
//                                        })
//                                        .setOkButton("확인", view -> {
//                                            LogUtil.d("확인 버튼의 selected 상태값 : " + view.isSelected());
//
//                                            if (!view.isSelected()) {
//                                                view.setSelected(true);
//                                                isOrdering.setValue(true);
//                                                orderState.setValue(ORDERSTATE.LOADING);
//                                                ////IFSA_ORDER_ID 확인
//                                                FirebaseDatabase.getInstance().getReference().child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                        .child("tables")
//                                                        .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                        .child(TabletSettingUtils.getInstacne().getTableNo())
//                                                        .child("IFSA_ORDER_ID")
//                                                        .addListenerForSingleValueEvent(new ValueEventListener() {
//                                                            @Override
//                                                            public void onDataChange(DataSnapshot orderId) {
//                                                                ///IFSA_ORDER_ID 을 확인해보자.
//                                                                String IFSA_ORDER_ID = null;
//                                                                if (orderId != null) {
//                                                                    IFSA_ORDER_ID = orderId.getValue(String.class);
//
//                                                                    if (IFSA_ORDER_ID != null) {
//                                                                        isFirstOrder.setValue(false);
//                                                                        TabletSettingUtils.getInstacne().setOrderId(IFSA_ORDER_ID);
//                                                                    } else //비워져 있다는건 어찌되었든 파베 데이터에 없다는 뜻. 새로운 값을 넣어라
//                                                                    {
//
//                                                                        isFirstOrder.setValue(true);
//
//                                                                        TabletSettingUtils.getInstacne().setOrderId(
//                                                                                Utils.initOrderID(
//                                                                                        TabletSettingUtils.getInstacne().getBranchInfo().storeBranchUid
//                                                                                        , TabletSettingUtils.getInstacne().getTableNo()
//                                                                                )
//                                                                        );
//                                                                    }
//                                                                } else    //비워져 있다는건 테이블이 초기화 되었다는 뜻. 새로운 값을 넣어라
//                                                                {
//                                                                    isFirstOrder.setValue(true);
//                                                                    TabletSettingUtils.getInstacne().setOrderId(
//                                                                            Utils.initOrderID(
//                                                                                    TabletSettingUtils.getInstacne().getBranchInfo().storeBranchUid
//                                                                                    , TabletSettingUtils.getInstacne().getTableNo()
//                                                                            )
//                                                                    );
//                                                                }
//                                                                IFSA_ORDER_ID = TabletSettingUtils.getInstacne().getOrderId();
//
//                                                                Map<String, Object> order = new HashMap<>();    //주문을 담을 객체
//                                                                ///1. IFSA_ORDER_ID를 table 밑에 저장
//                                                                order.put(
//
//                                                                        FBPathBuilder.getInstance()
//                                                                                .init()
//                                                                                .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                                                .set("tables")
//                                                                                .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                                                .set(TabletSettingUtils.getInstacne().getTableNo())
//                                                                                .set("IFSA_ORDER_ID")
//                                                                                .complete()
//                                                                        , IFSA_ORDER_ID);
//
//                                                                ///2. 주문내역 집어넣기
////                                                        Map<String, Object> orderList = new HashMap<>();
//                                                                Order tmpOrder;
//                                                                String key;
//                                                                String timeStamp = Utils.getDate("yy-MM-dd HH:mm:ss");
//                                                                for (DataSnapshot item : cartResult.getChildren()) {
//                                                                    tmpOrder = item.getValue(Order.class);
//                                                                    tmpOrder.time = timeStamp;
//                                                                    key = FirebaseDatabase.getInstance().getReference()
//                                                                            .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                                            .child("orderData")
//                                                                            .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                                            .child(IFSA_ORDER_ID)
//                                                                            .child("orders")
//                                                                            .push().getKey();
//                                                                    tmpOrder.orderKey = key;
//                                                                    order.put(
//                                                                            FBPathBuilder.getInstance()
//                                                                                    .init()
//                                                                                    .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                                                    .set("orderData")
//                                                                                    .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                                                    .set(IFSA_ORDER_ID)
//                                                                                    .set("orders")
//                                                                                    .set(key)
//                                                                                    .complete()
//
//                                                                            , tmpOrder
//                                                                    );
//
//                                                                    ///2. orderForKitchen 주문넣기
//                                                                    if (tmpOrder.isKitchen == 1) {
//                                                                        tmpOrder.orderId = IFSA_ORDER_ID;
//                                                                        order.put(
//                                                                                FBPathBuilder.getInstance()
//                                                                                        .init()
//                                                                                        .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                                                        .set("orderForKitchen")
//                                                                                        .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                                                        .set(key)
//                                                                                        .complete()
//                                                                                , tmpOrder
//                                                                        );
//                                                                    }
//                                                                }
//
//
//                                                                ///(2) 주문코드
//                                                                order.put(
//                                                                        FBPathBuilder.getInstance()
//                                                                                .init()
//                                                                                .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                                                .set("orderData")
//                                                                                .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                                                .set(IFSA_ORDER_ID)
//                                                                                .set("tableNo")
//                                                                                .complete()
//                                                                        ,
//                                                                        TabletSettingUtils.getInstacne().getTableNo()
//                                                                );
//
//                                                                ///3. 노티피케이션 데이터
//                                                                int totalPrice_pos = 0;
//                                                                HashMap<String, Order> map = new HashMap<>();
//                                                                Notification notification = new Notification();
//                                                                notification.content = "/--";
//                                                                notification.read = false;
//                                                                notification.tableNo = TabletSettingUtils.getInstacne().getTableNo();
//                                                                notification.title = OrderAppApplication.mContext.getString(R.string.notificationTitleOrder);
//                                                                notification.type = OrderAppApplication.mContext.getString(R.string.notificationTypeOrder);
//                                                                notification.time = Utils.getDate("yyyy-MM-dd HH:mm:ss");
//
//                                                                for (DataSnapshot item : cartResult.getChildren()) {
//                                                                    Order cart = item.getValue(Order.class);
//                                                                    notification.content = notification.content + "/ " + cart.foodName + ": " + cart.quantity + "  ";
//                                                                    totalPrice_pos += (cart.price * cart.quantity);
//                                                                    map.put(item.getKey(), cart);
//                                                                }
//                                                                if (notification.content != null) {
//                                                                    notification.content = notification.content.replace("/--/", "");
//                                                                }
//                                                                order.put(
//                                                                        FBPathBuilder.getInstance()
//                                                                                .init()
//                                                                                .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                                                .set("notification")
//                                                                                .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                                                .set(FirebaseDatabase.getInstance().getReference().child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                                                        .child("notification")
//                                                                                        .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                                                        .push().getKey())
//                                                                                .complete()
//                                                                        , notification
//                                                                );
//
//                                                                ///이후는 주문 성공시 돌아갈 항목들
//                                                                //4.카트삭제
//                                                                order.put(
//                                                                        FBPathBuilder.getInstance()
//                                                                                .init()
//                                                                                .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                                                .set("tables")
//                                                                                .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                                                .set(TabletSettingUtils.getInstacne().getTableNo())
//                                                                                .set("cart")
//                                                                                .complete()
//                                                                        ,
//                                                                        null
//                                                                );
//                                                                int finalTotalPrice_pos = totalPrice_pos;
//                                                                FirebaseDatabase.getInstance().getReference()
//                                                                        .updateChildren(order, (databaseError, databaseReference) -> {
//                                                                            LogUtil.d("주문이 성공했는가 : " + databaseError);
//                                                                            if (databaseError == null) {
//
//                                                                                MSSQLUtils myMssqlUtils = MSSQLUtils.getInstance(OrderAppApplication.mContext, TabletSettingUtils.getInstacne().getTableNo(), TabletSettingUtils.getInstacne().getOrderId());
//
//
//                                                                                String tableNo = TabletSettingUtils.getInstacne().getSTBL_CD();
//                                                                                if (tableNo == null) {
//                                                                                    tableNo = myMssqlUtils.getUserID();
//                                                                                    if (tableNo != null) {
//                                                                                        TabletSettingUtils.getInstacne().setSTBL_CD(tableNo);
//                                                                                    }
//                                                                                }
//                                                                                if (tableNo != null && !tableNo.startsWith("[error]")) {
//                                                                                    POSOrderProccessor tmp = new POSOrderProccessor(myMssqlUtils, finalTotalPrice_pos, map, notification, null);
//                                                                                    tmp.execute();
//
//                                                                                } else {
//                                                                                    ///노티 띄워라.
//                                                                                    notification.title = "[수기 입력] 메뉴 추가";
//                                                                                    Extra extra = new Extra();
//
//                                                                                    extra.reference = "androidApp";
//                                                                                    extra.reason = "포스에서 테이블번호와 매칭되는 코드를 찾을 수 없는경우입니다.";
//                                                                                    extra.orderId = TabletSettingUtils.getInstacne().getOrderId();
//                                                                                    notification.extra = extra;
//                                                                                    notification.type = "order_danger";
//
//                                                                                    FirebaseDatabase.getInstance()
//                                                                                            .getReference()
//                                                                                            .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                                                            .child("notification")
//                                                                                            .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                                                            .push()
//                                                                                            .setValue(notification);
//                                                                                    initHistoryPrice(TabletSettingUtils.getInstacne().getBranchInfo().branchId
//                                                                                            , TabletSettingUtils.getInstacne().getTableNo());
//
//                                                                                    orderState.setValue(ORDERSTATE.COMPLETEWITHERROR2);
//                                                                                    isOrdering.setValue(false);
//                                                                                }
//
//
//                                                                            } else {
////                                                                                Toast.makeText(OrderAppApplication.mContext, "일시적인 오류가 발생했습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
//                                                                                orderState.setValue(ORDERSTATE.ERROR);
//                                                                                isOrdering.setValue(false);
//                                                                            }
//                                                                        });
//
//
//                                                            }
//
//                                                            @Override
//                                                            public void onCancelled(DatabaseError databaseError) {
////                                                                Toast.makeText(OrderAppApplication.mContext, "일시적인 오류가 발생했습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
//                                                                orderState.setValue(ORDERSTATE.ERROR);
//                                                                isOrdering.setValue(false);
//                                                            }
//                                                        });
//
//                                            } else {
////                                                Toast.makeText(OrderAppApplication.mContext, "주문중입니다.", Toast.LENGTH_SHORT).show();
//                                            }
//                                        })
//                                        .complete();
//                            } else {
////                                Toast.makeText(OrderAppApplication.mContext, "주문할 메뉴를 선택해주세요.", Toast.LENGTH_SHORT).show();
//                                orderState.setValue(ORDERSTATE.NONE);
//                                isOrdering.setValue(false);
//                            }
//
//
//                        }
//
//
//                        @Override
//                        public void onCancelled(DatabaseError databaseError) {
//
//                        }
//                    });
//
//        } else {
//            orderState.setValue(ORDERSTATE.LOADING);  //이미 주문중
//        }
//    }
//
//
//    /**
//     * cart를 이용하지 않고 단일 주문 넣기
//     **/
//    public void OrderDirectPos(Context context, String selectedMenuCategoryName, MenuItemData selectedMenu, String myTableNo) {
//
//        this.isFirstOrder.setValue(true);
//
//        if (!isOrdering.getValue())  //주문중이 아닐경우
//        {
//            isOrdering.setValue(true);
//
//
//            queueState.setValue(QUEUESTATE.LOADING);
//            this.orderRepository.getValue().checkQueueState(QueueUtils.checkQueueState(TabletSettingUtils.getInstacne().getBranchInfo().posIp))
//                    .subscribeOn(Schedulers.computation())
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribe(
//                            response -> {
//                                if (response) {
//                                    MessageDialogBuilder.getInstance(OrderAppApplication.mContext)
//                                            .setContentWithHtmlTag(
//                                                    "<font color='#ff0000'><b>" + selectedMenu.foodName + " " + selectedMenu.quantity + "개" + "</b></font>"
//                                                            + "을(를) 주문하시겠습니까?")
//                                            .setOnDismissListener_(dialogInterface -> {
//                                                isOrdering.setValue(false);
//                                            })
//                                            .setCancelButton("취소", view -> {
//                                                MessageDialogBuilder.getInstance(OrderAppApplication.mContext)
//                                                        .dismiss();
////                                            loadingDialog.dismiss();
//                                                orderState.setValue(ORDERSTATE.NONE);
//                                                isOrdering.setValue(false);
//                                            })
//                                            .setOkButton("확인", view -> {
//                                                LogUtil.d("확인 버튼의 selected 상태값 : " + view.isSelected());
//
//                                                if (!view.isSelected()) {
//                                                    view.setSelected(true);
//                                                    isOrdering.setValue(true);
//                                                    orderState.setValue(ORDERSTATE.LOADING);
//
//
//                                                    ///카트에 담긴 아이템들을 불러온다
//                                                    JSONObject orderObject = new JSONObject();
//                                                    JSONObject member = new JSONObject();
//                                                    int totalPrice_pos = 0;
//
//                                                    try {
//
//
//                                                        orderObject.put("userId", TabletSettingUtils.getInstacne().getTableNo());
//                                                        orderObject.put("brandName", TabletSettingUtils.getInstacne().getBranchInfo().fbBrandName);
//                                                        orderObject.put("branchId", TabletSettingUtils.getInstacne().getBranchInfo().branchId);
//                                                        orderObject.put("tableNo", TabletSettingUtils.getInstacne().getTableNo());
//                                                        orderObject.put("orderType", "H");
//
//                                                        member.put("men", TabletSettingUtils.getInstacne().getTableInfoData().men);
//                                                        member.put("women", TabletSettingUtils.getInstacne().getTableInfoData().women);
//                                                        member.put("age", TabletSettingUtils.getInstacne().getTableInfoData().age);
//                                                        orderObject.put("members", member);
//                                                        totalPrice_pos = selectedMenu.price * selectedMenu.quantity;
//
//                                                        JSONArray orders = new JSONArray();
//                                                        JSONObject orderItem = new JSONObject();
//                                                        orderItem.put("foodUid", selectedMenu.foodUid);
//                                                        orderItem.put("foodName", selectedMenu.foodName);
//                                                        orderItem.put("quantity", selectedMenu.quantity);
//                                                        orderItem.put("price", selectedMenu.price);
//                                                        orderItem.put("CMDT_CD", selectedMenu.CMDTCD);
//                                                        orderItem.put("categoryName", selectedMenuCategoryName);
//                                                        orders.put(orderItem);
//                                                        orderObject.put("orders", orders);
//                                                        orderObject.put("totalPrice", totalPrice_pos);
//
//                                                    } catch (JSONException e) {
//                                                        e.printStackTrace();
//                                                    }
//
//
//                                                    goOrder(QueueUtils.order(TabletSettingUtils.getInstacne().getBranchInfo().posIp), orderObject.toString())
//                                                            .subscribe(
//                                                                    response2 -> {
//                                                                        LogUtil.d("주문 goOrder response : " + response2);
//                                                                        Toast.makeText(context, "주문이 완료되었습니다", Toast.LENGTH_LONG).show();
//                                                                        queueState.setValue(QUEUESTATE.NONE);
//                                                                        isOrdering.setValue(false);
//                                                                        MessageDialogBuilder.getInstance(context).dismiss();
//
//                                                                    },
//                                                                    error -> {
//                                                                        LogUtil.d("주문 modelView error : " + error);
//                                                                        queueState.setValue(QUEUESTATE.ERROR);
//                                                                        isOrdering.setValue(false);
//                                                                        MessageDialogBuilder.getInstance(context).dismiss();
//
//
//                                                                    },
//                                                                    () -> {
//                                                                        LogUtil.d("주문 modelView complete : ");
//
//                                                                    });
//
//
//                                                } else {
////                                                Toast.makeText(OrderAppApplication.mContext, "주문중입니다.", Toast.LENGTH_SHORT).show();
//                                                }
//                                            })
//                                            .complete();
//
//                                } else {
//                                    queueState.setValue(QUEUESTATE.ERROR);
//                                }
//                            },
//                            error -> {
//                                queueState.setValue(QUEUESTATE.ERROR);
//                            },
//                            () -> {
//
//                            });
//
//
//        } else {
//            orderState.setValue(ORDERSTATE.LOADING);  //이미 주문중
//        }
//    }


    /**
     * cart를 이용하지 않고 다른이에게 선물하기
     * (1) 내꺼에 집어 넣기
     **/
//    public void presentOrderPos1(MenuItemData selectedMenu, String myTableNo, String otherTableNo) {
//
//        this.isFirstOrder.setValue(true);
//
//        if (!isOrdering.getValue())  //주문중이 아닐경우
//        {
//            isOrdering.setValue(true);
//
//            MessageDialogBuilder.getInstance(OrderAppApplication.mContext)
//                    .setContent(otherTableNo + " 테이블로 [" + selectedMenu.foodName + "] 를 선물하시겠습니까?")
//                    .setOnDismissListener_(dialogInterface -> {
//                        isOrdering.setValue(false);
//                    })
//                    .setCancelButton("취소", view -> {
//                        MessageDialogBuilder.getInstance(OrderAppApplication.mContext)
//                                .dismiss();
////                                            loadingDialog.dismiss();
//                        orderState.setValue(ORDERSTATE.NONE);
//                        isOrdering.setValue(false);
//                    })
//                    .setOkButton("확인", view -> {
//                        LogUtil.d("확인 버튼의 selected 상태값 : " + view.isSelected());
//
//                        if (!view.isSelected()) {
//                            view.setSelected(true);
//                            isOrdering.setValue(true);
//                            orderState.setValue(ORDERSTATE.LOADING);
//                            ////IFSA_ORDER_ID 확인
//                            FirebaseDatabase.getInstance().getReference().child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                    .child("tables")
//                                    .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                    .child(myTableNo)
//                                    .child("IFSA_ORDER_ID")
//                                    .addListenerForSingleValueEvent(new ValueEventListener() {
//                                        @Override
//                                        public void onDataChange(DataSnapshot orderId) {
//                                            ///IFSA_ORDER_ID 을 확인해보자.
//                                            String IFSA_ORDER_ID = null;
//                                            if (orderId != null) {
//                                                IFSA_ORDER_ID = orderId.getValue(String.class);
//
//                                                if (IFSA_ORDER_ID != null) {
//                                                    isFirstOrder.setValue(false);
////                                                    TabletSettingUtils.getInstacne().setOrderId(IFSA_ORDER_ID);
//                                                } else //비워져 있다는건 어찌되었든 파베 데이터에 없다는 뜻. 새로운 값을 넣어라
//                                                {
//
//                                                    isFirstOrder.setValue(true);
//                                                    IFSA_ORDER_ID = Utils.initOrderID(
//                                                            TabletSettingUtils.getInstacne().getBranchInfo().storeBranchUid
//                                                            , myTableNo);
//
////                                                    TabletSettingUtils.getInstacne().setOrderId(
////                                                            Utils.initOrderID(
////                                                                    TabletSettingUtils.getInstacne().getBranchInfo().storeBranchUid
////                                                                    , TabletSettingUtils.getInstacne().getTableNo()
////                                                            )
////                                                    );
//                                                }
//                                            } else    //비워져 있다는건 테이블이 초기화 되었다는 뜻. 새로운 값을 넣어라
//                                            {
//                                                isFirstOrder.setValue(true);
//
//                                                IFSA_ORDER_ID = Utils.initOrderID(
//                                                        TabletSettingUtils.getInstacne().getBranchInfo().storeBranchUid
//                                                        , myTableNo);
//                                            }
////                                            IFSA_ORDER_ID = TabletSettingUtils.getInstacne().getOrderId();
//
//                                            Map<String, Object> order = new HashMap<>();    //주문을 담을 객체
//                                            ///1. IFSA_ORDER_ID를 table 밑에 저장
//                                            order.put(
//
//                                                    FBPathBuilder.getInstance()
//                                                            .init()
//                                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                            .set("tables")
//                                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                            .set(myTableNo)
//                                                            .set("IFSA_ORDER_ID")
//                                                            .complete()
//                                                    , IFSA_ORDER_ID);
//
//                                            ///2. 주문내역 집어넣기
////                                                        Map<String, Object> orderList = new HashMap<>();
//
////                                                        Map<String, Object> orderList = new HashMap<>();
//                                            Order tmpOrder = new Order();
//                                            String key;
//                                            String timeStamp = Utils.getDate("yy-MM-dd HH:mm:ss");
////                                            for (DataSnapshot item : cartResult.getChildren()) {
////                                                tmpOrder = item.getValue(Order.class);
//                                            tmpOrder.time = timeStamp;
//                                            key = FirebaseDatabase.getInstance().getReference()
//                                                    .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                    .child("orderData")
//                                                    .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                    .child(IFSA_ORDER_ID)
//                                                    .child("orders")
//                                                    .push().getKey();
//                                            tmpOrder.orderKey = key;
//                                            tmpOrder.foodName = selectedMenu.foodName + "(선물하기)";
//                                            tmpOrder.quantity = 1;
//                                            tmpOrder.price = selectedMenu.price;
//                                            tmpOrder.CMDTCD = "PRESENT";//selectedMenu.CMDTCD;
//
//                                            order.put(
//                                                    FBPathBuilder.getInstance()
//                                                            .init()
//                                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                            .set("orderData")
//                                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                            .set(IFSA_ORDER_ID)
//                                                            .set("orders")
//                                                            .set(key)
//                                                            .complete()
//
//                                                    , tmpOrder
//                                            );
//
//                                            ///2. orderForKitchen 주문넣기
//                                            if (tmpOrder.isKitchen == 1) {
//                                                tmpOrder.orderId = IFSA_ORDER_ID;
//                                                order.put(
//                                                        FBPathBuilder.getInstance()
//                                                                .init()
//                                                                .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                                .set("orderForKitchen")
//                                                                .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                                .set(key)
//                                                                .complete()
//                                                        , tmpOrder
//                                                );
//                                            }
////                                            }
//
//
//                                            ///(2) 주문코드
//                                            order.put(
//                                                    FBPathBuilder.getInstance()
//                                                            .init()
//                                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                            .set("orderData")
//                                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                            .set(IFSA_ORDER_ID)
//                                                            .set("tableNo")
//                                                            .complete()
//                                                    ,
//                                                    myTableNo
//                                            );
//
//                                            ///3. 노티피케이션 데이터
//                                            int totalPrice_pos = 0;
//                                            HashMap<String, Order> map = new HashMap<>();
//                                            Notification notification = new Notification();
//                                            notification.content = "[선물하기] " + selectedMenu.foodName + "를 " + otherTableNo + "에게 선물하였습니다.";
//                                            notification.read = false;
//                                            notification.tableNo = TabletSettingUtils.getInstacne().getTableNo();
//                                            notification.title = OrderAppApplication.mContext.getString(R.string.notificationTitleOrder);
//                                            notification.type = OrderAppApplication.mContext.getString(R.string.notificationTypeOrder);
//                                            notification.time = Utils.getDate("yyyy-MM-dd HH:mm:ss");
//                                            totalPrice_pos = selectedMenu.price;
//
//
//                                            map.put(FirebaseDatabase.getInstance().getReference().push().getKey(), tmpOrder);
//
//
//                                            order.put(
//                                                    FBPathBuilder.getInstance()
//                                                            .init()
//                                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                            .set("notification")
//                                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                            .set(FirebaseDatabase.getInstance().getReference().child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                                    .child("notification")
//                                                                    .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                                    .push().getKey())
//                                                            .complete()
//                                                    , notification
//                                            );
//
//                                            ///이후는 주문 성공시 돌아갈 항목들
//                                            //4.카트삭제
////                                            order.put(
////                                                    FBPathBuilder.getInstance()
////                                                            .init()
////                                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
////                                                            .set("tables")
////                                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
////                                                            .set(TabletSettingUtils.getInstacne().getTableNo())
////                                                            .set("cart")
////                                                            .complete()
////                                                    ,
////                                                    null
////                                            );
//                                            int finalTotalPrice_pos = totalPrice_pos;
//                                            String finalIFSA_ORDER_ID = IFSA_ORDER_ID;
//                                            FirebaseDatabase.getInstance().getReference()
//                                                    .updateChildren(order, (databaseError, databaseReference) -> {
//                                                        LogUtil.d("주문이 성공했는가 : " + databaseError);
//                                                        if (databaseError == null) {
//
//                                                            MSSQLUtils myMssqlUtils = MSSQLUtils.getInstance(OrderAppApplication.mContext, myTableNo, finalIFSA_ORDER_ID);
//
//                                                            String tableNo = TabletSettingUtils.getInstacne().getSTBL_CD();
//                                                            if (tableNo == null) {
//                                                                tableNo = myMssqlUtils.getUserID();
//                                                                if (tableNo != null) {
//                                                                    TabletSettingUtils.getInstacne().setSTBL_CD(tableNo);
//                                                                }
//                                                            }
//                                                            if (tableNo != null && !tableNo.startsWith("[error]")) {
//                                                                posOrderProccessor = new POSOrderProccessor(myMssqlUtils, finalTotalPrice_pos, map, notification, () -> {
//                                                                    presentOrderPos2(selectedMenu, myTableNo, otherTableNo);
//                                                                });
//                                                                posOrderProccessor.execute();
//
//                                                            } else {
//                                                                ///노티 띄워라.
//                                                                notification.title = "[수기 입력] 메뉴 추가";
//                                                                Extra extra = new Extra();
//
//                                                                extra.reference = "androidApp";
//                                                                extra.reason = "포스에서 테이블번호와 매칭되는 코드를 찾을 수 없는경우입니다.";
//                                                                extra.orderId = TabletSettingUtils.getInstacne().getOrderId();
//                                                                notification.extra = extra;
//                                                                notification.type = "order_danger";
//
//                                                                FirebaseDatabase.getInstance()
//                                                                        .getReference()
//                                                                        .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                                        .child("notification")
//                                                                        .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                                        .push()
//                                                                        .setValue(notification);
//                                                                initHistoryPrice(TabletSettingUtils.getInstacne().getBranchInfo().branchId
//                                                                        , myTableNo);
//
//                                                                orderState.setValue(ORDERSTATE.COMPLETEWITHERROR2);
//                                                                isOrdering.setValue(false);
//                                                            }
//
//
//                                                        } else {
////                                                                                Toast.makeText(OrderAppApplication.mContext, "일시적인 오류가 발생했습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
//                                                            orderState.setValue(ORDERSTATE.ERROR);
//                                                            isOrdering.setValue(false);
//                                                        }
//                                                    });
//
//
//                                        }
//
//                                        @Override
//                                        public void onCancelled(DatabaseError databaseError) {
////                                                                Toast.makeText(OrderAppApplication.mContext, "일시적인 오류가 발생했습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
//                                            orderState.setValue(ORDERSTATE.ERROR);
//                                            isOrdering.setValue(false);
//                                        }
//                                    });
//
//                        } else {
////                                                Toast.makeText(OrderAppApplication.mContext, "주문중입니다.", Toast.LENGTH_SHORT).show();
//                        }
//                    })
//                    .complete();
//
//
//        } else {
//            orderState.setValue(ORDERSTATE.LOADING);  //이미 주문중
//        }
//    }


    /**
     * cart를 이용하지 않고 다른이에게 선물하기
     * (2) 상대방꺼에 집어 넣기
     **/
//    private void presentOrderPos2(MenuItemData selectedMenu, String fromTableNo, String toTableNo) {
//
////        this.isFirstOrder.setValue(true);
//
////        if (!isOrdering.getValue())  //주문중이 아닐경우
////        {
////            isOrdering.setValue(true);
////
////            MessageDialogBuilder.getInstance(OrderAppApplication.mContext)
////                    .setContent(otherTableNo + " 테이블로 [" + selectedMenu.foodName + "] 를 선물하시겠습니까?")
////                    .setOnDismissListener_(dialogInterface -> {
////                        isOrdering.setValue(false);
////                    })
////                    .setCancelButton("취소", view -> {
////                        MessageDialogBuilder.getInstance(OrderAppApplication.mContext)
////                                .dismiss();
//////                                            loadingDialog.dismiss();
////                        orderState.setValue(ORDERSTATE.NONE);
////                        isOrdering.setValue(false);
////                    })
////                    .setOkButton("확인", view -> {
////                        LogUtil.d("확인 버튼의 selected 상태값 : " + view.isSelected());
////
////                        if (!view.isSelected()) {
////                            view.setSelected(true);
//        isOrdering.setValue(true);
//        orderState.setValue(ORDERSTATE.LOADING);
//        ////IFSA_ORDER_ID 확인
//        FirebaseDatabase.getInstance().getReference().child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                .child("tables")
//                .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                .child(toTableNo)
//                .child("IFSA_ORDER_ID")
//                .addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot orderId) {
//                        ///IFSA_ORDER_ID 을 확인해보자.
//                        String IFSA_ORDER_ID = null;
//                        if (orderId != null) {
//                            IFSA_ORDER_ID = orderId.getValue(String.class);
//
//                            if (IFSA_ORDER_ID != null) {
//                                isFirstOrder.setValue(false);
////                                                    TabletSettingUtils.getInstacne().setOrderId(IFSA_ORDER_ID);
//                            } else //비워져 있다는건 어찌되었든 파베 데이터에 없다는 뜻. 새로운 값을 넣어라
//                            {
//
//                                isFirstOrder.setValue(true);
//                                IFSA_ORDER_ID = Utils.initOrderID(
//                                        TabletSettingUtils.getInstacne().getBranchInfo().storeBranchUid
//                                        , toTableNo);
//
////                                                    TabletSettingUtils.getInstacne().setOrderId(
////                                                            Utils.initOrderID(
////                                                                    TabletSettingUtils.getInstacne().getBranchInfo().storeBranchUid
////                                                                    , TabletSettingUtils.getInstacne().getTableNo()
////                                                            )
////                                                    );
//                            }
//                        } else    //비워져 있다는건 테이블이 초기화 되었다는 뜻. 새로운 값을 넣어라
//                        {
//                            isFirstOrder.setValue(true);
//
//                            IFSA_ORDER_ID = Utils.initOrderID(
//                                    TabletSettingUtils.getInstacne().getBranchInfo().storeBranchUid
//                                    , toTableNo);
//                        }
////                                            IFSA_ORDER_ID = TabletSettingUtils.getInstacne().getOrderId();
//
//                        Map<String, Object> order = new HashMap<>();    //주문을 담을 객체
//                        ///1. IFSA_ORDER_ID를 table 밑에 저장
//                        order.put(
//
//                                FBPathBuilder.getInstance()
//                                        .init()
//                                        .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                        .set("tables")
//                                        .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                        .set(toTableNo)
//                                        .set("IFSA_ORDER_ID")
//                                        .complete()
//                                , IFSA_ORDER_ID);
//
//                        ///2. 주문내역 집어넣기
////                                                        Map<String, Object> orderList = new HashMap<>();
//
////                                                        Map<String, Object> orderList = new HashMap<>();
//                        Order tmpOrder = new Order();
//                        String key;
//                        String timeStamp = Utils.getDate("yy-MM-dd HH:mm:ss");
////                                            for (DataSnapshot item : cartResult.getChildren()) {
////                                                tmpOrder = item.getValue(Order.class);
//                        tmpOrder.time = timeStamp;
//                        key = FirebaseDatabase.getInstance().getReference()
//                                .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                .child("orderData")
//                                .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                .child(IFSA_ORDER_ID)
//                                .child("orders")
//                                .push().getKey();
//                        tmpOrder.orderKey = key;
//                        tmpOrder.foodName = selectedMenu.foodName + "(선물받기)";
//                        tmpOrder.quantity = 1;
//                        tmpOrder.price = 0;//selectedMenu.price;
//                        tmpOrder.CMDTCD = selectedMenu.CMDTCD;
//
//                        order.put(
//                                FBPathBuilder.getInstance()
//                                        .init()
//                                        .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                        .set("orderData")
//                                        .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                        .set(IFSA_ORDER_ID)
//                                        .set("orders")
//                                        .set(key)
//                                        .complete()
//
//                                , tmpOrder
//                        );
//
//                        ///2. orderForKitchen 주문넣기
//                        if (tmpOrder.isKitchen == 1) {
//                            tmpOrder.orderId = IFSA_ORDER_ID;
//                            order.put(
//                                    FBPathBuilder.getInstance()
//                                            .init()
//                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                            .set("orderForKitchen")
//                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                            .set(key)
//                                            .complete()
//                                    , tmpOrder
//                            );
//                        }
////                                            }
//
//
//                        ///(2) 주문코드
//                        order.put(
//                                FBPathBuilder.getInstance()
//                                        .init()
//                                        .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                        .set("orderData")
//                                        .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                        .set(IFSA_ORDER_ID)
//                                        .set("tableNo")
//                                        .complete()
//                                ,
//                                toTableNo
//                        );
//
//                        ///3. 노티피케이션 데이터
//                        int totalPrice_pos = 0;
//                        HashMap<String, Order> map = new HashMap<>();
//                        Notification notification = new Notification();
//                        notification.content = "[선물이 도착했습니다] " + fromTableNo + "테이블로부터 " + selectedMenu.foodName + "를 선물받았습니다.";
//                        notification.read = false;
//                        notification.tableNo = toTableNo;
//                        notification.title = OrderAppApplication.mContext.getString(R.string.notificationTitleOrder);
//                        notification.type = OrderAppApplication.mContext.getString(R.string.notificationTypeOrder);
//                        notification.time = Utils.getDate("yyyy-MM-dd HH:mm:ss");
//                        totalPrice_pos = selectedMenu.price;
//
//                        map.put(FirebaseDatabase.getInstance().getReference().push().getKey(), tmpOrder);
//
//                        order.put(
//                                FBPathBuilder.getInstance()
//                                        .init()
//                                        .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                        .set("notification")
//                                        .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                        .set(FirebaseDatabase.getInstance().getReference().child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                .child("notification")
//                                                .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                .push().getKey())
//                                        .complete()
//                                , notification
//                        );
//
//                        ///이후는 주문 성공시 돌아갈 항목들
//                        //4.카트삭제
////                                            order.put(
////                                                    FBPathBuilder.getInstance()
////                                                            .init()
////                                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
////                                                            .set("tables")
////                                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
////                                                            .set(TabletSettingUtils.getInstacne().getTableNo())
////                                                            .set("cart")
////                                                            .complete()
////                                                    ,
////                                                    null
////                                            );
//                        int finalTotalPrice_pos = totalPrice_pos;
//                        String finalIFSA_ORDER_ID = IFSA_ORDER_ID;
//                        FirebaseDatabase.getInstance().getReference()
//                                .updateChildren(order, (databaseError, databaseReference) -> {
//                                    LogUtil.d("주문이 성공했는가 : " + databaseError);
//                                    if (databaseError == null) {
//
//                                        MSSQLUtils myMssqlUtils = MSSQLUtils.getInstance(OrderAppApplication.mContext, toTableNo, finalIFSA_ORDER_ID);
//
//                                        String tableNo = TabletSettingUtils.getInstacne().getSTBL_CD();
//                                        if (tableNo == null) {
//                                            tableNo = myMssqlUtils.getUserID();
//                                            if (tableNo != null) {
//                                                TabletSettingUtils.getInstacne().setSTBL_CD(tableNo);
//                                            }
//                                        }
//                                        if (tableNo != null && !tableNo.startsWith("[error]")) {
//                                            posOrderProccessor = new POSOrderProccessor(myMssqlUtils, finalTotalPrice_pos, map, notification, null);
//                                            posOrderProccessor.execute();
//
//                                        } else {
//                                            ///노티 띄워라.
//                                            notification.title = "[수기 입력] 메뉴 추가";
//                                            Extra extra = new Extra();
//
//                                            extra.reference = "androidApp";
//                                            extra.reason = "포스에서 테이블번호와 매칭되는 코드를 찾을 수 없는경우입니다.";
//                                            extra.orderId = TabletSettingUtils.getInstacne().getOrderId();
//                                            notification.extra = extra;
//                                            notification.type = "order_danger";
//
//                                            FirebaseDatabase.getInstance()
//                                                    .getReference()
//                                                    .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                    .child("notification")
//                                                    .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                    .push()
//                                                    .setValue(notification);
//                                            initHistoryPrice(TabletSettingUtils.getInstacne().getBranchInfo().branchId
//                                                    , toTableNo);
//
//                                            orderState.setValue(ORDERSTATE.COMPLETEWITHERROR2);
//                                            isOrdering.setValue(false);
//                                        }
//
//
//                                    } else {
////                                                                                Toast.makeText(OrderAppApplication.mContext, "일시적인 오류가 발생했습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
//                                        orderState.setValue(ORDERSTATE.ERROR);
//                                        isOrdering.setValue(false);
//                                    }
//                                });
//
//
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
////                                                                Toast.makeText(OrderAppApplication.mContext, "일시적인 오류가 발생했습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
//                        orderState.setValue(ORDERSTATE.ERROR);
//                        isOrdering.setValue(false);
//                    }
//                });
//
////                        } else {
//////                                                Toast.makeText(OrderAppApplication.mContext, "주문중입니다.", Toast.LENGTH_SHORT).show();
////                        }
////                    })
////                    .complete();
////
////
////        } else {
////            orderState.setValue(ORDERSTATE.LOADING);  //이미 주문중
////        }
//    }

    /**
     * event 메뉴를 넣는 경우.
     * cart를 활용하지 않는다는 점에서 기존 주문코드와 다르다.
     **/
//    public void EventOrderPos(String menuCode, int menuPrice) {
//
//        this.isFirstOrder.setValue(true);
//
//        if (!isOrdering.getValue())  //주문중이 아닐경우
//        {
//            isOrdering.setValue(true);
//            orderState.setValue(ORDERSTATE.LOADING);
//            ////IFSA_ORDER_ID 확인
//            FirebaseDatabase.getInstance().getReference().child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                    .child("tables")
//                    .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                    .child(TabletSettingUtils.getInstacne().getTableNo())
//                    .child("IFSA_ORDER_ID")
//                    .addListenerForSingleValueEvent(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(DataSnapshot orderId) {
//                            ///IFSA_ORDER_ID 을 확인해보자.
//                            String IFSA_ORDER_ID = null;
//                            if (orderId != null) {
//                                IFSA_ORDER_ID = orderId.getValue(String.class);
//
//                                if (IFSA_ORDER_ID != null) {
//                                    isFirstOrder.setValue(false);
//                                    TabletSettingUtils.getInstacne().setOrderId(IFSA_ORDER_ID);
//                                } else //비워져 있다는건 어찌되었든 파베 데이터에 없다는 뜻. 새로운 값을 넣어라
//                                {
//
//                                    isFirstOrder.setValue(true);
//
//                                    TabletSettingUtils.getInstacne().setOrderId(
//                                            Utils.initOrderID(
//                                                    TabletSettingUtils.getInstacne().getBranchInfo().storeBranchUid
//                                                    , TabletSettingUtils.getInstacne().getTableNo()
//                                            )
//                                    );
//                                }
//                            } else    //비워져 있다는건 테이블이 초기화 되었다는 뜻. 새로운 값을 넣어라
//                            {
//                                isFirstOrder.setValue(true);
//                                TabletSettingUtils.getInstacne().setOrderId(
//                                        Utils.initOrderID(
//                                                TabletSettingUtils.getInstacne().getBranchInfo().storeBranchUid
//                                                , TabletSettingUtils.getInstacne().getTableNo()
//                                        )
//                                );
//                            }
//                            IFSA_ORDER_ID = TabletSettingUtils.getInstacne().getOrderId();
//
//                            Map<String, Object> order = new HashMap<>();    //주문을 담을 객체
//                            ///1. IFSA_ORDER_ID를 table 밑에 저장
//                            order.put(
//
//                                    FBPathBuilder.getInstance()
//                                            .init()
//                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                            .set("tables")
//                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                            .set(TabletSettingUtils.getInstacne().getTableNo())
//                                            .set("IFSA_ORDER_ID")
//                                            .complete()
//                                    , IFSA_ORDER_ID);
//
//                            ///2. 주문내역 집어넣기
////                                                        Map<String, Object> orderList = new HashMap<>();
//                            Order tmpOrder = new Order();
//                            String timeStamp = Utils.getDate("yy-MM-dd HH:mm:ss");
//                            tmpOrder.time = timeStamp;
//                            tmpOrder.foodName = "[광고 할인]";
//                            tmpOrder.quantity = 1;
//                            tmpOrder.price = menuPrice;
//                            tmpOrder.CMDTCD = menuCode;
//                            order.put(
//                                    FBPathBuilder.getInstance()
//                                            .init()
//                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                            .set("orderData")
//                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                            .set(IFSA_ORDER_ID)
//                                            .set("orders")
//                                            .set(FirebaseDatabase.getInstance().getReference()
//                                                    .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                    .child("orderData")
//                                                    .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                    .child(IFSA_ORDER_ID)
//                                                    .child("orders")
//                                                    .push().getKey())
//                                            .complete()
//
//                                    , tmpOrder
//                            );
//
//
//                            ///(2) 주문코드
//                            order.put(
//                                    FBPathBuilder.getInstance()
//                                            .init()
//                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                            .set("orderData")
//                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                            .set(IFSA_ORDER_ID)
//                                            .set("tableNo")
//                                            .complete()
//                                    ,
//                                    TabletSettingUtils.getInstacne().getTableNo()
//                            );
//
//                            ///3. 노티피케이션 데이터
//                            int totalPrice_pos = 0;
//                            HashMap<String, Order> map = new HashMap<>();
//                            Notification notification = new Notification();
//                            notification.content = "/--";
//                            notification.read = false;
//                            notification.tableNo = TabletSettingUtils.getInstacne().getTableNo();
//                            notification.title = OrderAppApplication.mContext.getString(R.string.notificationTitleOrder);
//                            notification.type = OrderAppApplication.mContext.getString(R.string.notificationTypeOrder);
//                            notification.time = Utils.getDate("yyyy-MM-dd HH:mm:ss");
//                            notification.content = "메뉴 할인 1건";
////
////                            for (DataSnapshot item : cartResult.getChildren()) {
////                                Order cart = item.getValue(Order.class);
////                                notification.content = notification.content + "/ " + cart.foodName + ": " + cart.quantity + "  ";
////                                totalPrice_pos += (cart.price * cart.quantity);
////                                map.put(item.getKey(), cart);
////                            }
////                            if (notification.content != null) {
////                                notification.content = notification.content.replace("/--/", "");
////                            }
//                            order.put(
//                                    FBPathBuilder.getInstance()
//                                            .init()
//                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                            .set("notification")
//                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                            .set(FirebaseDatabase.getInstance().getReference().child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                    .child("notification")
//                                                    .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                    .push().getKey())
//                                            .complete()
//                                    , notification
//                            );
//
//                            ///이후는 주문 성공시 돌아갈 항목들
//                            //4.카트삭제
////                            order.put(
////                                    FBPathBuilder.getInstance()
////                                            .init()
////                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
////                                            .set("tables")
////                                            .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
////                                            .set(TabletSettingUtils.getInstacne().getTableNo())
////                                            .set("cart")
////                                            .complete()
////                                    ,
////                                    null
////                            );
//                            map.put(menuCode, tmpOrder);
//
//                            int finalTotalPrice_pos = menuPrice;
//                            FirebaseDatabase.getInstance().getReference()
//                                    .updateChildren(order, (databaseError, databaseReference) -> {
//                                        LogUtil.d("주문이 성공했는가 : " + databaseError);
//                                        if (databaseError == null) {
//
//                                            MSSQLUtils myMssqlUtils = MSSQLUtils.getInstance(OrderAppApplication.mContext, TabletSettingUtils.getInstacne().getTableNo(), TabletSettingUtils.getInstacne().getOrderId());
//
//
//                                            String tableNo = TabletSettingUtils.getInstacne().getSTBL_CD();
//                                            if (tableNo == null) {
//                                                tableNo = myMssqlUtils.getUserID();
//                                                if (tableNo != null) {
//                                                    TabletSettingUtils.getInstacne().setSTBL_CD(tableNo);
//                                                }
//                                            }
//                                            if (tableNo != null && !tableNo.startsWith("[error]")) {
//                                                posOrderProccessor = new POSOrderProccessor(myMssqlUtils, finalTotalPrice_pos, map, notification, null);
//                                                posOrderProccessor.execute();
//
//                                            } else {
//                                                ///노티 띄워라.
//                                                notification.title = "[수기 입력] 메뉴 추가";
//                                                Extra extra = new Extra();
//
//                                                extra.reference = "androidApp";
//                                                extra.reason = "포스에서 테이블번호와 매칭되는 코드를 찾을 수 없는경우입니다.";
//                                                extra.orderId = TabletSettingUtils.getInstacne().getOrderId();
//                                                notification.extra = extra;
//                                                notification.type = "order_danger";
//
//                                                FirebaseDatabase.getInstance()
//                                                        .getReference()
//                                                        .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                                                        .child("notification")
//                                                        .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                                                        .push()
//                                                        .setValue(notification);
//                                                initHistoryPrice(TabletSettingUtils.getInstacne().getBranchInfo().branchId
//                                                        , TabletSettingUtils.getInstacne().getTableNo());
//
//                                                orderState.setValue(ORDERSTATE.COMPLETEWITHERROR2);
//                                                isOrdering.setValue(false);
//                                            }
//
//
//                                        } else {
//                                            orderState.setValue(ORDERSTATE.ERROR);
//                                            isOrdering.setValue(false);
//                                        }
//                                    });
//
//
//                        }
//
//                        @Override
//                        public void onCancelled(DatabaseError databaseError) {
//                            orderState.setValue(ORDERSTATE.ERROR);
//                            isOrdering.setValue(false);
//                        }
//                    });
//
//
//        } else {
//            orderState.setValue(ORDERSTATE.LOADING);  //이미 주문중
//        }
//    }


//    public class POSOrderProccessor extends AsyncTask<Void, Boolean, Boolean> {
//        int totalPrice_ = 0;
//        HashMap<String, Order> map = new HashMap<>();
//        Notification notification = null;
//        MSSQLUtils mssqlUtils;
//        OrderFinishListener listener;
//
//
//        public POSOrderProccessor(MSSQLUtils mssqlUtils, int totalPrice, HashMap<String, Order> order, Notification notification, OrderFinishListener listener) {
//            this.totalPrice_ = totalPrice;
//            this.map.putAll(order);
//            this.notification = notification;
//            this.mssqlUtils = mssqlUtils;
//            this.listener = listener;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            orderState.setValue(ORDERSTATE.LOADING);
//        }
//
//        @Override
//        protected Boolean doInBackground(Void... voids) {
//
//            publishProgress(true);
//            return (mssqlUtils.insertOrder_(totalPrice_, isFirstOrder.getValue(), map) == 1);
//        }
//
//        @Override
//        protected void onProgressUpdate(Boolean... values) {
//            super.onProgressUpdate(values);
//            orderState.setValue(ORDERSTATE.LOADING);
//        }
//
//        @Override
//        protected void onPostExecute(Boolean aBoolean) {
//            super.onPostExecute(aBoolean);
//            if (aBoolean)   //포스 넣기 성공
//            {
//                MessageDialogBuilder.getInstance(OrderAppApplication.mContext)
//                        .setContent("주문이 완료되었습니다.")
//                        .setDefaultButton()
//                        .complete();
//                initHistoryPrice(TabletSettingUtils.getInstacne().getBranchInfo().branchId
//                        , TabletSettingUtils.getInstacne().getTableNo());
//                orderState.setValue(ORDERSTATE.COMPLETE);
//                isOrdering.setValue(false);
//            } else {
//                ///노티 띄워라.
//                notification.title = "[수기 입력] 메뉴 추가";
//                Extra extra = new Extra();
//
//                extra.reference = "androidApp";
//                extra.reason = "포스에 주문넣기를 실패함. 상세한 로그는 queryLog를 참조하기바랍니다.";
//                extra.orderId = TabletSettingUtils.getInstacne().getOrderId();
//                notification.extra = extra;
//                notification.type = "order_danger";
//
//                FirebaseDatabase.getInstance()
//                        .getReference()
//                        .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                        .child("notification")
//                        .child(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                        .push()
//                        .setValue(notification);
//
//                initHistoryPrice(TabletSettingUtils.getInstacne().getBranchInfo().branchId
//                        , TabletSettingUtils.getInstacne().getTableNo());
//
//                orderState.setValue(ORDERSTATE.COMPLETEWITHERROR1);
//                isOrdering.setValue(false);
//            }
//            if (this.listener != null)
//                this.listener.complete();
//        }
//    }


    /**
     * 큐에 요청하는 이벤트 주문(선물,광고)
     **/
    public synchronized void requestQueueEvent(String url, String jsonParameter) {
        queueState.setValue(QUEUESTATE.LOADING);
        this.compositeSubscription.add(this.orderRepository.getValue().requestQueueEvent(url, jsonParameter)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        response -> {
                            queueState.setValue(QUEUESTATE.COMPLETE);
                        },
                        error -> {
                            queueState.setValue(QUEUESTATE.ERROR);
                        },
                        () -> {

                        }));
    }


    public enum ORDERSTATE {
        NONE, LOADING, NOCART, ERROR, COMPLETE, COMPLETEWITHERROR1, COMPLETEWITHERROR2
    }


}
