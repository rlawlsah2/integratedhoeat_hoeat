package com.cdef.hoeat.viewModel;

import androidx.lifecycle.MutableLiveData;

/**
 * Created by kimjinmo on 2018. 2. 2..
 */

public class OrderStateViewModel extends BaseViewModel {

    public MutableLiveData<String> mOrderId = new MutableLiveData<>();

    public MutableLiveData<LoginViewModel.INITSETTING> initSetting = new MutableLiveData<LoginViewModel.INITSETTING>() {
    };

    public enum INITSETTING {
        NONE, LOADING, ERROR, COMPLETE
    }

    public OrderStateViewModel() {

    }

    public void getOrderId(String branchId, String tableNo) {
        this.initSetting.setValue(LoginViewModel.INITSETTING.LOADING);
//        RxFirebaseDatabase.data(
//                FirebaseDatabase.getInstance().getReference()
//                        .child(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                        .child("tables")
//                        .child(branchId)
//                        .child(tableNo)
//                        .child("IFSA_ORDER_ID"))
//                .subscribe((snapshot) -> {
//                    try {
//                        this.initSetting.setValue(LoginViewModel.INITSETTING.COMPLETE);
//                        this.mOrderId.setValue(snapshot.getValue(String.class));
//                    } catch (Exception e) {
//                        this.initSetting.setValue(LoginViewModel.INITSETTING.ERROR);
//                        this.mOrderId.setValue(null);
//
//                    }
//
//
//                }, error -> {
//                    this.initSetting.setValue(LoginViewModel.INITSETTING.ERROR);
//                    this.mOrderId.setValue(null);
//                });
    }

}
