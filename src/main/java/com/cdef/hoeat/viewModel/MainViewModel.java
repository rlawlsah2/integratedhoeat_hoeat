package com.cdef.hoeat.viewModel;

import androidx.lifecycle.MutableLiveData;
import android.content.Context;

import com.cdef.commonmodule.utils.FBPathBuilder;
import com.cdef.commonmodule.utils.LogUtil;
import com.cdef.commonmodule.Repository.MainRepository;
import com.cdef.hoeat.Networking.HoeatNetworkUtil;
import com.cdef.hoeat.utils.TabletSettingUtils;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import rx.Observable;

/**
 * Created by kimjinmo on 2018. 2. 2..
 * mainactivity에서 버튼에 관련된 처리를 하는 뷰모델
 */

public class MainViewModel extends BaseViewModel {



    public MutableLiveData<String> IFSA_ORDER_ID = new MutableLiveData<>();   //주문 번호가 존재하는지 파악.
    public MutableLiveData<Boolean> STARTER_VISIVILITY = new MutableLiveData<>();

    MainRepository mainRepository;

    public MainViewModel() {

    }



    public Observable<JsonObject> getTableInfoList(String url) {
        return mainRepository.getTableInfoList(url);
    }

    public void initRepository(Context context) {
        this.mainRepository = new MainRepository(HoeatNetworkUtil.getInstance(context).getService());
        this.STARTER_VISIVILITY.setValue(true);
    }


    /**
     * fcm registrationToken
     */
    public void registrationToken(String url, String tokenKey) {
        this.compositeSubscription.add(mainRepository.registrationToken(url, tokenKey)
                .subscribe(
                        response -> {

                        },
                        error -> {

                        },
                        () -> {

                        }
                ));
    }

    /**
     * 사용자가 시작버튼을 눌러 사용을 시작한 경우
     * 1. IFSA_ORDER_ID 초기화
     * 2. IFSA_REMARK 초기화
     * 3. CART 초기화
     **/
    public void firstAccess(String branchId, String tableNo) {
        LogUtil.d("파이어베이스 rx 테스트 firstAccess 시작합니다.");
        LogUtil.d("파이어베이스 rx 테스트 firstAccess branchId : " + branchId);
        LogUtil.d("파이어베이스 rx 테스트 firstAccess tableNo : " + tableNo);


        Map<String, Object> map = new HashMap<>();
        ///IFSC_REMARK 삭제
//        map.put(
//                FBPathBuilder.getInstance().init()
//                        .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                        .set("tables")
//                        .set(branchId)
//                        .set(tableNo)
//                        .set("IFSC_REMARK")
//                        .complete()
//                , null
//        );
        ///cart 삭제
        map.put(
                FBPathBuilder.getInstance().init()
                        .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
                        .set("tables")
                        .set(branchId)
                        .set(tableNo)
                        .set("cart")
                        .complete()
                , null
        );

        ///채팅을 삭제해볼까
        //채팅 섬네일
//        map.put(FBPathBuilder.getInstance().init()
//                        .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                        .set("chat")
//                        .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                        .set("personal")
//                        .set(TabletSettingUtils.getInstacne().getTableNo())
//                        .set("lastMessages")
//                        .complete()
//                , null);
//        //채팅 내용 목록
//        map.put(FBPathBuilder.getInstance().init()
//                        .set(TabletSettingUtils.getInstacne().getBranchInfo().univInfo.fbBrandName)
//                        .set("chat")
//                        .set(TabletSettingUtils.getInstacne().getBranchInfo().branchId)
//                        .set("personal")
//                        .set(TabletSettingUtils.getInstacne().getTableNo())
//                        .set("messages")
//                        .complete()
//                , null);

        FirebaseDatabase.getInstance().getReference()
                .updateChildren(map, (databaseError, databaseReference) -> {
                    if (databaseError == null) {
                        LogUtil.d("초기화 완료");
                        STARTER_VISIVILITY.setValue(false);
                    } else {

                    }
                });
    }

}
